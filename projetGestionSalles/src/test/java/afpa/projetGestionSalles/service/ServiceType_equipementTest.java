package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import afpa.projetGestionSalles.beans.Type_equipement;


/**Test unitaire du service type d'équipement,  à lancer dans l'ordre
 * @author jordy
 *
 */
@SpringBootTest
public class ServiceType_equipementTest {

	@Autowired
	IServiceType_equipement serviceEquip;
	
	@Test
	public void addEquipTest() {
		Type_equipement equiptest = new Type_equipement();
		//equiptest.setIdtypeequipement(1);
		System.out.println(equiptest.getIdtypeequipement());
		equiptest.setNomtypeequipement("balai");
		serviceEquip.addOrMergeType_equipement(equiptest);
	}
	
	@Test
	public void getAllEquipTest() {
		List<Type_equipement> list = (List<Type_equipement>) serviceEquip.getAllType_equipement();
		assertEquals(1, list.size());
	}
	
	@Test
	public void getEquipTest() {
		Type_equipement equipTest = serviceEquip.findByNomtypeequipement("balai");
		Type_equipement equipTest2 = serviceEquip.getEquipementById(equipTest.getIdtypeequipement()).get();
		assertTrue(equipTest2 != null);	
	}
	
	@Test
	public void updateEquipTest() {
		Type_equipement equiptest = serviceEquip.findByNomtypeequipement("balai");
		Type_equipement equiptest2 = serviceEquip.getEquipementById(equiptest.getIdtypeequipement()).get();
		equiptest2.setNomtypeequipement("test");
		serviceEquip.addOrMergeType_equipement(equiptest2);
		assertTrue(equiptest2.getNomtypeequipement() == "test");	
	}
	
	@Test
	public void deleteEquipTest() {
		Type_equipement equiptest = serviceEquip.findByNomtypeequipement("test");
		Type_equipement equiptest2 = serviceEquip.getEquipementById(equiptest.getIdtypeequipement()).get();
		serviceEquip.deleteType_equipement(equiptest2);
		assertFalse(serviceEquip.getEquipementById(equiptest2.getIdtypeequipement()).isPresent());
	}
	
	
	
}
