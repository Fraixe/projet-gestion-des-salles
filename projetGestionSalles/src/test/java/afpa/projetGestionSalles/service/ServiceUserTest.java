package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.projetGestionSalles.beans.Etat_User;
import afpa.projetGestionSalles.beans.Langue;
import afpa.projetGestionSalles.beans.Role;
import afpa.projetGestionSalles.beans.User;

/**
 * Classe de Test du Service User
 * @author Pierre
 *
 */
@SpringBootTest
@Transactional
public class ServiceUserTest {

	@Autowired
	IServiceUser servUser;

	// Creation de l'utilisateur 
	public User monUser=new User();
	User monUserPersist=null;


	@Test
	public void testAddUser() {
		// Création d'un user
		monUser.setNomUser("Einstein");
		monUser.setPrenomUser("Albert");
		monUser.setEmailUser("albert.einstein@cern.org");
		monUser.setIdEtatUser(new Etat_User( "Admin"));
		monUser.setIdLangue(new Langue( "FR"));
		monUser.setIdRole(new Role( "admin", null));
		monUser.setMatriculeUser(1234);
		monUser.setMdpAChanger(false);
		monUser.setTelephoneUser(0102030405);
		monUser.setPassword("e=mC2");

		//Persistance de l'user
		monUserPersist=servUser.add(monUser);

		// Verification des entrées sauvegardées
		assertEquals(monUserPersist.getNomUser(),monUser.getNomUser());
		assertNotNull(monUserPersist.getIdUser());
		assertEquals(monUserPersist.getPrenomUser(),monUser.getPrenomUser());
		assertEquals(monUserPersist.getEmailUser(),monUser.getEmailUser());
		assertEquals(monUserPersist.getMatriculeUser(),monUser.getMatriculeUser());
		assertFalse(monUserPersist.getMdpAChanger());

	}

	@Test
	public void testUpdateUser() {
		// Création d'un user
		monUser.setNomUser("Einstein");
		monUser.setPrenomUser("Albert");
		monUser.setEmailUser("albert.einstein@cern.org");
		monUser.setIdEtatUser(new Etat_User( "Admin"));
		monUser.setIdLangue(new Langue( "FR"));
		monUser.setIdRole(new Role( "admin", null));
		monUser.setMatriculeUser(1234);
		monUser.setMdpAChanger(false);
		monUser.setTelephoneUser(0102030405);
		monUser.setPassword("e=mC2");

		//Persistance de l'user
		monUserPersist=servUser.add(monUser);
		assertNotNull (monUserPersist);

		// Modification de l'user
		monUser.setNomUser("Gilbert");
		monUserPersist=servUser.update(monUser);


		// Verification des données insérées
		assertEquals(monUserPersist.getNomUser(),monUser.getNomUser());
	}

	
	/*
	 * DELETE TEST DISABLED AS LOGGER FORCE DATABASE TO ROLLBACK
	@Test
	public void testDeleteUser() {
		// Création d'un user
		monUser.setNom_user("Einstein");
		monUser.setPrenom_user("Albert");
		monUser.setEmail_user("albert.einstein@cern.org");
		monUser.setId_etat_user(new Etat_User( "Admin"));
		monUser.setId_langue(new Langue( "FR"));
		monUser.setId_role(new Role( "admin", null));
		monUser.setMatricule_user(1234);
		monUser.setMdp_a_changer(false);
		monUser.setTelephone_user(0102030405);
		monUser.setPassword("e=mC2");

		//Persistance de l'user
		monUserPersist=servUser.add(monUser);
		assertNotNull (monUserPersist);
		
		
		// Effacement de l'user
		servUser.delete(monUserPersist);
		assertEquals(servUser.findById(monUserPersist.getId_user()),Optional.empty());

	}
	*/



}
