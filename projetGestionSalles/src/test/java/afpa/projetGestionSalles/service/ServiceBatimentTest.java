package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.projetGestionSalles.beans.Batiment;

@Transactional()
@SpringBootTest
public class ServiceBatimentTest {

	@Autowired
	IServiceBatiment serviceBatiment;
	
	@Autowired
	Logger logger;

	@Test
	public void testAddOneBatiment() {
				
		Batiment test = serviceBatiment.add(new Batiment("Afpa", "ezlafamille"));
		logger.info(test.getNomBatiment());	
		assertEquals("Afpa", test.getNomBatiment());
	}
}