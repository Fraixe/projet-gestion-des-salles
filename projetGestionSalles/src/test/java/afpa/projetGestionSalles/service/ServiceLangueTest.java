/**
 * 
 */
package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.projetGestionSalles.beans.Langue;

/**
 * Classe de Test du service Langue
 * @author Pierre
 *
 */
@SpringBootTest
@Transactional
public class ServiceLangueTest {

	@Autowired
	IServiceLangue servLangue;

	@Test
	public void testAddLangue() {
		Langue maLangue=new Langue("Wolof");

		// Persistance de la langue
		Langue maLanguePersist=servLangue.add(maLangue);
		assertEquals(maLanguePersist.getLangue(),maLangue.getLangue());
		assertNotNull(maLanguePersist.getIdLangue());

	}

	@Test
	public void testUpdateLangue() {
		Langue maLangue=new Langue("Wolof");

		// Persistance de la langue
		Langue maLanguePersist=servLangue.add(maLangue);
		assertNotNull(maLanguePersist.getIdLangue());

		//Modification de la langue
		maLangue.setLangue("Bambara");
		maLanguePersist=servLangue.update(maLangue);

		// Verification des données insérées
		assertEquals(maLanguePersist.getLangue(),maLangue.getLangue());

	}
	/*
	 * DELETE TEST DISABLED AS LOGGER FORCE DATABASE TO ROLLBACK
	@Test
	public void testDeleteLangue() {
		Langue maLangue=new Langue("Wolof");

		// Persistance de la langue
		Langue maLanguePersist=servLangue.add(maLangue);
		assertEquals(maLanguePersist.getLangue(),maLangue.getLangue());
		assertNotNull(maLanguePersist.getId_langue());
		
		// effacement de la langue
		servLangue.delete(maLanguePersist);
		System.out.println(maLanguePersist.getId_langue());
		assertEquals(servLangue.findById(maLanguePersist.getId_langue()),Optional.empty());
		
		
	}
	*/


}
