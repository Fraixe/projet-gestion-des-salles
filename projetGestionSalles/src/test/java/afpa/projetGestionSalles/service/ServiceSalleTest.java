package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.projetGestionSalles.beans.Salle;


/**Test unitaire du service Salle,  à lancer dans l'ordre
 * @author jordy
 *
 */
@SpringBootTest
public class ServiceSalleTest {
	
	@Autowired
	IServiceSalle servicesalle;
	
	@Test
	public void addSalleTest() {
		Salle salletest = new Salle();
		salletest.setIdsalle(200);
		salletest.setNomsalle("salletest");
		salletest.setCapacitesalle(300);
		salletest.setEtagesalle(2);
		salletest.setDescriptionsalle("super salle");
		servicesalle.addOrMergeSalle(salletest);
		assertTrue(salletest.getIdsalle() > 0);
	}
	
	@Test
	public void getAllSalleTest() {
		List<Salle> list = (List<Salle>) servicesalle.getAllSalles();
		assertEquals(1, list.size());
	}
	
	@Test
	public void getSalleTest() {
		Salle salletest = servicesalle.findByNomsalle("salletest");
		Salle sal = servicesalle.getSalleById(salletest.getIdsalle()).get();
		assertTrue(sal != null);	
	}
	
	@Test
	public void updateSalleTest() {
		Salle salletest = servicesalle.findByNomsalle("salletest");
		Salle sal = servicesalle.getSalleById(salletest.getIdsalle()).get();
		sal.setNomsalle("test");
		servicesalle.addOrMergeSalle(sal);
		assertTrue(sal.getNomsalle() == "test");	
	}
	
	@Test
	public void deleteSalleTest() {
		Salle salletest = servicesalle.findByNomsalle("test");
		Salle sal = servicesalle.getSalleById(salletest.getIdsalle()).get();
		servicesalle.deleteSalle(sal);
		assertFalse( servicesalle.getSalleById(sal.getIdsalle()).isPresent());
	}
	
	

}
