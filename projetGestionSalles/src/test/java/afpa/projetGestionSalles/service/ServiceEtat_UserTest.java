package afpa.projetGestionSalles.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import afpa.projetGestionSalles.beans.Etat_User;

/**
 * Classe de Test du Service Etat_User
 * @author Pierre
 *
 */
@SpringBootTest
@Transactional
public class ServiceEtat_UserTest {

	@Autowired
	IServiceEtat_User servEtatUser;
	
	@Test
	public void testAddEtatUser() {
		// Création d'un Etat User
		Etat_User monEtat_User=new Etat_User("Admin");
		
		// Persistance de l'Etat User
		Etat_User Etat_UserPersist=servEtatUser.add(monEtat_User);
		
		// Verification des entrées sauvegardées
		assertEquals(Etat_UserPersist.getEtat_user(),monEtat_User.getEtat_user());
		assertNotNull(Etat_UserPersist.getId_etat_user());
		
				
	}
	@Test
	public void testUpdateEtatUser() {
		// Création d'un Etat User
		Etat_User monEtat_User=new Etat_User("Admin");
		
		// Persistance de l'Etat User
		Etat_User Etat_UserPersist=servEtatUser.add(monEtat_User);
		
		// Verification des entrées sauvegardées
		assertEquals(Etat_UserPersist.getEtat_user(),monEtat_User.getEtat_user());
		assertNotNull(Etat_UserPersist);
		
		//Update de l'Etat user
		monEtat_User.setEtat_user("Visiteur");
		Etat_UserPersist=servEtatUser.update(monEtat_User);
		
		// Verification des données insérées
		assertEquals(monEtat_User.getEtat_user(),Etat_UserPersist.getEtat_user());
		
		
				
	}

}
