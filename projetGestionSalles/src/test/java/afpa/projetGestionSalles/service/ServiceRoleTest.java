package afpa.projetGestionSalles.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ServiceRoleTest {

	@Autowired
	IServiceRole serviceRole;
	
	@Test
	public void testGetAllRole() {
		serviceRole.getAllRole();
	}
	
	@Test
	public void testGetRoleById() {
		serviceRole.getRoleById(1);
	}
}
