(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "/7iW":
/*!*************************************!*\
  !*** ./src/service/user.service.ts ***!
  \*************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**
 *Service pour utiliser l'API BATIMENT
 */
class UserService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/user/getall'; //Récupérer tous les users avec toutes les info
        this.urlGetOne = 'http://localhost:8080/user/findbyid'; //Récupérer un user avec toutes ses infos
        this.urlLogin = 'http://localhost:8080/user/login'; //Récupérer login + mp
        this.urlDelete = 'http://localhost:8080/user/delete';
        this.urlAdd = 'http://localhost:8080/user/adduser';
        this.urlUpdate = 'http://localhost:8080/user/updateuser';
        this.urlMonProfil = 'http://localhost:8080/user/monprofil';
        this.urlDisconnect = 'http://localhost:8080/user/disconnect';
    }
    extractData(res) {
        let body = res;
        return body || {};
    }
    getAllUser() {
        return this.http.get(this.urlGetAll);
    }
    getOneUser(id) {
        return this.http.get(this.urlGetOne + '/' + id);
    }
    deleteOneUser(id) {
        return this.http.delete(this.urlDelete + '/' + id);
    }
    addOneUser(addUser) {
        console.log(addUser);
        return this.http.put(this.urlAdd, addUser);
    }
    updateOneUser(updateUser) {
        return this.http.post(this.urlUpdate, updateUser);
    }
    logUser(nom, pass) {
        this.myData = {
            idUser: null,
            prenomUser: null,
            nomUser: nom,
            matriculeUser: null,
            emailUser: null,
            password: pass,
            telephoneUser: null,
            mdpAChanger: null,
            idEtatUser: null,
            idLangue: null,
            roleUser: null
        };
        console.log(this.myData);
        return this.http.post(this.urlLogin, this.myData);
    }
    infoMonProfil() {
        let test = this.http.get(this.urlMonProfil);
        console.log("dans le service" + test);
        return test;
    }
    deconnexion() {
        this.http.get(this.urlDisconnect);
    }
}
UserService.ɵfac = function UserService_Factory(t) { return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: UserService, factory: UserService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "/FVT":
/*!**************************************************!*\
  !*** ./src/app/scheduler/scheduler.component.ts ***!
  \**************************************************/
/*! exports provided: SchedulerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchedulerComponent", function() { return SchedulerComponent; });
/* harmony import */ var dhtmlx_scheduler_evaluation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! dhtmlx-scheduler-evaluation */ "wBh4");
/* harmony import */ var dhtmlx_scheduler_evaluation__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(dhtmlx_scheduler_evaluation__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_timeline_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dhtmlx-scheduler-evaluation/codebase/ext/dhtmlxscheduler_timeline.js */ "QQKQ");
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_timeline_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_timeline_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_treetimeline_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dhtmlx-scheduler-evaluation/codebase/ext/dhtmlxscheduler_treetimeline.js */ "HX0H");
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_treetimeline_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_treetimeline_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_daytimeline_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! dhtmlx-scheduler-evaluation/codebase/ext/dhtmlxscheduler_daytimeline.js */ "tgKO");
/* harmony import */ var dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_daytimeline_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(dhtmlx_scheduler_evaluation_codebase_ext_dhtmlxscheduler_daytimeline_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_service_batiment_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/service/batiment.service */ "e3aT");






const _c0 = ["scheduler_here"];
class SchedulerComponent {
    constructor(batimentService) {
        this.batimentService = batimentService;
    }
    ngOnInit() {
        console.log(scheduler.load(this.getAllBatiment()));
        scheduler.locale.labels.timeline_tab = "Timeline";
        scheduler.locale.labels.section_custom = "Section";
        scheduler.config.details_on_create = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.config.time_step = 15;
        //console.log(this.batiments);
        //===============
        //Configuration
        //===============
        //scheduler.load(this.getAllBatiment());
        var elements = [ // original hierarhical array to display
        // scheduler.load(this.getAllBatiment())
        /*	{key:10, label:"Web Testing Dep.", open: true, children: [
                {key:20, label:"Elizabeth Taylor"},
                {key:30, label:"Managers", open: true,  children: [
                    {key:40, label:"John Williams"},
                    {key:50, label:"David Miller"}
                ]},
                {key:60, label:"Linda Brown"},
                {key:70, label:"George Lucas"}
            ]},
            {key:110, label:"Human Relations Dep.", open:true, children: [
                {key:80, label:"Kate Moss"},
                {key:90, label:"Dian Fossey"}
            ]}*/
        ];
        scheduler.createTimelineView({
            section_autoheight: false,
            name: "timeline",
            x_unit: "minute",
            x_date: "%H:%i",
            x_step: 30,
            x_size: 24,
            x_start: 16,
            x_length: 48,
            y_unit: scheduler.load(this.getAllBatiment()),
            cell_template: true,
            y_property: "section_id",
            render: "tree",
        });
        scheduler.templates.timeline_cell_value = function folderCellContent(evs, date, section) {
            if (section.children) {
                var timeline = scheduler.getView();
                var events = timeline.selectEvents({
                    section: section.key,
                    date: date,
                    selectNested: true
                });
                ;
                var className = "";
                if (!events.length) {
                    className = "load-marker-no";
                }
                else if (events.length < 3) {
                    className = "load-marker-light";
                }
                else {
                    className = "load-marker-high";
                }
                return "<div class='load-marker " + className + "'>" +
                    events.length
                    + "</div>";
            }
            return "";
        };
        scheduler.init('scheduler_here', new Date(), "timeline");
    }
    getAllBatiment() {
        this.batimentService.getAllBatiment().subscribe((response) => {
            this.batiments = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
}
SchedulerComponent.ɵfac = function SchedulerComponent_Factory(t) { return new (t || SchedulerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](src_service_batiment_service__WEBPACK_IMPORTED_MODULE_5__["BatimentService"])); };
SchedulerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: SchedulerComponent, selectors: [["scheduler"]], viewQuery: function SchedulerComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵviewQuery"](_c0, 3);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵloadQuery"]()) && (ctx.schedulerContainer = _t.first);
    } }, decls: 14, vars: 0, consts: [["id", "scheduler_here", 1, "dhx_cal_container", 2, "width", "100%", "height", "600px"], [1, "dhx_cal_navline"], [1, "dhx_cal_prev_button"], [1, "dhx_cal_next_button"], [1, "dhx_cal_today_button"], [1, "dhx_cal_date"], ["name", "day_tab", 1, "dhx_cal_tab", 2, "right", "204px"], ["name", "week_tab", 1, "dhx_cal_tab", 2, "right", "140px"], ["name", "timeline_tab", 1, "dhx_cal_tab", 2, "right", "280px"], ["name", "month_tab", 1, "dhx_cal_tab", 2, "right", "76px"], [1, "dhx_cal_header"], [1, "dhx_cal_data"]], template: function SchedulerComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "\u00A0");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](5, "\u00A0");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](11, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](12, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](13, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } }, styles: ["\r\n\r\n:host{\r\n    display: block;\r\n    height: 100%;\r\n    position: relative;\r\n    width: 100%;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjaGVkdWxlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixXQUFXO0NBQ2QiLCJmaWxlIjoic2NoZWR1bGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbjpob3N0e1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuIH0iXX0= */"], encapsulation: 2 });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Dev\Workspace Angular\angular-gestion-des-salles\src\main.ts */"zUnb");


/***/ }),

/***/ "2RJA":
/*!******************************************!*\
  !*** ./src/service/parametre.service.ts ***!
  \******************************************/
/*! exports provided: ParametreService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParametreService", function() { return ParametreService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


class ParametreService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/parametre/getall';
        this.urlDelete = 'http://localhost:8080/parametre/delete';
        this.urlAdd = 'http://localhost:8080/parametre/add';
        this.urlUpdate = 'http://localhost:8080/parametre/update';
        this.urlGetByID = 'http://localhost:8080/parametre/getbyid';
        this.urlImg = 'http://localhost:8080/parametre/getbyid/1';
    }
    getParametreById(id) {
        return this.http.get(this.urlGetByID + "/" + id);
    }
    getAllParametre() {
        return this.http.get(this.urlGetAll);
    }
    deleteParametre(deleteParametre) {
        return this.http.delete(this.urlDelete, deleteParametre);
    }
    addParametre(putParametre) {
        return this.http.put(this.urlAdd, putParametre);
    }
    updateParametre(updateParametre) {
        return this.http.post(this.urlUpdate, updateParametre);
    }
    getImg() {
        return this.http.get(this.urlImg);
    }
}
ParametreService.ɵfac = function ParametreService_Factory(t) { return new (t || ParametreService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ParametreService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ParametreService, factory: ParametreService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "2kyN":
/*!*************************************!*\
  !*** ./src/service/role.service.ts ***!
  \*************************************/
/*! exports provided: RoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleService", function() { return RoleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**TEST COMMIT 17h */
class RoleService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/role/getall';
        this.urlDelete = 'http://localhost:8080/role/delete';
        this.urlAdd = 'http://localhost:8080/role/add';
        this.urlUpdate = 'http://localhost:8080/role/update';
        this.urlGetByID = 'http://localhost:8080/role/getbyid';
    }
    getRoleById(id) {
        return this.http.get(this.urlGetByID + "/" + id);
    }
    getAllRole() {
        return this.http.get(this.urlGetAll);
    }
    deleteRole(id) {
        return this.http.delete(this.urlDelete + "/" + id);
    }
    addRole(putRole) {
        return this.http.put(this.urlAdd, putRole);
    }
    updateRole(updateRole) {
        return this.http.post(this.urlUpdate, updateRole);
    }
}
RoleService.ɵfac = function RoleService_Factory(t) { return new (t || RoleService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
RoleService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: RoleService, factory: RoleService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "3ZE1":
/*!**************************************!*\
  !*** ./src/service/salle.service.ts ***!
  \**************************************/
/*! exports provided: SalleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalleService", function() { return SalleService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**
 *Service pour utiliser l'API Salle
 */
class SalleService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/restSalle/listesalles';
        this.urlGetOneById = 'http://localhost:8080/restSalle/listesalles';
        this.urlAdd = 'http://localhost:8080/restSalle/salle';
        this.urlUpdate = 'http://localhost:8080/restSalle/salle';
        this.urlDelete = 'http://localhost:8080/restSalle/deletesalle';
        this.getOneByNom = 'http://localhost:8080/restSalle/sallenom';
        this.urlGetAllASC = 'http://localhost:8080/restSalle/capasc';
        this.urlGetAllDESC = 'http://localhost:8080/restSalle/capdesc';
    }
    getAllSalles() {
        return this.http.get(this.urlGetAll);
    }
    getOneSalle(id) {
        return this.http.get(this.urlGetOneById + '/' + id);
    }
    addSalle(salle) {
        return this.http.put(this.urlAdd, salle);
    }
    updateOneSalle(updateBatiment) {
        return this.http.post(this.urlUpdate, updateBatiment);
    }
    deleteOneSalle(id) {
        return this.http.delete(this.urlDelete + '/' + id);
    }
    addOneSalle(addBatiment) {
        return this.http.put(this.urlAdd, addBatiment);
    }
    getOneSalleByName(nom_salle) {
        return this.http.get(this.getOneByNom + '/' + nom_salle);
    }
    getAllSallesASC() {
        return this.http.get(this.urlGetAllASC);
    }
    getAllSallesDESC() {
        return this.http.get(this.urlGetAllDESC);
    }
}
SalleService.ɵfac = function SalleService_Factory(t) { return new (t || SalleService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
SalleService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SalleService, factory: SalleService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "40Iy":
/*!********************************************!*\
  !*** ./src/service/reservation.service.ts ***!
  \********************************************/
/*! exports provided: ReservationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservationService", function() { return ReservationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**
 * Service pour utiliser l'API Reservation
 */
class ReservationService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/reservation/getall';
        this.urlGetOneById = 'http://localhost:8080/reservation/getbyid';
        this.urlAdd = 'http://localhost:8080/reservation/add';
        this.urlUpdate = 'http://localhost:8080/reservation/update';
        this.urlDelete = 'http://localhost:8080/reservation/deletebyid';
    }
    ;
    /**
     * Méthode pour récuperer la liste de toute les reservations par appel REST
     * @returns la liste des reservations
     */
    getAllReservations() {
        return this.http.get(this.urlGetAll);
    }
    /**
     * Méthode récupèrant une reservation via son id par appel REST
     * @param id l'id de la reservation a chercher
     * @returns la reservation ciblée via son ID
     */
    getOneReservation(id) {
        return this.http.get(this.urlGetOneById + '/' + id);
    }
    /**
     * Méthode permattant dajuter une reservation en BDD par appel REST
     * @param reservation la reservation à sauvegarder
     * @returns la reservation sauvegardée dans la BDD
     */
    addReservation(reservation) {
        return this.http.put(this.urlAdd, reservation);
    }
    /**
     * Méthode modifiant une reservation par appel REST
     * @param updateBatiment  le batiment à modifier
     * @returns l'instance du batiment modifié en BDD
     */
    updateOneReservation(updateBatiment) {
        return this.http.post(this.urlUpdate, updateBatiment);
    }
    /**
     * Méthode supprimant une reservation par appel REST
     * @param id ID de la reservation à supprimer
     * @returns void
     */
    deleteOnenom_Reservation(id) {
        return this.http.delete(this.urlDelete + '/' + id);
    }
}
ReservationService.ɵfac = function ReservationService_Factory(t) { return new (t || ReservationService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ReservationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ReservationService, factory: ReservationService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "DUjH":
/*!******************************************!*\
  !*** ./src/service/etat_user.service.ts ***!
  \******************************************/
/*! exports provided: EtatUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtatUserService", function() { return EtatUserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**
*Service pour utiliser l'API Etat User
*/
class EtatUserService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/etatuser/getall';
    }
    getAllEtatUser() {
        return this.http.get(this.urlGetAll);
    }
}
EtatUserService.ɵfac = function EtatUserService_Factory(t) { return new (t || EtatUserService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
EtatUserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: EtatUserService, factory: EtatUserService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "HX0H":
/*!***************************************************************************************!*\
  !*** C:/Users/cleme/Downloads/scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*

@license
dhtmlxScheduler v.5.3.11 Professional Evaluation

This software is covered by DHTMLX Evaluation License. Contact sales@dhtmlx.com to get Commercial or Enterprise license. Usage without proper license is prohibited.

(c) XB Software Ltd.

*/
Scheduler.plugin(function(e){e.attachEvent("onTimelineCreated",function(t){"tree"==t.render&&(t.y_unit_original=t.y_unit,t.y_unit=e._getArrayToDisplay(t.y_unit_original),e.attachEvent("onOptionsLoadStart",function(){t.y_unit=e._getArrayToDisplay(t.y_unit_original)}),e.form_blocks[t.name]={render:function(e){return"<div class='dhx_section_timeline' style='overflow: hidden; height: "+e.height+"px'></div>"},set_value:function(t,a,n,i){
var r=e._getArrayForSelect(e.matrix[i.type].y_unit_original,i.type);t.innerHTML="";var o=document.createElement("select");t.appendChild(o);var _=t.getElementsByTagName("select")[0];!_._dhx_onchange&&i.onchange&&(_.onchange=i.onchange,_._dhx_onchange=!0);for(var d=0;d<r.length;d++){var s=document.createElement("option");s.value=r[d].key,s.value==n[e.matrix[i.type].y_property]&&(s.selected=!0),s.innerHTML=r[d].label,_.appendChild(s)}},get_value:function(e,t,a){return e.firstChild.value},
focus:function(e){}})}),e.attachEvent("onBeforeSectionRender",function(t,a,n){var i={};if("tree"==t){var r,o,_,d,s,l;d="dhx_matrix_scell",a.children?(r=n.folder_dy||n.dy,n.folder_dy&&!n.section_autoheight&&(_="height:"+n.folder_dy+"px;"),o="dhx_row_folder",d+=" folder",a.open?d+=" opened":d+=" closed",s="<div class='dhx_scell_expand'>"+(a.open?"-":"+")+"</div>",l=n.folder_events_available?"dhx_data_table folder_events":"dhx_data_table folder"):(r=n.dy,o="dhx_row_item",d+=" item",s="",
l="dhx_data_table"),d+=e.templates[n.name+"_scaley_class"](a.key,a.label,a)?" "+e.templates[n.name+"_scaley_class"](a.key,a.label,a):"";i={height:r,style_height:_,tr_className:o,td_className:d,td_content:"<div class='dhx_scell_level"+a.level+"'>"+s+"<div class='dhx_scell_name'>"+(e.templates[n.name+"_scale_label"](a.key,a.label,a)||a.label)+"</div></div>",table_className:l}}return i});var t;e.attachEvent("onBeforeEventChanged",function(a,n,i){
if(e._isRender("tree"))for(var r=e._get_event_sections?e._get_event_sections(a):[a[e.matrix[e._mode].y_property]],o=0;o<r.length;o++){var _=e.getSection(r[o]);if(_&&_.children&&!e.matrix[e._mode].folder_events_available)return i||(a[e.matrix[e._mode].y_property]=t),!1}return!0}),e.attachEvent("onBeforeDrag",function(a,n,i){if(e._isRender("tree")){var r,o=e._locate_cell_timeline(i);if(o&&(r=e.matrix[e._mode].y_unit[o.y].key,
e.matrix[e._mode].y_unit[o.y].children&&!e.matrix[e._mode].folder_events_available))return!1;var _=e.getEvent(a),d=e.matrix[e._mode].y_property;t=_&&_[d]?_[d]:r}return!0}),e._getArrayToDisplay=function(t){var a=[],n=function(t,i,r,o){for(var _=i||0,d=0;d<t.length;d++){var s=t[d];s.level=_,s.$parent=r||null,s.children&&void 0===s.key&&(s.key=e.uid()),o||a.push(s),s.children&&n(s.children,_+1,s.key,o||!s.open)}};return n(t),a},e._getArrayForSelect=function(t,a){var n=[],i=function(t){
for(var r=0;r<t.length;r++)e.matrix[a].folder_events_available?n.push(t[r]):t[r].children||n.push(t[r]),t[r].children&&i(t[r].children,a)};return i(t),n},e._toggleFolderDisplay=function(t,a,n){var i,r=function(e,t,a,n){for(var o=0;o<t.length&&(t[o].key!=e&&!n||!t[o].children||(t[o].open=void 0!==a?a:!t[o].open,i=!0,n||!i));o++)t[o].children&&r(e,t[o].children,a,n)},o=e.getSection(t);void 0!==a||n||(a=!o.open),
e.callEvent("onBeforeFolderToggle",[o,a,n])&&(r(t,e.matrix[e._mode].y_unit_original,a,n),e.matrix[e._mode].y_unit=e._getArrayToDisplay(e.matrix[e._mode].y_unit_original),e.callEvent("onOptionsLoad",[]),e.callEvent("onAfterFolderToggle",[o,a,n]))},e.attachEvent("onCellClick",function(t,a,n,i,r){e._isRender("tree")&&(e.matrix[e._mode].folder_events_available||void 0!==e.matrix[e._mode].y_unit[a]&&e.matrix[e._mode].y_unit[a].children&&e._toggleFolderDisplay(e.matrix[e._mode].y_unit[a].key))}),
e.attachEvent("onYScaleClick",function(t,a,n){e._isRender("tree")&&a.children&&e._toggleFolderDisplay(a.key)}),e.getSection=function(t){if(e._isRender("tree")){var a,n=function(e,t){for(var i=0;i<t.length;i++)t[i].key==e&&(a=t[i]),t[i].children&&n(e,t[i].children)};return n(t,e.matrix[e._mode].y_unit_original),a||null}},e.deleteSection=function(t){if(e._isRender("tree")){var a=!1,n=function(e,t){for(var i=0;i<t.length&&(t[i].key==e&&(t.splice(i,1),a=!0),!a);i++)t[i].children&&n(e,t[i].children)}
;return n(t,e.matrix[e._mode].y_unit_original),e.matrix[e._mode].y_unit=e._getArrayToDisplay(e.matrix[e._mode].y_unit_original),e.callEvent("onOptionsLoad",[]),a}},e.deleteAllSections=function(){e._isRender("tree")&&(e.matrix[e._mode].y_unit_original=[],e.matrix[e._mode].y_unit=e._getArrayToDisplay(e.matrix[e._mode].y_unit_original),e.callEvent("onOptionsLoad",[]))},e.addSection=function(t,a){if(e._isRender("tree")){var n=!1,i=function(e,t,r){
if(a)for(var o=0;o<r.length&&(r[o].key==t&&r[o].children&&(r[o].children.push(e),n=!0),!n);o++)r[o].children&&i(e,t,r[o].children);else r.push(e),n=!0};return i(t,a,e.matrix[e._mode].y_unit_original),e.matrix[e._mode].y_unit=e._getArrayToDisplay(e.matrix[e._mode].y_unit_original),e.callEvent("onOptionsLoad",[]),n}},e.openAllSections=function(){e._isRender("tree")&&e._toggleFolderDisplay(1,!0,!0)},e.closeAllSections=function(){e._isRender("tree")&&e._toggleFolderDisplay(1,!1,!0)},
e.openSection=function(t){e._isRender("tree")&&e._toggleFolderDisplay(t,!0)},e.closeSection=function(t){e._isRender("tree")&&e._toggleFolderDisplay(t,!1)}});

/***/ }),

/***/ "QQKQ":
/*!***********************************************************************************!*\
  !*** C:/Users/cleme/Downloads/scheduler/codebase/ext/dhtmlxscheduler_timeline.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*

@license
dhtmlxScheduler v.5.3.11 Professional Evaluation

This software is covered by DHTMLX Evaluation License. Contact sales@dhtmlx.com to get Commercial or Enterprise license. Usage without proper license is prohibited.

(c) XB Software Ltd.

*/
Scheduler.plugin(function(e){e._temp_matrix_scope=function(){function t(e,a){if(a=a||[],e.children)for(var n=0;n<e.children.length;n++)a.push(e.children[n].key),t(e.children[n],a);return a}function a(e,t){var a=t.order[e];return void 0===a&&(a="$_"+e),a}function n(e,t){if(t[e.key]=e,e.children)for(var a=0;a<e.children.length;a++)n(e.children[a],t)}function i(e){for(var t={},a=e.y_unit_original||e.y_unit,i=0;i<a.length;i++)n(a[i],t);return t}function r(t,n){function r(e,t,a,n){e[t]||(e[t]=[])
;for(var i=a;i<=n;i++)e[t][i]||(e[t][i]=[]),e[t][i].push(h)}for(var o=[],_=0;_<n.y_unit.length;_++)o[_]=[];var d;o[d]||(o[d]=[]);var s=i(n),l="tree"==n.render;l&&(o.$tree={});for(var c=n.y_property,_=0;_<t.length;_++){var h=t[_],u=h[c];d=a(u,n);var g=e._get_date_index(n,h.start_date),f=e._get_date_index(n,h.end_date);h.end_date.valueOf()==n._trace_x[f].valueOf()&&(f-=1),o[d]||(o[d]=[]),r(o,d,g,f);var v=s[u];if(l&&v&&v.$parent)for(;v.$parent;){var m=s[v.$parent];r(o.$tree,m.key,g,f),v=m}}return o
}function o(){var e=document.createElement("p");e.style.width="100%",e.style.height="200px";var t=document.createElement("div");t.style.position="absolute",t.style.top="0px",t.style.left="0px",t.style.visibility="hidden",t.style.width="200px",t.style.height="150px",t.style.overflow="hidden",t.appendChild(e),document.body.appendChild(t);var a=e.offsetWidth;t.style.overflow="scroll";var n=e.offsetWidth;return a==n&&(n=t.clientWidth),document.body.removeChild(t),a-n}function _(t){
return e._helpers.formatDate(t)}function d(t,a){var n=t.querySelector(".dhx_timeline_data_wrapper");return a.scrollable||(n=e.$container.querySelector(".dhx_cal_data")),n}function s(e){return e.querySelector(".dhx_timeline_label_wrapper")}function l(){return e.$container.querySelector(".dhx_cal_data .dhx_timeline_label_col")}function c(t,a,n,i){function r(e){if(e=e||window.event,!e.shiftKey){var t=e.deltaY||e.detail||-e.wheelDelta;t=t<0?-100:100,_.scrollTop+=t,
e.preventDefault&&e.preventDefault()}}a._is_ev_creating=!1;var _=d(t,a),c=e._els.dhx_cal_header[0],h=s(t);if(h&&(h.addEventListener?"onwheel"in document?h.addEventListener("wheel",r):"onmousewheel"in document&&h.addEventListener("mousewheel",r):h.attachEvent("onmousewheel",r),!h.$eventsAttached)){h.$eventsAttached=!0;var u={pageX:0,pageY:0};h.addEventListener("touchstart",function(e){var t=e;e.touches&&(t=e.touches[0]),u={pageX:t.pageX,pageY:t.pageY}}),h.addEventListener("touchmove",function(e){
var t=e;e.touches&&(t=e.touches[0]);var a=u.pageY-t.pageY;u={pageX:t.pageX,pageY:t.pageY},a&&(_.scrollTop+=a),e&&e.preventDefault&&e.preventDefault()})}var g;if(_.onscroll=function(r){var s=d(t,a),h=s.scrollTop,u=a.scrollHelper.getScrollValue(s),f=e._timeline_smart_render.getViewPort(a.scrollHelper,0,u,h),v=l();if(a.scrollable&&(v.style.top=-h+"px"),!1!==a.smart_rendering){
if((u!==a._x_scroll||a._is_ev_creating)&&(a.second_scale?e._timeline_smart_render.updateHeader(a,f,c.children[1]):e._timeline_smart_render.updateHeader(a,f,c.children[0])),e.config.rtl){var m=+e.$container.querySelector(".dhx_timeline_label_wrapper").style.height.replace("px",""),p=a._section_height[a.y_unit.length]+a._label_rows[a._label_rows.length-1].top;a.scrollHelper.getMode()==a.scrollHelper.modes.minMax&&(p>m||"tree"==a.render)?c.style.right=-1-u-o()+"px":c.style.right=-1-u+"px",
c.style.left="unset"}else c.style.left=-1-u+"px";(a._options_changed||h!==a._y_scroll||a._is_ev_creating)&&e._timeline_smart_render.updateLabels(a,f,v),a._is_ev_creating=!1,e._timeline_smart_render.updateGridCols(a,f),e._timeline_smart_render.updateGridRows(a,f);var y=!1;"cell"!=a.render&&(window.requestAnimationFrame?(y=!0,g&&cancelAnimationFrame(g),g=requestAnimationFrame(function(){a.name===e.getState().mode&&(e._timeline_smart_render.updateEvents(a,f),g=0,
a.callEvent("onScroll",[a.scrollHelper.getScrollValue(_),_.scrollTop]))})):e._timeline_smart_render.updateEvents(a,f));var b=0;a._scales={};var x;x="cell"===a.render?s.querySelectorAll(".dhx_timeline_data_col .dhx_timeline_data_row"):s.querySelectorAll(".dhx_timeline_data_col .dhx_matrix_line");for(var w=0,k=x.length;w<k;w++){var D=x[w].getAttribute("data-section-id"),E=a.order[D];n[E]=i[E].height,a._scales[D]=x[w]}for(var w=0,k=n.length;w<k;w++)b+=n[w]
;e.$container.querySelector(".dhx_timeline_data_col").style.height=b+"px";var S=s.scrollTop,N=a.scrollHelper.getScrollValue(s),M=a._summ-e.$container.querySelector(".dhx_cal_data").offsetWidth+a.dx+a.custom_scroll_width;e._timeline_save_scroll_pos(a,S,N,M),y||a.callEvent("onScroll",[N,S]),a._is_new_view=!1}},!_.$eventsAttached){_.$eventsAttached=!0;var f={pageX:0,pageY:0};_.addEventListener("touchstart",function(e){var t=e;e.touches&&(t=e.touches[0]),f={pageX:t.pageX,pageY:t.pageY}}),
_.addEventListener("touchmove",function(t){var n=t;t.touches&&(n=t.touches[0]);var i=l(),r=f.pageX-n.pageX,o=f.pageY-n.pageY;if(f={pageX:n.pageX,pageY:n.pageY},(r||o)&&!e.getState().drag_id){var d=Math.abs(r),s=Math.abs(o),c=Math.sqrt(r*r+o*o),h=d/c,u=s/c;h<.42?r=0:u<.42&&(o=0),a.scrollHelper.setScrollValue(_,a.scrollHelper.getScrollValue(_)+r),_.scrollTop+=o,a.scrollable&&o&&(i.style.top=-_.scrollTop+"px")}return t&&t.preventDefault&&t.preventDefault(),!1})}
a.scroll_position&&a._is_new_view?a.scrollTo(a.scroll_position):e._timeline_set_scroll_pos(t,a),a._is_ev_creating=!0}function h(e,t){if(e.closest)return e.closest(t);if(e.matches||e.msMatchesSelector||e.webkitMatchesSelector){var a=e;if(!document.documentElement.contains(a))return null;do{if((a.matches||a.msMatchesSelector||a.webkitMatchesSelector).call(a,t))return a;a=a.parentElement||a.parentNode}while(null!==a&&1===a.nodeType);return null}
return window.console.error("Your browser is not supported"),null}function u(t,a){var n=e.matrix[e._mode],i={},r={},o=a;for(i.x=t.touches?t.touches[0].pageX:t.pageX,i.y=t.touches?t.touches[0].pageY:t.pageY,r.left=o.offsetLeft+n.dx,r.top=o.offsetTop;o;)r.left+=o.offsetLeft,r.top+=o.offsetTop,o=o.offsetParent;return{x:i.x-r.left,y:i.y-r.top}}function g(t){M&&clearInterval(M),e._schedulerOuter=e.$container.querySelector(".dhx_timeline_data_wrapper");var a={
pageX:t.touches?t.touches[0].pageX:t.pageX,pageY:t.touches?t.touches[0].pageY:t.pageY};M=setInterval(function(){f(a)},N)}function f(t){if(!e.getState().drag_id)return clearInterval(M),void(A=null);var a=e.matrix[e._mode];if(a){var n=e._schedulerOuter,i=u(t,n),r=n.offsetWidth-a.dx,o=n.offsetHeight,_=i.x,d=i.y,s=a.autoscroll||{};e._merge(s,{range_x:200,range_y:100,speed_x:20,speed_y:10});var l=v(_,r,A?A.x:0,s.range_x);a.scrollable||(l=0);var c=v(d,o,A?A.y:0,s.range_y);!c&&!l||A||(A={x:_,y:d},l=0,
c=0),l*=s.speed_x,c*=s.speed_y,l&&c&&(Math.abs(l/5)>Math.abs(c)?c=0:Math.abs(c/5)>Math.abs(l)&&(l=0)),l||c?(A.started=!0,m(l,c)):clearInterval(M)}}function v(e,t,a,n){return e<n&&(!A||A.started||e<a)?-1:t-e<n&&(!A||A.started||e>a)?1:0}function m(t,a){var n=e._schedulerOuter;a&&(n.scrollTop+=a),t&&(n.scrollLeft+=t)}var p=function(){return function(){function e(){var e=document.createElement("div")
;e.style.cssText="direction: rtl;overflow: auto;width:100px;height: 100px;position:absolute;top: -100500px;left: -100500px;";var t=document.createElement("div");return t.style.cssText="width: 100500px;height: 1px;",e.appendChild(t),e}function t(){var t=_.minMax,a=e();return document.body.appendChild(a),a.scrollLeft>0?t=_.minMax:(a.scrollLeft=-50,t=-50===a.scrollLeft?_.nMaxMin:_.maxMin),document.body.removeChild(a),t}function a(e,t){var a=r();return a===_.nMaxMin?e?-e:0:a===_.minMax?t-e:e}
function n(e){var t=getComputedStyle(e).direction;if(t&&"ltr"!==t){var n=e.scrollWidth-e.offsetWidth;return a(e.scrollLeft,n)}return e.scrollLeft}function i(e,t){var n=getComputedStyle(e).direction;if(n&&"ltr"!==n){var i=e.scrollWidth-e.offsetWidth,r=a(t,i);e.scrollLeft=r}else e.scrollLeft=t}function r(){return o||(o=t()),o}var o,_={minMax:"[0;max]",maxMin:"[max;0]",nMaxMin:"[-max;0]"};return{modes:_,getMode:r,normalizeValue:a,getScrollValue:n,setScrollValue:i}}}();e.matrix={},
e._merge=function(e,t){for(var a in t)void 0===e[a]&&(e[a]=t[a])},e.createTimelineView=function(a){function n(a,n,i){var r=e._timeline_smart_render.getPreparedEvents(i),o=i.order[a],_=i.y_unit[o];if(!_)return[];var d=[a];n&&t(_,d);for(var s=[],l=0;l<d.length;l++){var o=i.order[d[l]];if(void 0!==o&&r[o])s=s.concat(r[o]);else if(r.undefined)for(var c=0;c<r.undefined.length;c++){var h=r.undefined[c];h[i.y_property]==d[l]&&s.push(h)}}return s}function i(t,a,n,i){
var r=e._timeline_smart_render.getPreparedEvents(i),o=[],_=[],d=i.order[t],s=i.y_unit[d];if(!s)return[];var l=e._get_date_index(i,a);return r.$matrix?(o=r.$matrix[d][l]||[],n&&r.$matrix.$tree&&r.$matrix.$tree[s.key]&&(_=r.$matrix.$tree[s.key][l]||[]),o.concat(_)):r[d]||[]}function r(t,a,n){for(var i=e.date[n.name+"_start"](new Date(t)),r=0,o=i,_=n.x_step,d=n.x_unit;o<a;)r++,o=e.date.add(o,_,d);return r}e._skin_init(),e._merge(a,{scrollHelper:p(),column_width:100,autoscroll:{range_x:200,
range_y:100,speed_x:20,speed_y:10},_is_new_view:!0,_section_autowidth:!0,_x_scroll:0,_y_scroll:0,_h_cols:{},_label_rows:[],section_autoheight:!0,name:"matrix",x:"time",y:"time",x_step:1,x_unit:"hour",y_unit:"day",y_step:1,x_start:0,x_size:24,y_start:0,y_size:7,render:"cell",dx:200,dy:50,event_dy:e.xy.bar_height-5,event_min_dy:e.xy.bar_height-5,resize_events:!0,fit_events:!0,show_unassigned:!1,second_scale:!1,round_position:!1,_logic:function(t,a,n){var i={}
;return e.checkEvent("onBeforeSectionRender")&&(i=e.callEvent("onBeforeSectionRender",[t,a,n])),i}}),a._original_x_start=a.x_start,"day"!=a.x_unit&&(a.first_hour=a.last_hour=0),a._start_correction=a.first_hour?60*a.first_hour*60*1e3:0,a._end_correction=a.last_hour?60*(24-a.last_hour)*60*1e3:0,e.checkEvent("onTimelineCreated")&&e.callEvent("onTimelineCreated",[a]),dhtmlxEventable(a);var _=e.render_data;e.render_data=function(t,n){if(this._mode!=a.name)return _.apply(this,arguments)
;if(n&&!a.show_unassigned&&"cell"!=a.render)for(var i=0;i<t.length;i++)this.clear_event(t[i]),this.render_timeline_event.call(this.matrix[this._mode],t[i],!0);else e._renderMatrix.call(a,!0,!0)},e.matrix[a.name]=a,e.templates[a.name+"_cell_value"]=function(e){return e?e.length:""},e.templates[a.name+"_cell_class"]=function(e){return""},e.templates[a.name+"_scalex_class"]=function(e){return""},e.templates[a.name+"_second_scalex_class"]=function(e){return""},
e.templates[a.name+"_row_class"]=function(e,t){return t.folder_events_available&&e.children?"folder":""},e.templates[a.name+"_scaley_class"]=function(e,t,a){return""},e.templates[a.name+"_scale_label"]=function(e,t,a){return t},e.templates[a.name+"_tooltip"]=function(e,t,a){return a.text},e.templates[a.name+"_date"]=function(t,a){
return t.getDay()==a.getDay()&&a-t<864e5||+t==+e.date.date_part(new Date(a))||+e.date.add(t,1,"day")==+a&&0===a.getHours()&&0===a.getMinutes()?e.templates.day_date(t):t.getDay()!=a.getDay()&&a-t<864e5?e.templates.day_date(t)+" &ndash; "+e.templates.day_date(a):e.templates.week_date(t,a)},e.templates[a.name+"_scale_date"]=e.date.date_to_str(a.x_date||e.config.hour_date),
e.templates[a.name+"_second_scale_date"]=e.date.date_to_str(a.second_scale&&a.second_scale.x_date?a.second_scale.x_date:e.config.hour_date),e.date["add_"+a.name+"_private"]=function(t,n){var i=n,r=a.x_unit;if("minute"==a.x_unit||"hour"==a.x_unit){var o=i;"hour"==a.x_unit&&(o*=60),o%1440||(i=o/1440,r="day")}return e.date.add(t,i,r)},e.date["add_"+a.name]=function(t,n,i){var r=e.date["add_"+a.name+"_private"](t,(a.x_length||a.x_size)*a.x_step*n);if("minute"==a.x_unit||"hour"==a.x_unit){
var o=a.x_length||a.x_size,_="hour"==a.x_unit?60*a.x_step:a.x_step;if(_*o%1440)if(+e.date.date_part(new Date(t))==+e.date.date_part(new Date(r)))a.x_start+=n*o;else{var d=1440/(o*_)-1,s=Math.round(d*o);a.x_start=n>0?a.x_start-s:s+a.x_start}}return r},e.date[a.name+"_start"]=function(t){var n=e.date[a.x_unit+"_start"]||e.date.day_start,i=n.call(e.date,t),r=i.getTimezoneOffset();i=e.date.add(i,a.x_step*a.x_start,a.x_unit);var o=i.getTimezoneOffset();return r!=o&&i.setTime(i.getTime()+6e4*(o-r)),i
},a.scrollTo=e.bind(function(t){if(t){var a;a=t.date?t.date:t.left?t.left:t;var n=-1;t.section?n=this.posFromSection(t.section):t.top&&(n=t.top);var i;if(i="number"==typeof a?a:this.posFromDate(a),e.config.rtl){var r=+e.$container.querySelector(".dhx_timeline_label_wrapper").style.height.replace("px",""),_=this._section_height[this.y_unit.length]+this._label_rows[this._label_rows.length-1].top;this.scrollHelper.getMode()==this.scrollHelper.modes.minMax&&(_>r||"tree"==this.render)&&(i-=o())}
var d=e.$container.querySelector(".dhx_timeline_data_wrapper");this.scrollable||(d=e.$container.querySelector(".dhx_cal_data")),this.scrollable&&this.scrollHelper.setScrollValue(d,i),n>0&&(d.scrollTop=n)}},a),a.getScrollPosition=e.bind(function(){return{left:this._x_scroll||0,top:this._y_scroll||0}},a),a.posFromDate=e.bind(function(t){return e._timeline_getX({start_date:t},!1,this)-1},a),a.posFromSection=e.bind(function(e){var t=this.order[e];if(void 0===t)return-1;var a=0
;for(var n in this.order)this.order[n]<t&&(a+=this._section_height[n]);return a},a),a.selectEvents=e.bind(function(e){var t=e.section,a=e.date,r=e.selectNested;return a?i(t,a,r,this):t?n(t,r,this):void 0},a),a.setRange=e.bind(function(t,a){var n=e.date[this.name+"_start"](new Date(t)),i=r(t,a,this);this.x_size=i,e.setCurrentView(n,this.name)},a),e.callEvent("onOptionsLoad",[a]),e[a.name+"_view"]=function(t){t?e._set_timeline_dates(a):e._renderMatrix.apply(a,arguments)};var d=new Date
;e.date.add(d,a.x_step,a.x_unit).valueOf(),d.valueOf();e["mouse_"+a.name]=function(t){var n=this._drag_event;if(this._drag_id&&(n=this.getEvent(this._drag_id)),a.scrollable&&!t.converted){if(t.converted=1,t.x+=-a.dx+a._x_scroll,e.config.rtl){var i=+e.$container.querySelector(".dhx_timeline_label_wrapper").style.height.replace("px",""),r=a._section_height[a.y_unit.length]+a._label_rows[a._label_rows.length-1].top;t.x+=e.xy.scale_width,
a.scrollHelper.getMode()==a.scrollHelper.modes.minMax&&(r>i||"tree"==a.render)&&(t.x+=o())}t.y+=a._y_scroll}else e.config.rtl?t.x-=a.dx-e.xy.scale_width:t.x-=a.dx;var _=e._timeline_drag_date(a,t.x);if(t.x=0,t.force_redraw=!0,t.custom=!0,"move"==this._drag_mode&&this._drag_id&&this._drag_event){var n=this.getEvent(this._drag_id),d=this._drag_event;if(t._ignores=this._ignores_detected||a._start_correction||a._end_correction,void 0===d._move_delta&&(d._move_delta=(n.start_date-_)/6e4,
this.config.preserve_length&&t._ignores&&(d._move_delta=this._get_real_event_length(n.start_date,_,a),d._event_length=this._get_real_event_length(n.start_date,n.end_date,a))),this.config.preserve_length&&t._ignores){var s=(d._event_length,this._get_fictional_event_length(_,d._move_delta,a,!0));_=new Date(_-s)}else _=e.date.add(_,d._move_delta,"minute")}
if("resize"==this._drag_mode&&n&&(this.config.timeline_swap_resize&&this._drag_id&&(this._drag_from_start&&+_>+n.end_date?this._drag_from_start=!1:!this._drag_from_start&&+_<+n.start_date&&(this._drag_from_start=!0)),t.resize_from_start=this._drag_from_start,!this.config.timeline_swap_resize&&this._drag_id&&this._drag_from_start&&+_>=+e.date.add(n.end_date,-e.config.time_step,"minute")&&(_=e.date.add(n.end_date,-e.config.time_step,"minute"))),a.round_position)switch(this._drag_mode){case"move":
this.config.preserve_length||(_=e._timeline_get_rounded_date.call(a,_,!1),"day"==a.x_unit&&(t.custom=!1));break;case"resize":this._drag_event&&(null!==this._drag_event._resize_from_start&&void 0!==this._drag_event._resize_from_start||(this._drag_event._resize_from_start=t.resize_from_start),t.resize_from_start=this._drag_event._resize_from_start,_=e._timeline_get_rounded_date.call(a,_,!this._drag_event._resize_from_start))}this._resolve_timeline_section(a,t),
t.section&&this._update_timeline_section({pos:t,event:this.getEvent(this._drag_id),view:a}),t.y=Math.round((this._correct_shift(_,1)-this._min_date)/(6e4*this.config.time_step)),t.shift=this.config.time_step,a.round_position&&"new-size"==this._drag_mode&&_<=this._drag_start&&(t.shift=e.date.add(this._drag_start,a.x_step,a.x_unit)-this._drag_start);var l=this._is_pos_changed(this._drag_pos,t);return this._drag_pos&&l&&(this._drag_event._dhx_changed=!0),
l||this._drag_pos.has_moved||(t.force_redraw=!1),t}},e._prepare_timeline_events=function(t){var a=[];if("cell"==t.render)a=e._timeline_trace_events.call(t);else{for(var n=e.get_visible_events(),i=t.order,r=0;r<n.length;r++){var o=n[r],_=o[t.y_property],d=t.order[_];t.y_unit[d];if(t.show_unassigned&&!_){for(var s in i)if(i.hasOwnProperty(s)){d=i[s],a[d]||(a[d]=[]);var l=e._lame_copy({},o);l[t.y_property]=s,a[d].push(l);break}}else a[d]||(a[d]=[]),a[d].push(o)}
a.$matrix=e._timeline_trace_events.call(t)}return a},e._populate_timeline_rendered=function(t){e._rendered=[];for(var a=t.querySelectorAll("div[event_id]"),n=0;n<a.length;n++)e._rendered.push(a[n])},e._get_timeline_event_height=function(e,t){var a=e[t.y_property],n=t.event_dy;return"full"==t.event_dy&&(n=t.section_autoheight?t._section_height[a]-6:t.dy-3),t.resize_events&&(n=Math.max(Math.floor(n/(e._count||1)),t.event_min_dy)),n},e._get_timeline_event_y=function(t,a){
var n=t||0,i=2+n*a+(n?2*n:0);return e.config.cascade_event_display&&(i=2+n*e.config.cascade_event_margin+(n?2*n:0)),i},e.render_timeline_event=function(t,a){var n=t[this.y_property];if(!n)return"";var i=t._sorder,r=e._timeline_getX(t,!1,this),o=e._timeline_getX(t,!0,this),_=e._get_timeline_event_height(t,this),d=_-2;t._inner||"full"!=this.event_dy||(d=(d+2)*(t._count-i)-2),d+=3;var s=e._get_timeline_event_y(t._sorder,_),l=_+s+2
;(!this._events_height[n]||this._events_height[n]<l)&&(this._events_height[n]=l);var c=e.templates.event_class(t.start_date,t.end_date,t);c="dhx_cal_event_line "+(c||""),e.getState().select_id==t.id&&(c+=" dhx_cal_event_selected"),t._no_drag_move&&(c+=" no_drag_move")
;var h=t.color?"background:"+t.color+";":"",u=t.textColor?"color:"+t.textColor+";":"",g=e.templates.event_bar_text(t.start_date,t.end_date,t),f="<div "+e._waiAria.eventBarAttrString(t)+" event_id='"+t.id+"' class='"+c+"' style='"+h+u+"position:absolute; top:"+s+"px; height: "+d+"px; "+(e.config.rtl?"right:":"left:")+r+"px; width:"+Math.max(0,o-r)+"px;"+(t._text_style||"")+"'>";if(e.config.drag_resize&&!e.config.readonly){
var v="dhx_event_resize",m=d+1,p="<div class='"+v+" "+v+"_start' style='height: "+m+"px;'></div>",y="<div class='"+v+" "+v+"_end' style='height: "+m+"px;'></div>";f+=(t._no_resize_start?"":p)+(t._no_resize_end?"":y)}if(f+=g+"</div>",!a)return f;var b=document.createElement("div");b.innerHTML=f;var x=this._scales[n];x&&(e._rendered.push(b.firstChild),x.appendChild(b.firstChild))};var y=function(e){return String(e).replace(/'/g,"&apos;").replace(/"/g,"&quot;")},b=function(e){
return String(e).replace(/'/g,"\\'").replace(/"/g,'\\"')};e._timeline_trace_events=function(){return r(e.get_visible_events(),this)},e._timeline_getX=function(t,a,n){var i=0,r=n._step,o=n.round_position,_=0,d=a?t.end_date:t.start_date;d.valueOf()>e._max_date.valueOf()&&(d=e._max_date);var s=d-e._min_date_timeline;if(s>0){var l=e._get_date_index(n,d);e._ignores[l]&&(o=!0);for(var c=0;c<l;c++)i+=e._cols[c];var h=e._timeline_get_rounded_date.apply(n,[d,!1]);o?+d>+h&&a&&(_=e._cols[l]):(s=d-h,
n.first_hour||n.last_hour?(s-=n._start_correction,s<0&&(s=0),(_=Math.round(s/r))>e._cols[l]&&(_=e._cols[l])):_=Math.round(s/r))}e._border_box_events();return i+=a?0===s||o?_-2:_:_+1},e._timeline_get_rounded_date=function(t,a){var n=e._get_date_index(this,t),i=this._trace_x[n];return a&&+t!=+this._trace_x[n]&&(i=this._trace_x[n+1]?this._trace_x[n+1]:e.date.add(this._trace_x[n],this.x_step,this.x_unit)),new Date(i)},e._timeline_skip_ignored=function(t){
if(e._ignores_detected)for(var a,n,i,r,o=0;o<t.length;o++){for(r=t[o],i=!1,a=e._get_date_index(this,r.start_date),n=e._get_date_index(this,r.end_date);a<n;){if(!e._ignores[a]){i=!0;break}a++}i||a!=n||e._ignores[n]||+r.end_date>+this._trace_x[n]&&(i=!0),i||(t.splice(o,1),o--)}},e._timeline_calculate_event_positions=function(t){if(t&&"cell"!=this.render){e._timeline_skip_ignored.call(this,t),t.sort(this.sort||function(e,t){
return e.start_date.valueOf()==t.start_date.valueOf()?e.id>t.id?1:-1:e.start_date>t.start_date?1:-1});for(var a=[],n=t.length,i=-1,r=null,o=0;o<n;o++){var _=t[o];_._inner=!1;var d=this.round_position?e._timeline_get_rounded_date.apply(this,[_.start_date,!1]):_.start_date;for(this.round_position?e._timeline_get_rounded_date.apply(this,[_.end_date,!0]):_.end_date;a.length;){if(!(a[a.length-1].end_date.valueOf()<=d.valueOf()))break;a.splice(a.length-1,1)}for(var s=!1,l=0;l<a.length;l++){var c=a[l]
;if(c.end_date.valueOf()<=d.valueOf()){s=!0,_._sorder=c._sorder,a.splice(l,1),_._inner=!0;break}}if(a.length&&(a[a.length-1]._inner=!0),!s)if(a.length)if(a.length<=a[a.length-1]._sorder){if(a[a.length-1]._sorder)for(var h=0;h<a.length;h++){for(var u=!1,g=0;g<a.length;g++)if(a[g]._sorder==h){u=!0;break}if(!u){_._sorder=h;break}}else _._sorder=0;_._inner=!0}else{for(var f=a[0]._sorder,v=1;v<a.length;v++)a[v]._sorder>f&&(f=a[v]._sorder);_._sorder=f+1,i<_._sorder&&(i=_._sorder,r=_),_._inner=!1
}else _._sorder=0;a.push(_),a.length>(a.max_count||0)?(a.max_count=a.length,_._count=a.length):_._count=_._count?_._count:1}for(var m=0;m<t.length;m++)t[m]._count=a.max_count,e._register_copy&&e._register_copy(t[m]);(r||t[0])&&e.render_timeline_event.call(this,r||t[0],!1)}},e._timeline_get_events_html=function(t){var a="";if(t&&"cell"!=this.render)for(var n=0;n<t.length;n++)a+=e.render_timeline_event.call(this,t[n],!1);return a},e._timeline_update_events_html=function(t){var a=""
;if(t&&"cell"!=this.render){var n=e.getView(),i={},r=function(e,t){return e+"_"+t};t.forEach(function(e){i[r(e.id,e[n.y_property])]=!0}),e._rendered.forEach(function(e){if(e.parentNode){var t=e.parentNode.getAttribute("data-section-id");i[r(e.getAttribute("event_id"),t)]&&e.parentNode.removeChild(e)}});for(var o=0;o<t.length;o++)a+=e.render_timeline_event.call(this,t[o],!1)}return a},e._timeline_get_block_stats=function(t,a){var n={};return a._sch_height=t.offsetHeight,
n.style_data_wrapper=(e.config.rtl?"padding-right:":"padding-left:")+a.dx+"px;",n.style_label_wrapper="width: "+a.dx+"px;",a.scrollable?(n.style_data_wrapper+="height:"+(a._sch_height-1)+"px;",void 0===a.html_scroll_width&&(a.html_scroll_width=o()),a._section_autowidth?a.custom_scroll_width=0:a.custom_scroll_width=a.html_scroll_width,n.style_label_wrapper+="height:"+(a._sch_height-1-a.custom_scroll_width)+"px;"):(n.style_data_wrapper+="height:"+(a._sch_height-1)+"px;",
n.style_label_wrapper+="height:"+(a._sch_height-1)+"px;overflow:visible;"),n},e._timeline_get_cur_row_stats=function(t,a){var n=t._logic(t.render,t.y_unit[a],t);if(e._merge(n,{height:t.dy}),t.section_autoheight){var i=t.scrollable?t._sch_height-e.xy.scroll_width:t._sch_height;t.y_unit.length*n.height<i&&(n.height=Math.max(n.height,Math.floor((i-1)/t.y_unit.length)))}return t._section_height[t.y_unit[a].key]=n.height,
n.td_className||(n.td_className="dhx_matrix_scell"+(e.templates[t.name+"_scaley_class"](t.y_unit[a].key,t.y_unit[a].label,t.y_unit[a])?" "+e.templates[t.name+"_scaley_class"](t.y_unit[a].key,t.y_unit[a].label,t.y_unit[a]):"")),n.td_content||(n.td_content=e.templates[t.name+"_scale_label"](t.y_unit[a].key,t.y_unit[a].label,t.y_unit[a])),e._merge(n,{tr_className:"",style_height:"height:"+n.height+"px;",style_width:"width:"+t.dx+"px;",summ_width:"width:"+t._summ+"px;",table_className:""}),n},
e._timeline_get_fit_events_stats=function(e,t,a){if(e.fit_events){var n=e._events_height[e.y_unit[t].key]||0;a.height=n>a.height?n:a.height,a.style_height="height:"+a.height+"px;",a.style_line_height="line-height:"+(a.height-1)+"px;",e._section_height[e.y_unit[t].key]=a.height}return a.style_height="height:"+a.height+"px;",a.style_line_height="line-height:"+(a.height-1)+"px;",e._section_height[e.y_unit[t].key]=a.height,a},e._timeline_set_scroll_pos=function(e,t){
var a=e.querySelector(".dhx_timeline_data_wrapper");a.scrollTop=t._y_scroll||0,t.scrollHelper.setScrollValue(a,t._x_scroll||0),t.scrollHelper.getMode()!=t.scrollHelper.modes.maxMin&&a.scrollLeft==t._summ-a.offsetWidth+t.dx&&(a.scrollLeft+=o())},e._timeline_save_scroll_pos=function(e,t,a,n){e._y_scroll=t||0,e._x_scroll=a||0},e._timeline_get_html_for_cell_data_row=function(e,t,a,n,i){var r="";return i.template&&(r+=" "+(i.template(i.section,i.view)||"")),
"<div class='dhx_timeline_data_row"+r+"' data-section-id='"+y(n)+"' data-section-index='"+e+"' style='"+t.summ_width+t.style_height+" position:absolute; top:"+a+"px;'>"},e._timeline_get_html_for_cell_ignores=function(e){return'<div class="dhx_matrix_cell dhx_timeline_data_cell" style="'+e.style_height+e.style_line_height+';display:none"></div>'},e._timeline_get_html_for_cell=function(t,a,n,i,r,o){var d=n._trace_x[t],s=n.y_unit[a],l=e._cols[t],c=_(d),h=e.templates[n.name+"_cell_value"](i,d,s)
;return"<div data-col-id='"+t+"' data-col-date='"+c+"' class='dhx_matrix_cell dhx_timeline_data_cell "+e.templates[n.name+"_cell_class"](i,d,s)+"' style='width:"+l+"px;"+r.style_height+r.style_line_height+(e.config.rtl?" right:":"  left:")+o+"px;'><div style='width:auto'>"+h+"</div></div>"},e._timeline_get_html_for_bar_matrix_line=function(e,t,a,n){
return"<div style='"+t.summ_width+" "+t.style_height+" position:absolute; top:"+a+"px;' data-section-id='"+y(n)+"' data-section-index='"+e+"' class='dhx_matrix_line'>"},e._timeline_get_html_for_bar_data_row=function(e,t){var a=e.table_className;return t.template&&(a+=" "+(t.template(t.section,t.view)||"")),"<div class='dhx_timeline_data_row "+a+"' style='"+e.summ_width+" "+e.style_height+"' >"},e._timeline_get_html_for_bar_ignores=function(){return""},
e._timeline_get_html_for_bar=function(t,a,n,i,r,o){var d=_(n._trace_x[t]),s=n.y_unit[a],l="";n.cell_template&&(l=e.templates[n.name+"_cell_value"](i,n._trace_x[t],s,o));var c="line-height:"+n._section_height[s.key]+"px;"
;return"<div class='dhx_matrix_cell dhx_timeline_data_cell "+e.templates[n.name+"_cell_class"](i,n._trace_x[t],s,o)+"' style='width:"+e._cols[t]+"px; "+(e.config.rtl?"right:":"left:")+r+"px;'  data-col-id='"+t+"' data-col-date='"+d+"' ><div style='width:auto; height:100%;position:relative;"+c+"'>"+l+"</div></div>"},e._timeline_render_scale_header=function(t,a){var n=e.$container.querySelector(".dhx_timeline_scale_header");if(n&&n.parentNode.removeChild(n),a){n=document.createElement("div")
;var i="dhx_timeline_scale_header";t.second_scale&&(i+=" dhx_timeline_second_scale");var r=e.xy.scale_height;n.className=i,n.style.cssText=["width:"+(t.dx-1)+"px","height:"+r+"px","line-height:"+r+"px","top:"+(e.xy.nav_height+2)+"px",e.config.rtl?"right:0":"left:0"].join(";"),n.innerHTML=e.locale.labels[t.name+"_scale_header"]||"",e.$container.appendChild(n)}},e._timeline_y_scale=function(t){
var a=e._timeline_get_block_stats(t,this),n=this.scrollable?" dhx_timeline_scrollable_data":"",i="<div class='dhx_timeline_table_wrapper'>",r="<div class='dhx_timeline_label_wrapper' style='"+a.style_label_wrapper+"'><div class='dhx_timeline_label_col'>",o="<div class='dhx_timeline_data_wrapper"+n+"' style='"+a.style_data_wrapper+"'><div class='dhx_timeline_data_col'>";e._load_mode&&e._load(),e._timeline_smart_render.clearPreparedEventsCache(_)
;var _=e._timeline_smart_render.getPreparedEvents(this);e._timeline_smart_render.cachePreparedEvents(_);for(var d=0,s=0;s<e._cols.length;s++)d+=e._cols[s];var l=new Date,h=e._cols.length-e._ignores_detected;l=(e.date.add(l,this.x_step*h,this.x_unit)-l-(this._start_correction+this._end_correction)*h)/d,this._step=l,this._summ=d;var u=e._colsS.heights=[],g=[];this._events_height={},this._section_height={},this._label_rows=[];var f=!1,v=0,m=null
;(this.scrollable||this.smart_rendering)&&(m=e._timeline_smart_render.getViewPort(this.scrollHelper,this._sch_height)),e._timeline_smart_render._rendered_labels_cache=[],e._timeline_smart_render._rendered_events_cache=[];var p,b=!!m;p=this.scrollable?!1!==this.smart_rendering&&!!b:!!this.smart_rendering&&b;for(var x=[],w=0,k=0;k<this.y_unit.length;k++){e._timeline_calculate_event_positions.call(this,_[k]);var D=e._timeline_get_cur_row_stats(this,k);D=e._timeline_get_fit_events_stats(this,k,D),
x.push(D),w+=D.height}m&&w<m.scrollTop&&(m.scrollTop=Math.max(0,w-m.height));for(var k=0;k<this.y_unit.length;k++){var D=x[k],E=this.y_unit[k],S="<div class='dhx_timeline_label_row "+D.tr_className+"' style='top:"+v+"px;"+D.style_height+D.style_line_height+"'data-row-index='"+k+"' data-row-id='"+y(E.key)+"'><div class='"+D.td_className+"' style='"+D.style_width+" height:"+D.height+"px;' "+e._waiAria.label(D.td_content)+">"+D.td_content+"</div></div>";if(p&&this._label_rows.push({div:S,top:v,
section:E}),p&&(e._timeline_smart_render.isInYViewPort({top:v,bottom:v+D.height},m)||(f=!0)),v+=D.height,f)f=!1;else{r+=S,p&&e._timeline_smart_render._rendered_labels_cache.push(k);var N=e.templates[this.name+"_row_class"],M={view:this,section:E,template:N},A=0;if("cell"==this.render){o+=e._timeline_get_html_for_cell_data_row(k,D,v-D.height,E.key,M);for(var C=0;C<e._cols.length;C++)e._ignores[C]&&!p?o+=e._timeline_get_html_for_cell_ignores(D):p&&b?e._timeline_smart_render.isInXViewPort({left:A,
right:A+e._cols[C]},m)&&(o+=e._timeline_get_html_for_cell(C,k,this,_[k][C],D,A)):o+=e._timeline_get_html_for_cell(C,k,this,_[k][C],D,A),A+=e._cols[C];o+="</div>"}else{o+=e._timeline_get_html_for_bar_matrix_line(k,D,v-D.height,E.key);var O=_[k];p&&b&&(O=e._timeline_smart_render.getVisibleEventsForRow(this,m,_,k));o+=e._timeline_get_events_html.call(this,O),o+=e._timeline_get_html_for_bar_data_row(D,M)
;for(var C=0;C<e._cols.length;C++)e._ignores[C]?o+=e._timeline_get_html_for_bar_ignores():p&&b?e._timeline_smart_render.isInXViewPort({left:A,right:A+e._cols[C]},m)&&(o+=e._timeline_get_html_for_bar(C,k,this,_[k],A)):o+=e._timeline_get_html_for_bar(C,k,this,_[k],A),A+=e._cols[C];o+="</div></div>"}}D.sectionKey=E.key,g.push(D)}if(i+=r+"</div></div>",i+=o+"</div></div>",i+="</div>",this._matrix=_,t.innerHTML=i,p){e.$container.querySelector(".dhx_timeline_data_col").style.height=v+"px"}
e._populate_timeline_rendered(t),this._scales={};for(var s=0,T=g.length;s<T;s++){u.push(g[s].height);var L=g[s].sectionKey;e._timeline_finalize_section_add(this,L,t)}p&&e._timeline_smart_render&&(e._timeline_smart_render._rendered_events_cache=[]),(p||this.scrollable)&&c(t,this,u,g)},e._timeline_finalize_section_add=function(t,a,n){var i=t._scales[a]=n.querySelector(".dhx_timeline_data_col [data-section-id='"+b(a)+"']");i&&e.callEvent("onScaleAdd",[i,a])},
e.attachEvent("onBeforeViewChange",function(t,a,n,i){if(e.matrix[n]){var r=e.matrix[n];if(r.scrollable){if("tree"==r.render&&t===n&&a===i)return!0;r._x_scroll=r._y_scroll=0,e.$container.querySelector(".dhx_timeline_scrollable_data")&&e._timeline_set_scroll_pos(e._els.dhx_cal_data[0],r)}}return!0}),e._timeline_x_dates=function(t){var a=e._min_date,n=e._max_date;e._process_ignores(a,this.x_size,this.x_unit,this.x_step,t);for(var i=(this.x_size,t&&e._ignores_detected,
0),r=0;+a<+n;)if(this._trace_x[r]=new Date(a),"month"==this.x_unit&&e.date[this.x_unit+"_start"]&&(a=e.date[this.x_unit+"_start"](new Date(a))),a=e.date.add(a,this.x_step,this.x_unit),e.date[this.x_unit+"_start"]&&(a=e.date[this.x_unit+"_start"](a)),e._ignores[r]||i++,r++,t)if(i<this.x_size&&!(+a<+n))n=e.date["add_"+this.name+"_private"](n,(this.x_length||this.x_size)*this.x_step);else if(i>=this.x_size){e._max_date=a;break}return{total:r,displayed:i}},e._timeline_x_scale=function(t){
var a=e.xy.scale_height,n=this._header_resized||e.xy.scale_height;e._cols=[],e._colsS={height:0},this._trace_x=[];var i=e.config.preserve_scale_length,r=e._timeline_x_dates.call(this,i),o=e._x-this.dx-e.xy.scroll_width;if(this.scrollable&&this.column_width>0){var d=this.column_width*r.displayed;d>o&&(o=d,this._section_autowidth=!1)}var s=[this.dx];e._els.dhx_cal_header[0].style.width=s[0]+o+"px"
;for(var l=e._min_date_timeline=e._min_date,c=r.displayed,h=r.total,u=0;u<h;u++)e._ignores[u]?(e._cols[u]=0,c++):e._cols[u]=Math.floor(o/(c-u)),o-=e._cols[u],s[u+1]=s[u]+e._cols[u];if(t.innerHTML="<div></div>",this.second_scale){for(var g=this.second_scale.x_unit,f=[this._trace_x[0]],v=[],m=[this.dx,this.dx],p=0,y=0;y<this._trace_x.length;y++){var b=this._trace_x[y];e._timeline_is_new_interval(g,b,f[p])&&(++p,f[p]=b,m[p+1]=m[p]);var x=p+1;v[p]=e._cols[y]+(v[p]||0),m[x]+=e._cols[y]}
t.innerHTML="<div></div><div></div>";var w=t.firstChild;w.style.height=n+"px";var k=t.lastChild;k.style.position="relative",k.className="dhx_bottom_scale_container";for(var D=0;D<f.length;D++){var E=f[D],S=e.templates[this.name+"_second_scalex_class"](E),N=document.createElement("div");N.className="dhx_scale_bar dhx_second_scale_bar"+(S?" "+S:""),e.set_xy(N,v[D]-1,n-3,m[D],0),N.innerHTML=e.templates[this.name+"_second_scale_date"](E),w.appendChild(N)}}e.xy.scale_height=n,t=t.lastChild,
this._h_cols={};for(var M=0;M<this._trace_x.length;M++)if(!e._ignores[M]){l=this._trace_x[M],e._render_x_header(M,s[M],l,t);var A=e.templates[this.name+"_scalex_class"](l);A&&(t.lastChild.className+=" "+A),t.lastChild.setAttribute("data-col-id",M),t.lastChild.setAttribute("data-col-date",_(l));var C=t.lastChild.cloneNode(!0);this._h_cols[M]={div:C,left:s[M]}}e.xy.scale_height=a;var O=this._trace_x;t.onclick=function(t){var a=e._timeline_locate_hcell(t)
;a&&e.callEvent("onXScaleClick",[a.x,O[a.x],t||event])},t.ondblclick=function(t){var a=e._timeline_locate_hcell(t);a&&e.callEvent("onXScaleDblClick",[a.x,O[a.x],t||event])}},e._timeline_is_new_interval=function(t,a,n){switch(t){case"hour":return a.getHours()!=n.getHours()||e._timeline_is_new_interval("day",a,n);case"day":return!(a.getDate()==n.getDate()&&a.getMonth()==n.getMonth()&&a.getFullYear()==n.getFullYear());case"week":
return!(e.date.week_start(new Date(a)).valueOf()==e.date.week_start(new Date(n)).valueOf());case"month":return!(a.getMonth()==n.getMonth()&&a.getFullYear()==n.getFullYear());case"year":return!(a.getFullYear()==n.getFullYear());default:return!1}},e._timeline_reset_scale_height=function(t){if(this._header_resized&&(!t||!this.second_scale)){e.xy.scale_height/=2,this._header_resized=!1;var a=e._els.dhx_cal_header[0];a.className=a.className.replace(/ dhx_second_cal_header/gi,"")}},
e._timeline_set_full_view=function(t){if(e._timeline_reset_scale_height.call(this,t),t){this.second_scale&&!this._header_resized&&(this._header_resized=e.xy.scale_height,e.xy.scale_height*=2,e._els.dhx_cal_header[0].className+=" dhx_second_cal_header"),e.set_sizes(),e._init_matrix_tooltip();var a=e._min_date;if(e._timeline_x_scale.call(this,e._els.dhx_cal_header[0]),e.$container.querySelector(".dhx_timeline_scrollable_data")){
var n=e._timeline_smart_render.getViewPort(this.scrollHelper),i=e._timeline_smart_render.getVisibleHeader(this,n);i&&(this.second_scale?e._els.dhx_cal_header[0].children[1].innerHTML=i:e._els.dhx_cal_header[0].children[0].innerHTML=i)}e._timeline_y_scale.call(this,e._els.dhx_cal_data[0]),e._min_date=a;var r=e._getNavDateElement();r&&(r.innerHTML=e.templates[this.name+"_date"](e._min_date,e._max_date)),e._mark_now&&e._mark_now(),e._timeline_reset_scale_height.call(this,t)}
e._timeline_render_scale_header(this,t),e._timeline_hideToolTip()},e._timeline_hideToolTip=function(){e._tooltip&&(e._tooltip.style.display="none",e._tooltip.date="")},e._timeline_showToolTip=function(t,a,n){if("cell"==t.render){var i=a.x+"_"+a.y,r=t._matrix[a.y][a.x];if(!r)return e._timeline_hideToolTip();if(r.sort(function(e,t){return e.start_date>t.start_date?1:-1}),e._tooltip){if(e._tooltip.date==i)return;e._tooltip.innerHTML=""}else{var o=e._tooltip=document.createElement("div")
;o.className="dhx_year_tooltip",e.config.rtl&&(o.className+=" dhx_tooltip_rtl"),document.body.appendChild(o),o.onclick=e._click.dhx_cal_data}for(var _="",d=0;d<r.length;d++){var s=r[d].color?"background-color:"+r[d].color+";":"",l=r[d].textColor?"color:"+r[d].textColor+";":"";_+="<div class='dhx_tooltip_line' event_id='"+r[d].id+"' style='"+s+l+"'>",_+="<div class='dhx_tooltip_date'>"+(r[d]._timed?e.templates.event_date(r[d].start_date):"")+"</div>",
_+="<div class='dhx_event_icon icon_details'>&nbsp;</div>",_+=e.templates[t.name+"_tooltip"](r[d].start_date,r[d].end_date,r[d])+"</div>"}e._tooltip.style.display="",e._tooltip.style.top="0px",e.config.rtl&&n.left-e._tooltip.offsetWidth>=0||document.body.offsetWidth-a.src.offsetWidth-n.left-e._tooltip.offsetWidth<0?e._tooltip.style.left=n.left-e._tooltip.offsetWidth+"px":e._tooltip.style.left=n.left+a.src.offsetWidth+"px",e._tooltip.date=i,e._tooltip.innerHTML=_,
document.body.offsetHeight-n.top-e._tooltip.offsetHeight<0?e._tooltip.style.top=n.top-e._tooltip.offsetHeight+a.src.offsetHeight+"px":e._tooltip.style.top=n.top+"px"}},e._matrix_tooltip_handler=function(t){var a=e.matrix[e._mode];if(a&&"cell"==a.render){if(a){var n=e._locate_cell_timeline(t),t=t||event;t.target||t.srcElement;if(n)return e._timeline_showToolTip(a,n,e.$domHelpers.getOffset(n.src))}e._timeline_hideToolTip()}},e._init_matrix_tooltip=function(){
e._detachDomEvent(e._els.dhx_cal_data[0],"mouseover",e._matrix_tooltip_handler),e.event(e._els.dhx_cal_data[0],"mouseover",e._matrix_tooltip_handler)},e._set_timeline_dates=function(t){e._min_date=e.date[t.name+"_start"](new Date(e._date)),e._max_date=e.date["add_"+t.name+"_private"](e._min_date,t.x_size*t.x_step),e.date[t.x_unit+"_start"]&&(e._max_date=e.date[t.x_unit+"_start"](e._max_date)),e._table_view=!0},e._renderMatrix=function(t,a){a||(e._els.dhx_cal_data[0].scrollTop=0),
e._set_timeline_dates(this),e._timeline_set_full_view.call(this,t)},e._timeline_html_index=function(t){for(var a=t.parentNode.childNodes,n=-1,i=0;i<a.length;i++)if(a[i]==t){n=i;break}var r=n;if(e._ignores_detected)for(var o in e._ignores)e._ignores[o]&&1*o<=r&&r++;return r},e._timeline_locate_hcell=function(t){t=t||event;for(var a=t.target?t.target:t.srcElement;a&&"DIV"!=a.tagName;)a=a.parentNode;if(a&&"DIV"==a.tagName){if("dhx_scale_bar"==e._getClassName(a).split(" ")[0])return{
x:e._timeline_html_index(a),y:-1,src:a,scale:!0}}},e._locate_cell_timeline=function(t){t=t||event;for(var a=t.target?t.target:t.srcElement,n={},i=e.matrix[e._mode],r=e.getActionData(t),o=e._ignores,_=0,d=0;d<i._trace_x.length-1&&!(+r.date<i._trace_x[d+1]);d++)o[d]||_++;n.x=0===_?0:d,n.y=i.order[r.section];var s=(e._isRender("cell"),0);if(i.scrollable&&"cell"===i.render){if(!i._scales[r.section]||!i._scales[r.section].querySelector(".dhx_matrix_cell"))return
;var l=i._scales[r.section].querySelector(".dhx_matrix_cell");if(!l)return;var c=l.offsetLeft;if(c>0){for(var u=e._timeline_drag_date(i,c),g=0;g<i._trace_x.length-1&&!(+u<i._trace_x[g+1]);g++);s=g}}n.src=i._scales[r.section]?i._scales[r.section].querySelectorAll(".dhx_matrix_cell")[d-s]:null;var f=!1,v=h(a,".dhx_matrix_scell");return v&&(a=v,f=!0),f?(n.x=-1,n.src=a,n.scale=!0):n.x=d,n};var x=e._click.dhx_cal_data;e._click.dhx_marked_timespan=e._click.dhx_cal_data=function(t){
var a=x.apply(this,arguments),n=e.matrix[e._mode];if(n){var i=e._locate_cell_timeline(t);i&&(i.scale?e.callEvent("onYScaleClick",[i.y,n.y_unit[i.y],t||event]):(e.callEvent("onCellClick",[i.x,i.y,n._trace_x[i.x],(n._matrix[i.y]||{})[i.x]||[],t||event]),e._timeline_set_scroll_pos(e._els.dhx_cal_data[0],n)))}return a},e.dblclick_dhx_matrix_cell=function(t){var a=e.matrix[e._mode];if(a){var n=e._locate_cell_timeline(t)
;n&&(n.scale?e.callEvent("onYScaleDblClick",[n.y,a.y_unit[n.y],t||event]):e.callEvent("onCellDblClick",[n.x,n.y,a._trace_x[n.x],(a._matrix[n.y]||{})[n.x]||[],t||event]))}};var w=e.dblclick_dhx_marked_timespan||function(){};e.dblclick_dhx_marked_timespan=function(t){return e.matrix[e._mode]?e.dblclick_dhx_matrix_cell(t):w.apply(this,arguments)},e.dblclick_dhx_matrix_scell=function(t){return e.dblclick_dhx_matrix_cell(t)},e._isRender=function(t){
return e.matrix[e._mode]&&e.matrix[e._mode].render==t},e.attachEvent("onCellDblClick",function(t,a,n,i,r){if(!this.config.readonly&&("dblclick"!=r.type||this.config.dblclick_create)){var o=e.matrix[e._mode],_={};_.start_date=o._trace_x[t],_.end_date=o._trace_x[t+1]?o._trace_x[t+1]:e.date.add(o._trace_x[t],o.x_step,o.x_unit),o._start_correction&&(_.start_date=new Date(1*_.start_date+o._start_correction)),o._end_correction&&(_.end_date=new Date(_.end_date-o._end_correction)),
_[o.y_property]=o.y_unit[a].key,e.addEventNow(_,null,r)}}),e.attachEvent("onBeforeDrag",function(t,a,n){return!e._isRender("cell")}),e.attachEvent("onEventChanged",function(e,t){t._timed=this.isOneDayEvent(t)}),e.attachEvent("onBeforeEventChanged",function(e,t,a,n){return e&&(e._move_delta=void 0),n&&(n._move_delta=void 0),!0}),e._is_column_visible=function(t){var a=e.matrix[e._mode],n=e._get_date_index(a,t);return!e._ignores[n]};var k=e._render_marked_timespan
;e._render_marked_timespan=function(t,a,n,i,r){if(!e.config.display_marked_timespans)return[];if(e.matrix&&e.matrix[e._mode]){if(e._isRender("cell"))return;var o=e._lame_copy({},e.matrix[e._mode]);o.round_position=!1;var _=[],d=[],s=[],l=t.sections?t.sections.units||t.sections.timeline:null;if(n)s=[a],d=[n];else{var c=o.order;if(l)c.hasOwnProperty(l)&&(d.push(l),s.push(o._scales[l]));else if(o._scales)for(var h in c)c.hasOwnProperty(h)&&o._scales[h]&&(d.push(h),s.push(o._scales[h]))}
var i=i?new Date(i):e._min_date,r=r?new Date(r):e._max_date;if(i.valueOf()<e._min_date.valueOf()&&(i=new Date(e._min_date)),r.valueOf()>e._max_date.valueOf()&&(r=new Date(e._max_date)),!o._trace_x)return;for(var u=0;u<o._trace_x.length&&!e._is_column_visible(o._trace_x[u]);u++);if(u==o._trace_x.length)return;var g=[];if(t.days>6){var f=new Date(t.days);e.date.date_part(new Date(i))<=+f&&+r>=+f&&g.push(f)}else g.push.apply(g,e._get_dates_by_index(t.days))
;for(var v=t.zones,m=e._get_css_classes_by_config(t),p=0;p<d.length;p++){a=s[p],n=d[p];for(var u=0;u<g.length;u++)for(var y=g[u],b=0;b<v.length;b+=2){var x=v[b],w=v[b+1],D=new Date(+y+60*x*1e3),E=new Date(+y+60*w*1e3);if(D=new Date(D.valueOf()+1e3*(D.getTimezoneOffset()-y.getTimezoneOffset())*60),E=new Date(E.valueOf()+1e3*(E.getTimezoneOffset()-y.getTimezoneOffset())*60),i<E&&r>D){var S=e._get_block_by_config(t);S.className=m;var N=e._timeline_getX({start_date:D},!1,o)-1,M=e._timeline_getX({
start_date:E},!1,o)-1,A=Math.max(1,M-N-1),C=o._section_height[n]-1||o.dy-1;S.style.cssText="height: "+C+"px; "+(e.config.rtl?"right: ":"left: ")+N+"px; width: "+A+"px; top: 0;",a.insertBefore(S,a.firstChild),_.push(S)}}}return _}return k.apply(e,[t,a,n])};var D=e._append_mark_now;e._append_mark_now=function(t,a){if(e.matrix&&e.matrix[e._mode]){var n=e._currentDate(),i=e._get_zone_minutes(n),r={days:+e.date.date_part(n),zones:[i,i+1],css:"dhx_matrix_now_time",type:"dhx_now_time"}
;return e._render_marked_timespan(r)}return D.apply(e,[t,a])};var E=e._mark_timespans;e._mark_timespans=function(){if(e.matrix&&e.matrix[e.getState().mode]){for(var t=[],a=e.matrix[e.getState().mode],n=a.y_unit,i=0;i<n.length;i++){var r=n[i].key,o=a._scales[r],_=e._on_scale_add_marker(o,r);t.push.apply(t,_)}return t}return E.apply(this,arguments)};var S=e._on_scale_add_marker;e._on_scale_add_marker=function(t,a){if(e.matrix&&e.matrix[e._mode]){var n=[],i=e._marked_timespans
;if(i&&e.matrix&&e.matrix[e._mode])for(var r=e._mode,o=e._min_date,_=e._max_date,d=i.global,s=e.date.date_part(new Date(o));s<_;s=e.date.add(s,1,"day")){var l=+s,c=s.getDay(),h=[],u=d[l]||d[c];if(h.push.apply(h,e._get_configs_to_render(u)),i[r]&&i[r][a]){var g=[],f=e._get_types_to_render(i[r][a][c],i[r][a][l]);g.push.apply(g,e._get_configs_to_render(f)),g.length&&(h=g)}for(var v=0;v<h.length;v++){var m=h[v],p=m.days;p<7?(p=l,
n.push.apply(n,e._render_marked_timespan(m,t,a,s,e.date.add(s,1,"day"))),p=c):n.push.apply(n,e._render_marked_timespan(m,t,a,s,e.date.add(s,1,"day")))}}return n}return S.apply(this,arguments)},e._resolve_timeline_section=function(e,t){var a=0,n=0;for(a;a<this._colsS.heights.length&&!((n+=this._colsS.heights[a])>t.y);a++);e.y_unit[a]||(a=e.y_unit.length-1),this._drag_event&&!this._drag_event._orig_section&&(this._drag_event._orig_section=e.y_unit[a].key),t.fields={},
a>=0&&e.y_unit[a]&&(t.section=t.fields[e.y_property]=e.y_unit[a].key)},e._update_timeline_section=function(e){var t=e.view,a=e.event,n=e.pos;if(a){if(a[t.y_property]!=n.section){var i=this._get_timeline_event_height(a,t);a._sorder=this._get_dnd_order(a._sorder,i,t._section_height[n.section])}a[t.y_property]=n.section}},e._get_date_index=function(e,t){for(var a=e._trace_x,n=0,i=a.length-1,r=t.valueOf();i-n>3;){var o=n+Math.floor((i-n)/2);a[o].valueOf()>r?i=o:n=o}for(var _=n;_<=i&&+t>=+a[_+1];)_++
;return _},e._timeline_drag_date=function(t,a){var n=t,i={x:a};if(!n._trace_x.length)return new Date(e.getState().date);var r,o,_=0,d=0;for(d;d<=this._cols.length-1;d++)if(o=this._cols[d],(_+=o)>i.x){r=(i.x-(_-o))/o,r=r<0?0:r;break}if(n.round_position){var s=1,l=e.getState().drag_mode;l&&"move"!=l&&"create"!=l&&(s=.5),r>=s&&d++,r=0}if(0===d&&this._ignores[0])for(d=1,r=0;this._ignores[d];)d++;else if(d==this._cols.length&&this._ignores[d-1]){for(d=this._cols.length-1,r=0;this._ignores[d];)d--;d++
}var c;if(d>=n._trace_x.length)c=e.date.add(n._trace_x[n._trace_x.length-1],n.x_step,n.x_unit),n._end_correction&&(c=new Date(c-n._end_correction));else{var h=r*o*n._step+n._start_correction;c=new Date(+n._trace_x[d]+h)}return c},e.attachEvent("onBeforeTodayDisplayed",function(){for(var t in e.matrix){var a=e.matrix[t];a.x_start=a._original_x_start}return!0}),e.attachEvent("onOptionsLoad",function(){for(var t in e.matrix){var a=e.matrix[t];a.order={},e.callEvent("onOptionsLoadStart",[])
;for(var t=0;t<a.y_unit.length;t++)a.order[a.y_unit[t].key]=t;e.callEvent("onOptionsLoadFinal",[]),e._date&&a.name==e._mode&&(a._options_changed=!0,e.setCurrentView(e._date,e._mode),setTimeout(function(){a._options_changed=!1}))}}),e.attachEvent("onEventIdChange",function(){var t=e.getView();t&&e.matrix[t.name]&&e._timeline_smart_render&&(e._timeline_smart_render.clearPreparedEventsCache(),e._timeline_smart_render.getPreparedEvents(t))}),e.attachEvent("onBeforeDrag",function(t,a,n){
if("resize"==a){var i=n.target||n.srcElement;e._getClassName(i).indexOf("dhx_event_resize_end")<0?e._drag_from_start=!0:e._drag_from_start=!1}return!0});var N=10,M=null,A=null,C=e.attachEvent("onSchedulerReady",function(){e.matrix&&(e.event(document.body,"mousemove",g),e.detachEvent(C))});e._timeline_smart_render={_prepared_events_cache:null,_rendered_events_cache:[],_rendered_header_cache:[],_rendered_labels_cache:[],_rows_to_delete:[],_rows_to_add:[],_cols_to_delete:[],_cols_to_add:[],
getViewPort:function(t,a,n,i){var r=e.$container.querySelector(".dhx_cal_data"),o=r.getBoundingClientRect(),_=e.$container.querySelector(".dhx_timeline_scrollable_data");_&&void 0===n&&(n=t.getScrollValue(_)),void 0===i&&(i=_?_.scrollTop:r.scrollTop);var d={};for(var s in o)d[s]=o[s];return d.scrollLeft=n||0,d.scrollTop=i||0,a&&(o.height=a),d},isInXViewPort:function(e,t){var a=t.scrollLeft,n=t.width+t.scrollLeft;return e.left<n+100&&e.right>a-100},isInYViewPort:function(e,t){
var a=t.scrollTop,n=t.height+t.scrollTop;return e.top<n+100&&e.bottom>a-100},getVisibleHeader:function(t,a){var n="";this._rendered_header_cache=[];for(var i in t._h_cols){var r=t._h_cols[i];if(this.isInXViewPort({left:r.left,right:r.left+e._cols[i]},a)){n+=r.div.outerHTML,this._rendered_header_cache.push(r.div.getAttribute("data-col-id"))}}return n},updateHeader:function(t,a,n){this._cols_to_delete=[],this._cols_to_add=[]
;for(var i=e.$container.querySelectorAll(".dhx_cal_header > div"),r=i[i.length-1].querySelectorAll(".dhx_scale_bar"),o=[],_=0;_<r.length;_++)o.push(r[_].getAttribute("data-col-id"));if(this.getVisibleHeader(t,a)){for(var d=this._rendered_header_cache.slice(),s=[],_=0,l=o.length;_<l;_++){var c=d.indexOf(o[_]);c>-1?d.splice(c,1):s.push(o[_])}s.length&&(this._cols_to_delete=s.slice(),this._deleteHeaderCells(s,t,n)),d.length&&(this._cols_to_add=d.slice(),this._addHeaderCells(d,t,n))}},
_deleteHeaderCells:function(e,t,a){for(var n=0;n<e.length;n++){var i=a.querySelector('[data-col-id="'+e[n]+'"]');i&&a.removeChild(i)}},_addHeaderCells:function(e,t,a){for(var n="",i=0;i<e.length;i++)n+=t._h_cols[e[i]].div.outerHTML;a.insertAdjacentHTML("beforeEnd",n)},getVisibleLabels:function(e,t){if(e._label_rows.length){var a="";this._rendered_labels_cache=[];for(var n=0;n<e._label_rows.length;n++)if(this.isInYViewPort({top:e._label_rows[n].top,
bottom:e._label_rows[n].top+e._section_height[e.y_unit[n].key]},t)){var i=e._label_rows[n].div;a+=i,this._rendered_labels_cache.push(n)}return a}},updateLabels:function(e,t,a){this._rows_to_delete=[],this._rows_to_add=[];var n=this._rendered_labels_cache.slice();if(n.length||(this.getVisibleLabels(e,t),n=this._rendered_labels_cache.slice()),this.getVisibleLabels(e,t)){for(var i=this._rendered_labels_cache.slice(),r=[],o=0,_=n.length;o<_;o++){var d=i.indexOf(n[o]);d>-1?i.splice(d,1):r.push(n[o])}
r.length&&(this._rows_to_delete=r.slice(),this._deleteLabelCells(r,e,a)),i.length&&(this._rows_to_add=i.slice(),this._addLabelCells(i,e,a))}},_deleteLabelCells:function(e,t,a){for(var n=0;n<e.length;n++){var i=a.querySelector('[data-row-index="'+e[n]+'"]');i&&a.removeChild(i)}},_addLabelCells:function(e,t,a){for(var n="",i=0;i<e.length;i++)n+=t._label_rows[e[i]].div;a.insertAdjacentHTML("beforeEnd",n)},clearPreparedEventsCache:function(){this.cachePreparedEvents(null)},
cachePreparedEvents:function(e){this._prepared_events_cache=e,this._prepared_events_coordinate_cache=e},getPreparedEvents:function(t){var a;return this._prepared_events_cache?a=this._prepared_events_cache:(a=e._prepare_timeline_events(t),a.$coordinates={},this.cachePreparedEvents(a)),a},updateEvents:function(t,a){var n=this.getPreparedEvents(t),i=this._rendered_events_cache.slice();this._rendered_events_cache=[];var r=e.$container.querySelector(".dhx_cal_data .dhx_timeline_data_col");if(r){
for(var o=0;o<this._rendered_labels_cache.length;o++){var _=this._rendered_labels_cache[o],d=[],s=i[_]?i[_].slice():[];e._timeline_calculate_event_positions.call(t,n[_]);for(var l=e._timeline_smart_render.getVisibleEventsForRow(t,a,n,_),c=0,h=l.length;c<h;c++){var u=s.indexOf(l[c].id);u>-1?s.splice(u,1):d.push(l[c])}var g=r.querySelector('[data-section-index="'+_+'"]');s.length&&this._deleteEvents(s,t,g),d.length&&this._addEvents(d,t,g,_)}e._populate_timeline_rendered(e.$container),t._matrix=n}
},_deleteEvents:function(e,t,a){for(var n=0;n<e.length;n++){var i=a.querySelector('[event_id="'+e[n]+'"]');i&&(i.classList.contains("dhx_in_move")||a.removeChild(i))}},_addEvents:function(t,a,n,i){var r=e._timeline_update_events_html.call(a,t);n.insertAdjacentHTML("beforeEnd",r)},getVisibleEventsForRow:function(t,a,n,i){var r=[];if("cell"==t.render)r=n;else{var o=n[i];if(o)for(var _=0,d=o.length;_<d;_++){var s,l,c=o[_],h=i+"_"+c.id;n.$coordinates&&n.$coordinates[h]?(s=n.$coordinates[h].xStart,
l=n.$coordinates[h].xEnd):(s=e._timeline_getX(c,!1,t),l=e._timeline_getX(c,!0,t),n.$coordinates&&(n.$coordinates[h]={xStart:s,xEnd:l})),e._timeline_smart_render.isInXViewPort({left:s,right:l},a)&&(r.push(c),this._rendered_events_cache[i]||(this._rendered_events_cache[i]=[]),this._rendered_events_cache[i].push(c.id))}}return r},getVisibleRowCellsHTML:function(t,a,n,i,r){for(var o,_="",d=this._rendered_header_cache,s=0;s<d.length;s++){var l=d[s];o=t._h_cols[l].left-t.dx,
e._ignores[l]?"cell"==t.render?_+=e._timeline_get_html_for_cell_ignores(n):_+=e._timeline_get_html_for_bar_ignores():"cell"==t.render?_+=e._timeline_get_html_for_cell(l,r,t,i[r][l],n,o):_+=e._timeline_get_html_for_bar(l,r,t,i[r],o)}return _},getVisibleTimelineRowsHTML:function(t,a,n,i){var r="",o=e._timeline_get_cur_row_stats(t,i);o=e._timeline_get_fit_events_stats(t,i,o);var _=t._label_rows[i],d=e.templates[t.name+"_row_class"],s={view:t,section:_.section,template:d}
;return"cell"==t.render?(r+=e._timeline_get_html_for_cell_data_row(i,o,_.top,_.section.key,s),r+=this.getVisibleRowCellsHTML(t,a,o,n,i),r+="</div>"):(r+=e._timeline_get_html_for_bar_matrix_line(i,o,_.top,_.section.key,s),r+=e._timeline_get_html_for_bar_data_row(o,s),r+=this.getVisibleRowCellsHTML(t,a,o,n,i),r+="</div></div>"),r},updateGridRows:function(e,t){this._rows_to_delete.length&&this._deleteGridRows(this._rows_to_delete),this._rows_to_add.length&&this._addGridRows(this._rows_to_add,e,t)},
_deleteGridRows:function(t){var a=e.$container.querySelector(".dhx_cal_data .dhx_timeline_data_col");if(a){for(var n=0;n<t.length;n++){var i=a.querySelector('[data-section-index="'+t[n]+'"]');a.removeChild(i)}this._rows_to_delete=[]}},_addGridRows:function(t,a,n){var i=e.$container.querySelector(".dhx_cal_data .dhx_timeline_data_col");if(i){for(var r=this.getPreparedEvents(a),o="",_=0;_<t.length;_++)o+=this.getVisibleTimelineRowsHTML(a,n,r,t[_]);i.insertAdjacentHTML("beforeEnd",o)
;for(var _=0;_<t.length;_++)e._timeline_finalize_section_add(a,a.y_unit[t[_]].key,i);e._mark_now&&e._mark_now(),this._rows_to_add=[]}},updateGridCols:function(t,a){for(var n=this._rendered_header_cache,i={},r={},o=0;o<n.length;o++)r[n[o]]=!0;var _=e.$container.querySelector(".dhx_timeline_data_row");if(_)for(var d=_.querySelectorAll("[data-col-id]"),o=0;o<d.length;o++)i[d[o].getAttribute("data-col-id")]=!0;var s=[],l=[];for(var o in i)r[o]||s.push(o);for(var o in r)i[o]||l.push(o)
;s.length&&this._deleteGridCols(s,t),l.length&&this._addGridCols(l,t,a)},_deleteGridCols:function(t,a){var n=e.$container.querySelector(".dhx_cal_data .dhx_timeline_data_col");if(n){for(var i=0;i<this._rendered_labels_cache.length;i++){var r;if(r="cell"==a.render?n.querySelector('[data-section-index="'+this._rendered_labels_cache[i]+'"]'):n.querySelector('[data-section-index="'+this._rendered_labels_cache[i]+'"] .dhx_timeline_data_row '))for(var o=0;o<t.length;o++){
var _=r.querySelector('[data-col-id="'+t[o]+'"]');_&&r.removeChild(_)}}this._cols_to_delete=[]}},_addGridCols:function(t,a,n){var i=e.$container.querySelector(".dhx_cal_data .dhx_timeline_data_col");if(i){for(var r=this.getPreparedEvents(a),o=0;o<this._rendered_labels_cache.length;o++){var _=this._rendered_labels_cache[o],d="",s=e._timeline_get_cur_row_stats(a,_);s=e._timeline_get_fit_events_stats(a,_,s);var l
;if(l="cell"==a.render?i.querySelector('[data-section-index="'+_+'"]'):i.querySelector('[data-section-index="'+_+'"] .dhx_timeline_data_row')){for(var c=0;c<t.length;c++){if(!l.querySelector('[data-col-id="'+t[c]+'"]')){var h=this.getVisibleGridCell(a,n,s,r,_,t[c]);h&&(d+=h)}}l.insertAdjacentHTML("beforeEnd",d)}}this._cols_to_add=[]}},getVisibleGridCell:function(t,a,n,i,r,o){if(t._h_cols[o]){var _="",d=t._h_cols[o].left-t.dx
;return"cell"==t.render?e._ignores[o]||(_+=e._timeline_get_html_for_cell(o,r,t,i[r][o],n,d)):e._ignores[o]||(_+=e._timeline_get_html_for_bar(o,r,t,i[r],d)),_}}}},e._temp_matrix_scope()});

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./barre-navigation/barre-navigation.component */ "WHZN");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");



class AppComponent {
    constructor() {
        this.title = 'angular-gesrion-des-salles';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 2, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-barre-navigation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
    } }, directives: [_barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_1__["BarreNavigationComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], encapsulation: 2 });


/***/ }),

/***/ "T/nz":
/*!****************************************************************!*\
  !*** ./src/app/corps-mon-profil/corps-mon-profil.component.ts ***!
  \****************************************************************/
/*! exports provided: CorpsMonProfilComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorpsMonProfilComponent", function() { return CorpsMonProfilComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _service_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../service/user.service */ "/7iW");
/* harmony import */ var src_service_batiment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/service/batiment.service */ "e3aT");
/* harmony import */ var src_service_salle_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/service/salle.service */ "3ZE1");
/* harmony import */ var src_service_typeEquipement_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/service/typeEquipement.service */ "Tolt");
/* harmony import */ var src_service_role_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/service/role.service */ "2kyN");
/* harmony import */ var src_service_etat_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/service/etat_user.service */ "DUjH");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");









function CorpsMonProfilComponent_div_61_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "input", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_div_61_Template_input_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const user_r8 = ctx.$implicit; const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.onOpenModal(user_r8, "editUser"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_div_61_Template_input_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r10); const user_r8 = ctx.$implicit; const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.onOpenModal(user_r8, "delateUser"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const user_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", user_r8.nomUser, " ", user_r8.prenomUser, "");
} }
function CorpsMonProfilComponent_option_106_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const role_r12 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngValue", role_r12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", role_r12.nom_role, "");
} }
function CorpsMonProfilComponent_div_118_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "input", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_div_118_Template_input_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const batiment_r13 = ctx.$implicit; const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r14.onOpenModal(batiment_r13, "editBatiment"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_div_118_Template_input_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const batiment_r13 = ctx.$implicit; const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r16.onOpenModal(batiment_r13, "deleteBatiment"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const batiment_r13 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", batiment_r13.nomBatiment, " ");
} }
function CorpsMonProfilComponent_div_194_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "input", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const salle_r17 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", salle_r17.nomsalle, " ");
} }
function CorpsMonProfilComponent_div_265_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "label", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "input", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const equipement_r18 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", equipement_r18.nomtypeequipement, " ");
} }
class CorpsMonProfilComponent {
    constructor(userService, batimentService, salleService, typeEquipementService, roleService, etatUserService) {
        this.userService = userService;
        this.batimentService = batimentService;
        this.salleService = salleService;
        this.typeEquipementService = typeEquipementService;
        this.roleService = roleService;
        this.etatUserService = etatUserService;
    }
    ngOnInit() {
        //Instructions pour activer le déroulement des accordéons dès le chargement de la page
        const accordionAllHeader = document.querySelectorAll(".accordion-header");
        const accordionAllBody = document.querySelectorAll(".accordion-body");
        accordionAllHeader.forEach(btn => {
            btn.addEventListener('click', () => {
                const panel = btn.nextElementSibling;
                panel.classList.toggle("active");
                btn.classList.toggle("active");
            });
        });
        //Fin des instruction pour activer le déroulement des accordéons
        //Récupérer les informations propres au profil connecté
        this.infoProfil();
        //En fonction du rôle adapté la mise en page
        //this.miseenpage();
        this.getAllUsers();
        this.getAllBatiment();
        this.getAllSalles();
        this.getAllEquipements();
        this.getAllRoles();
        this.getAllEtatUser();
    } //Fin méthode init()
    /****************************************METHODES PROPRES AUX SERVICES************************************ */
    // Méthode pour récupérer tous les bâtiments via le service bâtiment 
    getAllBatiment() {
        this.batimentService.getAllBatiment().subscribe((response) => {
            this.batiments = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    /**
     * getAllRoles()
     */
    getAllRoles() {
        this.roleService.getAllRole().subscribe((response) => {
            this.roles = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    // Méthode pour récupérer tous les salles via le service salle
    getAllSalles() {
        this.salleService.getAllSalles().subscribe((response) => {
            this.salles = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    // Méthode pour récupérer tous les user via le service user
    getAllUsers() {
        this.userService.getAllUser().subscribe((response) => {
            this.users = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    // Méthode pour récupérer tous les équipements via le service équipement
    getAllEquipements() {
        this.typeEquipementService.getAllTypeEquipement().subscribe((response) => {
            this.equipements = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    /**
     * getAllEtatUser
      : void
      */
    getAllEtatUser() {
        this.etatUserService.getAllEtatUser().subscribe((response) => {
            this.etatUsers = response;
        }, (error) => {
            alert(error.message);
        });
    }
    /**
     * Méthode qui ajoute un batiment via le formulaire du front
     * @param addForm formulaire coté front
     */
    //Méthode pour l'ajout d'un Batiment dans la base de données
    onAddBatiment(addForm) {
        document.getElementById('add-batiment-form').click();
        this.batimentService.addOneBatiment(addForm.value).subscribe((response) => {
            console.log(response);
            this.getAllBatiment();
            addForm.reset();
        }, (error) => {
            alert(error.message);
            addForm.reset();
        });
    }
    //Méthode pour la modification d'un Batiment dans la base de données
    onUpdateBatiment(batiment) {
        this.batimentService.updateOneBatiment(batiment).subscribe((response) => {
            console.log(response);
            this.getAllBatiment();
        }, (error) => {
            alert(error.message);
        });
    }
    //Méthode pour la suppression d'un Batiment dans la base de données
    onDeleteBatiment(id) {
        this.batimentService.deleteOneBatiment(id).subscribe((response) => {
            console.log(response);
            this.getAllBatiment();
        }, (error) => {
            alert(error.message);
        });
    }
    infoProfil() {
        this.userService.infoMonProfil().subscribe((response) => {
            this.profil = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    /****************************************METHODES PROPRES AUX POPUPS************************************ */
    miseenpage() {
        var indicateur = document.getElementById('role').innerHTML;
        console.log('dans la mise en page');
        if (indicateur = "administrateur") {
            document.getElementById('gestion-appli').remove();
            //toucher l'élément css de #trait : document.
            console.log("ok supprimer");
        }
    }
    onAddUser(addForm) {
        console.log("TEST " + addForm.value);
        document.getElementById('add-user-form').click();
        this.userService.addOneUser(addForm.value).subscribe((response) => {
            console.log(response);
            this.getAllUsers();
            addForm.reset();
        }, (error) => {
            alert(error.message);
            addForm.reset();
        });
    }
    //Switch entre les differents modals
    onOpenModal(any, mode) {
        const container = document.getElementById('container');
        const button = document.createElement('button');
        button.type = 'button';
        button.style.display = 'none';
        button.setAttribute('data-toggle', 'modal');
        switch (mode) {
            //BATIMENT
            case 'addBatiment':
                button.setAttribute('data-target', '#addBatimentModal');
                break;
            case 'editBatiment':
                this.editBatiment = any;
                button.setAttribute('data-target', '#updateBatimentModal');
                break;
            case 'deleteBatiment':
                this.deleteBatiment = any;
                button.setAttribute('data-target', '#deleteBatimentModal');
                break;
            //USER
            case 'addUser':
                button.setAttribute('data-target', '#addUserModal');
                break;
            case 'editUser':
                // this.editBatiment = any;
                button.setAttribute('data-target', '#updateUserModal');
                break;
            case 'deleteBatiment':
                //     this.deleteBatiment = any;
                button.setAttribute('data-target', '#deleteUserModal');
                break;
            //SALLES
            case 'addSalle':
                button.setAttribute('data-target', '#addSalleModal');
                break;
            case 'editBatiment':
                //  this.editBatiment = any;
                button.setAttribute('data-target', '#updateSalleModal');
                break;
            case 'deleteBatiment':
                //   this.deleteBatiment = any;
                button.setAttribute('data-target', '#deleteSalleModal');
                break;
            //EQUIPEMENTS
            case 'addEquipement':
                button.setAttribute('data-target', '#addEquipementModal');
                break;
            case 'editBatiment':
                //  this.editBatiment = any;
                button.setAttribute('data-target', '#updateEquipementModal');
                break;
            case 'deleteBatiment':
                //  this.deleteBatiment = any;
                button.setAttribute('data-target', '#deleteEquipementModal');
                break;
            default:
                break;
        }
        container.appendChild(button);
        button.click();
    }
}
CorpsMonProfilComponent.ɵfac = function CorpsMonProfilComponent_Factory(t) { return new (t || CorpsMonProfilComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_batiment_service__WEBPACK_IMPORTED_MODULE_2__["BatimentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_salle_service__WEBPACK_IMPORTED_MODULE_3__["SalleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_typeEquipement_service__WEBPACK_IMPORTED_MODULE_4__["TypeEquipementService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_etat_user_service__WEBPACK_IMPORTED_MODULE_6__["EtatUserService"])); };
CorpsMonProfilComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CorpsMonProfilComponent, selectors: [["app-corps-mon-profil"]], decls: 349, vars: 22, consts: [["rel", "stylesheet", "href", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"], ["id", "container", 1, "container", "row", "mx-auto", "mt-5"], ["id", "gestion-compte", 1, "col-lg-5", "col-sm-12", "order-1", "col-lg-offset-3"], ["action", "/user/updateuser", "method", "POST"], ["id", "trait", 1, "col-sm-12", "col-lg-12"], ["for", "matriculeuser", 1, "col-lg-5"], ["name", "matriculeuser", "type", "text", 3, "value"], ["for", "nomuser", 1, "col-lg-5"], ["name", "nomuser", "type", "text", 3, "value"], ["for", "prenomuser", 1, "col-lg-5"], ["name", "prenomuser", "type", "text", 3, "value"], [1, "col-lg-5"], ["type", "text", "id", "role", 3, "value"], ["for", "telephoneuser", 1, "col-lg-5"], ["name", "telephoneuser", "type", "text", 3, "value"], ["for", "emailuser", 1, "col-lg-5"], ["name", "emailuser", "type", "text", 3, "value"], ["type", "password", "name", "password", 3, "value"], ["type", "password"], ["type", "submit", "value", "Modifier", 1, "col-lg-3"], ["id", "gestion-appli", 1, "col-lg-7", "col-sm-12", "order-2"], ["id", "accordion", 1, "col-lg-12", "col-sm-12"], [1, "accordion-item"], [1, "accordion-header"], [1, "fas", "fa-angle-down"], [1, "accordion-bodyy"], ["class", "row", 4, "ngFor", "ngForOf"], [3, "click"], ["id", "addUserModal", "tabindex", "-1", "role", "dialog", "aria-labelledby", "addUserModalLabel", "aria-hidden", "true", 1, "modal", "fade"], ["role", "document", 1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], ["id", "addUserModalLabel", 1, "modal-title"], ["type", "button", "data-dismiss", "modal", "aria-label", "Close", 1, "close"], ["aria-hidden", "true"], [1, "modal-body"], [3, "ngSubmit"], ["addForm", "ngForm"], [1, "form-group"], ["for", "nomUser"], ["type", "text", "ngModel", "", "name", "prenomUser", "id", "prenomUser", "required", "", 1, "form-control"], ["type", "text", "ngModel", "", "name", "nomUser", "id", "nomUser", "required", "", 1, "form-control"], ["type", "password", "ngModel", "", "name", "password", "id", "password", "placeholder", "Mot de passe", "required", "", 1, "form-control"], ["for", "matriculeUser"], ["type", "text", "ngModel", "", "name", "matriculeUser", "id", "matriculeuser", "placeholder", "Matricule", "required", "", 1, "form-control"], ["for", "emailUser"], ["type", "email", "ngModel", "", "name", "emailUser", "id", "email", "placeholder", "exemple@mail.com", "required", "", 1, "form-control"], ["for", "telephoneUser"], ["type", "tel", "ngModel", "", "name", "telephoneUser", "id", "telephoneUser", "placeholder", "0635410...", "required", "", 1, "form-control"], ["for", "roleUser"], ["ngModel", "", "name", "roleUser", "id", "roleUser", "placeholder", "Role", "required", "", 1, "form-control"], [3, "ngValue", 4, "ngFor", "ngForOf"], ["type", "hidden", "ngModel", "", "name", "mdpAChanger", "id", "mdpAChanger", "value", "true", 1, "form-control"], [1, "modal-footer"], ["type", "button", "id", "add-user-form", "data-dismiss", "modal", 1, "btn", "btn-secondary"], ["type", "submit", 1, "btn", "btn-primary", 3, "disabled"], ["id", "addBatimentModal", "tabindex", "-1", "role", "dialog", "aria-labelledby", "addBatimentModalLabel", "aria-hidden", "true", 1, "modal", "fade"], ["id", "addEmployeeModalLabel", 1, "modal-title"], ["for", "nom_batiment"], ["type", "text", "ngModel", "", "name", "nomBatiment", "id", "name", "placeholder", "Nom", "required", "", 1, "form-control"], ["for", "adresseBatiment"], ["type", "text", "ngModel", "", "name", "adresseBatiment", "id", "email", "placeholder", "Adresse", "required", "", 1, "form-control"], ["type", "button", "id", "add-batiment-form", "data-dismiss", "modal", 1, "btn", "btn-secondary"], ["id", "updateBatimentModal", "tabindex", "-1", "role", "dialog", "aria-labelledby", "updateBatimentModalLabel", "aria-hidden", "true", 1, "modal", "fade"], ["id", "updateBatimentModalLabel", 1, "modal-title"], ["editForm", "ngForm"], ["type", "hidden", "name", "idBatiment", "id", "idBatiment", 1, "form-control", 3, "ngModel"], ["for", "name"], ["type", "text", "name", "nomBatiment", "id", "name", "aria-describedby", "emailHelp", 1, "form-control", 3, "ngModel"], ["type", "text", "name", "adresse_batiment", "id", "adresse_batiment", "placeholder", "", 1, "form-control", 3, "ngModel"], ["type", "button", "id", "", "data-dismiss", "modal", 1, "btn", "btn-secondary"], ["data-dismiss", "modal", 1, "btn", "btn-primary", 3, "click"], ["id", "deleteBatimentModal", "tabindex", "-1", "role", "dialog", "aria-labelledby", "deleteModelLabel", "aria-hidden", "true", 1, "modal", "fade"], ["id", "deleteModelLabel", 1, "modal-title"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-secondary"], ["data-dismiss", "modal", 1, "btn", "btn-danger", 3, "click"], ["id", "addsalle", 1, "jw-modal-wrapper", "col-lg-12", "col-sm-12"], [1, "jw-modal"], [1, "jw-modal-body"], ["action", "/restSalle/salle", "method", "POST"], ["for", "nomsalle"], ["type", "text", "name", "nomsalle"], ["for", "capacitesalle"], ["type", "number", "name", "capacitesalle"], ["for", "etagesalle"], ["type", "number", "name", "etagesalle"], ["for", "descriptionsalle"], ["type", "text", "name", "descriptionsalle"], ["for", "idbatiment"], ["name", "idbatiment"], ["value", ""], ["value", "administrateur"], ["value", "utilisateur"], ["for", "idtypesalle"], ["name", " idtypesalle"], ["for", "nomtypeequipement"], ["name", " nomtypeequipement", "multiple", ""], ["type", "submit", "value", "Envoyer", 1, "col-lg-3"], ["type", "submit", "value", "Annuler", 1, "col-lg-3"], [1, "jw-modal-background"], ["id", "addequipement", 1, "jw-modal-wrapper", "col-lg-12", "col-sm-12"], ["action", "/restequip/equipement", "method", "POST"], ["type", "text", "name", "nomtypeequipement"], ["id", "param", 1, "jw-modal-wrapper", "col-lg-12", "col-sm-12"], ["action", "/parametre/update", "method", "POST"], ["for", "logo"], ["type", "file", "name", "logo"], ["for", "idlangue"], ["name", " nomtypeequipement"], ["for", "debplagejournaliere"], ["type", "time", "name", "debplagejournaliere"], ["for", "finplagejournaliere"], ["type", "time", "name", "finplagejournaliere"], ["for", "granularite"], ["type", "time", "name", "granularite"], ["for", "server"], ["type", "text", "name", "server"], ["for", "port"], ["type", "numeric", "name", "port"], ["for", "loginserver"], ["type", "text", "name", "loginserver"], ["for", "motdepasseserver"], ["type", "password", "name", "motdepasseserver"], [1, "row"], [1, "col-lg-8"], ["type", "button", "value", "Valider"], ["type", "button", "value", "Refuser"], [1, "col-lg-6"], ["type", "button", "value", "D\u00E9sactiver"], ["type", "button", "value", "Modifier", 3, "click"], ["type", "button", "value", "Supprimer", 3, "click"], [3, "ngValue"], ["type", "button", "value", "Verrouiller"], ["type", "button", "value", "Modifier"], ["type", "button", "value", "Supprimer"]], template: function CorpsMonProfilComponent_Template(rf, ctx) { if (rf & 1) {
        const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Gestion du compte");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Matricule :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Nom :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "label", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Pr\u00E9nom :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Fonction :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "T\u00E9l\u00E9phone :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Mail :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Ancien mot de passe :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Nouveau mot de passe :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Confirmer le mot de passe :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Gestion de l'application");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, " Gestion des utilisateurs ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](61, CorpsMonProfilComponent_div_61_Template, 7, 2, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_Template_button_click_62_listener() { return ctx.onOpenModal(null, "addUser"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, " Ajouter ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "h5", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Ajouter un utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "form", 36, 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function CorpsMonProfilComponent_Template_form_ngSubmit_74_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](75); return ctx.onAddUser(_r1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](77, "json");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "label", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Pr\u00E9nom");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "input", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "label", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Nom ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "input", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "label", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Mot de passe ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](89, "input", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "label", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Matricule");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "input", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "label", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "input", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "label", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Telephone");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "input", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "label", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Role");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "select", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](106, CorpsMonProfilComponent_option_106_Template, 2, 2, "option", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "input", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "button", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Fermer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Ajouter");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, " Gestion des b\u00E2timents ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](118, CorpsMonProfilComponent_div_118_Template, 7, 1, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_Template_button_click_119_listener() { return ctx.onOpenModal(null, "addBatiment"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Ajouter");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "h5", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Ajouter un batiment");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "form", 36, 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function CorpsMonProfilComponent_Template_form_ngSubmit_131_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](75); return ctx.onAddBatiment(_r1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "label", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Nom du Batiment");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](136, "input", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "label", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "Adresse du batiment");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](140, "input", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "button", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, "Fermer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](145, "Ajouter");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "h5", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "form", null, 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](158, "input", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "label", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "input", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "label", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, "Adresse du batiment");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](166, "input", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "button", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "Close");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "button", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_Template_button_click_170_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](157); return ctx.onUpdateBatiment(_r5.value); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "Save changes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "h5", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "Supprimer Batiment");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "span", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "button", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, "No");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "button", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMonProfilComponent_Template_button_click_187_listener() { return ctx.onDeleteBatiment(ctx.deleteBatiment == null ? null : ctx.deleteBatiment.idBatiment); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "Yes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, " Gestion des salles ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](194, CorpsMonProfilComponent_div_194_Template, 7, 1, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "button");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](196, " Ajouter ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "div", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](198, "div", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "Cr\u00E9er une nouvelle salle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "form", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "label", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](205, "Nom de la salle:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](206, "input", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](207, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "label", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, "Capacit\u00E9 d'accueil (en personnes) :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](210, "input", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](211, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "label", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, "Etage :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "input", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](215, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](216, "label", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, "Description :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](218, "input", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](219, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "label", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, "Dans le b\u00E2timent : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](222, "select", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](223, "option", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](224, "--Please choose an batiment--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](225, "option", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](226, "administrateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](229, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "label", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, "Type de salle: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "select", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](233, "option", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, "--Please choose an type--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "option", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, "administrateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](238, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](239, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](240, "label", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](241, "Type de salle: ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](242, "select", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](243, "option", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](244, "--Please choose an equipement--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](245, "option", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](246, "administrateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](248, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](249, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](250, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](251, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](252, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "option", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](254, "utilisateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](255, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](256, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](257, "input", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](258, "input", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](259, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](260, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](261, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](262, " Gestion des \u00E9quipements ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](263, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](265, CorpsMonProfilComponent_div_265_Template, 6, 1, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](266, "button");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](267, " Ajouter ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "div", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](269, "div", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](270, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "Cr\u00E9er un nouveau \u00E9quipement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](273, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](274, "form", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](275, "label", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](276, "Nom de l'\u00E9quipement:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](277, "input", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](278, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](279, "input", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](280, "input", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](281, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](282, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](284, " Param\u00E8tres ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](285, "div", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](286, "div", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](287, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](288, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](289, "Param\u00E8tres");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](290, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](291, "form", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](292, "label", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](293, "Logo:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](294, "input", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](295, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](296, "label", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](297, "Langue:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](298, "select", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](299, "option", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](300, "--Please choose an equipement--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](301, "option", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](302, "administrateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](303, "option", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](304, "administrateur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](305, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](306, "label", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](307, "Heure de d\u00E9but:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](308, "input", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](309, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](310, "label", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](311, "Heure de fin:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](312, "input", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](313, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](314, "label", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](315, "Granularite:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](316, "input", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](317, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](318, "label", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](319, "Adresse de server:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](320, "input", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](321, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](322, "label", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](323, "Port de server:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](324, "input", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](325, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](326, "label", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](327, "Login du server:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](328, "input", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](329, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](330, "label", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](331, "Mot de passe du server:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](332, "input", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](333, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](334, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](335, "input", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](336, "input", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](337, "div", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](338, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](339, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](340, " Actions en attente ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](341, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](342, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](343, "div", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](344, "form");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](345, "label", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](346, "Action A ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](347, "input", 125);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](348, "input", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.matriculeUser);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.nomUser);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.prenomUser);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.idRole.nom_role);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.telephoneUser);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.emailUser);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("value", ctx.profil == null ? null : ctx.profil.password);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.users);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](77, 20, _r1.value), " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.roles);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r1.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.batiments);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r1.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Modifier Batiment ", ctx.editBatiment == null ? null : ctx.editBatiment.nomBatiment, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("ngModel", ctx.editBatiment == null ? null : ctx.editBatiment.idBatiment);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("ngModel", ctx.editBatiment == null ? null : ctx.editBatiment.nomBatiment);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("ngModel", ctx.editBatiment == null ? null : ctx.editBatiment.adresse_batiment);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\u00CAtes-vous sur de supprimer ", ctx.deleteBatiment == null ? null : ctx.deleteBatiment.nomBatiment, "?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.salles);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.equipements);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_z"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_8__["JsonPipe"]], encapsulation: 2 });


/***/ }),

/***/ "Tolt":
/*!***********************************************!*\
  !*** ./src/service/typeEquipement.service.ts ***!
  \***********************************************/
/*! exports provided: TypeEquipementService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeEquipementService", function() { return TypeEquipementService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


class TypeEquipementService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/restequip/listeequip';
        this.urlDelete = 'http://localhost:8080/restequip/deleteequip';
        this.urlAdd = 'http://localhost:8080/restequip/equipement';
        this.urlUpdate = 'http://localhost:8080/restequip/equipement';
        this.urlGetByID = 'http://localhost:8080/restequip/listeequip';
    }
    getTypeEquipementById(id) {
        return this.http.get(this.urlGetByID + "/" + id);
    }
    getAllTypeEquipement() {
        return this.http.get(this.urlGetAll);
    }
    deleteTypeEquipement(id) {
        return this.http.delete(this.urlDelete + "/" + id);
    }
    addTypeEquipement(putTypeEquipement) {
        return this.http.put(this.urlAdd, putTypeEquipement);
    }
    updateTypeEquipement(updateTypeEquipement) {
        return this.http.post(this.urlUpdate, updateTypeEquipement);
    }
}
TypeEquipementService.ɵfac = function TypeEquipementService_Factory(t) { return new (t || TypeEquipementService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
TypeEquipementService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TypeEquipementService, factory: TypeEquipementService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "W61y":
/*!****************************************************!*\
  !*** ./src/app/user-login/user-login.component.ts ***!
  \****************************************************/
/*! exports provided: UserLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoginComponent", function() { return UserLoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_service_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/service/user.service */ "/7iW");
/* harmony import */ var src_app_barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/barre-navigation/barre-navigation.component */ "WHZN");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");





function UserLoginComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 5, 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function UserLoginComponent_div_0_Template_form_ngSubmit_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.onSubmit(_r1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "input", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "label", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Mot de passe ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "h1", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Creation d'un compte");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "form", 16, 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function UserLoginComponent_div_0_Template_form_ngSubmit_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](23); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.submitinscr(_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "label", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Matricule :");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "input", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Nom :");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "input", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Pr\u00E9nom :");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "input", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "E-mail :");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "label", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "T\u00E9l\u00E9phone :");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "input", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Valider");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserLoginComponent_div_0_Template_button_click_49_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.closeModal(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Fermer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r1.invalid);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", _r2.invalid);
} }
class UserLoginComponent {
    constructor(userService, barreNav) {
        this.userService = userService;
        this.barreNav = barreNav;
    }
    ngOnInit() {
    }
    popup() {
        this.openModal = true;
    }
    closeModal() {
        this.barreNav.hidePopup();
        this.openModal = false;
    }
    onSubmit(form) {
        console.log("log form");
        this.userService.logUser(form.value.name, form.value.password).subscribe((response) => {
            this.getUser = response;
            console.log("Réponse user login =" + this.getUser.nomUser);
            sessionStorage.setItem('user', JSON.stringify(this.getUser));
            this.closeModal();
        });
    }
    submitinscr(form) {
        console.log("inscription Form");
        this.getUser = {
            idUser: null,
            prenomUser: form.value.matriculeUser,
            nomUser: form.value.nomuser,
            matriculeUser: form.value.matriculeUser,
            emailUser: form.value.emailuser,
            password: null,
            telephoneUser: form.value.telephoneuser,
            mdpAChanger: null,
            idEtatUser: null,
            idLangue: null,
            roleUser: null
        };
        console.log(this.getUser);
        this.userService.addOneUser(this.getUser).subscribe((response) => {
            this.getUser = response;
            console.log("Réponse user login =" + this.getUser.nomUser);
            sessionStorage.setItem('user', JSON.stringify(this.getUser));
        });
        this.closeModal();
    }
}
UserLoginComponent.ɵfac = function UserLoginComponent_Factory(t) { return new (t || UserLoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_2__["BarreNavigationComponent"])); };
UserLoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: UserLoginComponent, selectors: [["app-user-login"]], inputs: { openModal: "openModal" }, decls: 1, vars: 1, consts: [["id", "myModal", "class", "modal fade show", "style", "top:auto; display: flex; justify-content: center; align-items: center;", "role", "dialog", 4, "ngIf"], ["id", "myModal", "role", "dialog", 1, "modal", "fade", "show", 2, "top", "auto", "display", "flex", "justify-content", "center", "align-items", "center"], ["id", "container", 1, "container", "row", "bg-dark"], ["id", "trait", 1, "col-lg-6", "col-sm-12"], [2, "color", "white"], [1, "col-lg-6", "col-sm-12", 3, "ngSubmit"], ["f", "ngForm"], [1, "form-group", "inline"], ["id", "formType", "name", "formType", "type", "hidden", "value", "log", "ngModel", ""], ["for", "name", 1, "col-5", "nav-link"], ["type", "text", "id", "name", "name", "name", "ngModel", "", "required", "", 1, "form-control"], [1, "form-group"], ["for", "status", 1, "col-5", "nav-link"], ["type", "password", "id", "password", "name", "password", "ngModel", "", "required", "", 1, "form-control"], ["type", "submit", 1, "btn", "btn-primary", 3, "disabled"], ["id", "creation-compte", 1, "col-lg-6", "col-sm-12"], [1, "col-12", 3, "ngSubmit"], ["g", "ngForm"], [1, "form-group", "row"], ["for", "matriculeUser", 1, "col-5", "nav-link"], ["id", "matriculeUser", "name", "matriculeUser", "type", "number", "ngModel", "", "required", "", 1, "col-5", "offset-1"], ["for", "nomuser", 1, "col-5", "nav-link"], ["id", "nomuser", "name", "nomuser", "type", "text", "ngModel", "", "required", "", 1, "col-5", "offset-1"], ["for", "prenomuser", 1, "col-5", "nav-link"], ["name", "prenomuser", "type", "text", "ngModel", "", "required", "", 1, "col-5", "offset-1"], ["for", "emailuser", 1, "col-5", "nav-link"], ["name", "emailuser", "type", "email", "ngModel", "", "required", "", 1, "col-5", "offset-1"], ["for", "telephoneuser", 1, "col-5", "nav-link"], ["name", "telephoneuser", "type", "number", "ngModel", "", 1, "col-5", "offset-1"], ["value", "Connexion", 1, "col-4", "offset-4", "btn", "btn-primary", 3, "click"]], template: function UserLoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, UserLoginComponent_div_0_Template, 51, 2, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.openModal);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NumberValueAccessor"]], encapsulation: 2 });


/***/ }),

/***/ "WHZN":
/*!****************************************************************!*\
  !*** ./src/app/barre-navigation/barre-navigation.component.ts ***!
  \****************************************************************/
/*! exports provided: BarreNavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BarreNavigationComponent", function() { return BarreNavigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _service_parametre_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../service/parametre.service */ "2RJA");
/* harmony import */ var src_service_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/service/user.service */ "/7iW");



function BarreNavigationComponent_a_26_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BarreNavigationComponent_a_26_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deconnexion(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Deconnexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BarreNavigationComponent_a_27_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BarreNavigationComponent_a_27_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.popup(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function BarreNavigationComponent_app_user_login_28_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-user-login", 23);
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("openModal", ctx_r2.openModal);
} }
class BarreNavigationComponent {
    constructor(paramService, userService) {
        this.paramService = paramService;
        this.userService = userService;
        this.openModal = false;
    }
    ngOnInit() {
        this.getImg();
        this.userService.infoMonProfil().subscribe((response) => {
            this.user = response;
            console.log("Réponse=" + this.user.nomUser);
        });
    }
    popup() {
        this.openModal = true;
    }
    getImg() {
        this.paramService.getImg().subscribe((response) => {
            this.image = response;
            console.log(response);
        }, (error) => {
            alert(error.message);
        });
    }
    hidePopup() {
        this.user = JSON.parse(sessionStorage.getItem("user"));
        console.log("user =" + this.user);
        this.openModal = false;
    }
    deconnexion() {
        this.user = null;
        sessionStorage.removeItem("user");
    }
    ngOnChanges() {
        console.log("some changes has occured");
    }
}
BarreNavigationComponent.ɵfac = function BarreNavigationComponent_Factory(t) { return new (t || BarreNavigationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_parametre_service__WEBPACK_IMPORTED_MODULE_1__["ParametreService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"])); };
BarreNavigationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BarreNavigationComponent, selectors: [["app-barre-navigation"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 29, vars: 6, consts: [[1, "navbar", "navbar-expand-lg", "fixed-top-sm", "justify-content-start", "bg-dark", "navbar-dark", 2, "height", "70px"], ["alt", "le logo", "width", "100", "height", "75", 1, "navbar-brand", "align-items-center", 3, "src"], ["type", "button", "data-mdb-toggle", "collapse", "data-mdb-target", "#navbarText", "aria-controls", "navbarText", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], [1, "container-fluid"], ["id", "navbarText", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mt-2", "me-auto", "mb-2", "mb-lg-3", "col-xl-4"], [1, "container-fluid", "mt-2", "col-lg-3", "col-xl-4"], [1, "d-flex", "input-group", "w-auto"], ["type", "search", "placeholder", "Search", "aria-label", "Search", "aria-describedby", "search-addon", 1, "form-control", "rounded"], [1, "nav-item", "col-lg-3", "col-xl-7", "text-center"], ["aria-current", "page", "routerLink", "/calendar", "routerLinkActive", "active", 1, "nav-link", "active"], [1, "nav-item", "col-lg-4", "col-xl-6", "text-center"], ["routerLink", "/mesReservations", "routerLinkActive", "active", 1, "nav-link"], [1, "nav-item", "col-lg-4", "col-xl-7", "text-center"], ["routerLink", "/mon-profil", "routerLinkActive", "active", 1, "nav-link"], [1, "nav-item", "col-lg-3", "col-xl-6", "text-center"], [1, "nav-link"], [1, "nav-item", "col-lg-3", "col-xl-3", "text-center"], [1, "navbar-text"], ["class", "nav-link", 3, "click", 4, "ngIf"], [3, "openModal", 4, "ngIf"], [1, "nav-link", 3, "click"], [3, "openModal"]], template: function BarreNavigationComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Accueil");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Mes r\u00E9servations");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Mon Profil ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Langue");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "li", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, BarreNavigationComponent_a_26_Template, 2, 0, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, BarreNavigationComponent_a_27_Template, 2, 0, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, BarreNavigationComponent_app_user_login_28_Template, 1, 1, "app-user-login", 21);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", ctx.image == null ? null : ctx.image.logo, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" ", ctx.user == null ? null : ctx.user.prenomUser, " ", ctx.user == null ? null : ctx.user.nomUser, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.user);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.user == null);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.openModal);
    } }, styles: ["./style.scss"] });


/***/ }),

/***/ "WOnM":
/*!********************************************************************!*\
  !*** ./src/app/corps-page-accueil/corps-page-accueil.component.ts ***!
  \********************************************************************/
/*! exports provided: CorpsPageAccueilComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorpsPageAccueilComponent", function() { return CorpsPageAccueilComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class CorpsPageAccueilComponent {
    constructor() { }
    ngOnInit() {
    }
}
CorpsPageAccueilComponent.ɵfac = function CorpsPageAccueilComponent_Factory(t) { return new (t || CorpsPageAccueilComponent)(); };
CorpsPageAccueilComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CorpsPageAccueilComponent, selectors: [["app-corps-page-accueil"]], decls: 2, vars: 0, template: function CorpsPageAccueilComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "corps-page-accueil works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, encapsulation: 2 });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _corps_page_accueil_corps_page_accueil_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./corps-page-accueil/corps-page-accueil.component */ "WOnM");
/* harmony import */ var _barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./barre-navigation/barre-navigation.component */ "WHZN");
/* harmony import */ var _corps_mes_reservations_corps_mes_reservations_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./corps-mes-reservations/corps-mes-reservations.component */ "qAlh");
/* harmony import */ var _corps_mon_profil_corps_mon_profil_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./corps-mon-profil/corps-mon-profil.component */ "T/nz");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./scheduler/scheduler.component */ "/FVT");
/* harmony import */ var _user_login_user_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user-login/user-login.component */ "W61y");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ "ofXK");















class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({ providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _corps_page_accueil_corps_page_accueil_component__WEBPACK_IMPORTED_MODULE_4__["CorpsPageAccueilComponent"],
        _barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_5__["BarreNavigationComponent"],
        _corps_mes_reservations_corps_mes_reservations_component__WEBPACK_IMPORTED_MODULE_6__["CorpsMesReservationsComponent"],
        _corps_mon_profil_corps_mon_profil_component__WEBPACK_IMPORTED_MODULE_7__["CorpsMonProfilComponent"],
        _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_9__["SchedulerComponent"],
        _user_login_user_login_component__WEBPACK_IMPORTED_MODULE_10__["UserLoginComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"]] }); })();
_angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetComponentScope"](_barre_navigation_barre_navigation_component__WEBPACK_IMPORTED_MODULE_5__["BarreNavigationComponent"], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"], _angular_router__WEBPACK_IMPORTED_MODULE_12__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_12__["RouterLinkActive"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgIf"], _user_login_user_login_component__WEBPACK_IMPORTED_MODULE_10__["UserLoginComponent"]], []);


/***/ }),

/***/ "e3aT":
/*!*****************************************!*\
  !*** ./src/service/batiment.service.ts ***!
  \*****************************************/
/*! exports provided: BatimentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BatimentService", function() { return BatimentService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");


/**
 *Service pour utiliser l'API BATIMENT
 */
class BatimentService {
    constructor(http) {
        this.http = http;
        this.urlGetAll = 'http://localhost:8080/batiment/getall';
        this.urlGetOne = 'http://localhost:8080/batiment/getbyid';
        this.urlDelete = 'http://localhost:8080/batiment/deletebyid';
        this.urlAdd = 'http://localhost:8080/batiment/add';
        this.urlUpdate = 'http://localhost:8080/batiment/update';
    }
    getAllBatiment() {
        return this.http.get(this.urlGetAll);
    }
    getOneBatimen(id) {
        return this.http.get(this.urlGetOne + '/' + id);
    }
    deleteOneBatiment(id) {
        return this.http.delete(this.urlDelete + '/' + id);
    }
    addOneBatiment(addBatiment) {
        return this.http.put(this.urlAdd, addBatiment);
    }
    updateOneBatiment(updateBatiment) {
        return this.http.post(this.urlUpdate, updateBatiment);
    }
}
BatimentService.ɵfac = function BatimentService_Factory(t) { return new (t || BatimentService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
BatimentService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: BatimentService, factory: BatimentService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "qAlh":
/*!****************************************************************************!*\
  !*** ./src/app/corps-mes-reservations/corps-mes-reservations.component.ts ***!
  \****************************************************************************/
/*! exports provided: CorpsMesReservationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorpsMesReservationsComponent", function() { return CorpsMesReservationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_service_reservation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/service/reservation.service */ "40Iy");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");




function CorpsMesReservationsComponent_tbody_17_td_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Pas de description");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function CorpsMesReservationsComponent_tbody_17_td_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const reservation_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", reservation_r1.description, "");
} }
function CorpsMesReservationsComponent_tbody_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "th", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, CorpsMesReservationsComponent_tbody_17_td_5_Template, 2, 0, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, CorpsMesReservationsComponent_tbody_17_td_6_Template, 2, 1, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const reservation_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](reservation_r1.idSalle.nomsalle);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", reservation_r1.description == null);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", reservation_r1.description != null);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", reservation_r1.idPeriode.debut_date_periode, " + ", reservation_r1.idPeriode.debut_heure_periode, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", reservation_r1.idPeriode.fin_date_periode, " + ", reservation_r1.idPeriode.fin_heure_periode, "");
} }
class CorpsMesReservationsComponent {
    constructor(reservationService) {
        this.reservationService = reservationService;
    }
    ngOnInit() {
        this.getAllReservation();
    }
    /**
     * getAllReservation
     */
    getAllReservation() {
        this.reservationService.getAllReservations().subscribe((response) => {
            this.reservations = response;
            console.log(this.reservations);
        }, (error) => {
            alert(error.message);
        });
    }
    /****************************************METHODE PROPRE AUX POPUPS************************************ */
    openModal(namediv) {
        document.getElementById(namediv).style.display = 'block';
        document.body.classList.add('jw-modal-open');
    }
    closeModal(namediv) {
        document.getElementById(namediv).style.display = 'none';
        document.body.classList.remove('jw-modal-open');
    }
}
CorpsMesReservationsComponent.ɵfac = function CorpsMesReservationsComponent_Factory(t) { return new (t || CorpsMesReservationsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_service_reservation_service__WEBPACK_IMPORTED_MODULE_1__["ReservationService"])); };
CorpsMesReservationsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CorpsMesReservationsComponent, selectors: [["app-corps-mes-reservations"]], decls: 63, vars: 1, consts: [[1, "container", "mx-auto", "mt-5", 2, "width", "800px"], [1, "form-check", "row", "form-group"], [1, "container", 2, "overflow-y", "scroll", "height", "400px"], [1, "table"], ["scope", "col"], [4, "ngFor", "ngForOf"], ["id", "updateResa", 1, "jw-modal-wrapper"], [1, "jw-modal"], [1, "jw-modal-body"], ["action", "/reservation/update", "method", "POST"], ["for", "idsalle"], ["name", "idsalle"], ["value", ""], ["for", "periode"], ["type", "text", "name", "periode"], ["for", "descriptionReservation"], ["type", "text", "name", "descriptionReservation"], ["type", "submit", "value", "Envoyer", 1, "col-lg-3"], ["type", "submit", "value", "Retour", 1, "col-lg-3", 3, "click"], [1, "jw-modal-background"], [1, "container", "mt-5", "d-flex", "justify-content-center"], ["type", "button", 1, "btn", "btn-secondary", 3, "click"], ["type", "button", "href", "#", 1, "btn", "btn-secondary"], ["scope", "row", 1, "table-active"], ["class", "table-active", 4, "ngIf"], [1, "table-active"], ["type", "radio", "name", "flexRadioDefault", "id", "flexRadioDefault1", 1, "form-check-input"]], template: function CorpsMesReservationsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Mes r\u00E9servations");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "table", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "thead");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "th", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Salles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Motif de r\u00E9servation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "th", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "D\u00E9but");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "th", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Fin");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "th", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, CorpsMesReservationsComponent_tbody_17_Template, 14, 7, "tbody", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Modifier une r\u00E9servation");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "form", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Modifier la salle : ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "select", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, " Valeur rentrer en dure ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "--S\u00E9lectionnez une salle--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "A110");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "B320");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "A230");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "B150");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Modifier la p\u00E9riode :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Du ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, " au ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Modifier la description :");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMesReservationsComponent_Template_input_click_56_listener() { return ctx.closeModal("updateResa"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CorpsMesReservationsComponent_Template_button_click_59_listener() { return ctx.openModal("updateResa"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Modifier");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Annuler");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.reservations);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵangular_packages_forms_forms_z"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"]], encapsulation: 2 });


/***/ }),

/***/ "tgKO":
/*!**************************************************************************************!*\
  !*** C:/Users/cleme/Downloads/scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*

@license
dhtmlxScheduler v.5.3.11 Professional Evaluation

This software is covered by DHTMLX Evaluation License. Contact sales@dhtmlx.com to get Commercial or Enterprise license. Usage without proper license is prohibited.

(c) XB Software Ltd.

*/
Scheduler.plugin(function(e){!function(){e._inited_multisection_copies||(e.attachEvent("onEventIdChange",function(e,t){var a=this._multisection_copies;if(a&&a[e]&&!a[t]){var i=a[e];delete a[e],a[t]=i}}),e._inited_multisection_copies=!0),e._register_copies_array=function(e){for(var t=0;t<e.length;t++)this._register_copy(e[t])},e._register_copy=function(e){if(this._multisection_copies){this._multisection_copies[e.id]||(this._multisection_copies[e.id]={});var t=e[this._get_section_property()]
;this._multisection_copies[e.id][t]=e}},e._get_copied_event=function(t,a){if(!this._multisection_copies[t])return null;if(this._multisection_copies[t][a])return this._multisection_copies[t][a];var i=this._multisection_copies[t];if(e._drag_event&&e._drag_event._orig_section&&i[e._drag_event._orig_section])return i[e._drag_event._orig_section];var n=1/0,r=null;for(var o in i)i[o]._sorder<n&&(r=i[o],n=i[o]._sorder);return r},e._clear_copied_events=function(){this._multisection_copies={}},
e._restore_render_flags=function(t){for(var a=this._get_section_property(),i=0;i<t.length;i++){var n=t[i],r=e._get_copied_event(n.id,n[a]);if(r)for(var o in r)0===o.indexOf("_")&&(n[o]=r[o])}};var t=e.createTimelineView;e.createTimelineView=function(a){function i(){var t=new Date(e.getState().date),i=e.date[h+"_start"](t);i=e.date.date_part(i);var n=[],r=e.matrix[h];r.y_unit=n,r.order={};for(var o=0;o<a.days;o++)n.push({key:+i,label:i}),r.order[r.y_unit[o].key]=o,i=e.date.add(i,1,"day")}
function n(e){var t={};for(var a in e)t[a]=e[a];return t}function r(e,t){t.setDate(1),t.setFullYear(e.getFullYear()),t.setMonth(e.getMonth()),t.setDate(e.getDate())}function o(t){for(var a=[],i=0;i<t.length;i++){var n=d(t[i]);if(e.isOneDayEvent(n))_(n),a.push(n);else{for(var r=new Date(Math.min(+n.end_date,+e._max_date)),o=new Date(Math.max(+n.start_date,+e._min_date)),l=[];+o<+r;){var h=d(n);h.start_date=o,h.end_date=new Date(Math.min(+c(h.start_date),+r)),o=c(o),_(h),a.push(h),l.push(h)}s(l,n)
}}return a}function s(e,t){for(var a=!1,i=!1,n=0,r=e.length;n<r;n++){var o=e[n];a=+o._w_start_date==+t.start_date,i=+o._w_end_date==+t.end_date,o._no_resize_start=o._no_resize_end=!0,a&&(o._no_resize_start=!1),i&&(o._no_resize_end=!1)}}function d(t){var a=e.getEvent(t.event_pid);return a&&a.isPrototypeOf(t)?(t=e._copy_event(t),delete t.event_length,delete t.event_pid,delete t.rec_pattern,delete t.rec_type):t=e._lame_clone(t),t}function _(t){if(!t._w_start_date||!t._w_end_date){
var a=e.date,i=t._w_start_date=new Date(t.start_date),n=t._w_end_date=new Date(t.end_date);t[u]=+a.date_part(t.start_date),t._count||(t._count=1),t._sorder||(t._sorder=0);var r=n-i;t.start_date=new Date(e._min_date),l(i,t.start_date),t.end_date=new Date(+t.start_date+r),i.getTimezoneOffset()!=n.getTimezoneOffset()&&(t.end_date=new Date(t.end_date.valueOf()+6e4*(i.getTimezoneOffset()-n.getTimezoneOffset())))}}function l(e,t){t.setMinutes(e.getMinutes()),t.setHours(e.getHours())}function c(t){
var a=e.date.add(t,1,"day");return a=e.date.date_part(a)}if("days"!=a.render)return void t.apply(this,arguments);var h=a.name,u=a.y_property="timeline-week"+h;a.y_unit=[],a.render="bar",a.days=a.days||7,t.call(this,a),e.templates[h+"_scalex_class"]=function(){},e.templates[h+"_scaley_class"]=function(){},e.templates[h+"_scale_label"]=function(t,a,i){return e.templates.day_date(a)},e.date[h+"_start"]=function(t){return t=e.date.week_start(t),t=e.date.add(t,a.x_step*a.x_start,a.x_unit)},
e.date["add_"+h]=function(t,i){return e.date.add(t,i*a.days,"day")};var g=e._renderMatrix;e._renderMatrix=function(e,t){e&&i(),g.apply(this,arguments)};var f=e.checkCollision;e.checkCollision=function(t){if(t[u]){var t=n(t);delete t[u]}return f.apply(e,[t])},e.attachEvent("onBeforeDrag",function(t,a,i){var n=i.target||i.srcElement,r=e._getClassName(n)
;if("resize"==a)r.indexOf("dhx_event_resize_end")<0?e._w_line_drag_from_start=!0:e._w_line_drag_from_start=!1;else if("move"==a&&r.indexOf("no_drag_move")>=0)return!1;return!0});var v=e["mouse_"+h];e["mouse_"+h]=function(t){var a;this._drag_event&&(a=this._drag_event._move_delta);var i=e.matrix[this._mode];if(i.scrollable&&!t.converted&&(t.converted=1,t.x-=-i._x_scroll,t.y+=i._y_scroll),void 0===a&&"move"==e._drag_mode){var n={y:t.y};e._resolve_timeline_section(i,n)
;var r=t.x-i.dx,o=new Date(n.section);l(e._timeline_drag_date(i,r),o);var s=e._drag_event,d=this.getEvent(this._drag_id);d&&(s._move_delta=(d.start_date-o)/6e4,this.config.preserve_length&&t._ignores&&(s._move_delta=this._get_real_event_length(d.start_date,o,i),s._event_length=this._get_real_event_length(d.start_date,d.end_date,i)))}var t=v.apply(e,arguments);if(e._drag_mode&&"move"!=e._drag_mode){var _=null
;_=e._drag_event&&e._drag_event["timeline-week"+h]?new Date(e._drag_event["timeline-week"+h]):new Date(t.section),t.y+=Math.round((_-e.date.date_part(new Date(e._min_date)))/(6e4*this.config.time_step)),"resize"==e._drag_mode&&(t.resize_from_start=e._w_line_drag_from_start)}else if(e._drag_event){var c=Math.floor(Math.abs(t.y/(1440/e.config.time_step)));c*=t.y>0?1:-1,t.y=t.y%(1440/e.config.time_step);var u=e.date.date_part(new Date(e._min_date))
;u.valueOf()!=new Date(t.section).valueOf()&&(t.x=Math.floor((t.section-u)/864e5),t.x+=c)}return t},e.attachEvent("onEventCreated",function(t,a){return e._events[t]&&delete e._events[t][u],!0}),e.attachEvent("onBeforeEventChanged",function(t,a,i,n){return e._events[t.id]&&delete e._events[t.id][u],!0});var m=e._update_timeline_section;e._update_timeline_section=function(t){var a,i
;this._mode==h&&(a=t.event)&&(i=e._get_copied_event(a.id,e.date.day_start(new Date(a.start_date.valueOf()))))&&(t.event._sorder=i._sorder,t.event._count=i._count);m.apply(this,arguments);a&&i&&(i._count=a._count,i._sorder=a._sorder)};var p=e.render_view_data;e.render_view_data=function(t,a){return this._mode==h&&t&&(t=o(t),e._restore_render_flags(t)),p.apply(e,[t,a])};var x=e.get_visible_events;e.get_visible_events=function(){if(this._mode==h){this._clear_copied_events(),
e._max_date=e.date.date_part(e.date.add(e._min_date,a.days,"day"));var t=x.apply(e,arguments);return t=o(t),e._register_copies_array(t),t}return x.apply(e,arguments)};var b=e.addEventNow;e.addEventNow=function(t){if(e.getState().mode==h)if(t[u]){var a=new Date(t[u]);r(a,t.start_date),r(a,t.end_date)}else{var i=new Date(t.start_date);t[u]=+e.date.date_part(i)}return b.apply(e,arguments)};var y=e._render_marked_timespan;e._render_marked_timespan=function(){
if(e._mode!=h)return y.apply(this,arguments)}}}()});

/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _corps_mes_reservations_corps_mes_reservations_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./corps-mes-reservations/corps-mes-reservations.component */ "qAlh");
/* harmony import */ var _corps_mon_profil_corps_mon_profil_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./corps-mon-profil/corps-mon-profil.component */ "T/nz");
/* harmony import */ var _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./scheduler/scheduler.component */ "/FVT");
/* harmony import */ var _user_login_user_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-login/user-login.component */ "W61y");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");







const routes = [
    { path: 'mesReservations', component: _corps_mes_reservations_corps_mes_reservations_component__WEBPACK_IMPORTED_MODULE_1__["CorpsMesReservationsComponent"] },
    { path: 'mon-profil', component: _corps_mon_profil_corps_mon_profil_component__WEBPACK_IMPORTED_MODULE_2__["CorpsMonProfilComponent"] },
    { path: 'login-inscription', component: _user_login_user_login_component__WEBPACK_IMPORTED_MODULE_4__["UserLoginComponent"] },
    { path: 'calendar', component: _scheduler_scheduler_component__WEBPACK_IMPORTED_MODULE_3__["SchedulerComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "wBh4":
/*!**********************************************************************!*\
  !*** C:/Users/cleme/Downloads/scheduler/codebase/dhtmlxscheduler.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*

@license
dhtmlxScheduler v.5.3.11 Professional Evaluation

This software is covered by DHTMLX Evaluation License. Contact sales@dhtmlx.com to get Commercial or Enterprise license. Usage without proper license is prohibited.

(c) XB Software Ltd.

*/
!function(){function dtmlXMLLoaderObject(t,e,i,n){return this.xmlDoc="",this.async=void 0===i||i,this.onloadAction=t||null,this.mainObject=e||null,this.waitCall=null,this.rSeed=n||!1,this}function dhtmlDragAndDropObject(){return window.dhtmlDragAndDrop?window.dhtmlDragAndDrop:(this.lastLanding=0,this.dragNode=0,this.dragStartNode=0,this.dragStartObject=0,this.tempDOMU=null,this.tempDOMM=null,this.waitDrag=0,window.dhtmlDragAndDrop=this,this)}function _dhtmlxError(t,e,i){
return this.catches||(this.catches=[]),this}function dhtmlXHeir(t,e){for(var i in e)"function"==typeof e[i]&&(t[i]=e[i]);return t}window.dhtmlx||(window.dhtmlx=function(t){for(var e in t)dhtmlx[e]=t[e];return dhtmlx}),dhtmlx.extend_api=function(t,e,i){var n=window[t];n&&(window[t]=function(t){var i;if(t&&"object"==typeof t&&!t.tagName){i=n.apply(this,e._init?e._init(t):arguments);for(var a in dhtmlx)e[a]&&this[e[a]](dhtmlx[a])
;for(var a in t)e[a]?this[e[a]](t[a]):0===a.indexOf("on")&&this.attachEvent(a,t[a])}else i=n.apply(this,arguments);return e._patch&&e._patch(this),i||this},window[t].prototype=n.prototype,i&&dhtmlXHeir(window[t].prototype,i))},window.dhtmlxAjax={get:function(t,e){var i=new dtmlXMLLoaderObject(!0);return i.async=arguments.length<3,i.waitCall=e,i.loadXML(t),i},post:function(t,e,i){var n=new dtmlXMLLoaderObject(!0);return n.async=arguments.length<4,n.waitCall=i,n.loadXML(t,!0,e),n},
getSync:function(t){return this.get(t,null,!0)},postSync:function(t,e){return this.post(t,e,null,!0)}},window.dtmlXMLLoaderObject=dtmlXMLLoaderObject,dtmlXMLLoaderObject.count=0,dtmlXMLLoaderObject.prototype.waitLoadFunction=function(t){var e=!0;return this.check=function(){if(t&&t.onloadAction&&(!t.xmlDoc.readyState||4==t.xmlDoc.readyState)){if(!e)return;e=!1,dtmlXMLLoaderObject.count++,"function"==typeof t.onloadAction&&t.onloadAction(t.mainObject,null,null,null,t),
t.waitCall&&(t.waitCall.call(this,t),t.waitCall=null)}},this.check},dtmlXMLLoaderObject.prototype.getXMLTopNode=function(t,e){var i;if(this.xmlDoc.responseXML){var n=this.xmlDoc.responseXML.getElementsByTagName(t);if(0===n.length&&-1!=t.indexOf(":"))var n=this.xmlDoc.responseXML.getElementsByTagName(t.split(":")[1]);i=n[0]}else i=this.xmlDoc.documentElement;if(i)return this._retry=!1,i;if(!this._retry&&_isIE){this._retry=!0;var e=this.xmlDoc
;return this.loadXMLString(this.xmlDoc.responseText.replace(/^[\s]+/,""),!0),this.getXMLTopNode(t,e)}return dhtmlxError.throwError("LoadXML","Incorrect XML",[e||this.xmlDoc,this.mainObject]),document.createElement("div")},dtmlXMLLoaderObject.prototype.loadXMLString=function(t,e){if(_isIE)this.xmlDoc=new ActiveXObject("Microsoft.XMLDOM"),this.xmlDoc.async=this.async,this.xmlDoc.onreadystatechange=function(){},this.xmlDoc.loadXML(t);else{var i=new DOMParser
;this.xmlDoc=i.parseFromString(t,"text/xml")}e||(this.onloadAction&&this.onloadAction(this.mainObject,null,null,null,this),this.waitCall&&(this.waitCall(),this.waitCall=null))},dtmlXMLLoaderObject.prototype.loadXML=function(t,e,i,n){this.rSeed&&(t+=(-1!=t.indexOf("?")?"&":"?")+"a_dhx_rSeed="+(new Date).valueOf()),this.filePath=t,!_isIE&&window.XMLHttpRequest?this.xmlDoc=new XMLHttpRequest:this.xmlDoc=new ActiveXObject("Microsoft.XMLHTTP"),
this.async&&(this.xmlDoc.onreadystatechange=new this.waitLoadFunction(this)),"string"==typeof e?this.xmlDoc.open(e,t,this.async):this.xmlDoc.open(e?"POST":"GET",t,this.async),n?(this.xmlDoc.setRequestHeader("User-Agent","dhtmlxRPC v0.1 ("+navigator.userAgent+")"),this.xmlDoc.setRequestHeader("Content-type","text/xml")):e&&this.xmlDoc.setRequestHeader("Content-type","application/x-www-form-urlencoded"),this.xmlDoc.setRequestHeader("X-Requested-With","XMLHttpRequest"),this.xmlDoc.send(i),
this.async||new this.waitLoadFunction(this)()},dtmlXMLLoaderObject.prototype.destructor=function(){return this._filterXPath=null,this._getAllNamedChilds=null,this._retry=null,this.async=null,this.rSeed=null,this.filePath=null,this.onloadAction=null,this.mainObject=null,this.xmlDoc=null,this.doXPath=null,this.doXPathOpera=null,this.doXSLTransToObject=null,this.doXSLTransToString=null,this.loadXML=null,this.loadXMLString=null,this.doSerialization=null,this.xmlNodeToJSON=null,
this.getXMLTopNode=null,this.setXSLParamValue=null,null},dtmlXMLLoaderObject.prototype.xmlNodeToJSON=function(t){for(var e={},i=0;i<t.attributes.length;i++)e[t.attributes[i].name]=t.attributes[i].value;e._tagvalue=t.firstChild?t.firstChild.nodeValue:"";for(var i=0;i<t.childNodes.length;i++){var n=t.childNodes[i].tagName;n&&(e[n]||(e[n]=[]),e[n].push(this.xmlNodeToJSON(t.childNodes[i])))}return e},window.dhtmlDragAndDropObject=dhtmlDragAndDropObject,
dhtmlDragAndDropObject.prototype.removeDraggableItem=function(t){t.onmousedown=null,t.dragStarter=null,t.dragLanding=null},dhtmlDragAndDropObject.prototype.addDraggableItem=function(t,e){t.onmousedown=this.preCreateDragCopy,t.dragStarter=e,this.addDragLanding(t,e)},dhtmlDragAndDropObject.prototype.addDragLanding=function(t,e){t.dragLanding=e},dhtmlDragAndDropObject.prototype.preCreateDragCopy=function(t){
if(!t&&!window.event||2!=(t||event).button)return window.dhtmlDragAndDrop.waitDrag?(window.dhtmlDragAndDrop.waitDrag=0,document.body.onmouseup=window.dhtmlDragAndDrop.tempDOMU,document.body.onmousemove=window.dhtmlDragAndDrop.tempDOMM,!1):(window.dhtmlDragAndDrop.dragNode&&window.dhtmlDragAndDrop.stopDrag(t),window.dhtmlDragAndDrop.waitDrag=1,window.dhtmlDragAndDrop.tempDOMU=document.body.onmouseup,window.dhtmlDragAndDrop.tempDOMM=document.body.onmousemove,
window.dhtmlDragAndDrop.dragStartNode=this,window.dhtmlDragAndDrop.dragStartObject=this.dragStarter,document.body.onmouseup=window.dhtmlDragAndDrop.preCreateDragCopy,document.body.onmousemove=window.dhtmlDragAndDrop.callDrag,window.dhtmlDragAndDrop.downtime=(new Date).valueOf(),!(!t||!t.preventDefault)&&(t.preventDefault(),!1))},dhtmlDragAndDropObject.prototype.callDrag=function(t){t||(t=window.event);var e=window.dhtmlDragAndDrop;if(!((new Date).valueOf()-e.downtime<100)){if(!e.dragNode){
if(!e.waitDrag)return e.stopDrag(t,!0);if(e.dragNode=e.dragStartObject._createDragNode(e.dragStartNode,t),!e.dragNode)return e.stopDrag();e.dragNode.onselectstart=function(){return!1},e.gldragNode=e.dragNode,document.body.appendChild(e.dragNode),document.body.onmouseup=e.stopDrag,e.waitDrag=0,e.dragNode.pWindow=window,e.initFrameRoute()}if(e.dragNode.parentNode!=window.document.body&&e.gldragNode){var i=e.gldragNode;e.gldragNode.old&&(i=e.gldragNode.old),i.parentNode.removeChild(i)
;var n=e.dragNode.pWindow;if(i.pWindow&&i.pWindow.dhtmlDragAndDrop.lastLanding&&i.pWindow.dhtmlDragAndDrop.lastLanding.dragLanding._dragOut(i.pWindow.dhtmlDragAndDrop.lastLanding),_isIE){var a=document.createElement("div");a.innerHTML=e.dragNode.outerHTML,e.dragNode=a.childNodes[0]}else e.dragNode=e.dragNode.cloneNode(!0);e.dragNode.pWindow=window,e.gldragNode.old=e.dragNode,document.body.appendChild(e.dragNode),n.dhtmlDragAndDrop.dragNode=e.dragNode}
e.dragNode.style.left=t.clientX+15+(e.fx?-1*e.fx:0)+(document.body.scrollLeft||document.documentElement.scrollLeft)+"px",e.dragNode.style.top=t.clientY+3+(e.fy?-1*e.fy:0)+(document.body.scrollTop||document.documentElement.scrollTop)+"px";var r;r=t.srcElement?t.srcElement:t.target,e.checkLanding(r,t)}},dhtmlDragAndDropObject.prototype.calculateFramePosition=function(t){if(window.name){for(var e=parent.frames[window.name].frameElement.offsetParent,i=0,n=0;e;)i+=e.offsetLeft,n+=e.offsetTop,
e=e.offsetParent;if(parent.dhtmlDragAndDrop){var a=parent.dhtmlDragAndDrop.calculateFramePosition(1);i+=1*a.split("_")[0],n+=1*a.split("_")[1]}if(t)return i+"_"+n;this.fx=i,this.fy=n}return"0_0"},dhtmlDragAndDropObject.prototype.checkLanding=function(t,e){t&&t.dragLanding?(this.lastLanding&&this.lastLanding.dragLanding._dragOut(this.lastLanding),this.lastLanding=t,this.lastLanding=this.lastLanding.dragLanding._dragIn(this.lastLanding,this.dragStartNode,e.clientX,e.clientY,e),
this.lastLanding_scr=_isIE?e.srcElement:e.target):t&&"BODY"!=t.tagName?this.checkLanding(t.parentNode,e):(this.lastLanding&&this.lastLanding.dragLanding._dragOut(this.lastLanding,e.clientX,e.clientY,e),this.lastLanding=0,this._onNotFound&&this._onNotFound())},dhtmlDragAndDropObject.prototype.stopDrag=function(t,e){var i=window.dhtmlDragAndDrop;if(!e){i.stopFrameRoute();var n=i.lastLanding;i.lastLanding=null,
n&&n.dragLanding._drag(i.dragStartNode,i.dragStartObject,n,_isIE?event.srcElement:t.target)}i.lastLanding=null,i.dragNode&&i.dragNode.parentNode==document.body&&i.dragNode.parentNode.removeChild(i.dragNode),i.dragNode=0,i.gldragNode=0,i.fx=0,i.fy=0,i.dragStartNode=0,i.dragStartObject=0,document.body.onmouseup=i.tempDOMU,document.body.onmousemove=i.tempDOMM,i.tempDOMU=null,i.tempDOMM=null,i.waitDrag=0},dhtmlDragAndDropObject.prototype.stopFrameRoute=function(t){
t&&window.dhtmlDragAndDrop.stopDrag(1,1);for(var e=0;e<window.frames.length;e++)try{window.frames[e]!=t&&window.frames[e].dhtmlDragAndDrop&&window.frames[e].dhtmlDragAndDrop.stopFrameRoute(window)}catch(t){}try{parent.dhtmlDragAndDrop&&parent!=window&&parent!=t&&parent.dhtmlDragAndDrop.stopFrameRoute(window)}catch(t){}},dhtmlDragAndDropObject.prototype.initFrameRoute=function(t,e){t&&(window.dhtmlDragAndDrop.preCreateDragCopy(),
window.dhtmlDragAndDrop.dragStartNode=t.dhtmlDragAndDrop.dragStartNode,window.dhtmlDragAndDrop.dragStartObject=t.dhtmlDragAndDrop.dragStartObject,window.dhtmlDragAndDrop.dragNode=t.dhtmlDragAndDrop.dragNode,window.dhtmlDragAndDrop.gldragNode=t.dhtmlDragAndDrop.dragNode,window.document.body.onmouseup=window.dhtmlDragAndDrop.stopDrag,window.waitDrag=0,!_isIE&&e&&(!_isFF||_FFrv<1.8)&&window.dhtmlDragAndDrop.calculateFramePosition());try{
parent.dhtmlDragAndDrop&&parent!=window&&parent!=t&&parent.dhtmlDragAndDrop.initFrameRoute(window)}catch(t){}for(var i=0;i<window.frames.length;i++)try{window.frames[i]!=t&&window.frames[i].dhtmlDragAndDrop&&window.frames[i].dhtmlDragAndDrop.initFrameRoute(window,!t||e?1:0)}catch(t){}};var _isFF=!1,_isIE=!1,_isOpera=!1,_isKHTML=!1,_isMacOS=!1,_isChrome=!1,_FFrv=!1,_KHTMLrv=!1,_OperaRv=!1;-1!=navigator.userAgent.indexOf("Macintosh")&&(_isMacOS=!0),
navigator.userAgent.toLowerCase().indexOf("chrome")>-1&&(_isChrome=!0),-1!=navigator.userAgent.indexOf("Safari")||-1!=navigator.userAgent.indexOf("Konqueror")?(_KHTMLrv=parseFloat(navigator.userAgent.substr(navigator.userAgent.indexOf("Safari")+7,5)),_KHTMLrv>525?(_isFF=!0,_FFrv=1.9):_isKHTML=!0):-1!=navigator.userAgent.indexOf("Opera")?(_isOpera=!0,_OperaRv=parseFloat(navigator.userAgent.substr(navigator.userAgent.indexOf("Opera")+6,3))):-1!=navigator.appName.indexOf("Microsoft")?(_isIE=!0,
-1==navigator.appVersion.indexOf("MSIE 8.0")&&-1==navigator.appVersion.indexOf("MSIE 9.0")&&-1==navigator.appVersion.indexOf("MSIE 10.0")||"BackCompat"==document.compatMode||(_isIE=8)):"Netscape"==navigator.appName&&-1!=navigator.userAgent.indexOf("Trident")?_isIE=8:(_isFF=!0,_FFrv=parseFloat(navigator.userAgent.split("rv:")[1])),dtmlXMLLoaderObject.prototype.doXPath=function(t,e,i,n){if(_isKHTML||!_isIE&&!window.XPathResult)return this.doXPathOpera(t,e)
;if(_isIE)return e||(e=this.xmlDoc.nodeName?this.xmlDoc:this.xmlDoc.responseXML),e||dhtmlxError.throwError("LoadXML","Incorrect XML",[e||this.xmlDoc,this.mainObject]),i&&e.setProperty("SelectionNamespaces","xmlns:xsl='"+i+"'"),"single"==n?e.selectSingleNode(t):e.selectNodes(t)||new Array(0);var a=e;e||(e=this.xmlDoc.nodeName?this.xmlDoc:this.xmlDoc.responseXML),e||dhtmlxError.throwError("LoadXML","Incorrect XML",[e||this.xmlDoc,this.mainObject]),-1!=e.nodeName.indexOf("document")?a=e:(a=e,
e=e.ownerDocument);var r=XPathResult.ANY_TYPE;"single"==n&&(r=XPathResult.FIRST_ORDERED_NODE_TYPE);var s=[],o=e.evaluate(t,a,function(t){return i},r,null);if(r==XPathResult.FIRST_ORDERED_NODE_TYPE)return o.singleNodeValue;for(var d=o.iterateNext();d;)s[s.length]=d,d=o.iterateNext();return s},_dhtmlxError.prototype.catchError=function(t,e){this.catches[t]=e},_dhtmlxError.prototype.throwError=function(t,e,i){
return this.catches[t]?this.catches[t](t,e,i):this.catches.ALL?this.catches.ALL(t,e,i):(window.alert("Error type: "+arguments[0]+"\nDescription: "+arguments[1]),null)},window.dhtmlxError=new _dhtmlxError,dtmlXMLLoaderObject.prototype.doXPathOpera=function(t,e){var i=t.replace(/[\/]+/gi,"/").split("/"),n=null,a=1;if(!i.length)return[];if("."==i[0])n=[e];else{if(""!==i[0])return[];n=(this.xmlDoc.responseXML||this.xmlDoc).getElementsByTagName(i[a].replace(/\[[^\]]*\]/g,"")),a++}
for(a;a<i.length;a++)n=this._getAllNamedChilds(n,i[a]);return-1!=i[a-1].indexOf("[")&&(n=this._filterXPath(n,i[a-1])),n},dtmlXMLLoaderObject.prototype._filterXPath=function(t,e){for(var i=[],e=e.replace(/[^\[]*\[\@/g,"").replace(/[\[\]\@]*/g,""),n=0;n<t.length;n++)t[n].getAttribute(e)&&(i[i.length]=t[n]);return i},dtmlXMLLoaderObject.prototype._getAllNamedChilds=function(t,e){var i=[];_isKHTML&&(e=e.toUpperCase())
;for(var n=0;n<t.length;n++)for(var a=0;a<t[n].childNodes.length;a++)_isKHTML?t[n].childNodes[a].tagName&&t[n].childNodes[a].tagName.toUpperCase()==e&&(i[i.length]=t[n].childNodes[a]):t[n].childNodes[a].tagName==e&&(i[i.length]=t[n].childNodes[a]);return i},void 0===window.dhtmlxEvent&&(window.dhtmlxEvent=function(t,e,i){t.addEventListener?t.addEventListener(e,i,!1):t.attachEvent&&t.attachEvent("on"+e,i)}),dtmlXMLLoaderObject.prototype.xslDoc=null,
dtmlXMLLoaderObject.prototype.setXSLParamValue=function(t,e,i){i||(i=this.xslDoc),i.responseXML&&(i=i.responseXML);var n=this.doXPath("/xsl:stylesheet/xsl:variable[@name='"+t+"']",i,"http://www.w3.org/1999/XSL/Transform","single");n&&(n.firstChild.nodeValue=e)},dtmlXMLLoaderObject.prototype.doXSLTransToObject=function(t,e){t||(t=this.xslDoc),t.responseXML&&(t=t.responseXML),e||(e=this.xmlDoc),e.responseXML&&(e=e.responseXML);var i;if(_isIE){i=new ActiveXObject("Msxml2.DOMDocument.3.0");try{
e.transformNodeToObject(t,i)}catch(n){i=e.transformNode(t)}}else this.XSLProcessor||(this.XSLProcessor=new XSLTProcessor,this.XSLProcessor.importStylesheet(t)),i=this.XSLProcessor.transformToDocument(e);return i},dtmlXMLLoaderObject.prototype.doXSLTransToString=function(t,e){var i=this.doXSLTransToObject(t,e);return"string"==typeof i?i:this.doSerialization(i)},dtmlXMLLoaderObject.prototype.doSerialization=function(t){return t||(t=this.xmlDoc),t.responseXML&&(t=t.responseXML),
_isIE?t.xml:(new XMLSerializer).serializeToString(t)},window.dhtmlxEventable=function(obj){obj.attachEvent=function(t,e,i){return t="ev_"+t.toLowerCase(),this[t]||(this[t]=new this.eventCatcher(i||this)),t+":"+this[t].addEvent(e)},obj.callEvent=function(t,e){return t="ev_"+t.toLowerCase(),!this[t]||this[t].apply(this,e)},obj.checkEvent=function(t){return!!this["ev_"+t.toLowerCase()]},obj.eventCatcher=function(obj){var dhx_catch=[],z=function(){
for(var t=!0,e=0;e<dhx_catch.length;e++)if(dhx_catch[e]){var i=dhx_catch[e].apply(obj,arguments);t=t&&i}return t};return z.addEvent=function(ev){return"function"!=typeof ev&&(ev=eval(ev)),!!ev&&dhx_catch.push(ev)-1},z.removeEvent=function(t){dhx_catch[t]=null},z},obj.detachEvent=function(t){if(t){var e=t.split(":");this[e[0]].removeEvent(e[1])}},obj.detachAllEvents=function(){for(var t in this)0===t.indexOf("ev_")&&(this.detachEvent(t),this[t]=null)},obj=null},window.dhtmlx||(window.dhtmlx={}),
function(){function t(t,e){setTimeout(function(){if(t.box){var n=t.callback;i(!1),t.box.parentNode.removeChild(t.box),dhtmlx.callEvent("onAfterMessagePopup",[t.box]),c=t.box=null,n&&n(e)}},1)}function e(e){if(c){e=e||event;var i=e.which||event.keyCode,n=!1;if(dhtmlx.message.keyboard){if(13==i||32==i){var a=e.target||e.srcElement;scheduler._getClassName(a).indexOf("dhtmlx_popup_button")>-1&&a.click?a.click():(t(c,!0),n=!0)}27==i&&(t(c,!1),n=!0)}if(n)return e.preventDefault&&e.preventDefault(),
!(e.cancelBubble=!0)}else;}function i(t){i.cover||(i.cover=document.createElement("div"),i.cover.onkeydown=e,i.cover.className="dhx_modal_cover",document.body.appendChild(i.cover));document.body.scrollHeight;i.cover.style.display=t?"inline-block":"none"}function n(t,e,i){return"<div "+scheduler._waiAria.messageButtonAttrString(t)+"class='dhtmlx_popup_button dhtmlx_"+(i||t||"").toLowerCase().replace(/ /g,"_")+"_button' result='"+e+"' ><div>"+t+"</div></div>"}function a(t){
u.area||(u.area=document.createElement("div"),u.area.className="dhtmlx_message_area",u.area.style[u.position]="5px",document.body.appendChild(u.area)),u.hide(t.id);var e=document.createElement("div");return e.innerHTML="<div>"+t.text+"</div>",e.className="dhtmlx-info dhtmlx-"+t.type,e.onclick=function(){u.hide(t.id),t=null},scheduler._waiAria.messageInfoAttr(e),"bottom"==u.position&&u.area.firstChild?u.area.insertBefore(e,u.area.firstChild):u.area.appendChild(e),
t.expire>0&&(u.timers[t.id]=window.setTimeout(function(){u.hide(t.id)},t.expire)),u.pull[t.id]=e,e=null,t.id}function r(e,i,a){var r=document.createElement("div");r.className=" dhtmlx_modal_box dhtmlx-"+e.type,r.setAttribute("dhxbox",1);var s=scheduler.uid();scheduler._waiAria.messageModalAttr(r,s);var o="",d=!1;if(e.width&&(r.style.width=e.width),e.height&&(r.style.height=e.height),e.title&&(o+='<div class="dhtmlx_popup_title" id="'+s+'">'+e.title+"</div>",d=!0),
o+='<div class="dhtmlx_popup_text" '+(d?"":' id="'+s+'" ')+"><span>"+(e.content?"":e.text)+'</span></div><div  class="dhtmlx_popup_controls">',i){var l=e.ok||scheduler.locale.labels.message_ok;void 0===l&&(l="OK"),o+=n(l,!0,"ok")}if(a){var _=e.cancel||scheduler.locale.labels.message_cancel;void 0===_&&(_="Cancel"),o+=n(_,!1,"cancel")}if(e.buttons)for(var h=0;h<e.buttons.length;h++)o+=n(e.buttons[h],h);if(o+="</div>",r.innerHTML=o,e.content){var u=e.content
;"string"==typeof u&&(u=document.getElementById(u)),"none"==u.style.display&&(u.style.display=""),r.childNodes[e.title?1:0].appendChild(u)}return r.onclick=function(i){i=i||event;var n=i.target||i.srcElement,a=scheduler._getClassName(n);if(a||(n=n.parentNode),a=scheduler._getClassName(n),"dhtmlx_popup_button"==a.split(" ")[0]){var r=n.getAttribute("result");r="true"==r||"false"!=r&&r,t(e,r)}},e.box=r,c=e,r}function s(t,n,a){var s=t.tagName?t:r(t,n,a);t.hidden||i(!0),document.body.appendChild(s)
;var o=Math.abs(Math.floor(((window.innerWidth||document.documentElement.offsetWidth)-s.offsetWidth)/2)),d=Math.abs(Math.floor(((window.innerHeight||document.documentElement.offsetHeight)-s.offsetHeight)/2));return"top"==t.position?s.style.top="-3px":s.style.top=d+"px",s.style.left=o+"px",s.onkeydown=e,dhtmlx.modalbox.focus(s),t.hidden&&dhtmlx.modalbox.hide(s),dhtmlx.callEvent("onMessagePopup",[s]),s}function o(t){return s(t,!0,!1)}function d(t){return s(t,!0,!0)}function l(t){return s(t)}
function _(t,e,i){return"object"!=typeof t&&("function"==typeof e&&(i=e,e=""),t={text:t,type:e,callback:i}),t}function h(t,e,i,n){return"object"!=typeof t&&(t={text:t,type:e,expire:i,id:n}),t.id=t.id||u.uid(),t.expire=t.expire||u.expire,t}var c=null;document.attachEvent?document.attachEvent("onkeydown",e):document.addEventListener("keydown",e,!0),dhtmlx.alert=function(){var t=_.apply(this,arguments);return t.type=t.type||"confirm",o(t)},dhtmlx.confirm=function(){var t=_.apply(this,arguments)
;return t.type=t.type||"alert",d(t)},dhtmlx.modalbox=function(){var t=_.apply(this,arguments);return t.type=t.type||"alert",l(t)},dhtmlx.modalbox.hide=function(t){for(;t&&t.getAttribute&&!t.getAttribute("dhxbox");)t=t.parentNode;t&&(t.parentNode.removeChild(t),i(!1))},dhtmlx.modalbox.focus=function(t){setTimeout(function(){var e=scheduler._getFocusableNodes(t);e.length&&e[0].focus&&e[0].focus()},1)};var u=dhtmlx.message=function(t,e,i,n){switch(t=h.apply(this,arguments),t.type=t.type||"info",
t.type.split("-")[0]){case"alert":return o(t);case"confirm":return d(t);case"modalbox":return l(t);default:return a(t)}};u.seed=(new Date).valueOf(),u.uid=function(){return u.seed++},u.expire=4e3,u.keyboard=!0,u.position="top",u.pull={},u.timers={},u.hideAll=function(){for(var t in u.pull)u.hide(t)},u.hide=function(t){var e=u.pull[t];e&&e.parentNode&&(window.setTimeout(function(){e.parentNode.removeChild(e),e=null},2e3),e.className+=" hidden",u.timers[t]&&window.clearTimeout(u.timers[t]),
delete u.pull[t])}}(),dhtmlx.attachEvent||dhtmlxEventable(dhtmlx);var dataProcessor=window.dataProcessor=function(t){return this.serverProcessor=t,this.action_param="!nativeeditor_status",this.object=null,this.updatedRows=[],this.autoUpdate=!0,this.updateMode="cell",this._tMode="GET",this._headers=null,this._payload=null,this.post_delim="_",this._waitMode=0,this._in_progress={},this._invalid={},this.mandatoryFields=[],this.messages=[],this.styles={updated:"font-weight:bold;",
inserted:"font-weight:bold;",deleted:"text-decoration : line-through;",invalid:"background-color:FFE0E0;",invalid_cell:"border-bottom:2px solid red;",error:"color:red;",clear:"font-weight:normal;text-decoration:none;"},this.enableUTFencoding(!0),dhtmlxEventable(this),this};dataProcessor.prototype={setTransactionMode:function(t,e){function i(t,e){var i=e.toLowerCase();for(var n in t)if(n.toLowerCase()===i)return!0;return!1}"object"==typeof t?(this._tMode=t.mode||this._tMode,
void 0!==t.headers&&(this._headers=t.headers),void 0!==t.payload&&(this._payload=t.payload)):(this._tMode=t,this._tSend=e),"REST"==this._tMode&&(this._tSend=!1,this._endnm=!0);var n="Content-Type";"JSON"==this._tMode&&(this._tSend=!1,this._endnm=!0,this._headers=this._headers||{},this._headers[n]="application/json"),this._headers&&!i(this._headers,n)&&(this._headers[n]="application/x-www-form-urlencoded")},escape:function(t){return this._utf?encodeURIComponent(t):escape(t)},
enableUTFencoding:function(t){this._utf=!!t},setDataColumns:function(t){this._columns="string"==typeof t?t.split(","):t},getSyncState:function(){return!this.updatedRows.length},enableDataNames:function(t){this._endnm=!!t},enablePartialDataSend:function(t){this._changed=!!t},setUpdateMode:function(t,e){this.autoUpdate="cell"==t,this.updateMode=t,this.dnd=e},ignore:function(t,e){this._silent_mode=!0,t.call(e||window),this._silent_mode=!1},setUpdated:function(t,e,i){if(!this._silent_mode){
var n=this.findRow(t);i=i||"updated";var a=this.obj.getUserData(t,this.action_param);a&&"updated"==i&&(i=a),e?(this.set_invalid(t,!1),this.updatedRows[n]=t,this.obj.setUserData(t,this.action_param,i),this._in_progress[t]&&(this._in_progress[t]="wait")):this.is_invalid(t)||(this.updatedRows.splice(n,1),this.obj.setUserData(t,this.action_param,"")),e||this._clearUpdateFlag(t),this.markRow(t,e,i),e&&this.autoUpdate&&this.sendData(t)}},_clearUpdateFlag:function(t){},markRow:function(t,e,i){
var n="",a=this.is_invalid(t);if(a&&(n=this.styles[a],e=!0),this.callEvent("onRowMark",[t,e,i,a])&&(n=this.styles[e?i:"clear"]+n,this.obj[this._methods[0]](t,n),a&&a.details)){n+=this.styles[a+"_cell"];for(var r=0;r<a.details.length;r++)a.details[r]&&this.obj[this._methods[1]](t,r,n)}},getState:function(t){return this.obj.getUserData(t,this.action_param)},is_invalid:function(t){return this._invalid[t]},set_invalid:function(t,e,i){i&&(e={value:e,details:i,toString:function(){
return this.value.toString()}}),this._invalid[t]=e},checkBeforeUpdate:function(t){return!0},sendData:function(t){if(!this._waitMode||"tree"!=this.obj.mytype&&!this.obj._h2){if(this.obj.editStop&&this.obj.editStop(),void 0===t||this._tSend)return this.sendAllData();if(this._in_progress[t])return!1;if(this.messages=[],!this.checkBeforeUpdate(t)&&this.callEvent("onValidationError",[t,this.messages]))return!1;this._beforeSendData(this._getRowData(t),t)}},_beforeSendData:function(t,e){
if(!this.callEvent("onBeforeUpdate",[e,this.getState(e),t]))return!1;this._sendData(t,e)},serialize:function(t,e){if("string"==typeof t)return t;if(void 0!==e)return this.serialize_one(t,"");var i=[],n=[];for(var a in t)t.hasOwnProperty(a)&&(i.push(this.serialize_one(t[a],a+this.post_delim)),n.push(a));return i.push("ids="+this.escape(n.join(","))),(scheduler.security_key||dhtmlx.security_key)&&i.push("dhx_security="+(scheduler.security_key||dhtmlx.security_key)),i.join("&")},
serialize_one:function(t,e){if("string"==typeof t)return t;var i=[];for(var n in t)if(t.hasOwnProperty(n)){if(("id"==n||n==this.action_param)&&"REST"==this._tMode)continue;i.push(this.escape((e||"")+n)+"="+this.escape(t[n]))}return i.join("&")},_applyPayload:function(t){var e=this.obj.$ajax;if(this._payload)for(var i in this._payload)t=t+e.urlSeparator(t)+this.escape(i)+"="+this.escape(this._payload[i]);return t},_sendData:function(t,e){if(t){
if(!this.callEvent("onBeforeDataSending",e?[e,this.getState(e),t]:[null,null,t]))return!1;e&&(this._in_progress[e]=(new Date).valueOf());var i=this,n=function(n){var a=[];if(e)a.push(e);else if(t)for(var r in t)a.push(r);return i.afterUpdate(i,n,a)},a=this.obj.$ajax,r=this.serverProcessor+(this._user?a.urlSeparator(this.serverProcessor)+["dhx_user="+this._user,"dhx_version="+this.obj.getUserData(0,"version")].join("&"):""),s=this._applyPayload(r);if("GET"==this._tMode)a.query({
url:s+a.urlSeparator(s)+this.serialize(t,e),method:"GET",callback:n,headers:this._headers});else if("POST"==this._tMode)a.query({url:s,method:"POST",headers:this._headers,data:this.serialize(t,e),callback:n});else if("JSON"==this._tMode){var o=t[this.action_param],d={};for(var l in t)d[l]=t[l];delete d[this.action_param],delete d.id,delete d.gr_id,a.query({url:s,method:"POST",headers:this._headers,callback:n,data:JSON.stringify({id:e,action:o,data:d})})}else if("REST"==this._tMode){
var _=this.getState(e),h=r.replace(/(\&|\?)editing\=true/,""),d="",c="post";"inserted"==_?d=this.serialize(t,e):"deleted"==_?(c="DELETE",h=h+("/"==h.slice(-1)?"":"/")+e):(c="PUT",d=this.serialize(t,e),h=h+("/"==h.slice(-1)?"":"/")+e),h=this._applyPayload(h),a.query({url:h,method:c,headers:this._headers,data:d,callback:n})}this._waitMode++}},sendAllData:function(){if(this.updatedRows.length){this.messages=[]
;for(var t=!0,e=0;e<this.updatedRows.length;e++)t&=this.checkBeforeUpdate(this.updatedRows[e]);if(!t&&!this.callEvent("onValidationError",["",this.messages]))return!1;if(this._tSend)this._sendData(this._getAllData());else for(var e=0;e<this.updatedRows.length;e++)if(!this._in_progress[this.updatedRows[e]]){if(this.is_invalid(this.updatedRows[e]))continue;if(this._beforeSendData(this._getRowData(this.updatedRows[e]),this.updatedRows[e]),
this._waitMode&&("tree"==this.obj.mytype||this.obj._h2))return}}},_getAllData:function(t){for(var e={},i=!1,n=0;n<this.updatedRows.length;n++){var a=this.updatedRows[n];if(!this._in_progress[a]&&!this.is_invalid(a)){var r=this._getRowData(a);this.callEvent("onBeforeUpdate",[a,this.getState(a),r])&&(e[a]=r,i=!0,this._in_progress[a]=(new Date).valueOf())}}return i?e:null},setVerificator:function(t,e){this.mandatoryFields[t]=e||function(t){return""!==t}},clearVerificator:function(t){
this.mandatoryFields[t]=!1},findRow:function(t){var e=0;for(e=0;e<this.updatedRows.length&&t!=this.updatedRows[e];e++);return e},defineAction:function(t,e){this._uActions||(this._uActions=[]),this._uActions[t]=e},afterUpdateCallback:function(t,e,i,n){var a=t,r="error"!=i&&"invalid"!=i;if(r||this.set_invalid(t,i),this._uActions&&this._uActions[i]&&!this._uActions[i](n))return delete this._in_progress[a];"wait"!=this._in_progress[a]&&this.setUpdated(t,!1);var s=t;switch(i){case"inserted":
case"insert":e!=t&&(this.setUpdated(t,!1),this.obj[this._methods[2]](t,e),t=e);break;case"delete":case"deleted":return this.obj.setUserData(t,this.action_param,"true_deleted"),this.obj[this._methods[3]](t,e),delete this._in_progress[a],this.callEvent("onAfterUpdate",[t,i,e,n])}"wait"!=this._in_progress[a]?(r&&this.obj.setUserData(t,this.action_param,""),delete this._in_progress[a]):(delete this._in_progress[a],this.setUpdated(e,!0,this.obj.getUserData(t,this.action_param))),
this.callEvent("onAfterUpdate",[s,i,e,n])},_errorResponse:function(t,e){return this.obj&&this.obj.callEvent&&this.obj.callEvent("onSaveError",[e,t.xmlDoc]),this.cleanUpdate(e)},afterUpdate:function(t,e,i){var n=this.obj.$ajax;if(200!==e.xmlDoc.status)return void this._errorResponse(e,i);if(window.JSON){var a;try{a=JSON.parse(e.xmlDoc.responseText)}catch(t){e.xmlDoc.responseText.length||(a={})}if(a){var r=a.action||this.getState(i)||"updated",s=a.sid||i[0],o=a.tid||i[0]
;return t.afterUpdateCallback(s,o,r,a),void t.finalizeUpdate()}}var d=n.xmltop("data",e.xmlDoc);if(!d)return this._errorResponse(e,i);var l=n.xpath("//data/action",d);l.length||this._errorResponse(e,i);for(var _=0;_<l.length;_++){var h=l[_],r=h.getAttribute("type"),s=h.getAttribute("sid"),o=h.getAttribute("tid");t.afterUpdateCallback(s,o,r,h)}t.finalizeUpdate()},cleanUpdate:function(t){if(t)for(var e=0;e<t.length;e++)delete this._in_progress[t[e]]},finalizeUpdate:function(){
this._waitMode&&this._waitMode--,("tree"==this.obj.mytype||this.obj._h2)&&this.updatedRows.length&&this.sendData(),this.callEvent("onAfterUpdateFinish",[]),this.updatedRows.length||this.callEvent("onFullSync",[])},init:function(t){this.obj=t,this.obj._dp_init&&this.obj._dp_init(this)},setOnAfterUpdate:function(t){this.attachEvent("onAfterUpdate",t)},enableDebug:function(t){},setOnBeforeUpdateHandler:function(t){this.attachEvent("onBeforeDataSending",t)},setAutoUpdate:function(t,e){t=t||2e3,
this._user=e||(new Date).valueOf(),this._need_update=!1,this._update_busy=!1,this.attachEvent("onAfterUpdate",function(t,e,i,n){this.afterAutoUpdate(t,e,i,n)}),this.attachEvent("onFullSync",function(){this.fullSync()});var i=this;window.setInterval(function(){i.loadUpdate()},t)},afterAutoUpdate:function(t,e,i,n){return"collision"!=e||(this._need_update=!0,!1)},fullSync:function(){return this._need_update&&(this._need_update=!1,this.loadUpdate()),!0},getUpdates:function(t,e){var i=this.obj.$ajax
;if(this._update_busy)return!1;this._update_busy=!0,i.get(t,e)},_v:function(t){return t.firstChild?t.firstChild.nodeValue:""},_a:function(t){for(var e=[],i=0;i<t.length;i++)e[i]=this._v(t[i]);return e},loadUpdate:function(){var t=this.obj.$ajax,e=this,i=this.obj.getUserData(0,"version"),n=this.serverProcessor+t.urlSeparator(this.serverProcessor)+["dhx_user="+this._user,"dhx_version="+i].join("&");n=n.replace("editing=true&",""),this.getUpdates(n,function(i){var n=t.xpath("//userdata",i)
;e.obj.setUserData(0,"version",e._v(n[0]));var a=t.xpath("//update",i);if(a.length){e._silent_mode=!0;for(var r=0;r<a.length;r++){var s=a[r].getAttribute("status"),o=a[r].getAttribute("id"),d=a[r].getAttribute("parent");switch(s){case"inserted":e.callEvent("insertCallback",[a[r],o,d]);break;case"updated":e.callEvent("updateCallback",[a[r],o,d]);break;case"deleted":e.callEvent("deleteCallback",[a[r],o,d])}}e._silent_mode=!1}e._update_busy=!1,e=null})}},
window.dataProcessor&&!dataProcessor.prototype.init_original&&(dataProcessor.prototype.init_original=dataProcessor.prototype.init,dataProcessor.prototype.init=function(t){this.init_original(t),t._dataprocessor=this,this.setTransactionMode("POST",!0),this.serverProcessor+=(-1!=this.serverProcessor.indexOf("?")?"&":"?")+"editing=true"}),dhtmlxError.catchError("LoadXML",function(t,e,i){var n=i[0].responseText;switch(scheduler.config.ajax_error){case"alert":window.alert(n);break;case"console":
window.console.log(n)}});var Scheduler={_seed:0};Scheduler.plugin=function(t){this._schedulerPlugins.push(t),t(window.scheduler)},Scheduler._schedulerPlugins=[],Scheduler.getSchedulerInstance=function(){function t(t){var e=document.createElement("div");return(t||"").split(" ").forEach(function(t){e.classList.add(t)}),e}function e(t){var e;if(t.view)switch(t.view){case"today":case"next":case"prev":e=p.builtInButton;break;case"date":e=p.date;break;case"spacer":e=p.spacer;break;case"button":
e=p.button;break;case"minicalendar":e=p.minicalendarButton;break;default:e=p.view}else t.rows?e=p.rows_container:t.cols&&(e=p.row);return e}function i(t){var i=e(t);if(i){var n=i(t);if(t.css&&n.classList.add(t.css),t.width){var a=t.width;a===1*a&&(a+="px"),n.style.width=a}if(t.height){var a=t.height;a===1*a&&(a+="px"),n.style.height=a}if(t.click&&n.addEventListener("click",t.click),t.html&&(n.innerHTML=t.html),t.align){var a="";"right"==t.align?a="flex-end":"left"==t.align&&(a="flex-start"),
n.style.justifyContent=a}return n}}function n(t){return"string"==typeof t&&(t={view:t}),t.view||t.rows||t.cols||(t.view="button"),t}function a(t){var e,r=document.createDocumentFragment();e=Array.isArray(t)?t:[t];for(var s=0;s<e.length;s++){var o=n(e[s]);if("day"===o.view&&e[s+1]){var d=n(e[s+1]);"week"!==d.view&&"month"!==d.view||(o.$firstTab=!0)}if("month"===o.view&&e[s-1]){var d=n(e[s-1]);"week"!==d.view&&"day"!==d.view||(o.$lastTab=!0)}var l=i(o);r.appendChild(l),
(o.cols||o.rows)&&l.appendChild(a(o.cols||o.rows))}return r}function r(t){return!!(t.querySelector(".dhx_cal_header")&&t.querySelector(".dhx_cal_data")&&t.querySelector(".dhx_cal_navline"))}function s(t){var e=["day","week","month"],i=["date"],n=["prev","today","next"];if(t.matrix)for(var a in t.matrix)e.push(a);if(t._props)for(var a in t._props)e.push(a);if(t._grid&&t._grid.names)for(var a in t._grid.names)e.push(a);return["map","agenda","week_agenda","year"].forEach(function(i){
t[i+"_view"]&&e.push(i)}),e.concat(i).concat(n)}function o(t){return Array.isArray?Array.isArray(t):t&&void 0!==t.length&&t.pop&&t.push}function d(t){return t&&"object"==typeof t&&"function String() { [native code] }"===Function.prototype.toString.call(t.constructor)}function l(t){return t&&"object"==typeof t&&"function Number() { [native code] }"===Function.prototype.toString.call(t.constructor)}function _(t){
return t&&"object"==typeof t&&"function Boolean() { [native code] }"===Function.prototype.toString.call(t.constructor)}function h(t){return!(!t||"object"!=typeof t)&&!!(t.getFullYear&&t.getMonth&&t.getDate)}function c(){if(void 0===A){var t=document.createElement("div");t.style.position="absolute",t.style.left="-9999px",t.style.top="-9999px",t.innerHTML="<div class='dhx_cal_container'><div class='dhx_cal_scale_placeholder'></div><div>",document.body.appendChild(t)
;var e=window.getComputedStyle(t.querySelector(".dhx_cal_scale_placeholder")),i=e.getPropertyValue("position");A="absolute"===i,setTimeout(function(){A=null},500)}return A}function u(){if(g._is_material_skin())return!0;if(void 0!==S)return S;var t=document.createElement("div");t.style.position="absolute",t.style.left="-9999px",t.style.top="-9999px",t.innerHTML="<div class='dhx_cal_container'><div class='dhx_cal_data'><div class='dhx_cal_event'><div class='dhx_body'></div></div><div>",
document.body.appendChild(t);var e=window.getComputedStyle(t.querySelector(".dhx_body")),i=e.getPropertyValue("box-sizing");document.body.removeChild(t),(S=!("border-box"!==i))||setTimeout(function(){S=void 0},1e3)}function f(){if(!g._is_material_skin()&&!g._border_box_events()){var t=S;S=void 0,A=void 0;t!==u()&&g.$container&&g.getState().mode&&g.setCurrentView()}}var g={version:"5.3.11"},v={agenda:"https://docs.dhtmlx.com/scheduler/agenda_view.html",
grid:"https://docs.dhtmlx.com/scheduler/grid_view.html",map:"https://docs.dhtmlx.com/scheduler/map_view.html",unit:"https://docs.dhtmlx.com/scheduler/units_view.html",timeline:"https://docs.dhtmlx.com/scheduler/timeline_view.html",week_agenda:"https://docs.dhtmlx.com/scheduler/weekagenda_view.html",year:"https://docs.dhtmlx.com/scheduler/year_view.html",anythingElse:"https://docs.dhtmlx.com/scheduler/views.html"},m={agenda:"ext/dhtmlxscheduler_agenda_view.js",
grid:"ext/dhtmlxscheduler_grid_view.js",map:"ext/dhtmlxscheduler_map_view.js",unit:"ext/dhtmlxscheduler_units.js",timeline:"ext/dhtmlxscheduler_timeline.js, ext/dhtmlxscheduler_treetimeline.js, ext/dhtmlxscheduler_daytimeline.js",week_agenda:"ext/dhtmlxscheduler_week_agenda.js",year:"ext/dhtmlxscheduler_year_view.js",limit:"ext/dhtmlxscheduler_limit.js"};g._commonErrorMessages={unknownView:function(t){var e="Related docs: "+(v[t]||v.anythingElse),i=m[t]?"You're probably missing "+m[t]+".":""
;return"`"+t+"` view is not defined. \nPlease check parameters you pass to `scheduler.init` or `scheduler.setCurrentView` in your code and ensure you've imported appropriate extensions. \n"+e+"\n"+(i?i+"\n":"")},collapsedContainer:function(t){
return"Scheduler container height is set to *100%* but the rendered height is zero and the scheduler is not visible. \nMake sure that the container has some initial height or use different units. For example:\n<div id='scheduler_here' class='dhx_cal_container' style='width:100%; height:600px;'> \n"}},g.createTimelineView=function(){throw new Error("scheduler.createTimelineView is not implemented. Be sure to add the required extension: "+m.timeline+"\nRelated docs: "+v.timeline)},
g.createUnitsView=function(){throw new Error("scheduler.createUnitsView is not implemented. Be sure to add the required extension: "+m.unit+"\nRelated docs: "+v.unit)},g.createGridView=function(){throw new Error("scheduler.createGridView is not implemented. Be sure to add the required extension: "+m.grid+"\nRelated docs: "+v.grid)},g.addMarkedTimespan=function(){
throw new Error("scheduler.addMarkedTimespan is not implemented. Be sure to add the required extension: ext/dhtmlxscheduler_limit.js\nRelated docs: https://docs.dhtmlx.com/scheduler/limits.html")},g.renderCalendar=function(){throw new Error("scheduler.renderCalendar is not implemented. Be sure to add the required extension: ext/dhtmlxscheduler_minical.js\nhttps://docs.dhtmlx.com/scheduler/minicalendar.html")},g.exportToPNG=function(){
throw new Error(["scheduler.exportToPNG is not implemented.","This feature requires an additional module, be sure to check the related doc here https://docs.dhtmlx.com/scheduler/png.html","Licensing info: https://dhtmlx.com/docs/products/dhtmlxScheduler/export.shtml"].join("\n"))},g.exportToPDF=function(){
throw new Error(["scheduler.exportToPDF is not implemented.","This feature requires an additional module, be sure to check the related doc here https://docs.dhtmlx.com/scheduler/pdf.html","Licensing info: https://dhtmlx.com/docs/products/dhtmlxScheduler/export.shtml"].join("\n"))},dhtmlxEventable(g);var p={rows_container:function(){return t("dhx_cal_navbar_rows_container")},row:function(){return t("dhx_cal_navbar_row")},view:function(e){var i=t("dhx_cal_tab")
;return i.setAttribute("name",e.view+"_tab"),i.setAttribute("data-viewname",e.view),g.config.fix_tab_position&&(e.$firstTab?i.classList.add("dhx_cal_tab_first"):e.$lastTab?i.classList.add("dhx_cal_tab_last"):"week"!==e.view&&i.classList.add("dhx_cal_tab_standalone")),i},date:function(){return t("dhx_cal_date")},button:function(e){return t("dhx_cal_nav_button dhx_cal_nav_button_custom dhx_cal_tab")},builtInButton:function(e){return t("dhx_cal_"+e.view+"_button dhx_cal_nav_button")},
spacer:function(){return t("dhx_cal_line_spacer")},minicalendarButton:function(e){var i=t("dhx_minical_icon");return e.click||(i.onclick=function(){g.isCalendarVisible()?g.destroyCalendar():g.renderCalendar({position:this,date:g.getState().date,navigation:!0,handler:function(t,e){g.setCurrentView(t),g.destroyCalendar()}})}),i},html_element:function(e){return t("dhx_cal_nav_content")}};g._init_nav_bar=function(t){var e=this.$container.querySelector(".dhx_cal_navline")
;return e||(e=document.createElement("div"),e.className="dhx_cal_navline dhx_cal_navline_flex",g._update_nav_bar(t,e),e)};var x=null,b=null;g._update_nav_bar=function(t,e){if(t){var i=!1,n=!1,r=t.height||g.xy.nav_height;null!==b&&b===r||(i=!0),x&&JSON.stringify(t)===x||(n=!0),i&&(g.xy.nav_height=r),n&&(e.innerHTML="",e.appendChild(a(t))),(i||n)&&(g._els=[],g.get_elements(),g.set_actions()),e.style.display=0===r?"none":"",b=r}},g._detachDomEvent=function(t,e,i){
t.removeEventListener?t.removeEventListener(e,i,!1):t.detachEvent&&t.detachEvent("on"+e,i)},g._init_once=function(){function t(t){for(var e=document.body;t&&t!=e;)t=t.parentNode;return!(e!=t)}function e(t){return{w:t.innerWidth||document.documentElement.clientWidth,h:t.innerHeight||document.documentElement.clientHeight}}function i(t,e){return t.w==e.w&&t.h==e.h}function n(n,a){var r,s=e(a);n.event(a,"resize",function(){clearTimeout(r),r=setTimeout(function(){if(t(n.$container)){var r=e(a)
;i(s,r)||(s=r,n.callEvent("onSchedulerResize",[])&&(n.updateView(),n.callEvent("onAfterSchedulerResize",[])))}},150)})}function a(t){var e=t.$container;"static"==window.getComputedStyle(e).getPropertyValue("position")&&(e.style.position="relative");var i=document.createElement("iframe");i.className="scheduler_container_resize_watcher",i.tabIndex=-1,t.config.wai_aria_attributes&&(i.setAttribute("role","none"),i.setAttribute("aria-hidden",!0)),e.appendChild(i),
i.contentWindow?n(t,i.contentWindow):(e.removeChild(i),n(t,window))}a(g),g._init_once=function(){}};var y={navbar:{render:function(t){return g._init_nav_bar(t)}},header:{render:function(t){var e=document.createElement("div");return e.className="dhx_cal_header",e}},dataArea:{render:function(t){var e=document.createElement("div");return e.className="dhx_cal_data",e}},html_element:{render:function(t){return t.html}}};g.init=function(t,e,i){if(e=e||g._currentDate(),i=i||"week",
this._obj&&this.unset_actions(),this._obj="string"==typeof t?document.getElementById(t):t,this.$container=this._obj,!this.$container.offsetHeight&&this.$container.offsetWidth&&"100%"===this.$container.style.height&&window.console.error(g._commonErrorMessages.collapsedContainer(),this.$container),this.config.wai_aria_attributes&&this.config.wai_aria_application_role&&this.$container.setAttribute("role","application"),this.config.header||r(this.$container)||(this.config.header=s(this),
console.log(["Required DOM elements are missing from the scheduler container and **scheduler.config.header** is not specified.","Using a default header configuration: ","scheduler.config.header = "+JSON.stringify(this.config.header,null,2),"Check this article for the details: https://docs.dhtmlx.com/scheduler/initialization.html"].join("\n"))),this.config.header)this.$container.innerHTML="",this.$container.classList.add("dhx_cal_container"),
this.config.header.height&&(this.xy.nav_height=this.config.header.height),this.$container.appendChild(y.navbar.render(this.config.header)),this.$container.appendChild(y.header.render()),
this.$container.appendChild(y.dataArea.render());else if(!r(this.$container))throw new Error(["Required DOM elements are missing from the scheduler container.","Be sure to either specify them manually in the markup: https://docs.dhtmlx.com/scheduler/initialization.html#initializingschedulerviamarkup","Or to use **scheduler.config.header** setting so they could be created automatically: https://docs.dhtmlx.com/scheduler/initialization.html#initializingschedulerviaheaderconfig"].join("\n"))
;this.config.rtl&&(this.$container.className+=" dhx_cal_container_rtl"),this._skin_init&&g._skin_init(),g.date.init(),this._scroll=!0,this._quirks=this.$env.isIE&&"BackCompat"==document.compatMode,this._quirks7=this.$env.isIE&&-1==navigator.appVersion.indexOf("MSIE 8"),this._els=[],this.get_elements(),this.init_templates(),this.set_actions(),this._init_once(),this._init_touch_events(),this.set_sizes(),g.callEvent("onSchedulerReady",[]),this.setCurrentView(e,i)},g.xy={min_event_height:40,
scale_width:50,scroll_width:18,scale_height:20,month_scale_height:20,menu_width:25,margin_top:0,margin_left:0,editor_width:140,month_head_height:22,event_header_height:14},g.keys={edit_save:13,edit_cancel:27},g.bind=function(t,e){return t.bind?t.bind(e):function(){return t.apply(e,arguments)}},g.set_sizes=function(){
var t=this._x=this._obj.clientWidth-this.xy.margin_left,e=this._y=this._obj.clientHeight-this.xy.margin_top,i=this._table_view?0:this.xy.scale_width+this.xy.scroll_width,n=this._table_view?-1:this.xy.scale_width,a=this.$container.querySelector(".dhx_cal_scale_placeholder");g._is_material_skin()?(a||(a=document.createElement("div"),a.className="dhx_cal_scale_placeholder",this.$container.insertBefore(a,this._els.dhx_cal_header[0])),a.style.display="block",
this.set_xy(a,t,this.xy.scale_height+1,0,this.xy.nav_height+(this._quirks?-1:1))):a&&a.parentNode.removeChild(a),this._lightbox&&(g.$container.offsetWidth<1200||this._setLbPosition(document.querySelector(".dhx_cal_light"))),this.set_xy(this._els.dhx_cal_navline[0],t,this.xy.nav_height,0,0),this.set_xy(this._els.dhx_cal_header[0],t-i,this.xy.scale_height,n,this.xy.nav_height+(this._quirks?-1:1));var r=this._els.dhx_cal_navline[0].offsetHeight;r>0&&(this.xy.nav_height=r)
;var s=this.xy.scale_height+this.xy.nav_height+(this._quirks?-2:0);this.set_xy(this._els.dhx_cal_data[0],t,e-(s+2),0,s+2)},g.set_xy=function(t,e,i,n,a){var r="left";t.style.width=Math.max(0,e)+"px",t.style.height=Math.max(0,i)+"px",arguments.length>3&&(this.config.rtl&&(r="right"),t.style[r]=n+"px",t.style.top=a+"px")},g.get_elements=function(){for(var t=this._obj.getElementsByTagName("DIV"),e=0;e<t.length;e++){var i=g._getClassName(t[e]),n=t[e].getAttribute("name")||"";i&&(i=i.split(" ")[0]),
this._els[i]||(this._els[i]=[]),this._els[i].push(t[e]);var a=g.locale.labels[n||i];"string"!=typeof a&&n&&!t[e].innerHTML&&(a=n.split("_")[0]),a&&(this._waiAria.labelAttr(t[e],a),t[e].innerHTML=a)}},g.unset_actions=function(){for(var t in this._els)if(this._click[t])for(var e=0;e<this._els[t].length;e++)this._els[t][e].onclick=null;this._obj.onselectstart=null,this._obj.onmousemove=null,this._obj.onmousedown=null,this._obj.onmouseup=null,this._obj.ondblclick=null,this._obj.oncontextmenu=null},
g.set_actions=function(){for(var t in this._els)if(this._click[t])for(var e=0;e<this._els[t].length;e++)this._els[t][e].onclick=g._click[t];this._obj.onselectstart=function(t){return!1},this._obj.onmousemove=function(t){g._temp_touch_block||g._on_mouse_move(t||event)},this._obj.onmousedown=function(t){g._ignore_next_click||g._on_mouse_down(t||event)},this._obj.onmouseup=function(t){g._ignore_next_click||g._on_mouse_up(t||event)},this._obj.ondblclick=function(t){g._on_dbl_click(t||event)},
this._obj.oncontextmenu=function(t){var e=t||event,i=e.target||e.srcElement;return g.callEvent("onContextMenu",[g._locate_event(i),e])}},g.select=function(t){this._select_id!=t&&(g._close_not_saved(),this.editStop(!1),this.unselect(),this._select_id=t,this.updateEvent(t),this.callEvent("onEventSelected",[t]))},g.unselect=function(t){if(!t||t==this._select_id){var e=this._select_id;this._select_id=null,e&&this.getEvent(e)&&this.updateEvent(e),this.callEvent("onEventUnselected",[e])}},
g.getState=function(){return{mode:this._mode,date:new Date(this._date),min_date:new Date(this._min_date),max_date:new Date(this._max_date),editor_id:this._edit_id,lightbox_id:this._lightbox_id,new_event:this._new_event,select_id:this._select_id,expanded:this.expanded,drag_id:this._drag_id,drag_mode:this._drag_mode}},g._click={dhx_cal_data:function(t){if(g._ignore_next_click)return t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,g._ignore_next_click=!1,!1
;var e=t?t.target:event.srcElement,i=g._locate_event(e);if(t=t||event,i){if(!g.callEvent("onClick",[i,t])||g.config.readonly)return}else g.callEvent("onEmptyClick",[g.getActionData(t).date,t]);if(i&&g.config.select){g.select(i);var n=g._getClassName(e);-1!=n.indexOf("_icon")&&g._click.buttons[n.split(" ")[1].replace("icon_","")](i)}else g._close_not_saved(),(new Date).valueOf()-(g._new_event||0)>500&&g.unselect()},dhx_cal_prev_button:function(){g._click.dhx_cal_next_button(0,-1)},
dhx_cal_next_button:function(t,e){var i=1;g.config.rtl&&(e=-e,i=-i),g.setCurrentView(g.date.add(g.date[g._mode+"_start"](new Date(g._date)),e||i,g._mode))},dhx_cal_today_button:function(){g.callEvent("onBeforeTodayDisplayed",[])&&g.setCurrentView(g._currentDate())},dhx_cal_tab:function(){var t=this.getAttribute("name"),e=t.substring(0,t.search("_tab"));g.setCurrentView(g._date,e)},buttons:{delete:function(t){var e=g.locale.labels.confirm_deleting
;g._dhtmlx_confirm(e,g.locale.labels.title_confirm_deleting,function(){g.deleteEvent(t)})},edit:function(t){g.edit(t)},save:function(t){g.editStop(!0)},details:function(t){g.showLightbox(t)},cancel:function(t){g.editStop(!1)}}},g._dhtmlx_confirm=function(t,e,i){if(!t)return i();var n={text:t};e&&(n.title=e),i&&(n.callback=function(t){t&&i()}),dhtmlx.confirm(n)},g.addEventNow=function(t,e,i){var n={};g._isObject(t)&&!g._isDate(t)&&(n=t,t=null)
;var a=6e4*(this.config.event_duration||this.config.time_step);t||(t=n.start_date||Math.round(g._currentDate().valueOf()/a)*a);var r=new Date(t);if(!e){var s=this.config.first_hour;s>r.getHours()&&(r.setHours(s),t=r.valueOf()),e=t.valueOf()+a}var o=new Date(e);r.valueOf()==o.valueOf()&&o.setTime(o.valueOf()+a),n.start_date=n.start_date||r,n.end_date=n.end_date||o,n.text=n.text||this.locale.labels.new_event,n.id=this._drag_id=n.id||this.uid(),this._drag_mode="new-size",this._loading=!0
;var d=this.addEvent(n);return this.callEvent("onEventCreated",[this._drag_id,i]),this._loading=!1,this._drag_event={},this._on_mouse_up(i),d},g._on_dbl_click=function(t,e){if(e=e||t.target||t.srcElement,!this.config.readonly){var i=g._getClassName(e).split(" ")[0];switch(i){case"dhx_scale_holder":case"dhx_scale_holder_now":case"dhx_month_body":case"dhx_wa_day_data":if(!g.config.dblclick_create)break;this.addEventNow(this.getActionData(t).date,null,t);break;case"dhx_cal_event":
case"dhx_wa_ev_body":case"dhx_agenda_line":case"dhx_grid_event":case"dhx_cal_event_line":case"dhx_cal_event_clear":var n=this._locate_event(e);if(!this.callEvent("onDblClick",[n,t]))return;this.config.details_on_dblclick||this._table_view||!this.getEvent(n)._timed||!this.config.select?this.showLightbox(n):this.edit(n);break;case"dhx_time_block":case"dhx_cal_container":return;default:var a=this["dblclick_"+i];if(a)a.call(this,t);else if(e.parentNode&&e!=this)return g._on_dbl_click(t,e.parentNode)
}}},g._get_column_index=function(t){var e=0;if(this._cols){for(var i=0,n=0;i+this._cols[n]<t&&n<this._cols.length;)i+=this._cols[n],n++;if(e=n+(this._cols[n]?(t-i)/this._cols[n]:0),this._ignores&&e>=this._cols.length)for(;e>=1&&this._ignores[Math.floor(e)];)e--}return e},g._week_indexes_from_pos=function(t){if(this._cols){var e=this._get_column_index(t.x);return t.x=Math.min(this._cols.length-1,Math.max(0,Math.ceil(e)-1)),
t.y=Math.max(0,Math.ceil(60*t.y/(this.config.time_step*this.config.hour_size_px))-1)+this.config.first_hour*(60/this.config.time_step),t}return t},g._mouse_coords=function(t){var e,i=document.body,n=document.documentElement;e=this.$env.isIE||!t.pageX&&!t.pageY?{x:t.clientX+(i.scrollLeft||n.scrollLeft||0)-i.clientLeft,y:t.clientY+(i.scrollTop||n.scrollTop||0)-i.clientTop}:{x:t.pageX,y:t.pageY},this.config.rtl&&this._colsS?(e.x=this.$container.querySelector(".dhx_cal_data").offsetWidth-e.x,
"month"!==this._mode&&(e.x-=this.xy.scale_width)):e.x-=this.$domHelpers.getAbsoluteLeft(this._obj)+(this._table_view?0:this.xy.scale_width);var a=this.$container.querySelector(".dhx_cal_data");e.y-=this.$domHelpers.getAbsoluteTop(a)-this._els.dhx_cal_data[0].scrollTop,e.ev=t;var r=this["mouse_"+this._mode];if(r)e=r.call(this,e);else if(this._table_view){var s=this._get_column_index(e.x);if(!this._cols||!this._colsS)return e;var o=0
;for(o=1;o<this._colsS.heights.length&&!(this._colsS.heights[o]>e.y);o++);e.y=Math.ceil(24*(Math.max(0,s)+7*Math.max(0,o-1))*60/this.config.time_step),(g._drag_mode||"month"==this._mode)&&(e.y=24*(Math.max(0,Math.ceil(s)-1)+7*Math.max(0,o-1))*60/this.config.time_step),"move"==this._drag_mode&&g._ignores_detected&&g.config.preserve_length&&(e._ignores=!0,
this._drag_event._event_length||(this._drag_event._event_length=this._get_real_event_length(this._drag_event.start_date,this._drag_event.end_date,{x_step:1,x_unit:"day"}))),e.x=0}else e=this._week_indexes_from_pos(e);return e.timestamp=+new Date,e},g._close_not_saved=function(){if((new Date).valueOf()-(g._new_event||0)>500&&g._edit_id){var t=g.locale.labels.confirm_closing;g._dhtmlx_confirm(t,g.locale.labels.title_confirm_closing,function(){g.editStop(g.config.positive_closing)}),
t&&(this._drag_id=this._drag_pos=this._drag_mode=null)}},g._correct_shift=function(t,e){return t-=6e4*(new Date(g._min_date).getTimezoneOffset()-new Date(t).getTimezoneOffset())*(e?-1:1)},g._is_pos_changed=function(t,e){function i(t,e,i){return!!(Math.abs(t-e)>i)}if(!t||!this._drag_pos)return!0;var n=5;return!!(this._drag_pos.has_moved||!this._drag_pos.timestamp||e.timestamp-this._drag_pos.timestamp>100||i(t.ev.clientX,e.ev.clientX,n)||i(t.ev.clientY,e.ev.clientY,n))},
g._correct_drag_start_date=function(t){var e;g.matrix&&(e=g.matrix[g._mode]),e=e||{x_step:1,x_unit:"day"},t=new Date(t);var i=1;return(e._start_correction||e._end_correction)&&(i=60*(e.last_hour||0)-(60*t.getHours()+t.getMinutes())||1),1*t+(g._get_fictional_event_length(t,i,e)-i)},g._correct_drag_end_date=function(t,e){var i;g.matrix&&(i=g.matrix[g._mode]),i=i||{x_step:1,x_unit:"day"};var n=1*t+g._get_fictional_event_length(t,e,i);return new Date(1*n-(g._get_fictional_event_length(n,-1,i,-1)+1))
},g._on_mouse_move=function(t){if(this._drag_mode){var e=this._mouse_coords(t);if(this._is_pos_changed(this._drag_pos,e)){var i,n;if(this._edit_id!=this._drag_id&&this._close_not_saved(),!this._drag_mode)return;var a=null;if(this._drag_pos&&!this._drag_pos.has_moved&&(a=this._drag_pos,a.has_moved=!0),this._drag_pos=e,this._drag_pos.has_moved=!0,"create"==this._drag_mode){if(a&&(e=a),this._close_not_saved(),this.unselect(this._select_id),this._loading=!0,i=this._get_date_from_pos(e).valueOf(),
!this._drag_start){return this.callEvent("onBeforeEventCreated",[t,this._drag_id])?(this._loading=!1,void(this._drag_start=i)):void(this._loading=!1)}n=i,this._drag_start;var r=new Date(this._drag_start),s=new Date(n);"day"!=this._mode&&"week"!=this._mode||r.getHours()!=s.getHours()||r.getMinutes()!=s.getMinutes()||(s=new Date(this._drag_start+1e3)),this._drag_id=this.uid(),this.addEvent(r,s,this.locale.labels.new_event,this._drag_id,e.fields),this.callEvent("onEventCreated",[this._drag_id,t]),
this._loading=!1,this._drag_mode="new-size"}var o,d=this.config.time_step,l=this.getEvent(this._drag_id);if(g.matrix&&(o=g.matrix[g._mode]),o=o||{x_step:1,x_unit:"day"},"move"==this._drag_mode)i=this._min_date.valueOf()+6e4*(e.y*this.config.time_step+24*e.x*60),!e.custom&&this._table_view&&(i+=1e3*this.date.time_part(l.start_date)),!this._table_view&&this._dragEventBody&&void 0===this._drag_event._move_event_shift&&(this._drag_event._move_event_shift=i-l.start_date),
this._drag_event._move_event_shift&&(i-=this._drag_event._move_event_shift),i=this._correct_shift(i),e._ignores&&this.config.preserve_length&&this._table_view?(i=g._correct_drag_start_date(i),n=g._correct_drag_end_date(i,this._drag_event._event_length)):n=l.end_date.valueOf()-(l.start_date.valueOf()-i);else{if(i=l.start_date.valueOf(),n=l.end_date.valueOf(),this._table_view){var _=this._min_date.valueOf()+e.y*this.config.time_step*6e4+(e.custom?0:864e5)
;if("month"==this._mode)if(_=this._correct_shift(_,!1),this._drag_from_start){var h=864e5;_<=g.date.date_part(new Date(n+h-1)).valueOf()&&(i=_-h)}else n=_;else this.config.preserve_length?e.resize_from_start?i=g._correct_drag_start_date(_):n=g._correct_drag_end_date(_,0):e.resize_from_start?i=_:n=_}else{var c=this.date.date_part(new Date(l.end_date.valueOf()-1)).valueOf(),u=new Date(c),f=this.config.first_hour,v=this.config.last_hour,m=60/d*(v-f);this.config.time_step=1
;var p=this._mouse_coords(t);this.config.time_step=d;var x=e.y*d*6e4,b=Math.min(e.y+1,m)*d*6e4,y=6e4*p.y;n=Math.abs(x-y)>Math.abs(b-y)?c+b:c+x,n+=6e4*(new Date(n).getTimezoneOffset()-u.getTimezoneOffset()),this._els.dhx_cal_data[0].style.cursor="s-resize","week"!=this._mode&&"day"!=this._mode||(n=this._correct_shift(n))}if("new-size"==this._drag_mode)if(n<=this._drag_start){var w=e.shift||(this._table_view&&!e.custom?864e5:0);i=n-(e.shift?0:w),n=this._drag_start+(w||6e4*d)
}else i=this._drag_start;else n<=i&&(n=i+6e4*d)}var D=new Date(n-1),E=new Date(i);if("move"==this._drag_mode&&g.config.limit_drag_out&&(+E<+g._min_date||+n>+g._max_date)){if(+l.start_date<+g._min_date||+l.end_date>+g._max_date)E=new Date(l.start_date),n=new Date(l.end_date);else{var A=n-E;+E<+g._min_date?(E=new Date(g._min_date),e._ignores&&this.config.preserve_length&&this._table_view?(E=new Date(g._correct_drag_start_date(E)),o._start_correction&&(E=new Date(E.valueOf()+o._start_correction)),
n=new Date(1*E+this._get_fictional_event_length(E,this._drag_event._event_length,o))):n=new Date(+E+A)):(n=new Date(g._max_date),e._ignores&&this.config.preserve_length&&this._table_view?(o._end_correction&&(n=new Date(n.valueOf()-o._end_correction)),n=new Date(1*n-this._get_fictional_event_length(n,0,o,!0)),E=new Date(1*n-this._get_fictional_event_length(n,this._drag_event._event_length,o,!0)),this._ignores_detected&&(E=g.date.add(E,o.x_step,o.x_unit),
n=new Date(1*n-this._get_fictional_event_length(n,0,o,!0)),n=g.date.add(n,o.x_step,o.x_unit))):E=new Date(+n-A))}var D=new Date(n-1)}if(!this._table_view&&this._dragEventBody&&!g.config.all_timed&&(!g._get_section_view()&&e.x!=this._get_event_sday({start_date:new Date(i),end_date:new Date(i)})||new Date(i).getHours()<this.config.first_hour)){var A=n-E;if("move"==this._drag_mode){var h=this._min_date.valueOf()+24*e.x*60*6e4;E=new Date(h),E.setHours(this.config.first_hour),
n=new Date(E.valueOf()+A),D=new Date(n-1)}}if(!this._table_view&&!g.config.all_timed&&(!g.getView()&&e.x!=this._get_event_sday({start_date:new Date(n),end_date:new Date(n)})||new Date(n).getHours()>=this.config.last_hour)){var A=n-E,h=this._min_date.valueOf()+24*e.x*60*6e4;n=g.date.date_part(new Date(h)),n.setHours(this.config.last_hour),D=new Date(n-1),"move"==this._drag_mode&&(E=new Date(+n-A))}
if(this._table_view||D.getDate()==E.getDate()&&D.getHours()<this.config.last_hour||g._allow_dnd)if(l.start_date=E,l.end_date=new Date(n),this.config.update_render){var S=g._els.dhx_cal_data[0].scrollTop;this.update_view(),g._els.dhx_cal_data[0].scrollTop=S}else this.updateEvent(this._drag_id);this._table_view&&this.for_rendered(this._drag_id,function(t){t.className+=" dhx_in_move dhx_cal_event_drag"}),this.callEvent("onEventDrag",[this._drag_id,this._drag_mode,t])}
}else if(g.checkEvent("onMouseMove")){var M=this._locate_event(t.target||t.srcElement);this.callEvent("onMouseMove",[M,t])}},g._on_mouse_down=function(t,e){if(2!=t.button&&!this.config.readonly&&!this._drag_mode){e=e||t.target||t.srcElement;var i=g._getClassName(e).split(" ")[0];switch(this.config.drag_event_body&&"dhx_body"==i&&e.parentNode&&-1===e.parentNode.className.indexOf("dhx_cal_select_menu")&&(i="dhx_event_move",this._dragEventBody=!0),i){case"dhx_cal_event_line":
case"dhx_cal_event_clear":this._table_view&&(this._drag_mode="move");break;case"dhx_event_move":case"dhx_wa_ev_body":this._drag_mode="move";break;case"dhx_event_resize":this._drag_mode="resize";g._getClassName(e).indexOf("dhx_event_resize_end")<0?g._drag_from_start=!0:g._drag_from_start=!1;break;case"dhx_scale_holder":case"dhx_scale_holder_now":case"dhx_month_body":case"dhx_matrix_cell":case"dhx_marked_timespan":this._drag_mode="create";break;case"":
if(e.parentNode)return g._on_mouse_down(t,e.parentNode);break;default:if((!g.checkEvent("onMouseDown")||g.callEvent("onMouseDown",[i,t]))&&e.parentNode&&e!=this&&"dhx_body"!=i)return g._on_mouse_down(t,e.parentNode);this._drag_mode=null,this._drag_id=null}if(this._drag_mode){var n=this._locate_event(e);if(this.config["drag_"+this._drag_mode]&&this.callEvent("onBeforeDrag",[n,this._drag_mode,t])){if(this._drag_id=n,
(this._edit_id!=this._drag_id||this._edit_id&&"create"==this._drag_mode)&&this._close_not_saved(),!this._drag_mode)return;this._drag_event=g._lame_clone(this.getEvent(this._drag_id)||{}),this._drag_pos=this._mouse_coords(t)}else this._drag_mode=this._drag_id=0}this._drag_start=null}},g._get_private_properties=function(t){var e={};for(var i in t)0===i.indexOf("_")&&(e[i]=!0);return e},g._clear_temporary_properties=function(t,e){
var i=this._get_private_properties(t),n=this._get_private_properties(e);for(var a in n)i[a]||delete e[a]},g._on_mouse_up=function(t){if(!t||2!=t.button||!this._mobile){if(this._drag_mode&&this._drag_id){this._els.dhx_cal_data[0].style.cursor="default";var e=this._drag_id,i=this._drag_mode,n=!this._drag_pos||this._drag_pos.has_moved;delete this._drag_event._move_event_shift;var a=this.getEvent(this._drag_id)
;if(n&&(this._drag_event._dhx_changed||!this._drag_event.start_date||a.start_date.valueOf()!=this._drag_event.start_date.valueOf()||a.end_date.valueOf()!=this._drag_event.end_date.valueOf())){var r="new-size"==this._drag_mode;if(this.callEvent("onBeforeEventChanged",[a,t,r,this._drag_event]))if(this._drag_id=this._drag_mode=null,r&&this.config.edit_on_create){if(this.unselect(),this._new_event=new Date,
this._table_view||this.config.details_on_create||!this.config.select||!this.isOneDayEvent(this.getEvent(e)))return g.callEvent("onDragEnd",[e,i,t]),this.showLightbox(e);this._drag_pos=!0,this._select_id=this._edit_id=e}else this._new_event||this.callEvent(r?"onEventAdded":"onEventChanged",[e,this.getEvent(e)]);else r?this.deleteEvent(a.id,!0):(this._drag_event._dhx_changed=!1,this._clear_temporary_properties(a,this._drag_event),g._lame_copy(a,this._drag_event),this.updateEvent(a.id))}
this._drag_pos&&(this._drag_pos.has_moved||!0===this._drag_pos)&&(this._drag_id=this._drag_mode=null,this.render_view_data()),g.callEvent("onDragEnd",[e,i,t])}this._drag_id=null,this._drag_mode=null,this._drag_pos=null}},g._trigger_dyn_loading=function(){return!(!this._load_mode||!this._load())&&(this._render_wait=!0,!0)},g.update_view=function(){this._reset_ignores(),this._update_nav_bar(this.config.header,this.$container.querySelector(".dhx_cal_navline"));var t=this[this._mode+"_view"]
;if(t?t(!0):this._reset_scale(),this._trigger_dyn_loading())return!0;this.render_view_data()},g.isViewExists=function(t){return!!(g[t+"_view"]||g.date[t+"_start"]&&g.templates[t+"_date"]&&g.templates[t+"_scale_date"])},g._set_aria_buttons_attrs=function(){for(var t=["dhx_cal_next_button","dhx_cal_prev_button","dhx_cal_tab","dhx_cal_today_button"],e=0;e<t.length;e++)for(var i=this._els[t[e]],n=0;i&&n<i.length;n++){var a=i[n].getAttribute("name"),r=this.locale.labels[t[e]]
;a&&(r=this.locale.labels[a]||r),"dhx_cal_next_button"==t[e]?r=this.locale.labels.next:"dhx_cal_prev_button"==t[e]&&(r=this.locale.labels.prev),this._waiAria.headerButtonsAttributes(i[n],r||"")}},g.updateView=function(t,e){if(!this.$container)throw new Error("The scheduler is not initialized. \n **scheduler.updateView** or **scheduler.setCurrentView** can be called only after **scheduler.init**");t=t||this._date,e=e||this._mode
;var i="dhx_cal_data",n=this._obj,a="dhx_scheduler_"+this._mode,r="dhx_scheduler_"+e;this._mode&&-1!=n.className.indexOf(a)?n.className=n.className.replace(a,r):n.className+=" "+r;var s,o="dhx_multi_day",d=!(this._mode!=e||!this.config.preserve_scroll)&&this._els[i][0].scrollTop;this._els[o]&&this._els[o][0]&&(s=this._els[o][0].scrollTop),this[this._mode+"_view"]&&e&&this._mode!=e&&this[this._mode+"_view"](!1),this._close_not_saved(),
this._els[o]&&(this._els[o][0].parentNode.removeChild(this._els[o][0]),this._els[o]=null),this._mode=e,this._date=t,this._table_view="month"==this._mode,this._dy_shift=0,this.update_view(),this._set_aria_buttons_attrs();var l=this._els.dhx_cal_tab;if(l)for(var _=0;_<l.length;_++){var h=l[_];h.getAttribute("name")==this._mode+"_tab"?(h.classList.add("active"),this._waiAria.headerToggleState(h,!0)):(h.classList.remove("active"),this._waiAria.headerToggleState(h,!1))}
"number"==typeof d&&(this._els[i][0].scrollTop=d),"number"==typeof s&&this._els[o]&&this._els[o][0]&&(this._els[o][0].scrollTop=s)},g.setCurrentView=function(t,e){this.callEvent("onBeforeViewChange",[this._mode,this._date,e||this._mode,t||this._date])&&(this.updateView(t,e),this.callEvent("onViewChange",[this._mode,this._date]))},g.render=function(t,e){g.setCurrentView(t,e)},g._render_x_header=function(t,e,i,n,a){a=a||0;var r=document.createElement("div");r.className="dhx_scale_bar",
this.templates[this._mode+"_scalex_class"]&&(r.className+=" "+this.templates[this._mode+"_scalex_class"](i));var s=this._cols[t]-1;"month"==this._mode&&0===t&&this.config.left_border&&(r.className+=" dhx_scale_bar_border",e+=1),this.set_xy(r,s,this.xy.scale_height-2,e,a);var o=this.templates[this._mode+"_scale_date"](i,this._mode);r.innerHTML=o,this._waiAria.dayHeaderAttr(r,o),n.appendChild(r)},g._get_columns_num=function(t,e){var i=7;if(!g._table_view){var n=g.date["get_"+g._mode+"_end"]
;n&&(e=n(t)),i=Math.round((e.valueOf()-t.valueOf())/864e5)}return i},g._get_timeunit_start=function(){return this.date[this._mode+"_start"](new Date(this._date.valueOf()))},g._get_view_end=function(){var t=this._get_timeunit_start(),e=g.date.add(t,1,this._mode);if(!g._table_view){var i=g.date["get_"+g._mode+"_end"];i&&(e=i(t))}return e},g._calc_scale_sizes=function(t,e,i){var n=this.config.rtl,a=t,r=this._get_columns_num(e,i);this._process_ignores(e,r,"day",1)
;for(var s=r-this._ignores_detected,o=0;o<r;o++)this._ignores[o]?(this._cols[o]=0,s++):this._cols[o]=Math.floor(a/(s-o)),a-=this._cols[o],this._colsS[o]=(this._cols[o-1]||0)+(this._colsS[o-1]||(this._table_view?0:(n?this.xy.scroll_width:this.xy.scale_width)+2));this._colsS.col_length=r,this._colsS[r]=this._cols[r-1]+this._colsS[r-1]||0},g._set_scale_col_size=function(t,e,i){var n=this.config;this.set_xy(t,e-1,n.hour_size_px*(n.last_hour-n.first_hour),i+this.xy.scale_width+1,0)},
g._render_scales=function(t,e){var i=new Date(g._min_date),n=new Date(g._max_date),a=this.date.date_part(g._currentDate()),r=parseInt(t.style.width,10),s=new Date(this._min_date),o=this._get_columns_num(i,n);this._calc_scale_sizes(r,i,n);var d=0;t.innerHTML="";for(var l=0;l<o;l++){if(this._ignores[l]||this._render_x_header(l,d,s,t),!this._table_view){var _=document.createElement("div"),h="dhx_scale_holder";s.valueOf()==a.valueOf()&&(h="dhx_scale_holder_now"),
_.setAttribute("data-column-index",l),this._ignores_detected&&this._ignores[l]&&(h+=" dhx_scale_ignore"),_.className=h+" "+this.templates.week_date_class(s,a),this._waiAria.dayColumnAttr(_,s),this._set_scale_col_size(_,this._cols[l],d),e.appendChild(_),this.callEvent("onScaleAdd",[_,s])}d+=this._cols[l],s=this.date.add(s,1,"day"),s=this.date.day_start(s)}},g._getNavDateElement=function(){return this.$container.querySelector(".dhx_cal_date")},g._reset_scale=function(){
if(this.templates[this._mode+"_date"]){var t=this._els.dhx_cal_header[0],e=this._els.dhx_cal_data[0],i=this.config;t.innerHTML="",e.innerHTML="";var n=(i.readonly||!i.drag_resize?" dhx_resize_denied":"")+(i.readonly||!i.drag_move?" dhx_move_denied":"");e.className="dhx_cal_data"+n,this._scales={},this._cols=[],this._colsS={height:0},this._dy_shift=0,this.set_sizes();var a,r,s=this._get_timeunit_start(),o=g._get_view_end();a=r=this._table_view?g.date.week_start(s):s,this._min_date=a
;var d=this.templates[this._mode+"_date"](s,o,this._mode),l=this._getNavDateElement();if(l&&(l.innerHTML=d,this._waiAria.navBarDateAttr(l,d)),this._max_date=o,g._render_scales(t,e),this._table_view)this._reset_month_scale(e,s,r);else if(this._reset_hours_scale(e,s,r),i.multi_day){var _="dhx_multi_day";this._els[_]&&(this._els[_][0].parentNode.removeChild(this._els[_][0]),this._els[_]=null)
;var h=this._els.dhx_cal_navline[0],c=h.offsetHeight+this._els.dhx_cal_header[0].offsetHeight+1,u=document.createElement("div");u.className=_,u.style.visibility="hidden";var f=this._colsS[this._colsS.col_length],v=i.rtl?this.xy.scale_width:this.xy.scroll_width,m=Math.max(f+v-2,0);this.set_xy(u,m,0,0,c),e.parentNode.insertBefore(u,e);var p=u.cloneNode(!0);p.className=_+"_icon",p.style.visibility="hidden",this.set_xy(p,this.xy.scale_width,0,0,c),u.appendChild(p),this._els[_]=[u,p],
this._els[_][0].onclick=this._click.dhx_cal_data}}},g._reset_hours_scale=function(t,e,i){var n=document.createElement("div");n.className="dhx_scale_holder";for(var a=new Date(1980,1,1,this.config.first_hour,0,0),r=1*this.config.first_hour;r<this.config.last_hour;r++){var s=document.createElement("div");s.className="dhx_scale_hour",s.style.height=this.config.hour_size_px+"px";var o=this.xy.scale_width;this.config.left_border&&(s.className+=" dhx_scale_hour_border"),s.style.width=o+"px"
;var d=g.templates.hour_scale(a);s.innerHTML=d,this._waiAria.hourScaleAttr(s,d),n.appendChild(s),a=this.date.add(a,1,"hour")}t.appendChild(n),this.config.scroll_hour&&(t.scrollTop=this.config.hour_size_px*(this.config.scroll_hour-this.config.first_hour))},g._currentDate=function(){return g.config.now_date?new Date(g.config.now_date):new Date},g._reset_ignores=function(){this._ignores={},this._ignores_detected=0},g._process_ignores=function(t,e,i,n,a){this._reset_ignores()
;var r=g["ignore_"+this._mode];if(r)for(var s=new Date(t),o=0;o<e;o++)r(s)&&(this._ignores_detected+=1,this._ignores[o]=!0,a&&e++),s=g.date.add(s,n,i),g.date[i+"_start"]&&(s=g.date[i+"_start"](s))},g._render_month_scale=function(t,e,i,n){function a(t){var e=g._colsS.height;return void 0!==g._colsS.heights[t+1]&&(e=g._colsS.heights[t+1]-(g._colsS.heights[t]||0)),e}var r=g.date.add(e,1,"month"),s=new Date(i),o=g._currentDate();this.date.date_part(o),this.date.date_part(i),
n=n||Math.ceil(Math.round((r.valueOf()-i.valueOf())/864e5)/7);for(var d=[],l=0;l<=7;l++){var _=(this._cols[l]||0)-1;0===l&&this.config.left_border&&(_-=1),d[l]=_+"px"}var h=0,c=document.createElement("table");c.setAttribute("cellpadding","0"),c.setAttribute("cellspacing","0");var u=document.createElement("tbody");c.appendChild(u);for(var f=[],l=0;l<n;l++){var v=document.createElement("tr");u.appendChild(v);for(var m=Math.max(a(l)-g.xy.month_head_height,0),p=0;p<7;p++){
var x=document.createElement("td");v.appendChild(x);var b="";i<e?b="dhx_before":i>=r?b="dhx_after":i.valueOf()==o.valueOf()&&(b="dhx_now"),this._ignores_detected&&this._ignores[p]&&(b+=" dhx_scale_ignore"),x.className=b+" "+this.templates.month_date_class(i,o),x.setAttribute("data-cell-date",g.templates.format_date(i));var y="dhx_month_body",w="dhx_month_head";if(0===p&&this.config.left_border&&(y+=" dhx_month_body_border",w+=" dhx_month_head_border"),
this._ignores_detected&&this._ignores[p])x.appendChild(document.createElement("div")),x.appendChild(document.createElement("div"));else{this._waiAria.monthCellAttr(x,i);var D=document.createElement("div");D.className=w,D.innerHTML=this.templates.month_day(i),x.appendChild(D);var E=document.createElement("div");E.className=y,E.style.height=m+"px",E.style.width=d[p],x.appendChild(E)}f.push(i);var A=i.getDate();i=this.date.add(i,1,"day"),
i.getDate()-A>1&&(i=new Date(i.getFullYear(),i.getMonth(),A+1,12,0))}g._colsS.heights[l]=h,h+=a(l)}this._min_date=s,this._max_date=i,t.innerHTML="",t.appendChild(c),this._scales={};for(var S=t.getElementsByTagName("div"),l=0;l<f.length;l++){var t=S[2*l+1],M=f[l];this._scales[+M]=t}for(var l=0;l<f.length;l++){var M=f[l];this.callEvent("onScaleAdd",[this._scales[+M],M])}return this._max_date},g._reset_month_scale=function(t,e,i,n){var a=g.date.add(e,1,"month"),r=g._currentDate()
;this.date.date_part(r),this.date.date_part(i),n=n||Math.ceil(Math.round((a.valueOf()-i.valueOf())/864e5)/7);var s=Math.floor(t.clientHeight/n)-this.xy.month_head_height;return this._colsS.height=s+this.xy.month_head_height,this._colsS.heights=[],g._render_month_scale(t,e,i,n)},g.getView=function(t){return t||(t=g.getState().mode),g.matrix&&g.matrix[t]?g.matrix[t]:g._props&&g._props[t]?g._props[t]:null},g.getLabel=function(t,e){
for(var i=this.config.lightbox.sections,n=0;n<i.length;n++)if(i[n].map_to==t)for(var a=i[n].options,r=0;r<a.length;r++)if(a[r].key==e)return a[r].label;return""},g.updateCollection=function(t,e){var i=g.serverList(t);return!!i&&(i.splice(0,i.length),i.push.apply(i,e||[]),g.callEvent("onOptionsLoad",[]),g.resetLightbox(),g.hideCover(),!0)},g._lame_clone=function(t,e){var i,n,a;for(e=e||[],i=0;i<e.length;i+=2)if(t===e[i])return e[i+1];if(t&&"object"==typeof t){for(a=Object.create(t),
n=[Array,Date,Number,String,Boolean],i=0;i<n.length;i++)t instanceof n[i]&&(a=i?new n[i](t):new n[i]);e.push(t,a);for(i in t)Object.prototype.hasOwnProperty.apply(t,[i])&&(a[i]=g._lame_clone(t[i],e))}return a||t},g._lame_copy=function(t,e){for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t},g._get_date_from_pos=function(t){var e=this._min_date.valueOf()+6e4*(t.y*this.config.time_step+24*(this._table_view?0:t.x)*60);return new Date(this._correct_shift(e))},g.getActionData=function(t){
var e=this._mouse_coords(t);return{date:this._get_date_from_pos(e),section:e.section}},g._focus=function(t,e){if(t&&t.focus)if(this._mobile)window.setTimeout(function(){t.focus()},10);else try{e&&t.select&&t.offsetWidth&&t.select(),t.focus()}catch(t){}},g._get_real_event_length=function(t,e,i){var n,a=e-t,r=i._start_correction+i._end_correction||0,s=this["ignore_"+this._mode],o=0;i.render?(o=this._get_date_index(i,t),n=this._get_date_index(i,e)):n=Math.round(a/60/60/1e3/24);for(var d=!0;o<n;){
var l=g.date.add(e,-i.x_step,i.x_unit);s&&s(e)&&(!d||d&&s(l))?a-=e-l:(d=!1,a-=r),e=l,n--}return a},g._get_fictional_event_length=function(t,e,i,n){var a=new Date(t),r=n?-1:1;if(i._start_correction||i._end_correction){var s;s=n?60*a.getHours()+a.getMinutes()-60*(i.first_hour||0):60*(i.last_hour||0)-(60*a.getHours()+a.getMinutes());var o=60*(i.last_hour-i.first_hour),d=Math.ceil((e/6e4-s)/o);d<0&&(d=0),e+=d*(1440-o)*60*1e3}var l,_=new Date(1*t+e*r),h=this["ignore_"+this._mode],c=0
;for(i.render?(c=this._get_date_index(i,a),l=this._get_date_index(i,_)):l=Math.round(e/60/60/1e3/24);c*r<=l*r;){var u=g.date.add(a,i.x_step*r,i.x_unit);h&&h(a)&&(e+=(u-a)*r,l+=r),a=u,c+=r}return e},g._get_section_view=function(){return this.getView()},g._get_section_property=function(){return this.matrix&&this.matrix[this._mode]?this.matrix[this._mode].y_property:this._props&&this._props[this._mode]?this._props[this._mode].map_to:null},g._is_initialized=function(){var t=this.getState()
;return this._obj&&t.date&&t.mode},g._is_lightbox_open=function(){var t=this.getState();return null!==t.lightbox_id&&void 0!==t.lightbox_id},g._getClassName=function(t){if(!t)return"";var e=t.className||"";return e.baseVal&&(e=e.baseVal),e.indexOf||(e=""),e||""},g.event=function(t,e,i){t.addEventListener?t.addEventListener(e,i,!1):t.attachEvent&&t.attachEvent("on"+e,i)},g.eventRemove=function(t,e,i){t.removeEventListener?t.removeEventListener(e,i,!1):t.detachEvent&&t.detachEvent("on"+e,i)},
function(){function t(t){var e=!1,i=!1;if(window.getComputedStyle){var n=window.getComputedStyle(t,null);e=n.display,i=n.visibility}else t.currentStyle&&(e=t.currentStyle.display,i=t.currentStyle.visibility);var a=!1,r=g._locate_css({target:t},"dhx_form_repeat",!1);return r&&(a=!("0px"!=r.style.height)),a=a||!t.offsetHeight,"none"!=e&&"hidden"!=i&&!a}function e(t){return!isNaN(t.getAttribute("tabindex"))&&1*t.getAttribute("tabindex")>=0}function i(t){return!{a:!0,area:!0
}[t.nodeName.loLowerCase()]||!!t.getAttribute("href")}function n(t){return!{input:!0,select:!0,textarea:!0,button:!0,object:!0}[t.nodeName.toLowerCase()]||!t.hasAttribute("disabled")}g._getFocusableNodes=function(a){for(var r=a.querySelectorAll(["a[href]","area[href]","input","select","textarea","button","iframe","object","embed","[tabindex]","[contenteditable]"].join(", ")),s=Array.prototype.slice.call(r,0),o=0;o<s.length;o++){var d=s[o];(e(d)||n(d)||i(d))&&t(d)||(s.splice(o,1),o--)}return s}
}(),g._trim=function(t){return(String.prototype.trim||function(){return this.replace(/^\s+|\s+$/g,"")}).apply(t)},g._isDate=function(t){return!(!t||"object"!=typeof t)&&!!(t.getFullYear&&t.getMonth&&t.getDate)},g._isObject=function(t){return t&&"object"==typeof t},function(){function t(t){return(t+"").replace(n," ").replace(a," ")}function e(t){return(t+"").replace(r,"&#39;")}function i(){return!g.config.wai_aria_attributes}
var n=new RegExp("<(?:.|\n)*?>","gm"),a=new RegExp(" +","gm"),r=new RegExp("'","gm");g._waiAria={getAttributeString:function(i){var n=[" "];for(var a in i)if("function"!=typeof i[a]&&"object"!=typeof i[a]){var r=e(t(i[a]));n.push(a+"='"+r+"'")}return n.push(" "),n.join(" ")},setAttributes:function(e,i){for(var n in i)e.setAttribute(n,t(i[n]));return e},labelAttr:function(t,e){return this.setAttributes(t,{"aria-label":e})},label:function(t){return g._waiAria.getAttributeString({"aria-label":t})},
hourScaleAttr:function(t,e){this.labelAttr(t,e)},monthCellAttr:function(t,e){this.labelAttr(t,g.templates.day_date(e))},navBarDateAttr:function(t,e){this.labelAttr(t,e)},dayHeaderAttr:function(t,e){this.labelAttr(t,e)},dayColumnAttr:function(t,e){this.dayHeaderAttr(t,g.templates.day_date(e))},headerButtonsAttributes:function(t,e){return this.setAttributes(t,{role:"button","aria-label":e})},headerToggleState:function(t,e){return this.setAttributes(t,{"aria-pressed":e?"true":"false"})},
getHeaderCellAttr:function(t){return g._waiAria.getAttributeString({"aria-label":t})},eventAttr:function(t,e){this._eventCommonAttr(t,e)},_eventCommonAttr:function(e,i){i.setAttribute("aria-label",t(g.templates.event_text(e.start_date,e.end_date,e))),g.config.readonly&&i.setAttribute("aria-readonly",!0),e.$dataprocessor_class&&i.setAttribute("aria-busy",!0),i.setAttribute("aria-selected",g.getState().select_id==e.id?"true":"false")},setEventBarAttr:function(t,e){this._eventCommonAttr(t,e)},
_getAttributes:function(t,e){var i={setAttribute:function(t,e){this[t]=e}};return t.apply(this,[e,i]),i},eventBarAttrString:function(t){return this.getAttributeString(this._getAttributes(this.setEventBarAttr,t))},agendaHeadAttrString:function(){return this.getAttributeString({role:"row"})},agendaHeadDateString:function(t){return this.getAttributeString({role:"columnheader","aria-label":t})},agendaHeadDescriptionString:function(t){return this.agendaHeadDateString(t)},
agendaDataAttrString:function(){return this.getAttributeString({role:"grid"})},agendaEventAttrString:function(t){var e=this._getAttributes(this._eventCommonAttr,t);return e.role="row",this.getAttributeString(e)},agendaDetailsBtnString:function(){return this.getAttributeString({role:"button","aria-label":g.locale.labels.icon_details})},gridAttrString:function(){return this.getAttributeString({role:"grid"})},gridRowAttrString:function(t){return this.agendaEventAttrString(t)},
gridCellAttrString:function(t,e,i){return this.getAttributeString({role:"gridcell","aria-label":[void 0===e.label?e.id:e.label,": ",i]})},mapAttrString:function(){return this.gridAttrString()},mapRowAttrString:function(t){return this.gridRowAttrString(t)},mapDetailsBtnString:function(){return this.agendaDetailsBtnString()},minicalHeader:function(t,e){this.setAttributes(t,{id:e+"","aria-live":"assertice","aria-atomic":"true"})},minicalGrid:function(t,e){this.setAttributes(t,{
"aria-labelledby":e+"",role:"grid"})},minicalRow:function(t){this.setAttributes(t,{role:"row"})},minicalDayCell:function(t,e){var i=e.valueOf()<g._max_date.valueOf()&&e.valueOf()>=g._min_date.valueOf();this.setAttributes(t,{role:"gridcell","aria-label":g.templates.day_date(e),"aria-selected":i?"true":"false"})},minicalHeadCell:function(t){this.setAttributes(t,{role:"columnheader"})},weekAgendaDayCell:function(t,e){
var i=t.querySelector(".dhx_wa_scale_bar"),n=t.querySelector(".dhx_wa_day_data"),a=g.uid()+"";this.setAttributes(i,{id:a}),this.setAttributes(n,{"aria-labelledby":a})},weekAgendaEvent:function(t,e){this.eventAttr(e,t)},lightboxHiddenAttr:function(t){t.setAttribute("aria-hidden","true")},lightboxVisibleAttr:function(t){t.setAttribute("aria-hidden","false")},lightboxSectionButtonAttrString:function(t){return this.getAttributeString({role:"button","aria-label":t,tabindex:"0"})},
yearHeader:function(t,e){this.setAttributes(t,{id:e+""})},yearGrid:function(t,e){this.minicalGrid(t,e)},yearHeadCell:function(t){return this.minicalHeadCell(t)},yearRow:function(t){return this.minicalRow(t)},yearDayCell:function(t){this.setAttributes(t,{role:"gridcell"})},lightboxAttr:function(t){t.setAttribute("role","dialog"),t.setAttribute("aria-hidden","true"),t.firstChild.setAttribute("role","heading")},lightboxButtonAttrString:function(t){return this.getAttributeString({role:"button",
"aria-label":g.locale.labels[t],tabindex:"0"})},eventMenuAttrString:function(t){return this.getAttributeString({role:"button","aria-label":g.locale.labels[t]})},lightboxHeader:function(t,e){t.setAttribute("aria-label",e)},lightboxSelectAttrString:function(t){var e="";switch(t){case"%Y":e=g.locale.labels.year;break;case"%m":e=g.locale.labels.month;break;case"%d":e=g.locale.labels.day;break;case"%H:%i":e=g.locale.labels.hour+" "+g.locale.labels.minute}return g._waiAria.getAttributeString({
"aria-label":e})},messageButtonAttrString:function(t){return"tabindex='0' role='button' aria-label='"+t+"'"},messageInfoAttr:function(t){t.setAttribute("role","alert")},messageModalAttr:function(t,e){t.setAttribute("role","dialog"),e&&t.setAttribute("aria-labelledby",e)},quickInfoAttr:function(t){t.setAttribute("role","dialog")},quickInfoHeaderAttrString:function(){return" role='heading' "},quickInfoHeader:function(t,e){t.setAttribute("aria-label",e)},quickInfoButtonAttrString:function(t){
return g._waiAria.getAttributeString({role:"button","aria-label":t,tabindex:"0"})},tooltipAttr:function(t){t.setAttribute("role","tooltip")},tooltipVisibleAttr:function(t){t.setAttribute("aria-hidden","false")},tooltipHiddenAttr:function(t){t.setAttribute("aria-hidden","true")}};for(var s in g._waiAria)g._waiAria[s]=function(t){return function(){return i()?" ":t.apply(this,arguments)}}(g._waiAria[s])}(),g.utils={mixin:function(t,e,i){for(var n in e)(void 0===t[n]||i)&&(t[n]=e[n]);return t},
copy:function t(e){var i,n;if(e&&"object"==typeof e)switch(!0){case h(e):n=new Date(e);break;case o(e):for(n=new Array(e.length),i=0;i<e.length;i++)n[i]=t(e[i]);break;case d(e):n=new String(e);break;case l(e):n=new Number(e);break;case _(e):n=new Boolean(e);break;default:n={};for(i in e)Object.prototype.hasOwnProperty.apply(e,[i])&&(n[i]=t(e[i]))}return n||e}},g.$domHelpers={getAbsoluteLeft:function(t){return this.getOffset(t).left},getAbsoluteTop:function(t){return this.getOffset(t).top},
getOffsetSum:function(t){for(var e=0,i=0;t;)e+=parseInt(t.offsetTop),i+=parseInt(t.offsetLeft),t=t.offsetParent;return{top:e,left:i}},getOffsetRect:function(t){var e=t.getBoundingClientRect(),i=0,n=0;if(/Mobi/.test(navigator.userAgent)){var a=document.createElement("div");a.style.position="absolute",a.style.left="0px",a.style.top="0px",a.style.width="1px",a.style.height="1px",document.body.appendChild(a);var r=a.getBoundingClientRect();i=e.top-r.top,n=e.left-r.left,a.parentNode.removeChild(a)
}else{var s=document.body,o=document.documentElement,d=window.pageYOffset||o.scrollTop||s.scrollTop,l=window.pageXOffset||o.scrollLeft||s.scrollLeft,_=o.clientTop||s.clientTop||0,h=o.clientLeft||s.clientLeft||0;i=e.top+d-_,n=e.left+l-h}return{top:Math.round(i),left:Math.round(n)}},getOffset:function(t){return t.getBoundingClientRect?this.getOffsetRect(t):this.getOffsetSum(t)},closest:function(t,e){return t&&e?w(t,e):null},insertAfter:function(t,e){
e.nextSibling?e.parentNode.insertBefore(t,e.nextSibling):e.parentNode.appendChild(t)}};var w;if(Element.prototype.closest)w=function(t,e){return t.closest(e)};else{var D=Element.prototype.matches||Element.prototype.msMatchesSelector||Element.prototype.webkitMatchesSelector;w=function(t,e){var i=t;do{if(D.call(i,e))return i;i=i.parentElement||i.parentNode}while(null!==i&&1===i.nodeType);return null}}g.$env={isIE:navigator.userAgent.indexOf("MSIE")>=0||navigator.userAgent.indexOf("Trident")>=0,
isIE6:!window.XMLHttpRequest&&navigator.userAgent.indexOf("MSIE")>=0,isIE7:navigator.userAgent.indexOf("MSIE 7.0")>=0&&navigator.userAgent.indexOf("Trident")<0,isIE8:navigator.userAgent.indexOf("MSIE 8.0")>=0&&navigator.userAgent.indexOf("Trident")>=0,isOpera:navigator.userAgent.indexOf("Opera")>=0,isChrome:navigator.userAgent.indexOf("Chrome")>=0,isKHTML:navigator.userAgent.indexOf("Safari")>=0||navigator.userAgent.indexOf("Konqueror")>=0,isFF:navigator.userAgent.indexOf("Firefox")>=0,
isIPad:navigator.userAgent.search(/iPad/gi)>=0,isEdge:-1!=navigator.userAgent.indexOf("Edge")},g.$ajax={_obj:g,cache:!0,method:"get",parse:function(t){if("string"!=typeof t)return t;var e;return t=t.replace(/^[\s]+/,""),window.DOMParser&&!g.$env.isIE?e=(new window.DOMParser).parseFromString(t,"text/xml"):window.ActiveXObject!==window.undefined&&(e=new window.ActiveXObject("Microsoft.XMLDOM"),e.async="false",e.loadXML(t)),e},xmltop:function(t,e,i){if(void 0===e.status||e.status<400){
var n=e.responseXML?e.responseXML||e:this.parse(e.responseText||e);if(n&&null!==n.documentElement&&!n.getElementsByTagName("parsererror").length)return n.getElementsByTagName(t)[0]}return-1!==i&&this._obj.callEvent("onLoadXMLError",["Incorrect XML",arguments[1],i]),document.createElement("DIV")},xpath:function(t,e){if(e.nodeName||(e=e.responseXML||e),g.$env.isIE)return e.selectNodes(t)||[];for(var i,n=[],a=(e.ownerDocument||e).evaluate(t,e,null,XPathResult.ANY_TYPE,null);;){
if(!(i=a.iterateNext()))break;n.push(i)}return n},query:function(t){this._call(t.method||"GET",t.url,t.data||"",t.async||!0,t.callback,null,t.headers)},get:function(t,e){this._call("GET",t,null,!0,e)},getSync:function(t){return this._call("GET",t,null,!1)},put:function(t,e,i){this._call("PUT",t,e,!0,i)},del:function(t,e,i){this._call("DELETE",t,e,!0,i)},post:function(t,e,i){1==arguments.length?e="":2!=arguments.length||"function"!=typeof e&&"function"!=typeof window[e]?e=String(e):(i=e,e=""),
this._call("POST",t,e,!0,i)},postSync:function(t,e){return e=null===e?"":String(e),this._call("POST",t,e,!1)},getLong:function(t,e){this._call("GET",t,null,!0,e,{url:t})},postLong:function(t,e,i){2!=arguments.length||"function"!=typeof e&&(window[e],0)||(i=e,e=""),this._call("POST",t,e,!0,i,{url:t,postData:e})},_call:function(t,e,i,n,a,r,s){
var o=this._obj,d=window.XMLHttpRequest&&!o.$env.isIE?new XMLHttpRequest:new ActiveXObject("Microsoft.XMLHTTP"),l=null!==navigator.userAgent.match(/AppleWebKit/)&&null!==navigator.userAgent.match(/Qt/)&&null!==navigator.userAgent.match(/Safari/);if(n&&(d.onreadystatechange=function(){if(4==d.readyState||l&&3==d.readyState){if((200!=d.status||""===d.responseText)&&!o.callEvent("onAjaxError",[d]))return;window.setTimeout(function(){"function"==typeof a&&a.apply(window,[{xmlDoc:d,filePath:e}]),
r&&(void 0!==r.postData?this.postLong(r.url,r.postData,a):this.getLong(r.url,a)),a=null,d=null},1)}}),"GET"!=t||this.cache||(e+=(e.indexOf("?")>=0?"&":"?")+"dhxr"+(new Date).getTime()+"=1"),d.open(t,e,n),s)for(var _ in s)d.setRequestHeader(_,s[_]);else"POST"==t.toUpperCase()||"PUT"==t||"DELETE"==t?d.setRequestHeader("Content-Type","application/x-www-form-urlencoded"):"GET"==t&&(i=null);if(d.setRequestHeader("X-Requested-With","XMLHttpRequest"),d.send(i),!n)return{xmlDoc:d,filePath:e}},
urlSeparator:function(t){return-1!=t.indexOf("?")?"&":"?"}};var E=function(t,e){for(var i="var temp=date.match(/[a-zA-Z]+|[0-9]+/g);",n=t.match(/%[a-zA-Z]/g),a=0;a<n.length;a++)switch(n[a]){case"%j":case"%d":i+="set[2]=temp["+a+"]||1;";break;case"%n":case"%m":i+="set[1]=(temp["+a+"]||1)-1;";break;case"%y":i+="set[0]=temp["+a+"]*1+(temp["+a+"]>50?1900:2000);";break;case"%g":case"%G":case"%h":case"%H":i+="set[3]=temp["+a+"]||0;";break;case"%i":i+="set[4]=temp["+a+"]||0;";break;case"%Y":
i+="set[0]=temp["+a+"]||0;";break;case"%a":case"%A":i+="set[3]=set[3]%12+((temp["+a+"]||'').toLowerCase()=='am'?0:12);";break;case"%s":i+="set[5]=temp["+a+"]||0;";break;case"%M":i+="set[1]=this.locale.date.month_short_hash[temp["+a+"]]||0;";break;case"%F":i+="set[1]=this.locale.date.month_full_hash[temp["+a+"]]||0;"}var r="set[0],set[1],set[2],set[3],set[4],set[5]";return e&&(r=" Date.UTC("+r+")"),new Function("date","var set=[0,0,1,0,0,0]; "+i+" return new Date("+r+");")};g.date={
init:function(){for(var t=g.locale.date.month_short,e=g.locale.date.month_short_hash={},i=0;i<t.length;i++)e[t[i]]=i;for(var t=g.locale.date.month_full,e=g.locale.date.month_full_hash={},i=0;i<t.length;i++)e[t[i]]=i},_bind_host_object:function(t){return t.bind?t.bind(g):function(){return t.apply(g,arguments)}},date_part:function(t){var e=new Date(t);return t.setHours(0),t.setMinutes(0),t.setSeconds(0),t.setMilliseconds(0),
t.getHours()&&(t.getDate()<e.getDate()||t.getMonth()<e.getMonth()||t.getFullYear()<e.getFullYear())&&t.setTime(t.getTime()+36e5*(24-t.getHours())),t},time_part:function(t){return(t.valueOf()/1e3-60*t.getTimezoneOffset())%86400},week_start:function(t){var e=t.getDay();return g.config.start_on_monday&&(0===e?e=6:e--),this.date_part(this.add(t,-1*e,"day"))},month_start:function(t){return t.setDate(1),this.date_part(t)},year_start:function(t){return t.setMonth(0),this.month_start(t)},
day_start:function(t){return this.date_part(t)},_add_days:function(t,e){var i=new Date(t.valueOf());if(i.setDate(i.getDate()+e),e==Math.round(e)&&e>0){var n=+i-+t,a=n%864e5;if(a&&t.getTimezoneOffset()==i.getTimezoneOffset()){var r=a/36e5;i.setTime(i.getTime()+60*(24-r)*60*1e3)}}return e>=0&&!t.getHours()&&i.getHours()&&(i.getDate()<t.getDate()||i.getMonth()<t.getMonth()||i.getFullYear()<t.getFullYear())&&i.setTime(i.getTime()+36e5*(24-i.getHours())),i},add:function(t,e,i){
var n=new Date(t.valueOf());switch(i){case"day":n=g.date._add_days(n,e);break;case"week":n=g.date._add_days(n,7*e);break;case"month":n.setMonth(n.getMonth()+e);break;case"year":n.setYear(n.getFullYear()+e);break;case"hour":n.setTime(n.getTime()+60*e*60*1e3);break;case"minute":n.setTime(n.getTime()+60*e*1e3);break;default:return g.date["add_"+i](t,e,i)}return n},to_fixed:function(t){return t<10?"0"+t:t},copy:function(t){return new Date(t.valueOf())},date_to_str:function(t,e){
t=t.replace(/%[a-zA-Z]/g,function(t){switch(t){case"%d":return'"+this.date.to_fixed(date.getDate())+"';case"%m":return'"+this.date.to_fixed((date.getMonth()+1))+"';case"%j":return'"+date.getDate()+"';case"%n":return'"+(date.getMonth()+1)+"';case"%y":return'"+this.date.to_fixed(date.getFullYear()%100)+"';case"%Y":return'"+date.getFullYear()+"';case"%D":return'"+this.locale.date.day_short[date.getDay()]+"';case"%l":return'"+this.locale.date.day_full[date.getDay()]+"';case"%M":
return'"+this.locale.date.month_short[date.getMonth()]+"';case"%F":return'"+this.locale.date.month_full[date.getMonth()]+"';case"%h":return'"+this.date.to_fixed((date.getHours()+11)%12+1)+"';case"%g":return'"+((date.getHours()+11)%12+1)+"';case"%G":return'"+date.getHours()+"';case"%H":return'"+this.date.to_fixed(date.getHours())+"';case"%i":return'"+this.date.to_fixed(date.getMinutes())+"';case"%a":return'"+(date.getHours()>11?"pm":"am")+"';case"%A":return'"+(date.getHours()>11?"PM":"AM")+"'
;case"%s":return'"+this.date.to_fixed(date.getSeconds())+"';case"%W":return'"+this.date.to_fixed(this.date.getISOWeek(date))+"';default:return t}}),e&&(t=t.replace(/date\.get/g,"date.getUTC"));var i=new Function("date",'return "'+t+'";');return g.date._bind_host_object(i)},str_to_date:function(t,e,i){
var n=E(t,e),a=/^[0-9]{4}(\-|\/)[0-9]{2}(\-|\/)[0-9]{2} ?(([0-9]{1,2}:[0-9]{1,2})(:[0-9]{1,2})?)?$/,r=/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} ?(([0-9]{1,2}:[0-9]{2})(:[0-9]{1,2})?)?$/,s=/^[0-9]{2}\-[0-9]{2}\-[0-9]{4} ?(([0-9]{1,2}:[0-9]{1,2})(:[0-9]{1,2})?)?$/,o=/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/,d=function(t){
return a.test(String(t))},l=function(t){return r.test(String(t))},_=function(t){return s.test(String(t))},h=function(t){return o.test(t)},c=E("%Y-%m-%d %H:%i:%s",e),u=E("%m/%d/%Y %H:%i:%s",e),f=E("%d-%m-%Y %H:%i:%s",e);return function(t){if(!i&&!g.config.parse_exact_format){if(t&&t.getISOWeek)return new Date(t);if("number"==typeof t)return new Date(t);if(d(t))return c(t);if(l(t))return u(t);if(_(t))return f(t);if(h(t))return new Date(t)}return n.call(g,t)}},getISOWeek:function(t){if(!t)return!1
;t=this.date_part(new Date(t));var e=t.getDay();0===e&&(e=7);var i=new Date(t.valueOf());i.setDate(t.getDate()+(4-e));var n=i.getFullYear(),a=Math.round((i.getTime()-new Date(n,0,1).getTime())/864e5);return 1+Math.floor(a/7)},getUTCISOWeek:function(t){return this.getISOWeek(this.convert_to_utc(t))},convert_to_utc:function(t){return new Date(t.getUTCFullYear(),t.getUTCMonth(),t.getUTCDate(),t.getUTCHours(),t.getUTCMinutes(),t.getUTCSeconds())}},g.locale={date:{
month_full:["January","February","March","April","May","June","July","August","September","October","November","December"],month_short:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],day_full:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],day_short:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]},labels:{dhx_cal_today_button:"Today",day_tab:"Day",week_tab:"Week",month_tab:"Month",new_event:"New event",icon_save:"Save",icon_cancel:"Cancel",
icon_details:"Details",icon_edit:"Edit",icon_delete:"Delete",confirm_closing:"",confirm_deleting:"Event will be deleted permanently, are you sure?",section_description:"Description",section_time:"Time period",full_day:"Full day",confirm_recurring:"Do you want to edit the whole set of repeated events?",section_recurring:"Repeat event",button_recurring:"Disabled",button_recurring_open:"Enabled",button_edit_series:"Edit series",button_edit_occurrence:"Edit occurrence",agenda_tab:"Agenda",
date:"Date",description:"Description",year_tab:"Year",week_agenda_tab:"Agenda",grid_tab:"Grid",drag_to_create:"Drag to create",drag_to_move:"Drag to move",message_ok:"OK",message_cancel:"Cancel",next:"Next",prev:"Previous",year:"Year",month:"Month",day:"Day",hour:"Hour",minute:"Minute",repeat_radio_day:"Daily",repeat_radio_week:"Weekly",repeat_radio_month:"Monthly",repeat_radio_year:"Yearly",repeat_radio_day_type:"Every",repeat_text_day_count:"day",repeat_radio_day_type2:"Every workday",
repeat_week:" Repeat every",repeat_text_week_count:"week next days:",repeat_radio_month_type:"Repeat",repeat_radio_month_start:"On",repeat_text_month_day:"day every",repeat_text_month_count:"month",repeat_text_month_count2_before:"every",repeat_text_month_count2_after:"month",repeat_year_label:"On",select_year_day2:"of",repeat_text_year_day:"day",select_year_month:"month",repeat_radio_end:"No end date",repeat_text_occurences_count:"occurrences",repeat_radio_end2:"After",
repeat_radio_end3:"End by",month_for_recurring:["January","February","March","April","May","June","July","August","September","October","November","December"],day_for_recurring:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]}},g.config={default_date:"%j %M %Y",month_date:"%F %Y",load_date:"%Y-%m-%d",week_date:"%l",day_date:"%D, %F %j",hour_date:"%H:%i",month_day:"%d",date_format:"%Y-%m-%d %H:%i",api_date:"%d-%m-%Y %H:%i",parse_exact_format:!1,preserve_length:!0,
time_step:5,start_on_monday:!0,first_hour:0,last_hour:24,readonly:!1,drag_resize:!0,drag_move:!0,drag_create:!0,drag_event_body:!0,dblclick_create:!0,edit_on_create:!0,details_on_create:!1,header:null,resize_month_events:!1,resize_month_timed:!1,responsive_lightbox:!1,rtl:!1,cascade_event_display:!1,cascade_event_count:4,cascade_event_margin:30,multi_day:!0,multi_day_height_limit:0,drag_lightbox:!0,preserve_scroll:!0,select:!0,server_utc:!1,touch:!0,touch_tip:!0,touch_drag:500,
touch_swipe_dates:!1,quick_info_detached:!0,positive_closing:!1,drag_highlight:!0,limit_drag_out:!1,icons_edit:["icon_save","icon_cancel"],icons_select:["icon_details","icon_edit","icon_delete"],buttons_left:["dhx_save_btn","dhx_cancel_btn"],buttons_right:["dhx_delete_btn"],lightbox:{sections:[{name:"description",map_to:"text",type:"textarea",focus:!0},{name:"time",height:72,type:"time",map_to:"auto"}]},highlight_displayed_event:!0,left_border:!1,ajax_error:"alert",delay_render:0,
timeline_swap_resize:!0,wai_aria_attributes:!0,wai_aria_application_role:!0},g.config.buttons_left.$inital=g.config.buttons_left.join(),g.config.buttons_right.$inital=g.config.buttons_right.join(),g._helpers={parseDate:function(t){return(g.templates.xml_date||g.templates.parse_date)(t)},formatDate:function(t){return(g.templates.xml_format||g.templates.format_date)(t)}},g.templates={},g.init_templates=function(){var t=g.locale.labels;t.dhx_save_btn=t.icon_save,t.dhx_cancel_btn=t.icon_cancel,
t.dhx_delete_btn=t.icon_delete;var e=g.date.date_to_str,i=g.config;(function(t,e){for(var i in e)t[i]||(t[i]=e[i])})(g.templates,{day_date:e(i.default_date),month_date:e(i.month_date),week_date:function(t,e){return i.rtl?g.templates.day_date(g.date.add(e,-1,"day"))+" &ndash; "+g.templates.day_date(t):g.templates.day_date(t)+" &ndash; "+g.templates.day_date(g.date.add(e,-1,"day"))},day_scale_date:e(i.default_date),month_scale_date:e(i.week_date),week_scale_date:e(i.day_date),
hour_scale:e(i.hour_date),time_picker:e(i.hour_date),event_date:e(i.hour_date),month_day:e(i.month_day),load_format:e(i.load_date),format_date:e(i.date_format,i.server_utc),parse_date:g.date.str_to_date(i.date_format,i.server_utc),api_date:g.date.str_to_date(i.api_date,!1,!1),event_header:function(t,e,i){return g.templates.event_date(t)+" - "+g.templates.event_date(e)},event_text:function(t,e,i){return i.text},event_class:function(t,e,i){return""},month_date_class:function(t){return""},
week_date_class:function(t){return""},event_bar_date:function(t,e,i){return g.templates.event_date(t)+" "},event_bar_text:function(t,e,i){return i.text},month_events_link:function(t,e){return"<a>View more("+e+" events)</a>"},drag_marker_class:function(t,e,i){return""},drag_marker_content:function(t,e,i){return""},tooltip_date_format:g.date.date_to_str("%Y-%m-%d %H:%i"),tooltip_text:function(t,e,i){
return"<b>Event:</b> "+i.text+"<br/><b>Start date:</b> "+g.templates.tooltip_date_format(t)+"<br/><b>End date:</b> "+g.templates.tooltip_date_format(e)}}),this.callEvent("onTemplatesReady",[])},g.uid=function(){return this._seed||(this._seed=(new Date).valueOf()),this._seed++},g._events={},g.clearAll=function(){this._events={},this._loaded={},this._edit_id=null,this._select_id=null,this._drag_id=null,this._drag_mode=null,this._drag_pos=null,this._new_event=null,this.clear_view(),
this.callEvent("onClearAll",[])},g.addEvent=function(t,e,i,n,a){if(!arguments.length)return this.addEventNow();var r=t;1!=arguments.length&&(r=a||{},r.start_date=t,r.end_date=e,r.text=i,r.id=n),r.id=r.id||g.uid(),r.text=r.text||"","string"==typeof r.start_date&&(r.start_date=this.templates.api_date(r.start_date)),"string"==typeof r.end_date&&(r.end_date=this.templates.api_date(r.end_date));var s=6e4*(this.config.event_duration||this.config.time_step)
;r.start_date.valueOf()==r.end_date.valueOf()&&r.end_date.setTime(r.end_date.valueOf()+s),r._timed=this.isOneDayEvent(r);var o=!this._events[r.id];return this._events[r.id]=r,this.event_updated(r),this._loading||this.callEvent(o?"onEventAdded":"onEventChanged",[r.id,r]),r.id},g.deleteEvent=function(t,e){var i=this._events[t];(e||this.callEvent("onBeforeEventDelete",[t,i])&&this.callEvent("onConfirmedBeforeEventDelete",[t,i]))&&(i&&(this._select_id=null,delete this._events[t],
this.event_updated(i),this._drag_id==i.id&&(this._drag_id=null,this._drag_mode=null,this._drag_pos=null)),this.callEvent("onEventDeleted",[t,i]))},g.getEvent=function(t){return this._events[t]},g.setEvent=function(t,e){e.id||(e.id=t),this._events[t]=e},g.for_rendered=function(t,e){for(var i=this._rendered.length-1;i>=0;i--)this._rendered[i].getAttribute("event_id")==t&&e(this._rendered[i],i)},g.changeEventId=function(t,e){if(t!=e){var i=this._events[t];i&&(i.id=e,this._events[e]=i,
delete this._events[t]),this.for_rendered(t,function(t){t.setAttribute("event_id",e)}),this._select_id==t&&(this._select_id=e),this._edit_id==t&&(this._edit_id=e),this.callEvent("onEventIdChange",[t,e])}},function(){for(var t=["text","Text","start_date","StartDate","end_date","EndDate"],e=function(t){return function(e){return g.getEvent(e)[t]}},i=function(t){return function(e,i){var n=g.getEvent(e);n[t]=i,n._changed=!0,n._timed=this.isOneDayEvent(n),g.event_updated(n,!0)}
},n=0;n<t.length;n+=2)g["getEvent"+t[n+1]]=e(t[n]),g["setEvent"+t[n+1]]=i(t[n])}(),g.event_updated=function(t,e){this.is_visible_events(t)?this.render_view_data():this.clear_event(t.id)},g.is_visible_events=function(t){if(!this._min_date||!this._max_date)return!1;if(t.start_date.valueOf()<this._max_date.valueOf()&&this._min_date.valueOf()<t.end_date.valueOf()){var e=t.start_date.getHours(),i=t.end_date.getHours()+t.end_date.getMinutes()/60,n=this.config.last_hour,a=this.config.first_hour
;return!(!this._table_view&&(i>n||i<a)&&(e>=n||e<a))||!!((t.end_date.valueOf()-t.start_date.valueOf())/36e5>24-(this.config.last_hour-this.config.first_hour)||e<n&&i>=a)}return!1},g.isOneDayEvent=function(t){var e=new Date(t.end_date.valueOf()-1);return t.start_date.getFullYear()===e.getFullYear()&&t.start_date.getMonth()===e.getMonth()&&t.start_date.getDate()===e.getDate()&&t.end_date.valueOf()-t.start_date.valueOf()<864e5},g.get_visible_events=function(t){var e=[]
;for(var i in this._events)this.is_visible_events(this._events[i])&&(t&&!this._events[i]._timed||this.filter_event(i,this._events[i])&&e.push(this._events[i]));return e},g.filter_event=function(t,e){var i=this["filter_"+this._mode];return!i||i(t,e)},g._is_main_area_event=function(t){return!!t._timed},g.render_view_data=function(t,e){var i=!1;if(!t){if(i=!0,this._not_render)return void(this._render_wait=!0);this._render_wait=!1,this.clear_view(),
t=this.get_visible_events(!(this._table_view||this.config.multi_day))}for(var n=0,a=t.length;n<a;n++)this._recalculate_timed(t[n]);if(this.config.multi_day&&!this._table_view){for(var r=[],s=[],n=0;n<t.length;n++)this._is_main_area_event(t[n])?r.push(t[n]):s.push(t[n]);if(!this._els.dhx_multi_day){var o=g._commonErrorMessages.unknownView(this._mode);throw new Error(o)}this._rendered_location=this._els.dhx_multi_day[0],this._table_view=!0,this.render_data(s,e),this._table_view=!1,
this._rendered_location=this._els.dhx_cal_data[0],this._table_view=!1,this.render_data(r,e)}else{var d=document.createDocumentFragment(),l=this._els.dhx_cal_data[0];this._rendered_location=d,this.render_data(t,e),l.appendChild(d),this._rendered_location=l}i&&this.callEvent("onDataRender",[])},g._view_month_day=function(t){var e=g.getActionData(t).date;g.callEvent("onViewMoreClick",[e])&&g.setCurrentView(e,"day")},g._render_month_link=function(t){
for(var e=this._rendered_location,i=this._lame_clone(t),n=t._sday;n<t._eday;n++){i._sday=n,i._eday=n+1;var a=g.date,r=g._min_date;r=a.add(r,i._sweek,"week"),r=a.add(r,i._sday,"day");var s=g.getEvents(r,a.add(r,1,"day")).length,o=this._get_event_bar_pos(i),d=o.x2-o.x,l=document.createElement("div");l.onclick=function(t){g._view_month_day(t||event)},l.className="dhx_month_link",l.style.top=o.y+"px",l.style.left=o.x+"px",l.style.width=d+"px",l.innerHTML=g.templates.month_events_link(r,s),
this._rendered.push(l),e.appendChild(l)}},g._recalculate_timed=function(t){if(t){var e;e="object"!=typeof t?this._events[t]:t,e&&(e._timed=g.isOneDayEvent(e))}},g.attachEvent("onEventChanged",g._recalculate_timed),g.attachEvent("onEventAdded",g._recalculate_timed),g.render_data=function(t,e){t=this._pre_render_events(t,e);for(var i={},n=0;n<t.length;n++)if(this._table_view)if("month"!=g._mode)this.render_event_bar(t[n]);else{var a=g.config.max_month_events
;a!==1*a||t[n]._sorder<a?this.render_event_bar(t[n]):void 0!==a&&t[n]._sorder==a&&g._render_month_link(t[n])}else{var r=t[n],s=g.locate_holder(r._sday);if(!s)continue;i[r._sday]||(i[r._sday]={real:s,buffer:document.createDocumentFragment(),width:s.clientWidth});var o=i[r._sday];this.render_event(r,o.buffer,o.width)}for(var n in i){var o=i[n];o.real&&o.buffer&&o.real.appendChild(o.buffer)}},g._get_first_visible_cell=function(t){
for(var e=0;e<t.length;e++)if(-1==(t[e].className||"").indexOf("dhx_scale_ignore"))return t[e];return t[0]},g._pre_render_events=function(t,e){var i=this.xy.bar_height,n=this._colsS.heights,a=this._colsS.heights=[0,0,0,0,0,0,0],r=this._els.dhx_cal_data[0];if(t=this._table_view?this._pre_render_events_table(t,e):this._pre_render_events_line(t,e),this._table_view)if(e)this._colsS.heights=n;else{var s=r.firstChild;if(s.rows){for(var o=0;o<s.rows.length;o++){a[o]++
;var d=s.rows[o].cells,l=this._colsS.height-this.xy.month_head_height;if(a[o]*i>l){var _=l;1*this.config.max_month_events!==this.config.max_month_events||a[o]<=this.config.max_month_events?_=a[o]*i:(this.config.max_month_events+1)*i>l&&(_=(this.config.max_month_events+1)*i);for(var h=0;h<d.length;h++)d[h].childNodes[1].style.height=_+"px"}a[o]=(a[o-1]||0)+g._get_first_visible_cell(d).offsetHeight}if(a.unshift(0),
s.parentNode.offsetHeight<s.parentNode.scrollHeight&&!g._colsS.scroll_fix&&g.xy.scroll_width){var c=g._colsS,u=c[c.col_length],f=c.heights.slice();u-=g.xy.scroll_width||0,this._calc_scale_sizes(u,this._min_date,this._max_date),g._colsS.heights=f,this.set_xy(this._els.dhx_cal_header[0],u,this.xy.scale_height),g._render_scales(this._els.dhx_cal_header[0]),g._render_month_scale(this._els.dhx_cal_data[0],this._get_timeunit_start(),this._min_date),c.scroll_fix=!0}
}else if(t.length||"visible"!=this._els.dhx_multi_day[0].style.visibility||(a[0]=-1),t.length||-1==a[0]){var v=(s.parentNode.childNodes,(a[0]+1)*i+1),m=v,p=v+"px";this.config.multi_day_height_limit&&(m=Math.min(v,this.config.multi_day_height_limit),p=m+"px"),r.style.top=this._els.dhx_cal_navline[0].offsetHeight+this._els.dhx_cal_header[0].offsetHeight+m+"px",r.style.height=this._obj.offsetHeight-parseInt(r.style.top,10)-(this.xy.margin_top||0)+"px";var x=this._els.dhx_multi_day[0]
;x.style.height=p,x.style.visibility=-1==a[0]?"hidden":"visible";var b=this._els.dhx_multi_day[1];b.style.height=p,b.style.visibility=-1==a[0]?"hidden":"visible",b.className=a[0]?"dhx_multi_day_icon":"dhx_multi_day_icon_small",this._dy_shift=(a[0]+1)*i,this.config.multi_day_height_limit&&(this._dy_shift=Math.min(this.config.multi_day_height_limit,this._dy_shift)),a[0]=0,m!=v&&(r.style.top=parseInt(r.style.top)+2+"px",x.style.overflowY="auto",b.style.position="fixed",b.style.top="",
b.style.left="")}}return t},g._get_event_sday=function(t){var e=this.date.day_start(new Date(t.start_date));return Math.round((e.valueOf()-this._min_date.valueOf())/864e5)},g._get_event_mapped_end_date=function(t){var e=t.end_date;if(this.config.separate_short_events){var i=(t.end_date-t.start_date)/6e4;i<this._min_mapped_duration&&(e=this.date.add(e,this._min_mapped_duration-i,"minute"))}return e},g._pre_render_events_line=function(t,e){t.sort(function(t,e){
return t.start_date.valueOf()==e.start_date.valueOf()?t.id>e.id?1:-1:t.start_date>e.start_date?1:-1});var i=[],n=[];this._min_mapped_duration=Math.ceil(60*this.xy.min_event_height/this.config.hour_size_px);for(var a=0;a<t.length;a++){var r=t[a],s=r.start_date,o=r.end_date,d=s.getHours(),l=o.getHours();if(r._sday=this._get_event_sday(r),this._ignores[r._sday])t.splice(a,1),a--;else{if(i[r._sday]||(i[r._sday]=[]),!e){r._inner=!1;for(var _=i[r._sday];_.length;){
var h=_[_.length-1],c=this._get_event_mapped_end_date(h);if(!(c.valueOf()<=r.start_date.valueOf()))break;_.splice(_.length-1,1)}for(var u=_.length,f=!1,g=0;g<_.length;g++){var h=_[g],c=this._get_event_mapped_end_date(h);if(c.valueOf()<=r.start_date.valueOf()){f=!0,r._sorder=h._sorder,u=g,r._inner=!0;break}}if(_.length&&(_[_.length-1]._inner=!0),!f)if(_.length)if(_.length<=_[_.length-1]._sorder){if(_[_.length-1]._sorder)for(g=0;g<_.length;g++){for(var v=!1,m=0;m<_.length;m++)if(_[m]._sorder==g){
v=!0;break}if(!v){r._sorder=g;break}}else r._sorder=0;r._inner=!0}else{var p=_[0]._sorder;for(g=1;g<_.length;g++)_[g]._sorder>p&&(p=_[g]._sorder);r._sorder=p+1,r._inner=!1}else r._sorder=0;_.splice(u,u==_.length?0:1,r),_.length>(_.max_count||0)?(_.max_count=_.length,r._count=_.length):r._count=r._count?r._count:1}(d<this.config.first_hour||l>=this.config.last_hour)&&(n.push(r),t[a]=r=this._copy_event(r),d<this.config.first_hour&&(r.start_date.setHours(this.config.first_hour),
r.start_date.setMinutes(0)),l>=this.config.last_hour&&(r.end_date.setMinutes(0),r.end_date.setHours(this.config.last_hour)),r.start_date>r.end_date||d==this.config.last_hour)&&(t.splice(a,1),a--)}}if(!e){for(var a=0;a<t.length;a++)t[a]._count=i[t[a]._sday].max_count;for(var a=0;a<n.length;a++)n[a]._count=i[n[a]._sday].max_count}return t},g._time_order=function(t){t.sort(function(t,e){
return t.start_date.valueOf()==e.start_date.valueOf()?t._timed&&!e._timed?1:!t._timed&&e._timed?-1:t.id>e.id?1:-1:t.start_date>e.start_date?1:-1})},g._is_any_multiday_cell_visible=function(t,e,i){var n=this._cols.length,a=!1,r=t,s=!0,o=new Date(e);for(g.date.day_start(new Date(e)).valueOf()!=e.valueOf()&&(o=g.date.day_start(o),o=g.date.add(o,1,"day"));r<o;){s=!1;var d=this.locate_holder_day(r,!1,i),l=d%n;if(!this._ignores[l]){a=!0;break}r=g.date.add(r,1,"day")}return s||a},
g._pre_render_events_table=function(t,e){this._time_order(t);for(var i,n=[],a=[[],[],[],[],[],[],[]],r=this._colsS.heights,s=this._cols.length,o={},d=0;d<t.length;d++){var l=t[d],_=l.id;o[_]||(o[_]={first_chunk:!0,last_chunk:!0});var h=o[_],c=i||l.start_date,u=l.end_date;c<this._min_date&&(h.first_chunk=!1,c=this._min_date),u>this._max_date&&(h.last_chunk=!1,u=this._max_date);var f=this.locate_holder_day(c,!1,l);if(l._sday=f%s,!this._ignores[l._sday]||!l._timed){
var v=this.locate_holder_day(u,!0,l)||s;l._eday=v%s||s,l._length=v-f,l._sweek=Math.floor((this._correct_shift(c.valueOf(),1)-this._min_date.valueOf())/(864e5*s));if(g._is_any_multiday_cell_visible(c,u,l)){var m,p=a[l._sweek];for(m=0;m<p.length&&!(p[m]._eday<=l._sday);m++);if(l._sorder&&e||(l._sorder=m),l._sday+l._length<=s)i=null,n.push(l),p[m]=l,r[l._sweek]=p.length-1,l._first_chunk=h.first_chunk,l._last_chunk=h.last_chunk;else{var x=this._copy_event(l);x.id=l.id,x._length=s-l._sday,x._eday=s,
x._sday=l._sday,x._sweek=l._sweek,x._sorder=l._sorder,x.end_date=this.date.add(c,x._length,"day"),x._first_chunk=h.first_chunk,h.first_chunk&&(h.first_chunk=!1),n.push(x),p[m]=x,i=x.end_date,r[l._sweek]=p.length-1,d--}}}}return n},g._copy_dummy=function(){var t=new Date(this.start_date),e=new Date(this.end_date);this.start_date=t,this.end_date=e},g._copy_event=function(t){return this._copy_dummy.prototype=t,new this._copy_dummy},g._rendered=[],g.clear_view=function(){
for(var t=0;t<this._rendered.length;t++){var e=this._rendered[t];e.parentNode&&e.parentNode.removeChild(e)}this._rendered=[]},g.updateEvent=function(t){var e=this.getEvent(t);this.clear_event(t),e&&this.is_visible_events(e)&&this.filter_event(t,e)&&(this._table_view||this.config.multi_day||e._timed)&&(this.config.update_render?this.render_view_data():"month"!=this.getState().mode||this.getState().drag_id||this.isOneDayEvent(e)?this.render_view_data([e],!0):this.render_view_data())},
g.clear_event=function(t){this.for_rendered(t,function(t,e){t.parentNode&&t.parentNode.removeChild(t),g._rendered.splice(e,1)})},g._y_from_date=function(t){var e=60*t.getHours()+t.getMinutes();return Math.round((60*e*1e3-60*this.config.first_hour*60*1e3)*this.config.hour_size_px/36e5)%(24*this.config.hour_size_px)},g._calc_event_y=function(t,e){e=e||0;var i=60*t.start_date.getHours()+t.start_date.getMinutes(),n=60*t.end_date.getHours()+t.end_date.getMinutes()||60*g.config.last_hour;return{
top:this._y_from_date(t.start_date),height:Math.max(e,(n-i)*this.config.hour_size_px/60)}},g.render_event=function(t,e,i){var n=g.xy.menu_width,a=this.config.use_select_menu_space?0:n;if(!(t._sday<0)){var r=g.locate_holder(t._sday);if(r){e=e||r;var s=this._calc_event_y(t,g.xy.min_event_height),o=s.top,d=s.height,l=t._count||1,_=t._sorder||0;i=i||r.clientWidth;var h=Math.floor((i-a)/l),c=_*h+1;if(t._inner||(h*=l-_),this.config.cascade_event_display){
var u=this.config.cascade_event_count,f=this.config.cascade_event_margin;c=_%u*f;var v=t._inner?(l-_-1)%u*f/2:0;h=Math.floor(i-a-c-v)}var m=this._render_v_bar(t,a+c,o,h,d,t._text_style,g.templates.event_header(t.start_date,t.end_date,t),g.templates.event_text(t.start_date,t.end_date,t));this._waiAria.eventAttr(t,m),this._rendered.push(m),e.appendChild(m);if(c=c+parseInt(this.config.rtl?r.style.right:r.style.left,10)+a,this._edit_id==t.id){m.style.zIndex=1,h=Math.max(h-4,g.xy.editor_width),
m=document.createElement("div"),m.setAttribute("event_id",t.id),this._waiAria.eventAttr(t,m),m.className="dhx_cal_event dhx_cal_editor",this.config.rtl&&c++,this.set_xy(m,h,d-20,c,o+(g.xy.event_header_height||14)),t.color&&(m.style.backgroundColor=t.color);var p=g.templates.event_class(t.start_date,t.end_date,t);p&&(m.className+=" "+p);var x=document.createElement("div");this.set_xy(x,h-6,d-26),x.style.cssText+=";margin:2px 2px 2px 2px;overflow:hidden;",m.appendChild(x),
this._els.dhx_cal_data[0].appendChild(m),this._rendered.push(m),x.innerHTML="<textarea class='dhx_cal_editor'>"+t.text+"</textarea>",this._editor=x.querySelector("textarea"),this._quirks7&&(this._editor.style.height=d-12+"px"),this._editor.onkeydown=function(t){if((t||event).shiftKey)return!0;var e=(t||event).keyCode;e==g.keys.edit_save&&g.editStop(!0),e==g.keys.edit_cancel&&g.editStop(!1),e!=g.keys.edit_save&&e!=g.keys.edit_cancel||t.preventDefault&&t.preventDefault()},
this._editor.onselectstart=function(t){return(t||event).cancelBubble=!0,!0},g._focus(this._editor,!0),this._els.dhx_cal_data[0].scrollLeft=0}if(0!==this.xy.menu_width&&this._select_id==t.id){this.config.cascade_event_display&&this._drag_mode&&(m.style.zIndex=1);for(var b,y=this.config["icons_"+(this._edit_id==t.id?"edit":"select")],w="",D=t.color?"background-color: "+t.color+";":"",E=t.textColor?"color: "+t.textColor+";":"",A=0;A<y.length;A++)b=this._waiAria.eventMenuAttrString(y[A]),
w+="<div class='dhx_menu_icon "+y[A]+"' style='"+D+E+"' title='"+this.locale.labels[y[A]]+"'"+b+"></div>";var S=this._render_v_bar(t,c-n+1,o,n,20*y.length+26-2,"","<div style='"+D+E+"' class='dhx_menu_head'></div>",w,!0);S.style.left=c-n+1,this._els.dhx_cal_data[0].appendChild(S),this._rendered.push(S)}this.config.drag_highlight&&this._drag_id==t.id&&this.highlightEventPosition(t)}}},g._render_v_bar=function(t,e,i,n,a,r,s,o,d){
var l=document.createElement("div"),_=t.id,h=d?"dhx_cal_event dhx_cal_select_menu":"dhx_cal_event",c=g.getState();c.drag_id==t.id&&(h+=" dhx_cal_event_drag"),c.select_id==t.id&&(h+=" dhx_cal_event_selected");var u=g.templates.event_class(t.start_date,t.end_date,t);u&&(h=h+" "+u),this.config.cascade_event_display&&(h+=" dhx_cal_event_cascade")
;var f=t.color?"background-color:"+t.color+";":"",v=t.textColor?"color:"+t.textColor+";":"",m=g._border_box_events(),p=n-2,x=m?p:n-4,b=m?p:n-6,y=m?p:n-(this._quirks?4:14),w=m?p-2:n-8,D=m?a-this.xy.event_header_height-1:a-(this._quirks?20:30)+1,E='<div event_id="'+_+'" class="'+h+'" style="position:absolute; top:'+i+"px; "+(this.config.rtl?"right:":"left:")+e+"px; width:"+x+"px; height:"+a+"px;"+(r||"")+'"></div>';l.innerHTML=E;var A=l.cloneNode(!0).firstChild
;if(!d&&g.renderEvent(A,t,n,a,s,o))return A;A=l.firstChild;var S='<div class="dhx_event_move dhx_header" style=" width:'+b+"px;"+f+'" >&nbsp;</div>';S+='<div class="dhx_event_move dhx_title" style="'+f+v+'">'+s+"</div>",S+='<div class="dhx_body" style=" width:'+y+"px; height:"+D+"px;"+f+v+'">'+o+"</div>";var M="dhx_event_resize dhx_footer";return(d||!1===t._drag_resize)&&(M="dhx_resize_denied "+M),S+='<div class="'+M+'" style=" width:'+w+"px;"+(d?" margin-top:-1px;":"")+f+v+'" ></div>',
A.innerHTML=S,A},g.renderEvent=function(){return!1},g.locate_holder=function(t){return"day"==this._mode?this._els.dhx_cal_data[0].firstChild:this._els.dhx_cal_data[0].childNodes[t]},g.locate_holder_day=function(t,e){var i=Math.floor((this._correct_shift(t,1)-this._min_date)/864e5);return e&&this.date.time_part(t)&&i++,i},g._get_dnd_order=function(t,e,i){if(!this._drag_event)return t;this._drag_event._orig_sorder?t=this._drag_event._orig_sorder:this._drag_event._orig_sorder=t
;for(var n=e*t;n+e>i;)t--,n-=e;return t=Math.max(t,0)},g._get_event_bar_pos=function(t){var e=this.config.rtl,i=this._colsS,n=i[t._sday],a=i[t._eday];e&&(n=i[i.col_length]-i[t._eday]+i[0],a=i[i.col_length]-i[t._sday]+i[0]),a==n&&(a=i[t._eday+1]);var r=this.xy.bar_height,s=t._sorder;if(t.id==this._drag_id){var o=i.heights[t._sweek+1]-i.heights[t._sweek]-this.xy.month_head_height;s=g._get_dnd_order(s,r,o)}var d=s*r;return{x:n,x2:a,y:i.heights[t._sweek]+(i.height?this.xy.month_scale_height+2:2)+d}
},g.render_event_bar=function(t){var e=this._rendered_location,i=this._get_event_bar_pos(t),n=i.y,a=i.x,r=i.x2,s="";if(r){var o=g.config.resize_month_events&&"month"==this._mode&&(!t._timed||g.config.resize_month_timed),d=document.createElement("div"),l=t.hasOwnProperty("_first_chunk")&&t._first_chunk,_=t.hasOwnProperty("_last_chunk")&&t._last_chunk,h=o&&(t._timed||l),c=o&&(t._timed||_),u=!0,f="dhx_cal_event_clear";t._timed&&!o||(u=!1,f="dhx_cal_event_line"),l&&(f+=" dhx_cal_event_line_start"),
_&&(f+=" dhx_cal_event_line_end"),h&&(s+="<div class='dhx_event_resize dhx_event_resize_start'></div>"),c&&(s+="<div class='dhx_event_resize dhx_event_resize_end'></div>");var v=g.templates.event_class(t.start_date,t.end_date,t);v&&(f+=" "+v)
;var m=t.color?"background:"+t.color+";":"",p=t.textColor?"color:"+t.textColor+";":"",x=["position:absolute","top:"+n+"px","left:"+a+"px","width:"+(r-a-3-(u?1:0))+"px",p,m,t._text_style||""].join(";"),b="<div event_id='"+t.id+"' class='"+f+"' style='"+x+"'"+this._waiAria.eventBarAttrString(t)+">";o&&(b+=s),"month"==g.getState().mode&&(t=g.getEvent(t.id)),t._timed&&(b+=g.templates.event_bar_date(t.start_date,t.end_date,t)),b+=g.templates.event_bar_text(t.start_date,t.end_date,t)+"</div>",
b+="</div>",d.innerHTML=b,this._rendered.push(d.firstChild),e.appendChild(d.firstChild)}},g._locate_event=function(t){for(var e=null;t&&!e&&t.getAttribute;)e=t.getAttribute("event_id"),t=t.parentNode;return e},g._locate_css=function(t,e,i){void 0===i&&(i=!0);for(var n=t.target||t.srcElement,a="";n;){if(a=g._getClassName(n)){var r=a.indexOf(e);if(r>=0){if(!i)return n;var s=0===r||!g._trim(a.charAt(r-1)),o=r+e.length>=a.length||!g._trim(a.charAt(r+e.length));if(s&&o)return n}}n=n.parentNode}
return null},g.edit=function(t){this._edit_id!=t&&(this.editStop(!1,t),this._edit_id=t,this.updateEvent(t))},g.editStop=function(t,e){if(!e||this._edit_id!=e){var i=this.getEvent(this._edit_id);i&&(t&&(i.text=this._editor.value),this._edit_id=null,this._editor=null,this.updateEvent(i.id),this._edit_stop_event(i,t))}},g._edit_stop_event=function(t,e){this._new_event?(e?this.callEvent("onEventAdded",[t.id,t]):t&&this.deleteEvent(t.id,!0),
this._new_event=null):e&&this.callEvent("onEventChanged",[t.id,t])},g.getEvents=function(t,e){var i=[];for(var n in this._events){var a=this._events[n];a&&(!t&&!e||a.start_date<e&&a.end_date>t)&&i.push(a)}return i},g.getRenderedEvent=function(t){if(t){for(var e=g._rendered,i=0;i<e.length;i++){var n=e[i];if(n.getAttribute("event_id")==t)return n}return null}},g.showEvent=function(t,e){var i;t&&"object"==typeof t&&(e=t.mode,i=t.section,t=t.section)
;var n="number"==typeof t||"string"==typeof t?g.getEvent(t):t;if(e=e||g._mode,n&&(!this.checkEvent("onBeforeEventDisplay")||this.callEvent("onBeforeEventDisplay",[n,e]))){var a=g.config.scroll_hour;g.config.scroll_hour=n.start_date.getHours();var r=g.config.preserve_scroll;g.config.preserve_scroll=!1;var s=n.color,o=n.textColor;if(g.config.highlight_displayed_event&&(n.color=g.config.displayed_event_color,n.textColor=g.config.displayed_event_text_color),
g.setCurrentView(new Date(n.start_date),e),n.color=s,n.textColor=o,g.config.scroll_hour=a,g.config.preserve_scroll=r,g.matrix&&g.matrix[e]){var d=g.getView(),l=d.y_property,_=g.getEvent(n.id);if(_){if(!i){var i=_[l];Array.isArray(i)?i=i[0]:"string"==typeof i&&g.config.section_delimiter&&i.indexOf(g.config.section_delimiter)>-1&&(i=i.split(g.config.section_delimiter)[0])}var h=d.posFromSection(i),c=d.posFromDate(_.start_date),u=g.$container.querySelector(".dhx_timeline_data_wrapper")
;c-=(u.offsetWidth-d.dx)/2,h=h-u.offsetHeight/2+d.dy/2,d.scrollTo({left:c,top:h})}}g.callEvent("onAfterEventDisplay",[n,e])}},g._append_drag_marker=function(t){if(!t.parentNode){var e=g._els.dhx_cal_data[0],i=e.lastChild,n=g._getClassName(i);n.indexOf("dhx_scale_holder")<0&&i.previousSibling&&(i=i.previousSibling),n=g._getClassName(i),i&&0===n.indexOf("dhx_scale_holder")&&i.appendChild(t)}},g._update_marker_position=function(t,e){var i=g._calc_event_y(e,0);t.style.top=i.top+"px",
t.style.height=i.height+"px"},g.highlightEventPosition=function(t){var e=document.createElement("div");e.setAttribute("event_id",t.id),this._rendered.push(e),this._update_marker_position(e,t);var i=this.templates.drag_marker_class(t.start_date,t.end_date,t),n=this.templates.drag_marker_content(t.start_date,t.end_date,t);e.className="dhx_drag_marker",i&&(e.className+=" "+i),n&&(e.innerHTML=n),this._append_drag_marker(e)},g._loaded={},g._load=function(t,e){function i(t){g.on_load(t),
g.callEvent("onLoadEnd",[])}if(t=t||this._load_url){t+=(-1==t.indexOf("?")?"?":"&")+"timeshift="+(new Date).getTimezoneOffset(),this.config.prevent_cache&&(t+="&uid="+this.uid());var n;if(e=e||this._date,this._load_mode){var a=this.templates.load_format;for(e=this.date[this._load_mode+"_start"](new Date(e.valueOf()));e>this._min_date;)e=this.date.add(e,-1,this._load_mode);n=e;for(var r=!0;n<this._max_date;)n=this.date.add(n,1,this._load_mode),
this._loaded[a(e)]&&r?e=this.date.add(e,1,this._load_mode):r=!1;var s=n;do{n=s,s=this.date.add(n,-1,this._load_mode)}while(s>e&&this._loaded[a(s)]);if(n<=e)return!1;for(g.$ajax.get(t+"&from="+a(e)+"&to="+a(n),i);e<n;)this._loaded[a(e)]=!0,e=this.date.add(e,1,this._load_mode)}else g.$ajax.get(t,i);return this.callEvent("onXLS",[]),this.callEvent("onLoadStart",[]),!0}},g._parsers={},g._parsers.xml={canParse:function(t,e){if(e.responseXML&&e.responseXML.firstChild)return!0;try{
var i=g.$ajax.parse(e.responseText),n=g.$ajax.xmltop("data",i);if(n&&"data"===n.tagName)return!0}catch(t){}return!1},parse:function(t){var e;if(t.xmlDoc.responseXML||(t.xmlDoc.responseXML=g.$ajax.parse(t.xmlDoc.responseText)),e=g.$ajax.xmltop("data",t.xmlDoc),"data"!=e.tagName)return null;var i=e.getAttribute("dhx_security");i&&(window.dhtmlx&&(dhtmlx.security_key=i),g.security_key=i);for(var n=g.$ajax.xpath("//coll_options",t.xmlDoc),a=0;a<n.length;a++){
var r=n[a].getAttribute("for"),s=g.serverList[r];s||(g.serverList[r]=s=[]),s.splice(0,s.length);for(var o=g.$ajax.xpath(".//item",n[a]),d=0;d<o.length;d++){for(var l=o[d],_=l.attributes,h={key:o[d].getAttribute("value"),label:o[d].getAttribute("label")},c=0;c<_.length;c++){var u=_[c];"value"!=u.nodeName&&"label"!=u.nodeName&&(h[u.nodeName]=u.nodeValue)}s.push(h)}}n.length&&g.callEvent("onOptionsLoad",[]);for(var f=g.$ajax.xpath("//userdata",t.xmlDoc),a=0;a<f.length;a++){
var v=g._xmlNodeToJSON(f[a]);g._userdata[v.name]=v.text}var m=[];e=g.$ajax.xpath("//event",t.xmlDoc);for(var a=0;a<e.length;a++){var p=m[a]=g._xmlNodeToJSON(e[a]);g._init_event(p)}return m}},g.json=g._parsers.json={canParse:function(t){if(t&&"object"==typeof t)return!0;if("string"==typeof t)try{var e=JSON.parse(t);return"[object Object]"===Object.prototype.toString.call(e)||"[object Array]"===Object.prototype.toString.call(e)}catch(t){return!1}return!1},parse:function(t){var e=[]
;"string"==typeof t&&(t=JSON.parse(t)),e="[object Array]"===Object.prototype.toString.call(t)?t:t?t.data:[],e=e||[],t.dhx_security&&(window.dhtmlx&&(dhtmlx.security_key=t.dhx_security),g.security_key=t.dhx_security);var i=t&&t.collections?t.collections:{},n=!1;for(var a in i)if(i.hasOwnProperty(a)){n=!0;var r=i[a],s=g.serverList[a];s||(g.serverList[a]=s=[]),s.splice(0,s.length);for(var o=0;o<r.length;o++){var d=r[o],l={key:d.value,label:d.label};for(var _ in d)if(d.hasOwnProperty(_)){
if("value"==_||"label"==_)continue;l[_]=d[_]}s.push(l)}}n&&g.callEvent("onOptionsLoad",[]);for(var h=[],c=0;c<e.length;c++){var u=e[c];g._init_event(u),h.push(u)}return h}},g.ical=g._parsers.ical={canParse:function(t){return"string"==typeof t&&new RegExp("^BEGIN:VCALENDAR").test(t)},parse:function(t){var e=t.match(RegExp(this.c_start+"[^\f]*"+this.c_end,""));if(e.length){e[0]=e[0].replace(/[\r\n]+ /g,""),e[0]=e[0].replace(/[\r\n]+(?=[a-z \t])/g," "),e[0]=e[0].replace(/\;[^:\r\n]*:/g,":")
;for(var i,n=[],a=RegExp("(?:"+this.e_start+")([^\f]*?)(?:"+this.e_end+")","g");null!==(i=a.exec(e));){for(var r,s={},o=/[^\r\n]+[\r\n]+/g;null!==(r=o.exec(i[1]));)this.parse_param(r.toString(),s);s.uid&&!s.id&&(s.id=s.uid),n.push(s)}return n}},parse_param:function(t,e){var i=t.indexOf(":");if(-1!=i){var n=t.substr(0,i).toLowerCase(),a=t.substr(i+1).replace(/\\\,/g,",").replace(/[\r\n]+$/,"");"summary"==n?n="text":"dtstart"==n?(n="start_date",a=this.parse_date(a,0,0)):"dtend"==n&&(n="end_date",
a=this.parse_date(a,0,0)),e[n]=a}},parse_date:function(t,e,i){var n=t.split("T"),a=!1;n[1]&&(e=n[1].substr(0,2),i=n[1].substr(2,2),a=!("Z"!=n[1][6]));var r=n[0].substr(0,4),s=parseInt(n[0].substr(4,2),10)-1,o=n[0].substr(6,2);return g.config.server_utc||a?new Date(Date.UTC(r,s,o,e,i)):new Date(r,s,o,e,i)},c_start:"BEGIN:VCALENDAR",e_start:"BEGIN:VEVENT",e_end:"END:VEVENT",c_end:"END:VCALENDAR"},g.on_load=function(t){this.callEvent("onBeforeParse",[]);var e,i=!1,n=!1;for(var a in this._parsers){
var r=this._parsers[a];if(r.canParse(t.xmlDoc.responseText,t.xmlDoc)){try{var s=t.xmlDoc.responseText;"xml"===a&&(s=t),e=r.parse(s),e||(i=!0)}catch(t){i=!0}n=!0;break}}if(!n)if(this._process&&this[this._process])try{e=this[this._process].parse(t.xmlDoc.responseText)}catch(t){i=!0}else i=!0;(i||t.xmlDoc.status&&t.xmlDoc.status>=400)&&(this.callEvent("onLoadError",[t.xmlDoc]),e=[]),this._process_loading(e),this.callEvent("onXLE",[]),this.callEvent("onParse",[])},g._process_loading=function(t){
this._loading=!0,this._not_render=!0;for(var e=0;e<t.length;e++)this.callEvent("onEventLoading",[t[e]])&&this.addEvent(t[e]);this._not_render=!1,this._render_wait&&this.render_view_data(),this._loading=!1,this._after_call&&this._after_call(),this._after_call=null},g._init_event=function(t){t.text=t.text||t._tagvalue||"",t.start_date=g._init_date(t.start_date),t.end_date=g._init_date(t.end_date)},g._init_date=function(t){return t?"string"==typeof t?g._helpers.parseDate(t):new Date(t):null},
g.json={},g.json.parse=function(t){var e=[];"string"==typeof t&&(t=JSON.parse(t)),e="[object Array]"===Object.prototype.toString.call(t)?t:t?t.data:[],e=e||[],t.dhx_security&&(window.dhtmlx&&(dhtmlx.security_key=t.dhx_security),g.security_key=t.dhx_security);var i=t&&t.collections?t.collections:{},n=!1;for(var a in i)if(i.hasOwnProperty(a)){n=!0;var r=i[a],s=g.serverList[a];s||(g.serverList[a]=s=[]),s.splice(0,s.length);for(var o=0;o<r.length;o++){var d=r[o],l={key:d.value,label:d.label}
;for(var _ in d)if(d.hasOwnProperty(_)){if("value"==_||"label"==_)continue;l[_]=d[_]}s.push(l)}}n&&g.callEvent("onOptionsLoad",[]);for(var h=[],c=0;c<e.length;c++){var u=e[c];g._init_event(u),h.push(u)}return h},g.parse=function(t,e){this._process=e,this.on_load({xmlDoc:{responseText:t}})},g.load=function(t,e){"string"==typeof e&&(this._process=e,e=arguments[2]),this._load_url=t,this._after_call=e,this._load(t,this._date)},g.setLoadMode=function(t){"all"==t&&(t=""),this._load_mode=t},
g.serverList=function(t,e){return e?(this.serverList[t]=e.slice(0),this.serverList[t]):(this.serverList[t]=this.serverList[t]||[],this.serverList[t])},g._userdata={},g._xmlNodeToJSON=function(t){for(var e={},i=0;i<t.attributes.length;i++)e[t.attributes[i].name]=t.attributes[i].value;for(var i=0;i<t.childNodes.length;i++){var n=t.childNodes[i];1==n.nodeType&&(e[n.tagName]=n.firstChild?n.firstChild.nodeValue:"")}return e.text||(e.text=t.firstChild?t.firstChild.nodeValue:""),e},
g.attachEvent("onXLS",function(){if(!0===this.config.show_loading){var t;t=this.config.show_loading=document.createElement("div"),t.className="dhx_loading",t.style.left=Math.round((this._x-128)/2)+"px",t.style.top=Math.round((this._y-15)/2)+"px",this._obj.appendChild(t)}}),g.attachEvent("onXLE",function(){var t=this.config.show_loading;t&&"object"==typeof t&&(t.parentNode&&t.parentNode.removeChild(t),this.config.show_loading=!0)}),g._lightbox_controls={},g.formSection=function(t){
var e=this.config.lightbox.sections,i=0;for(i;i<e.length&&e[i].name!=t;i++);var n=e[i];g._lightbox||g.getLightbox();var a=document.getElementById(n.id),r=a.nextSibling,s={section:n,header:a,node:r,getValue:function(t){return g.form_blocks[n.type].get_value(r,t||{},n)},setValue:function(t,e){return g.form_blocks[n.type].set_value(r,t,e||{},n)}},o=g._lightbox_controls["get_"+n.type+"_control"];return o?o(s):s},g._lightbox_controls.get_template_control=function(t){return t.control=t.node,t},
g._lightbox_controls.get_select_control=function(t){return t.control=t.node.getElementsByTagName("select")[0],t},g._lightbox_controls.get_textarea_control=function(t){return t.control=t.node.getElementsByTagName("textarea")[0],t},g._lightbox_controls.get_time_control=function(t){return t.control=t.node.getElementsByTagName("select"),t},g._lightbox_controls.defaults={template:{height:30},textarea:{height:200},select:{height:23},time:{height:20}},g.form_blocks={template:{render:function(t){
var e=g._lightbox_controls.defaults.template,i=e?e.height:30;return"<div class='dhx_cal_ltext dhx_cal_template' style='height:"+(t.height||i||30)+"px;'></div>"},set_value:function(t,e,i,n){t.innerHTML=e||""},get_value:function(t,e,i){return t.innerHTML||""},focus:function(t){}},textarea:{render:function(t){var e=g._lightbox_controls.defaults.textarea,i=e?e.height:200;return"<div class='dhx_cal_ltext' style='height:"+(t.height||i||"130")+"px;'><textarea></textarea></div>"},
set_value:function(t,e,i){g.form_blocks.textarea._get_input(t).value=e||""},get_value:function(t,e){return g.form_blocks.textarea._get_input(t).value},focus:function(t){var e=g.form_blocks.textarea._get_input(t);g._focus(e,!0)},_get_input:function(t){return t.getElementsByTagName("textarea")[0]}},select:{render:function(t){
for(var e=g._lightbox_controls.defaults.select,i=e?e.height:23,n=(t.height||i||"23")+"px",a="<div class='dhx_cal_ltext' style='height:"+n+";'><select style='width:100%;'>",r=0;r<t.options.length;r++)a+="<option value='"+t.options[r].key+"'>"+t.options[r].label+"</option>";return a+="</select></div>"},set_value:function(t,e,i,n){var a=t.firstChild;!a._dhx_onchange&&n.onchange&&(a.onchange=n.onchange,a._dhx_onchange=!0),void 0===e&&(e=(a.options[0]||{}).value),a.value=e||""},
get_value:function(t,e){return t.firstChild.value},focus:function(t){var e=t.firstChild;g._focus(e,!0)}},time:{render:function(t){t.time_format||(t.time_format=["%H:%i","%d","%m","%Y"]),t._time_format_order={};var e=t.time_format,i=g.config,n=g.date.date_part(g._currentDate()),a=1440,r=0;g.config.limit_time_select&&(a=60*i.last_hour+1,r=60*i.first_hour,n.setHours(i.first_hour));for(var s="",o=0;o<e.length;o++){var d=e[o];o>0&&(s+=" ");var l="",_="";switch(d){case"%Y":
l="dhx_lightbox_year_select",t._time_format_order[3]=o;var h,c,u;t.year_range&&(isNaN(t.year_range)?t.year_range.push&&(c=t.year_range[0],u=t.year_range[1]):h=t.year_range),h=h||10;var f=f||Math.floor(h/2);c=c||n.getFullYear()-f,u=u||c+h;for(var v=c;v<u;v++)_+="<option value='"+v+"'>"+v+"</option>";break;case"%m":l="dhx_lightbox_month_select",t._time_format_order[2]=o;for(var v=0;v<12;v++)_+="<option value='"+v+"'>"+this.locale.date.month_full[v]+"</option>";break;case"%d":
l="dhx_lightbox_day_select",t._time_format_order[1]=o;for(var v=1;v<32;v++)_+="<option value='"+v+"'>"+v+"</option>";break;case"%H:%i":l="dhx_lightbox_time_select",t._time_format_order[0]=o;var v=r,m=n.getDate();for(t._time_values=[];v<a;){_+="<option value='"+v+"'>"+this.templates.time_picker(n)+"</option>",t._time_values.push(v),n.setTime(n.valueOf()+60*this.config.time_step*1e3);v=24*(n.getDate()!=m?1:0)*60+60*n.getHours()+n.getMinutes()}}if(_){var p=g._waiAria.lightboxSelectAttrString(d)
;s+="<select class='"+l+"' "+(t.readonly?"disabled='disabled'":"")+p+">"+_+"</select> "}}var x=g._lightbox_controls.defaults.select;return"<div style='height:"+((x?x.height:23)||30)+"px;padding-top:0px;font-size:inherit;' class='dhx_section_time'>"+s+"<span style='font-weight:normal; font-size:10pt;'> &nbsp;&ndash;&nbsp; </span>"+s+"</div>"},set_value:function(t,e,i,n){function a(t,e,i){for(var a=n._time_values,r=60*i.getHours()+i.getMinutes(),s=r,o=!1,d=0;d<a.length;d++){var _=a[d];if(_===r){
o=!0;break}_<r&&(s=_)}t[e+l[0]].value=o?r:s,o||s||(t[e+l[0]].selectedIndex=-1),t[e+l[1]].value=i.getDate(),t[e+l[2]].value=i.getMonth(),t[e+l[3]].value=i.getFullYear()}var r,s,o=g.config,d=t.getElementsByTagName("select"),l=n._time_format_order;if(o.full_day){if(!t._full_day){var _="<label class='dhx_fullday'><input type='checkbox' name='full_day' value='true'> "+g.locale.labels.full_day+"&nbsp;</label></input>";g.config.wide_form||(_=t.previousSibling.innerHTML+_),t.previousSibling.innerHTML=_,
t._full_day=!0}var h=t.previousSibling.getElementsByTagName("input")[0];h.checked=0===g.date.time_part(i.start_date)&&0===g.date.time_part(i.end_date),d[l[0]].disabled=h.checked,d[l[0]+d.length/2].disabled=h.checked,h.onclick=function(){if(h.checked){var e={};g.form_blocks.time.get_value(t,e,n),r=g.date.date_part(e.start_date),s=g.date.date_part(e.end_date),(+s==+r||+s>=+r&&(0!==i.end_date.getHours()||0!==i.end_date.getMinutes()))&&(s=g.date.add(s,1,"day"))}else r=null,s=null
;d[l[0]].disabled=h.checked,d[l[0]+d.length/2].disabled=h.checked,a(d,0,r||i.start_date),a(d,4,s||i.end_date)}}if(o.auto_end_date&&o.event_duration)for(var c=function(){r=new Date(d[l[3]].value,d[l[2]].value,d[l[1]].value,0,d[l[0]].value),s=new Date(r.getTime()+60*g.config.event_duration*1e3),a(d,4,s)},u=0;u<4;u++)d[u].onchange=c;a(d,0,i.start_date),a(d,4,i.end_date)},get_value:function(t,e,i){var n=t.getElementsByTagName("select"),a=i._time_format_order
;if(e.start_date=new Date(n[a[3]].value,n[a[2]].value,n[a[1]].value,0,n[a[0]].value),e.end_date=new Date(n[a[3]+4].value,n[a[2]+4].value,n[a[1]+4].value,0,n[a[0]+4].value),!n[a[3]].value||!n[a[3]+4].value){var r=g.getEvent(g._lightbox_id);r&&(e.start_date=r.start_date,e.end_date=r.end_date)}return e.end_date<=e.start_date&&(e.end_date=g.date.add(e.start_date,g.config.time_step,"minute")),{start_date:new Date(e.start_date),end_date:new Date(e.end_date)}},focus:function(t){
g._focus(t.getElementsByTagName("select")[0])}}},g._setLbPosition=function(t){if(t){var e=window.pageYOffset||document.body.scrollTop||document.documentElement.scrollTop,i=window.pageXOffset||document.body.scrollLeft||document.documentElement.scrollLeft,n=window.innerHeight||document.documentElement.clientHeight;t.style.top=e?Math.round(e+Math.max((n-t.offsetHeight)/2,0))+"px":Math.round(Math.max((n-t.offsetHeight)/2,0)+9)+"px",
document.documentElement.scrollWidth>document.body.offsetWidth?t.style.left=Math.round(i+(document.body.offsetWidth-t.offsetWidth)/2)+"px":t.style.left=Math.round((document.body.offsetWidth-t.offsetWidth)/2)+"px"}},g.showCover=function(t){t&&(t.style.display="block",this._setLbPosition(t)),g.config.responsive_lightbox&&(document.documentElement.classList.add("dhx_cal_overflow_container"),document.body.classList.add("dhx_cal_overflow_container")),this.show_cover()},g.showLightbox=function(t){
if(t){if(!this.callEvent("onBeforeLightbox",[t]))return void(this._new_event&&(this._new_event=null));var e=this.getLightbox();this.showCover(e),this._fill_lightbox(t,e),this._waiAria.lightboxVisibleAttr(e),this.callEvent("onLightbox",[t])}},g._fill_lightbox=function(t,e){var i=this.getEvent(t),n=e.getElementsByTagName("span"),a=[];if(g.templates.lightbox_header){a.push("");var r=g.templates.lightbox_header(i.start_date,i.end_date,i);a.push(r),n[1].innerHTML="",n[2].innerHTML=r}else{
var s=this.templates.event_header(i.start_date,i.end_date,i),o=(this.templates.event_bar_text(i.start_date,i.end_date,i)||"").substr(0,70);a.push(s),a.push(o),n[1].innerHTML=s,n[2].innerHTML=o}this._waiAria.lightboxHeader(e,a.join(" "));for(var d=this.config.lightbox.sections,l=0;l<d.length;l++){var _=d[l],h=g._get_lightbox_section_node(_),c=this.form_blocks[_.type],u=void 0!==i[_.map_to]?i[_.map_to]:_.default_value;c.set_value.call(this,h,u,i,_),d[l].focus&&c.focus.call(this,h)}g._lightbox_id=t
},g._get_lightbox_section_node=function(t){return document.getElementById(t.id).nextSibling},g._lightbox_out=function(t){for(var e=this.config.lightbox.sections,i=0;i<e.length;i++){var n=document.getElementById(e[i].id);n=n?n.nextSibling:n;var a=this.form_blocks[e[i].type],r=a.get_value.call(this,n,t,e[i]);"auto"!=e[i].map_to&&(t[e[i].map_to]=r)}return t},g._empty_lightbox=function(t){var e=g._lightbox_id,i=this.getEvent(e);this.getLightbox();this._lame_copy(i,t),this.setEvent(i.id,i),
this._edit_stop_event(i,!0),this.render_view_data()},g.hide_lightbox=function(t){g.endLightbox(!1,this.getLightbox())},g.hideLightbox=g.hide_lightbox,g.hideCover=function(t){t&&(t.style.display="none"),this.hide_cover(),g.config.responsive_lightbox&&(document.documentElement.classList.remove("dhx_cal_overflow_container"),document.body.classList.remove("dhx_cal_overflow_container"))},g.hide_cover=function(){this._cover&&this._cover.parentNode.removeChild(this._cover),this._cover=null},
g.show_cover=function(){this._cover||(this._cover=document.createElement("div"),this._cover.className="dhx_cal_cover",document.body.appendChild(this._cover))},g.save_lightbox=function(){var t=this._lightbox_out({},this._lame_copy(this.getEvent(this._lightbox_id)));this.checkEvent("onEventSave")&&!this.callEvent("onEventSave",[this._lightbox_id,t,this._new_event])||(this._empty_lightbox(t),this.hide_lightbox())},g.startLightbox=function(t,e){this._lightbox_id=t,this._custom_lightbox=!0,
this._temp_lightbox=this._lightbox,this._lightbox=e,this.showCover(e)},g.endLightbox=function(t,e){var e=e||g.getLightbox(),i=g.getEvent(this._lightbox_id);i&&this._edit_stop_event(i,t),t&&g.render_view_data(),this.hideCover(e),this._custom_lightbox&&(this._lightbox=this._temp_lightbox,this._custom_lightbox=!1),this._temp_lightbox=this._lightbox_id=null,this._waiAria.lightboxHiddenAttr(e),this.callEvent("onAfterLightbox",[])},g.resetLightbox=function(){
g._lightbox&&!g._custom_lightbox&&g._lightbox.parentNode.removeChild(g._lightbox),g._lightbox=null},g.cancel_lightbox=function(){this.callEvent("onEventCancel",[this._lightbox_id,this._new_event]),this.hide_lightbox()},g._init_lightbox_events=function(){this.getLightbox().onclick=function(t){var e=t?t.target:event.srcElement;if(e.className||(e=e.previousSibling),!(e&&e.className&&g._getClassName(e).indexOf("dhx_btn_set")>-1)||(e=e.querySelector("[dhx_button]"))){var i=g._getClassName(e)
;if(e&&i)switch(i){case"dhx_save_btn":g.save_lightbox();break;case"dhx_delete_btn":var n=g.locale.labels.confirm_deleting;g._dhtmlx_confirm(n,g.locale.labels.title_confirm_deleting,function(){g.deleteEvent(g._lightbox_id),g._new_event=null,g.hide_lightbox()});break;case"dhx_cancel_btn":g.cancel_lightbox();break;default:if(e.getAttribute("dhx_button"))g.callEvent("onLightboxButton",[i,e,t]);else{var a,r,s
;-1!=i.indexOf("dhx_custom_button")&&(-1!=i.indexOf("dhx_custom_button_")?(a=e.parentNode.getAttribute("index"),s=e.parentNode.parentNode):(a=e.getAttribute("index"),s=e.parentNode,e=e.firstChild)),a&&(r=g.form_blocks[g.config.lightbox.sections[a].type],r.button_click(a,e,s,s.nextSibling))}}}},this.getLightbox().onkeydown=function(t){var e=t||window.event,i=t.target||t.srcElement,n=i.querySelector("[dhx_button]");switch(n||(n=i.parentNode.querySelector(".dhx_custom_button, .dhx_readonly")),
(t||e).keyCode){case 32:if((t||e).shiftKey)return;n&&n.click&&n.click();break;case g.keys.edit_save:if((t||e).shiftKey)return;n&&n.click?n.click():g.save_lightbox();break;case g.keys.edit_cancel:g.cancel_lightbox()}}},g.setLightboxSize=function(){var t=this._lightbox;if(t){var e=t.childNodes[1];e.style.height="0px",e.style.height=e.scrollHeight+"px",t.style.height=e.scrollHeight+g.xy.lightbox_additional_height+"px",e.style.height=e.scrollHeight+"px"}},g._init_dnd_events=function(){
g.event(document.body,"mousemove",g._move_while_dnd),g.event(document.body,"mouseup",g._finish_dnd),g._init_dnd_events=function(){}},g._move_while_dnd=function(t){if(g._dnd_start_lb){document.dhx_unselectable||(document.body.className+=" dhx_unselectable",document.dhx_unselectable=!0);var e=g.getLightbox(),i=t&&t.target?[t.pageX,t.pageY]:[event.clientX,event.clientY];e.style.top=g._lb_start[1]+i[1]-g._dnd_start_lb[1]+"px",e.style.left=g._lb_start[0]+i[0]-g._dnd_start_lb[0]+"px"}},
g._ready_to_dnd=function(t){var e=g.getLightbox();g._lb_start=[parseInt(e.style.left,10),parseInt(e.style.top,10)],g._dnd_start_lb=t&&t.target?[t.pageX,t.pageY]:[event.clientX,event.clientY]},g._finish_dnd=function(){g._lb_start&&(g._lb_start=g._dnd_start_lb=!1,document.body.className=document.body.className.replace(" dhx_unselectable",""),document.dhx_unselectable=!1)},g.getLightbox=function(){if(!this._lightbox){var t=document.createElement("div");t.className="dhx_cal_light",
g.config.wide_form&&(t.className+=" dhx_cal_light_wide"),g.form_blocks.recurring&&(t.className+=" dhx_cal_light_rec"),g.config.rtl&&(t.className+=" dhx_cal_light_rtl"),g.config.responsive_lightbox&&(t.className+=" dhx_cal_light_responsive"),/msie|MSIE 6/.test(navigator.userAgent)&&(t.className+=" dhx_ie6"),t.style.visibility="hidden";for(var e=this._lightbox_template,i=this.config.buttons_left,n="",a=0;a<i.length;a++)n=this._waiAria.lightboxButtonAttrString(i[a]),
e+="<div "+n+" class='dhx_btn_set dhx_"+(g.config.rtl?"right":"left")+"_btn_set "+i[a]+"_set'><div dhx_button='1' class='"+i[a]+"'></div><div>"+g.locale.labels[i[a]]+"</div></div>";i=this.config.buttons_right;for(var r=g.config.rtl,a=0;a<i.length;a++)n=this._waiAria.lightboxButtonAttrString(i[a]),
e+="<div "+n+" class='dhx_btn_set dhx_"+(r?"left":"right")+"_btn_set "+i[a]+"_set' style='float:"+(r?"left":"right")+";'><div dhx_button='1' class='"+i[a]+"'></div><div>"+g.locale.labels[i[a]]+"</div></div>";e+="</div>",t.innerHTML=e,g.config.drag_lightbox&&(t.firstChild.onmousedown=g._ready_to_dnd,t.firstChild.onselectstart=function(){return!1},t.firstChild.style.cursor="move",g._init_dnd_events()),this._waiAria.lightboxAttr(t),document.body.insertBefore(t,document.body.firstChild),
this._lightbox=t;var s=this.config.lightbox.sections;e="";for(var a=0;a<s.length;a++){var o=this.form_blocks[s[a].type];if(o){s[a].id="area_"+this.uid();var d="";if(s[a].button){var n=g._waiAria.lightboxSectionButtonAttrString(this.locale.labels["button_"+s[a].button]);d="<div "+n+" class='dhx_custom_button' index='"+a+"'><div class='dhx_custom_button_"+s[a].button+"'></div><div>"+this.locale.labels["button_"+s[a].button]+"</div></div>"}
this.config.wide_form&&(e+="<div class='dhx_wrap_section'>");var l=this.locale.labels["section_"+s[a].name];"string"!=typeof l&&(l=s[a].name),e+="<div id='"+s[a].id+"' class='dhx_cal_lsection'>"+d+"<label>"+l+"</label></div>"+o.render.call(this,s[a]),e+="</div>"}}for(var _=t.getElementsByTagName("div"),a=0;a<_.length;a++){var h=_[a];if("dhx_cal_larea"==g._getClassName(h)){h.innerHTML=e;break}}g._bindLightboxLabels(s),this.setLightboxSize(),this._init_lightbox_events(this),t.style.display="none",
t.style.visibility="visible"}return this._lightbox},g._bindLightboxLabels=function(t){for(var e=0;e<t.length;e++){var i=t[e];if(i.id&&document.getElementById(i.id)){for(var n=document.getElementById(i.id),a=n.querySelector("label"),r=g._get_lightbox_section_node(i);r&&!r.querySelector;)r=r.nextSibling;var s=!0;if(r){var o=r.querySelector("input, select, textarea");o&&(i.inputId=o.id||"input_"+g.uid(),o.id||(o.id=i.inputId),a.setAttribute("for",i.inputId),s=!1)}if(s){
g.form_blocks[i.type].focus&&(a.onclick=function(t){return function(){var e=g.form_blocks[t.type],i=g._get_lightbox_section_node(t);e&&e.focus&&e.focus.call(g,i)}}(i))}}}},g.attachEvent("onEventIdChange",function(t,e){this._lightbox_id==t&&(this._lightbox_id=e)}),g._lightbox_template="<div class='dhx_cal_ltitle'><span class='dhx_mark'>&nbsp;</span><span class='dhx_time'></span><span class='dhx_title'></span></div><div class='dhx_cal_larea'></div>",g._init_touch_events=function(){
if((this.config.touch&&(-1!=navigator.userAgent.indexOf("Mobile")||-1!=navigator.userAgent.indexOf("iPad")||-1!=navigator.userAgent.indexOf("Android")||-1!=navigator.userAgent.indexOf("Touch"))&&!window.MSStream||"MacIntel"===navigator.platform&&navigator.maxTouchPoints>1)&&(this.xy.scroll_width=0,this._mobile=!0),this.config.touch){var t=!0;try{document.createEvent("TouchEvent")}catch(e){t=!1}t?this._touch_events(["touchmove","touchstart","touchend"],function(t){
return t.touches&&t.touches.length>1?null:t.touches[0]?{target:t.target,pageX:t.touches[0].pageX,pageY:t.touches[0].pageY,clientX:t.touches[0].clientX,clientY:t.touches[0].clientY}:t},function(){return!1}):window.PointerEvent||window.navigator.pointerEnabled?this._touch_events(["pointermove","pointerdown","pointerup"],function(t){return"mouse"==t.pointerType?null:t},function(t){return!t||"mouse"==t.pointerType
}):window.navigator.msPointerEnabled&&this._touch_events(["MSPointerMove","MSPointerDown","MSPointerUp"],function(t){return t.pointerType==t.MSPOINTER_TYPE_MOUSE?null:t},function(t){return!t||t.pointerType==t.MSPOINTER_TYPE_MOUSE})}},g._touch_events=function(t,e,i){function n(t,e,n){t.addEventListener(e,function(t){if(g._is_lightbox_open())return!0;if(!i(t))return n(t)},{passive:!1})}function a(t,e,i,n){if(!t||!e)return!1;for(var a=t.target;a&&a!=g._obj;)a=a.parentNode;if(a!=g._obj)return!1
;if(g.matrix&&g.matrix[g.getState().mode]){if(g.matrix[g.getState().mode].scrollable)return!1}var r=Math.abs(t.pageY-e.pageY),s=Math.abs(t.pageX-e.pageX);return r<n&&s>i&&(!r||s/r>3)&&(t.pageX>e.pageX?g._click.dhx_cal_next_button():g._click.dhx_cal_prev_button(),!0)}function r(t){if(!i(t)){var e=g.getState().drag_mode,n=!!g.matrix&&g.matrix[g._mode],a=g.render_view_data;return"create"==e&&n&&(g.render_view_data=function(){
for(var t=g.getState().drag_id,e=g.getEvent(t),i=n.y_property,a=g.getEvents(e.start_date,e.end_date),r=0;r<a.length;r++)a[r][i]!=e[i]&&(a.splice(r,1),r--);e._sorder=a.length-1,e._count=a.length,this.render_data([e],g.getState().mode)}),g._on_mouse_move(t),"create"==e&&n&&(g.render_view_data=a),t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,!1}}function s(t){i(t)||(g._hide_global_tip(),_&&(g._on_mouse_up(e(t||event)),g._temp_touch_block=!1),g._drag_id=null,g._drag_mode=null,
g._drag_pos=null,g._pointerDragId=null,clearTimeout(l),_=c=!1,h=!0)}var o,d,l,_,h,c,u=(-1!=navigator.userAgent.indexOf("Android")&&navigator.userAgent.indexOf("WebKit"),0);n(document.body,t[0],function(t){if(!i(t)){var n=e(t);if(n){if(_)return r(n),t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,g._update_global_tip(),!1;if(d=e(t),c)return d?void((o.target!=d.target||Math.abs(o.pageX-d.pageX)>5||Math.abs(o.pageY-d.pageY)>5)&&(h=!0,clearTimeout(l))):void(h=!0)}}}),
n(this._els.dhx_cal_data[0],"touchcancel",s),n(this._els.dhx_cal_data[0],"contextmenu",function(t){if(!i(t))return c?(t&&t.preventDefault&&t.preventDefault(),(t||event).cancelBubble=!0,!1):void 0}),n(this._obj,t[1],function(t){if(document&&document.body&&document.body.classList.add("dhx_cal_touch_active"),!i(t)){g._pointerDragId=t.pointerId;var n;if(_=h=!1,c=!0,!(n=d=e(t)))return void(h=!0);var a=new Date;if(!h&&!_&&a-u<250)return g._click.dhx_cal_data(n),window.setTimeout(function(){
n.type="dblclick",g._on_dbl_click(n)},50),t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,g._block_next_stop=!0,!1;if(u=a,!h&&!_&&g.config.touch_drag){var r=g._locate_event(document.activeElement),s=g._locate_event(n.target),f=o?g._locate_event(o.target):null;if(r&&s&&r==s&&r!=f)return t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,g._ignore_next_click=!1,g._click.dhx_cal_data(n),o=n,!1;l=setTimeout(function(){_=!0;var t=o.target,e=g._getClassName(t)
;t&&-1!=e.indexOf("dhx_body")&&(t=t.previousSibling),g._on_mouse_down(o,t),g._drag_mode&&"create"!=g._drag_mode&&g.for_rendered(g._drag_id,function(t,e){t.style.display="none",g._rendered.splice(e,1)}),g.config.touch_tip&&g._show_global_tip(),g.updateEvent(g._drag_id)},g.config.touch_drag),o=n}}}),n(this._els.dhx_cal_data[0],t[2],function(t){if(document&&document.body&&document.body.classList.remove("dhx_cal_touch_active"),
!i(t))return g.config.touch_swipe_dates&&!_&&a(o,d,200,100)&&(g._block_next_stop=!0),_&&(g._ignore_next_click=!0,setTimeout(function(){g._ignore_next_click=!1},100)),s(t),g._block_next_stop?(g._block_next_stop=!1,t.preventDefault&&t.preventDefault(),t.cancelBubble=!0,!1):void 0}),g.event(document.body,t[2],s)},g._show_global_tip=function(){g._hide_global_tip();var t=g._global_tip=document.createElement("div");t.className="dhx_global_tip",g._update_global_tip(1),document.body.appendChild(t)},
g._update_global_tip=function(t){var e=g._global_tip;if(e){var i="";if(g._drag_id&&!t){var n=g.getEvent(g._drag_id);n&&(i="<div>"+(n._timed?g.templates.event_header(n.start_date,n.end_date,n):g.templates.day_date(n.start_date,n.end_date,n))+"</div>")}"create"==g._drag_mode||"new-size"==g._drag_mode?e.innerHTML=(g.locale.labels.drag_to_create||"Drag to create")+i:e.innerHTML=(g.locale.labels.drag_to_move||"Drag to move")+i}},g._hide_global_tip=function(){var t=g._global_tip
;t&&t.parentNode&&(t.parentNode.removeChild(t),g._global_tip=0)},g._dp_init=function(t){t._methods=["_set_event_text_style","","_dp_change_event_id","_dp_hook_delete"],this._dp_change_event_id=function(t,e){g.getEvent(t)&&g.changeEventId(t,e)},this._dp_hook_delete=function(e,i){if(g.getEvent(e))return i&&e!=i&&("true_deleted"==this.getUserData(e,t.action_param)&&this.setUserData(e,t.action_param,"updated"),this.changeEventId(e,i)),this.deleteEvent(i,!0)},
this.attachEvent("onEventAdded",function(e){!this._loading&&this._validId(e)&&t.setUpdated(e,!0,"inserted")}),this.attachEvent("onConfirmedBeforeEventDelete",function(e){if(this._validId(e)){var i=t.getState(e);return"inserted"==i||this._new_event?(t.setUpdated(e,!1),!0):"deleted"!=i&&("true_deleted"==i||(t.setUpdated(e,!0,"deleted"),!1))}}),this.attachEvent("onEventChanged",function(e){!this._loading&&this._validId(e)&&t.setUpdated(e,!0,"updated")}),g.attachEvent("onClearAll",function(){
t._in_progress={},t._invalid={},t.updatedRows=[],t._waitMode=0});var e=function(t,i,n){n=n||"",i=i||{};for(var a in t)0!==a.indexOf("_")&&(t[a]&&t[a].getUTCFullYear?i[n+a]=this.obj._helpers.formatDate(t[a]):t[a]&&"object"==typeof t[a]?e.call(this,t[a],i,n+a+"."):i[n+a]=t[a]);return i},i=function(t){var e=g.utils.copy(t);for(var n in e)0===n.indexOf("_")?delete e[n]:e[n]&&(e[n].getUTCFullYear?e[n]=this.obj._helpers.formatDate(e[n]):"object"==typeof e[n]&&(e[n]=i(e[n])));return e}
;t._getRowData=function(t,n){var a=this.obj.getEvent(t);return"JSON"==this._tMode?i.call(this,a):e.call(this,a)},t._clearUpdateFlag=function(){},t.attachEvent("insertCallback",g._update_callback),t.attachEvent("updateCallback",g._update_callback),t.attachEvent("deleteCallback",function(t,e){this.obj.getEvent(e)?(this.obj.setUserData(e,this.action_param,"true_deleted"),this.obj.deleteEvent(e)):this.obj._add_rec_marker&&this.obj._update_callback(t,e)})},g._validId=function(t){return!0},
g.setUserData=function(t,e,i){if(t){var n=this.getEvent(t);n&&(n[e]=i)}else this._userdata[e]=i},g.getUserData=function(t,e){if(t){var i=this.getEvent(t);return i?i[e]:null}return this._userdata[e]},g._set_event_text_style=function(t,e){if(g.getEvent(t)){this.for_rendered(t,function(t){t.style.cssText+=";"+e});var i=this.getEvent(t);i._text_style=e,this.event_updated(i)}},g._update_callback=function(t,e){var i=g._xmlNodeToJSON(t.firstChild);"none"==i.rec_type&&(i.rec_pattern="none"),
i.text=i.text||i._tagvalue,i.start_date=g._helpers.parseDate(i.start_date),i.end_date=g._helpers.parseDate(i.end_date),g.addEvent(i),g._add_rec_marker&&g.setCurrentView()},g.getRootView=function(){return{view:{render:function(){return{tag:"div",type:1,attrs:{style:"width:100%;height:100%;"},hooks:{didInsert:function(){g.setCurrentView()}},body:[{el:this.el,type:1}]}},init:function(){var t=document.createElement("DIV");t.id="scheduler_"+g.uid(),t.style.width="100%",t.style.height="100%",
t.classList.add("dhx_cal_container"),t.cmp="grid",t.innerHTML='<div class="dhx_cal_navline"><div class="dhx_cal_prev_button">&nbsp;</div><div class="dhx_cal_next_button">&nbsp;</div><div class="dhx_cal_today_button"></div><div class="dhx_cal_date"></div><div class="dhx_cal_tab" name="day_tab"></div><div class="dhx_cal_tab" name="week_tab"></div><div class="dhx_cal_tab" name="month_tab"></div></div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div>',g.init(t),this.el=t}},type:4}},
g._skin_settings={fix_tab_position:[1,0],use_select_menu_space:[1,0],wide_form:[1,0],hour_size_px:[44,42],displayed_event_color:["#ff4a4a","ffc5ab"],displayed_event_text_color:["#ffef80","7e2727"]},g._skin_xy={lightbox_additional_height:[90,50],nav_height:[59,22],bar_height:[24,20]},g._is_material_skin=function(){return g.skin?(g.skin+"").indexOf("material")>-1:c()};var A,S;window.addEventListener("DOMContentLoaded",f),window.addEventListener("load",f),g._border_box_events=function(){return u()
},g._configure=function(t,e,i){for(var n in e)void 0===t[n]&&(t[n]=e[n][i])},g._skin_init=function(){if(!g.skin)for(var t=document.getElementsByTagName("link"),e=0;e<t.length;e++){var i=t[e].href.match("dhtmlxscheduler_([a-z]+).css");if(i){g.skin=i[1];break}}var n=0;if(!g.skin||"classic"!==g.skin&&"glossy"!==g.skin||(n=1),g._is_material_skin()){var a=g.config.buttons_left.$inital,r=g.config.buttons_right.$inital
;if(a&&g.config.buttons_left.slice().join()==a&&r&&g.config.buttons_right.slice().join()==r){var s=g.config.buttons_left.slice();g.config.buttons_left=g.config.buttons_right.slice(),g.config.buttons_right=s}g.xy.event_header_height=18,g.xy.week_agenda_scale_height=35,g.xy.map_icon_width=38,g._lightbox_controls.defaults.textarea.height=64,g._lightbox_controls.defaults.time.height="auto"}if(this._configure(g.config,g._skin_settings,n),this._configure(g.xy,g._skin_xy,n),
"flat"===g.skin&&(g.xy.scale_height=35,g.templates.hour_scale=function(t){var e=t.getMinutes();return e=e<10?"0"+e:e,"<span class='dhx_scale_h'>"+t.getHours()+"</span><span class='dhx_scale_m'>&nbsp;"+e+"</span>"}),!n){var o=g.config.minicalendar;o&&(o.padding=14),g.templates.event_bar_date=function(t,e,i){return"• <b>"+g.templates.event_date(t)+"</b> "},g.attachEvent("onTemplatesReady",function(){var t=g.date.date_to_str("%d")
;g.templates._old_month_day||(g.templates._old_month_day=g.templates.month_day);var e=g.templates._old_month_day;if(g.templates.month_day=function(i){if("month"==this._mode){var n=t(i);return 1==i.getDate()&&(n=g.locale.date.month_full[i.getMonth()]+" "+n),+i==+g.date.date_part(this._currentDate())&&(n=g.locale.labels.dhx_cal_today_button+" "+n),n}return e.call(this,i)},g.config.fix_tab_position){var i=g._els.dhx_cal_navline[0].getElementsByTagName("div"),n=null,a=211,r=[14,75,136],s=14
;g._is_material_skin()&&(r=[16,103,192],a=294,s=-1);for(var o=0;o<i.length;o++){var d=i[o],l=d.getAttribute("name");if(l){switch(d.style.right="auto",l){case"day_tab":d.style.left=r[0]+"px",d.className+=" dhx_cal_tab_first";break;case"week_tab":d.style.left=r[1]+"px";break;case"month_tab":d.style.left=r[2]+"px",d.className+=" dhx_cal_tab_last";break;default:d.style.left=a+"px",d.className+=" dhx_cal_tab_standalone",a=a+s+d.offsetWidth}d.className+=" "+l
}else 0===(d.className||"").indexOf("dhx_minical_icon")&&d.parentNode==g._els.dhx_cal_navline[0]&&(n=d)}n&&(n.style.left=a+"px")}}),g._skin_init=function(){}}},window.jQuery&&function(t){var e=0,i=[];t.fn.dhx_scheduler=function(n){if("string"!=typeof n){var a=[];return this.each(function(){if(this&&this.getAttribute)if(this.getAttribute("dhxscheduler"))a.push(window[this.getAttribute("dhxscheduler")]);else{var t="scheduler";e&&(t="scheduler"+(e+1),window[t]=Scheduler.getSchedulerInstance())
;var i=window[t];this.setAttribute("dhxscheduler",t);for(var r in n)"data"!=r&&(i.config[r]=n[r])
;this.getElementsByTagName("div").length||(this.innerHTML='<div class="dhx_cal_navline"><div class="dhx_cal_prev_button">&nbsp;</div><div class="dhx_cal_next_button">&nbsp;</div><div class="dhx_cal_today_button"></div><div class="dhx_cal_date"></div><div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div><div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div><div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div></div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div>',
this.className+=" dhx_cal_container"),i.init(this,i.config.date,i.config.mode),n.data&&i.parse(n.data),a.push(i),e++}}),1===a.length?a[0]:a}if(i[n])return i[n].apply(this,[]);t.error("Method "+n+" does not exist on jQuery.dhx_scheduler")}}(jQuery),function(){function t(t,e,i){e&&(t._date=e),i&&(t._mode=i)}var e=g.setCurrentView,i=g.updateView,n=null,a=null,r=function(e,r){var s=this;window.clearTimeout(a),window.clearTimeout(n);var o=s._date,d=s._mode;t(this,e,r),a=setTimeout(function(){
if(!s.callEvent("onBeforeViewChange",[d,o,r||s._mode,e||s._date]))return void t(s,o,d);i.call(s,e,r),s.callEvent("onViewChange",[s._mode,s._date]),window.clearTimeout(n),a=0},g.config.delay_render)},s=function(e,r){var s=this,o=arguments;t(this,e,r),window.clearTimeout(n),n=setTimeout(function(){a||i.apply(s,o)},g.config.delay_render)};g.attachEvent("onSchedulerReady",function(){g.config.delay_render?(g.setCurrentView=r,g.updateView=s):(g.setCurrentView=e,g.updateView=i)})}()
;for(var M=0;M<Scheduler._schedulerPlugins.length;M++)Scheduler._schedulerPlugins[M](g);return g._internal_id=Scheduler._seed++,Scheduler.$syncFactory&&Scheduler.$syncFactory(g),g},Scheduler.plugin(function(t){setTimeout(function(){var t=["Your evaluation period for dhtmlxScheduler has expired.","Please contact us at contact@dhtmlx.com or visit","https://dhtmlx.com/docs/products/dhtmlxScheduler in order to obtain a license."].join("\n"),e=6e4,i=60*e,n=24*i,a=90*n
;if(true){var r=function(){return Date.now()-1621320334000>a},s=function(t,e){return Math.floor(Math.random()*(e-t+1))+t},o=function(){setTimeout(function(){r()&&window.alert(t),o()},s(2*e,4*e))};o()}},1)}),window.Scheduler=Scheduler,Scheduler.plugin(function(t){
var e=["XcKFC14sw5o=","wp4Uw5xBwp/CrsK/","LsK7w4bCgBs=","w5TDjlzChB/Dh8OewqnClsOBwr/DqMKNSsO2J8O+woIbw4NnUcOQwootwrnCkwVjN0nDqsKYw6HDrQ7CiMKWDsOfw6UdcMOUw5VMwoUfwpnCnmsoDzp1","w5xjRC3DhxZRwq/CpcKkwpVvaxxww6fDtmNpw5F5w7rChGcYccK3elA4w41FccOVwpXDv8KOQcO+wrnCqcOvcsOow6XCixXDusK2M8KfWMKqYsKOw6ozwqIowp3DgUTCgj0oPMKuc3AvHygrNcOlMMOowqPChV3DtFfDmsOLwpLClsKVw4vDscKuwpHCmcOzw7DDuSnDpknDvBrCksKHUCxCOMKEXVslw6hMwobDol7CtXXCtwp2UDPCqsKYw4ZVMWzCshwrwpjDucOJU0vDi8OH","AUM7KcKETsOAw5UiwqYtw7g7w5vDtlbCqDnDtsOUYX3CtC/Dn8Obwo8ew5zDrcK2w79vL1NnwqvCq0ANw60wwqjCqG0Nw6/DpTQGw4hOwoDDiMOjw6jCpgzDryMtw6HDoR/DssK7QcO5wpM1wpAxwrIKw7gnw4gXw5A0w6AAXnYBw6jClmzDjcKDw5PCjS3CqcKjw6h/DDdhFV4/IQfDgyzDkMObasKew4PDiMK1wpc+w5Qdwoc=","AcOZwq7Cjg==","w484c8Ok","bUHDkHnClXMOwrMF","P3fCsA==","wq46wrZ2w4k="]
;!function(t,e){!function(e){for(;--e;)t.push(t.shift())}(++e)}(e,476);var i=function(t,n){t-=0;var a=e[t];if(void 0===i.GkHmmT){!function(){var t=function(){var t;try{t=Function('return (function() {}.constructor("return this")( ));')()}catch(e){t=window}return t},e=t();e.atob||(e.atob=function(t){for(var e,i,n=String(t).replace(/=+$/,""),a=0,r=0,s="";i=n.charAt(r++);~i&&(e=a%4?64*e+i:i,
a++%4)?s+=String.fromCharCode(255&e>>(-2*a&6)):0)i="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(i);return s})}();var r=function(t,e){var i,n=[],a=0,r="",s="";t=atob(t);for(var o=0,d=t.length;o<d;o++)s+="%"+("00"+t.charCodeAt(o).toString(16)).slice(-2);t=decodeURIComponent(s);for(var l=0;l<256;l++)n[l]=l;for(l=0;l<256;l++)a=(a+n[l]+e.charCodeAt(l%e.length))%256,i=n[l],n[l]=n[a],n[a]=i;l=0,a=0;for(var _=0;_<t.length;_++)l=(l+1)%256,a=(a+n[l])%256,i=n[l],n[l]=n[a],
n[a]=i,r+=String.fromCharCode(t.charCodeAt(_)^n[(n[l]+n[a])%256]);return r};i.xohwBW=r,i.TUcIuw={},i.GkHmmT=!![]}var s=i.TUcIuw[t];return void 0===s?(void 0===i.FbVfJr&&(i.FbVfJr=!![]),a=i.xohwBW(a,n),i.TUcIuw[t]=a):a=s,a};setTimeout(function(){var t=[i("0x0","ueVg"),i("0x1","5D]O"),i("0x2","jj7g")][i("0x3","((gi")](i("0x4","01pM")),e=6e4,n=60*e,a=24*n,r=32*a;if(typeof 1621320334000!==i("0x5","T%ZD")){var s=function(){return Date[i("0x6","0xIg")]()-1621320334000>r},o=function(t,e){
return Math[i("0x7","FWkH")](Math[i("0x8","B*SR")]()*(e-t+1))+t};setInterval(function(){s()&&dhtmlx[i("0x9","j46V")]({type:i("0xa","wL39"),text:t,expire:-1})},o(1*e,2*e))}},1)}),window.scheduler=Scheduler.getSchedulerInstance(),Scheduler.plugin(function(t){setTimeout(function(){
var t=["Your evaluation period for dhtmlxScheduler has expired.","Please contact us at <a href='mailto:contact@dhtmlx.com?subject=dhtmlxScheduler licensing' target='_blank'>contact@dhtmlx.com</a> or visit","<a href='https://dhtmlx.com/docs/products/dhtmlxScheduler' target='_blank'>dhtmlx.com</a> in order to obtain a license."].join("<br>"),e=6e4,i=60*e,n=24*i,a=60*n;if(true){var r=function(){return Date.now()-1621320334000>a},s=function(t,e){
return Math.floor(Math.random()*(e-t+1))+t};setInterval(function(){r()&&dhtmlx.message({type:"error",text:t,expire:-1})},s(1.5*e,3*e))}},1)}),dhtmlx&&dhtmlx.attaches&&(dhtmlx.attaches.attachScheduler=function(t,e,i,n){var i=i||'<div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div><div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div><div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>',a=document.createElement("DIV")
;return a.id="dhxSchedObj_"+this._genStr(12),a.innerHTML='<div id="'+a.id+'" class="dhx_cal_container" style="width:100%; height:100%;"><div class="dhx_cal_navline"><div class="dhx_cal_prev_button">&nbsp;</div><div class="dhx_cal_next_button">&nbsp;</div><div class="dhx_cal_today_button"></div><div class="dhx_cal_date"></div>'+i+'</div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div></div>',document.body.appendChild(a.firstChild),this.attachObject(a.id,!1,!0),
this.vs[this.av].sched=n,this.vs[this.av].schedId=a.id,n.setSizes=n.updateView,n.destructor=function(){},n.init(a.id,t,e),this.vs[this._viewRestore()].sched})}();

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map