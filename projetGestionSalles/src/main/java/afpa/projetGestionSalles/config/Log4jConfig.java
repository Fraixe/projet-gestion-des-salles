package afpa.projetGestionSalles.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classe de configuration du Logger Log4j
 * @author ClémentF
 *
 */
@Configuration
public class Log4jConfig {
	
	/**
	 * Création d'un Bean Spring qui retourne un Logger 
	 * qui peut être utiliser dans les autres classes de 
	 * l'application
	 * @return un Logger
	 */
	@Bean
	public Logger getLogger() {
		return LogManager.getLogger();
	}
}
