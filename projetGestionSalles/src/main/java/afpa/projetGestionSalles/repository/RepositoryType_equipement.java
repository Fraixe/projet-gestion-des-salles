package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.Type_equipement;


/**
 * Interface RepositoryType_equipement qui hérite de l'interface CrudRepository
 * qui permet d'utiliser toutes les méthodes du CRUD
 * Cette interface est injecté dans le service 
 * @author jordy
 *
 */
public interface RepositoryType_equipement extends CrudRepository<Type_equipement, Integer> {

	public Type_equipement findByNomtypeequipement(String nomtypeequipement);
}
