package afpa.projetGestionSalles.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import afpa.projetGestionSalles.beans.Salle;


/**
 * Interface RepositorySalle qui hérite de l'interface CrudRepository
 * qui permet d'utiliser toutes les méthodes du CRUD
 * Cette interface est injecté dans le service 
 * @author jordy
 *
 */
@Repository
public interface RepositorySalle extends CrudRepository<Salle, Integer>{

	public Salle findByNomsalle(String nomsalle);
	
	public List<Salle> findAllByOrderByCapacitesalleAsc();
	
	public List<Salle> findAllByOrderByCapacitesalleDesc();
	
	
}
