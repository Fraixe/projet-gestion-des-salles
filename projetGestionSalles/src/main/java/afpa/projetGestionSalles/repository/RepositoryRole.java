package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import afpa.projetGestionSalles.beans.Role;

/**
 * Repository du POJO Role qui hérite de CrudRepository qui nous
 * permet de faire le crud
 * @author ClémentF
 *
 */
@Repository
public interface RepositoryRole extends CrudRepository<Role, Integer> {

}
