/**
 * 
 */
package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.Periode;

/**
 * @author 31010-83-02
 *
 */
public interface RepositoryPeriode extends CrudRepository <Periode, Integer>{

}
