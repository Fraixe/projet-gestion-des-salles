/**
 * 
 */
package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.Etat_User;

/**
 * @author 31010-83-02
 *
 */
public interface RepositoryEtat_User extends CrudRepository <Etat_User, Integer> {

	
	}