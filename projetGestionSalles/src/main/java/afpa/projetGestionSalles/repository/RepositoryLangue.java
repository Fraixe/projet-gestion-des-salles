/**
 * 
 */
package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.Langue;

/**
 * @author 31010-83-02
 *
 */
public interface RepositoryLangue extends CrudRepository <Langue, Integer>{

}
