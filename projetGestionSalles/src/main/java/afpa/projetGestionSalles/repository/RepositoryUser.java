/**
 * 
 */
package afpa.projetGestionSalles.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.User;

/**
 * @author 31010-83-02
 *
 */
public interface RepositoryUser extends CrudRepository <User, Integer>{
	public Optional<User> findByNomuserAndPassword (String nom_user, String password);
}
