package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import afpa.projetGestionSalles.beans.TypeSalle;

/**
 * Repository du POJO TypeSalle qui hérite de CrudRepository qui nous
 * permet de faire le crud
 * @author ClémentF
 *
 */
@Repository
public interface RepositoryTypeSalle extends CrudRepository<TypeSalle, Integer> {

}
