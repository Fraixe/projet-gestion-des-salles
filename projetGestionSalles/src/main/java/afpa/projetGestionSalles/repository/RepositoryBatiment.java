/**
 * 
 */
package afpa.projetGestionSalles.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import afpa.projetGestionSalles.beans.Batiment;

/**
 * Interface permettant d'utiliser les méthodes crud sur la table t_batiment
 * @author Lucas
 *
 */

@Repository
public interface RepositoryBatiment extends CrudRepository <Batiment, Integer>{

	/**
	 * méthode permettant de chercher un batiment dans la BDD via son nom
	 * @param nomBatiment nom du batiment a chercher dans la BDD 
	 * @return le batiment trouvé via la requête
	 */
	public List<Batiment> findByNomBatiment(String nomBatiment);
}
