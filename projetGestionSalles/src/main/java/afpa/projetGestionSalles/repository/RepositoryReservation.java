package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import afpa.projetGestionSalles.beans.Reservation;

/**
 * Repository du POJO Reservation qui hérite de CrudRepository qui nous
 * permet de faire le crud
 * @author ClémentF
 *
 */
@Repository
public interface RepositoryReservation extends CrudRepository<Reservation, Integer> {

}
