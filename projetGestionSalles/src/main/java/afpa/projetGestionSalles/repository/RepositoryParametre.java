/**
 * 
 */
package afpa.projetGestionSalles.repository;

import org.springframework.data.repository.CrudRepository;

import afpa.projetGestionSalles.beans.Parametre;

/**
 * @author 31010-83-02
 *
 */
public interface RepositoryParametre  extends CrudRepository <Parametre, Integer>{

}
