package afpa.projetGestionSalles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetGestionSallesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetGestionSallesApplication.class, args);
	}

}
