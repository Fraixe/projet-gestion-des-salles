package afpa.projetGestionSalles.service;


import java.util.List;
import java.util.Optional;

import afpa.projetGestionSalles.beans.Salle;


/**
 * Interface IServiceSalle qui est implémenté par ServiceSalleImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author jordy
 *
 */
public interface IServiceSalle {
	
	public Iterable<Salle> getAllSalles();
	
	public Optional<Salle> getSalleById(Integer id);
	
	public Salle addOrMergeSalle(Salle salle);

	public void deleteSalle(Salle salle);
	
	public void deleteSalleById(Integer id);
	
	public Salle findByNomsalle(String nom_salle);
	
	public List<Salle> findAllByOrderByCapacitesalleAsc();
	
	public List<Salle> findAllByOrderByCapacitesalleDesc();

}
