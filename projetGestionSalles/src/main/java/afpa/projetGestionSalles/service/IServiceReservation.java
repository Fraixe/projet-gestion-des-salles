package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Reservation;

/**
 * Interface IServiceTypeSalle qui est implémenter par ServiceTypeSalleImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author ClémentF
 *
 */
public interface IServiceReservation {
	
	Iterable<Reservation> getAllReservation();
	
	Optional<Reservation> getReservationById(Integer id);

	Reservation addReservation(Reservation reservation);

	Reservation updateReservation(Reservation reservation);

	void deleteReservationById(Integer id);

}
