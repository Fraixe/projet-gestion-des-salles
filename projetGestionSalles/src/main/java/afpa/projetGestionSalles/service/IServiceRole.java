package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Role;

/**
 * Interface IServiceRole qui est implémenter par ServiceRoleImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author ClémentF
 *
 */
public interface IServiceRole {

	Iterable<Role> getAllRole();

	Optional<Role> getRoleById(Integer id);

	Role addRole(Role role);

	Role updateRole(Role role);

	void deleteRoleById(Integer id);
}
