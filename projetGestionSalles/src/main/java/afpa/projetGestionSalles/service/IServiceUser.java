package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.User;

/**
 * @author Pierre
 *
 */
public interface IServiceUser {
	public User add (User u);
	public Optional<User> findById(Integer i);
	public User update(User u);
	public Iterable<User> findAll();
	public void delete(User u);
	public Optional<User> findByNomuserAndPassword (String nom_user, String password);
}