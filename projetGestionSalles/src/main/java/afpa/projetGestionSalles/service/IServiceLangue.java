/**
 * 
 */
package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Langue;

/**
 * @author Pierre
 *
 */

public interface IServiceLangue  {
	public Langue add (Langue l);
	public Optional<Langue> findById(Integer i);
	public Langue update(Langue l);
	public Iterable<Langue> findAll();
	public void delete(Langue l);
}
