package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.TypeSalle;

/**
 * Interface IServiceTypeSalle qui est implémenter par ServiceTypeSalleImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author ClémentF
 *
 */
public interface IServiceTypeSalle {

	Iterable<TypeSalle> getAllTypeSalle();
	
	Optional<TypeSalle> getTypeSalleById(Integer id);
	
	TypeSalle addTypeSalle(TypeSalle typeSalle);
	
	TypeSalle updateTypeSalle(TypeSalle typeSalle);
	
	void deleteTypeSalleById(Integer id);
}
