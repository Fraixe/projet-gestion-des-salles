package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Type_equipement;


/**
 * Interface IserviceType_equipement qui est implémenter par ServiceType_equipementImpl
 * Permet de découpler les classes et donc de réduire des dépendances
 * On y retrouve les méthodes du CRUD
 * @author jordy
 *
 */
public interface IServiceType_equipement {

	public Iterable<Type_equipement> getAllType_equipement();
	
	public Optional<Type_equipement> getEquipementById(Integer id);
	
	public Type_equipement addOrMergeType_equipement(Type_equipement type_equipement);
	
	public void deleteType_equipement(Type_equipement type_equipement);
	
	public Type_equipement findByNomtypeequipement(String nomtypeequipement);
	
}
