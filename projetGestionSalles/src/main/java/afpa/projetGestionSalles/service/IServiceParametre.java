/**
 * 
 */
package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Parametre;

/**
 * @author Pierre
 *
 */

public interface IServiceParametre {
	public Parametre add (Parametre p);
	public Optional<Parametre> findById(Integer i);
	public Parametre update(Parametre p);
	public Iterable<Parametre> findAll();
	public void delete(Parametre p);
}
