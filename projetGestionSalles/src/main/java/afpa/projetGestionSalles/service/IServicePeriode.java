package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Periode;

/**
 * @author Pierre
 *
 */

public interface IServicePeriode {
	public Periode add (Periode p);
	public Optional<Periode> findById(Integer i);
	public Periode update(Periode p);
	public Iterable<Periode> findAll();
	public void delete(Periode p);
}