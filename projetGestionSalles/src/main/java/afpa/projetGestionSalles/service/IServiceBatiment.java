/**
 * 
 */
package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Batiment;

/**
 * Interface pour les méthodes utilisées par le service batimentService
 * @author Lucas
 *
 */

public interface IServiceBatiment  {
	
	public Batiment add (Batiment b);
	
	public Optional<Batiment> findById(Integer i);
	
	public Batiment update(Batiment b);
	
	public Iterable<Batiment> findAll();
	
	public void delete(Batiment b);
	
	public void deleteById(Integer id);
	
	public Batiment findByNomBatiment(String nomBatiment);
}
