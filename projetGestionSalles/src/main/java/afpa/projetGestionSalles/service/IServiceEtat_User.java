/**
 * 
 */
package afpa.projetGestionSalles.service;

import java.util.Optional;

import afpa.projetGestionSalles.beans.Etat_User;

/**
 * @author Pierre
 *
 */

public interface IServiceEtat_User  {
	public Etat_User add (Etat_User e);
	public Optional<Etat_User> findById(Integer i);
	public Etat_User update(Etat_User e);
	public Iterable<Etat_User> findAll();
	public void delete(Etat_User e);
	
}