/**
 * 
 */
package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Langue;
import afpa.projetGestionSalles.repository.RepositoryLangue;
import afpa.projetGestionSalles.service.IServiceLangue;

/**
 * Classe service des Langue qui implémente l'interface IServiceLangue.
 * Injection du Repository de Langue nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Pierre
 */
@Service
public class ServiceLangueImpl implements IServiceLangue {

	
	@Autowired
	RepositoryLangue daoLangue ;
	
	@Autowired
	Logger log;
	
	/**
	 * Service de création d'une langue
	 * @param Objet langue a créer
	 * @return Objet langue tel que persisté
	 */
	@Override
	public Langue add(Langue l) {
		log.info("ServiceLangueImpl.add appelé");
		log.info("Avec pour parametre");
		log.info(l);
		log.info("valeur retournee :");
		Langue maLangue=daoLangue.save(l);
		log.info(maLangue);
		return maLangue;
		
	}

	/**
	 * Recherche d'une langue par son ID
	 * @param id de la langue a rechercher
	 * @return Optionnel : la langue recherchée
	 */
	@Override
	public Optional<Langue> findById(Integer i) {
		log.info("ServiceLangueImpl.findById appelé");
		log.info("Avec pour parametre");
		log.info(i);
		log.info("valeur retournee :");
		Langue maLangue=daoLangue.findById(i).get();
		log.info(maLangue);
		return Optional.ofNullable(maLangue);
	}

	/**
	 * Methode de mise a jour d'une langue
	 * @param la langue à mettre à jour
	 * @return la langue mise a jour telle que persistée
	 */
	@Override
	public Langue update(Langue l) {
		log.info("ServiceLangueImpl.update appelé");
		log.info("Avec pour parametre");
		log.info(l);
		log.info("valeur retournee :");
		Langue maLangue=daoLangue.save(l);
		log.info(maLangue);
		return maLangue;
	}

	/**
	 * Liste Iterable de l'ensemble des langues présentes
	 * @return Liste Iterable des langues
	 */
	@Override
	public Iterable<Langue> findAll() {
		log.info("ServiceLangueImpl.findAll appelé");
		log.info("valeurs retournées");
		daoLangue.findAll().forEach(Langue -> log.info(Langue));
		return daoLangue.findAll();
	}

	/**
	 * suppression d'une langue de la BDD
	 * @param l'objet Langue a supprimer
	 */
	@Override
	public void delete(Langue l) {
		log.info("ServiceLangueImpl.delete appelé");
		log.info("Avec pour parametre");
		log.info(l);
		daoLangue.delete(l);
		
	}

}
