/**
 * 
 */
package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Parametre;
import afpa.projetGestionSalles.repository.RepositoryParametre;
import afpa.projetGestionSalles.service.IServiceParametre;

/**
 * Classe service des Parametre qui implémente l'interface IServiceParametre.
 * Injection du Repository de Parametre nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Pierre
 */
@Service
public class ServiceParametreImpl implements IServiceParametre {
	
	@Autowired
	RepositoryParametre daoParametre ;

	/**
	 * Service de création d'un parametre
	 * @param Objet parametre a créer
	 * @return Objet parametre tel que persisté
	 */
	@Override
	public Parametre add(Parametre p) {
		return daoParametre.save(p);
	}

	/**
	 * Recherche d'un parametre par son ID
	 * @param id du parametre a rechercher
	 * @return Optionnel : le parametre recherché
	 */
	@Override
	public Optional<Parametre> findById(Integer i) {
		return daoParametre.findById(i);
	}

	/**
	 * Methode de mise a jour d'un parametre
	 * @param le parametre à mattre à jour
	 * @return le parametre mis a jour telle que persisté
	 */
	@Override
	public Parametre update(Parametre p) {
		return daoParametre.save(p);
	}

	/**
	 * Liste Iterable de l'ensemble des parametres présents
	 * @return Liste Iterable des Parametres
	 */
	@Override
	public Iterable<Parametre> findAll() {
		return daoParametre.findAll();
	}

	/**
	 * suppression d'un Parametre de la BDD
	 * @param l'objet Parametre a supprimer
	 */
	@Override
	public void delete(Parametre p) {
		daoParametre.delete(p);
		
	}
	
	

}
