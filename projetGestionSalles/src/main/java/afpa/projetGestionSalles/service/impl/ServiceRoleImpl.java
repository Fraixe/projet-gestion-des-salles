package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Role;
import afpa.projetGestionSalles.repository.RepositoryRole;
import afpa.projetGestionSalles.service.IServiceRole;
/**
 * Classe service des rôles qui implémente l'interface IServiceRole.
 * Injection du Repository de Role qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author ClémentF
 *
 */
@Service
public class ServiceRoleImpl implements IServiceRole {
	
	/*Injection du logger*/
	@Autowired
	Logger logger;
	
	/*Injection du Repository Role*/
	@Autowired
	RepositoryRole repositoryRole;

	/**
	 * Récupération de la liste des rôles
	 * @return un Iterable qui renvoi la liste des rôles
	 */
	@Override
	public Iterable<Role> getAllRole() {
		logger.info("dans ServiceRoleImpl.getAllRole");

		repositoryRole.findAll().forEach(role -> logger.info(role.getNom_role()));;
		return repositoryRole.findAll();
	}

	/**
	 * Récupère un Rôle via son id
	 * @param id du role à récupèrer
	 * @return un Role 
	 */
	@Override
	public Optional<Role> getRoleById(Integer id) {
		logger.info("dans ServiceRoleImpl.getRoleById");

		logger.info(repositoryRole.findById(id));
		return repositoryRole.findById(id);
	}

	/**
	 * Ajout d'un rôle dans la bdd
	 * @param role à ajouter
	 * @return le role qui a été ajouté
	 */
	@Override
	public Role addRole(Role role) {
		logger.info("dans ServiceRoleImpl.addRole");

		repositoryRole.save(role);
		return role;
	}

	/**
	 * update d'un role déjà existant
	 * @param role à modifier
	 * @return Role mis à jour
	 */
	@Override
	public Role updateRole(Role role) {
		logger.info("dans ServiceRoleImpl.updateRole");
		
		return repositoryRole.save(role);	
	}

	/**
	 * Suppression d'un role via son id
	 * @param id du role à supprimer
	 */
	@Override
	public void deleteRoleById(Integer id) {
		logger.info("dans ServiceRoleImpl.deleteRoleById");
		
		repositoryRole.deleteById(id);
	}
}
