package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Periode;
import afpa.projetGestionSalles.repository.RepositoryPeriode;
import afpa.projetGestionSalles.service.IServicePeriode;

/**
 * Classe service des Periodes qui implémente l'interface IServicePeriode.
 * Injection du Repository de Periode nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Pierre
 */
@Service
public class ServicePeriodeImpl implements IServicePeriode{

	@Autowired
	RepositoryPeriode daoPeriode ;

	@Override
	public Periode add(Periode p) {
		return daoPeriode.save(p);	
	}

	@Override
	public Optional<Periode> findById(Integer i) {
		return daoPeriode.findById(i);
	}

	@Override
	public Periode update(Periode p) {
		return daoPeriode.save(p);
	}

	@Override
	public Iterable<Periode> findAll() {
		return daoPeriode.findAll();
	}

	@Override
	public void delete(Periode p) {
		daoPeriode.delete(p);

	}



}
