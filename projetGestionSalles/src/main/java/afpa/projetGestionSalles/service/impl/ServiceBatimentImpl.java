package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Batiment;
import afpa.projetGestionSalles.repository.RepositoryBatiment;
import afpa.projetGestionSalles.service.IServiceBatiment;

/**
 * Classe service des Batiments qui implémente l'interface IServiceBatiment.
 * Injection du Repository de Batiment nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Lucas
 */


@Service
public class ServiceBatimentImpl implements IServiceBatiment {


	@Autowired
	RepositoryBatiment daoBatiment;

	@Autowired
	Logger logger;


	/**
	 * Ajout d'un Bâtiment.
	 * @param le batiment a persister
	 * @return le batiment persisté
	 */
	@Override
	public Batiment add(Batiment b) {
		logger.info("dans ServiceBatimentImpl.add");
		return daoBatiment.save(b);
	}

	/**
	 * Recherche d'un Bâtiment a partir de son ID
	 * @param l'identifiant du batiment a rechercher
	 * @return un Optional contenant l'objet Batiment
	 */
	@Override
	public Optional<Batiment> findById(Integer i) {
		logger.info("dans ServiceBatimentImpl.findById");
		return daoBatiment.findById(i);
	}

	/**
	 *Mise a jour d'un Batiment déjà existant
	 *@param b batiment à mettre a jour
	 */
	@Override
	public Batiment update(Batiment b) {
		logger.info("dans ServiceBatimentImpl.update");
		return daoBatiment.save(b);
	}


	/**
	 * Retourne la liste de tout les bâtiments
	 */
	@Override
	public Iterable<Batiment> findAll() {
		logger.info("dans ServiceBatimentImpl.findAll");
		daoBatiment.findAll().forEach(batiment -> logger.info(batiment.getNomBatiment()));
		return daoBatiment.findAll();
	}

	/**
	 * méthode pour spupprimer un batiment dans la BDD
	 * @param b le batiment a supprimer
	 */
	@Override
	public void delete(Batiment b) {
		logger.info("dans ServiceBatimentImpl.delete");
		
		daoBatiment.delete(b);
	}


	/**
	 * {@link RepositoryBatiment.findByNom_batiment}
	 */
	
	@Override
	public Batiment findByNomBatiment(String nomBatiment) {

		logger.info("dans ServiceBatimentImpl.findByNom_batiment");
		return (Batiment) daoBatiment.findByNomBatiment(nomBatiment);
	}

	/**
	 * Méthode pour supprimer un batiment via son id
	 * @param id l'id du batiment a supprimer
	 */
	@Override
	public void deleteById(Integer id) {
		logger.info("dans ServiceBatimentImpl.deleteById");
		Batiment batimentToDelete =  daoBatiment.findById(id).get();
		daoBatiment.delete(batimentToDelete);
	}
}