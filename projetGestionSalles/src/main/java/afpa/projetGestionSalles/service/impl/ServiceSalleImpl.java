package afpa.projetGestionSalles.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Salle;
import afpa.projetGestionSalles.repository.RepositorySalle;
import afpa.projetGestionSalles.service.IServiceSalle;


/**
 * Classe service des salles qui implémente l'interface IServiceSalle.
 * Injection du Repository de Salle qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author jordy
 *
 */
@Transactional
@Service
public class ServiceSalleImpl implements IServiceSalle {

	@Autowired
	RepositorySalle reposalle;
	
	@Autowired
	Logger logger;
	
	
	/**
	 * Retourne la liste des salles qui correspond à la table
	 * t_salle
	 * @return un Iterable de Salles
	 */
	@Override
	public Iterable<Salle> getAllSalles() {
		logger.info("Voici la liste de toutes les salles");
		return reposalle.findAll();
	}

	/**
	 * Retourne la salle qui correspond à l'id passé en paramètre
	 * 
	 * @return un Optional de salle, permet d'encapsuler un object null
	 * Evite l'exception nullpointerexception
	 */
	@Override
	public Optional<Salle> getSalleById(Integer id) {
		logger.info("La salle séléctionnée est ");
		return reposalle.findById(id);
	}

	/**
	 * Ajoute ou met à jour une nouvelle salle dans la bdd
	 * 
	 * @return une salle
	 */
	@Override
	public Salle addOrMergeSalle(Salle salle) {
		logger.info("Nouvelle Salle ajoutée");
		return reposalle.save(salle);
	}

	/**
	 * Supprime une salle de la bdd via son id
	 */
	@Override
	public void deleteSalle(Salle salle) {
		logger.info("Salle supprimée");
		reposalle.delete(salle);
	}

	@Override
	public Salle findByNomsalle(String nom_salle) {
		logger.info("Recherche par nom de salle");
		return reposalle.findByNomsalle(nom_salle);
	}

	@Override
	public List<Salle> findAllByOrderByCapacitesalleAsc() {
		logger.info("recherche des salles par ordre ascendant des capacités");
		return reposalle.findAllByOrderByCapacitesalleAsc();
	}

	@Override
	public List<Salle> findAllByOrderByCapacitesalleDesc() {
		logger.info("recherche des salles par ordre descendant des capacités");
		return reposalle.findAllByOrderByCapacitesalleDesc();
	}

	@Override
	public void deleteSalleById(Integer id) {
		logger.info("Salle supprimée par nom");
		reposalle.deleteById(id);
	}

}
