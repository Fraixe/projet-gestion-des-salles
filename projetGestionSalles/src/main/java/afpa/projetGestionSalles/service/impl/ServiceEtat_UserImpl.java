package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Etat_User;
import afpa.projetGestionSalles.repository.RepositoryEtat_User;
import afpa.projetGestionSalles.service.IServiceEtat_User;

/**
 * Classe service des Etat_User qui implémente l'interface IServiceEtat_User.
 * Injection du Repository de Etat_User nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Pierre
 */
@Service
public class ServiceEtat_UserImpl implements IServiceEtat_User{
	
	@Autowired
	RepositoryEtat_User daoEtat_User ;
	
	@Autowired
	Logger log;

	/**
	 * Sauvegarde d'un nouvel Etat_User
	 * @param l'Etat_User a creer
	 * @return l'état user créé
	 */
	@Override
	public Etat_User add(Etat_User e) {
		log.info("ServiceEtat_UserImpl.add appelé");
		log.info("Avec pour parametre");
		log.info(e);
		log.info("valeur retournee :");
		Etat_User monEtat_User=daoEtat_User.save(e);
		log.info(monEtat_User);
		return monEtat_User;
	}

	/**
	 * Recherche d'un Etat user par son ID
	 * @param id de l'état User a récupérer
	 * @return l'Etat_User 
	 */
	@Override
	public  Optional<Etat_User> findById(Integer i) {
		log.info("ServiceUserImpl.findById appelé");
		log.info("Avec pour parametre");
		log.info(i);
		log.info("valeur retournee :");
		Etat_User monEtat_User=daoEtat_User.findById(i).get();
		log.info(monEtat_User);
		return Optional.ofNullable(monEtat_User);
	}

	/**
	 * Mise a jour d'un Etat_User
	 * @param l'Etat_User a mettre a jour
	 * @return l'Etat user tel que persisté
	 */
	@Override
	public Etat_User update(Etat_User e) {
		log.info("ServiceEtat_UserImpl.update appelé");
		log.info("Avec pour parametre");
		log.info(e);
		log.info("valeur retournee :");
		Etat_User monEtat_User=daoEtat_User.save(e);
		log.info(monEtat_User);
		return monEtat_User;
	}

	/**
	 * Récupération d'une liste Iterable de tous les etats de l'utilisateur
	 * @return Iterable de tous les etats user
	 */
	@Override
	public Iterable<Etat_User> findAll() {
		log.info("ServiceEtat_UserImpl.findAll appelé");
		log.info("valeurs retournées");
		daoEtat_User.findAll().forEach(Etat_User -> log.info(Etat_User));
		return daoEtat_User.findAll();
	}

	/**
	 * 	Suppresion d'un Etat user
	 * @param Etat_User a supprimer
	 */
	@Override
	public void delete(Etat_User e) {
		log.info("ServiceEtat_UserImpl.delete appelé");
		log.info("Avec pour parametre");
		log.info(e);
		daoEtat_User.delete(e);
		
	}



	

}
