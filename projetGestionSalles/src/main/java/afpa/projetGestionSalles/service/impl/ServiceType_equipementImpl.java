package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Type_equipement;
import afpa.projetGestionSalles.repository.RepositoryType_equipement;
import afpa.projetGestionSalles.service.IServiceType_equipement;

/**
 * Classe service des types d'équipements qui implémente l'interface IServiceType_equipement.
 * Injection du Repository de type_equipement qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author jordy
 *
 */
@Transactional
@Service
public class ServiceType_equipementImpl implements IServiceType_equipement {

	@Autowired
	RepositoryType_equipement repotype_equip;
	
	@Autowired
	Logger logger;
	
	/**
	 * Retourne la liste des types d'équipements qui correspond à la table
	 * t_type_equipement
	 * @return un Iterable de Salles
	 */
	@Override
	public Iterable<Type_equipement> getAllType_equipement() {
		logger.info("Voici la liste de toutes les types d'équipements disponibles");
		return repotype_equip.findAll();
	}

	/**
	 * Retourne le type d'équipement qui correspond à l'id passé en paramètre
	 * 
	 * @return un Optional de type d'équipement, permet d'encapsuler un object null
	 * Evite l'exception nullpointerexception
	 */
	@Override
	public Optional<Type_equipement> getEquipementById(Integer id) {
		logger.info("Le type d'équipement séléctionné est ");
		return repotype_equip.findById(id);
	}

	/**
	 * Ajoute ou met à jour un nouveau type d'équipement dans la bdd
	 * 
	 * @return une Type d'équipement
	 */
	@Override
	public Type_equipement addOrMergeType_equipement(Type_equipement type_equipement) {
		logger.info("Nouveau type d'équipement ajouté");
		return repotype_equip.save(type_equipement);
	}

	/**
	 * Supprime un type d'équipement de la bdd via son id
	 */
	@Override
	public void deleteType_equipement(Type_equipement type_equipement) {
		logger.info("Type d'équipement supprimé");
		repotype_equip.delete(type_equipement);
	}

	@Override
	public Type_equipement findByNomtypeequipement(String nomtypeequipement) {
		logger.info("service findbyname");
		return repotype_equip.findByNomtypeequipement(nomtypeequipement);
	}

}
