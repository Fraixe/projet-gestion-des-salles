package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.Reservation;
import afpa.projetGestionSalles.repository.RepositoryReservation;
import afpa.projetGestionSalles.service.IServiceReservation;

/**
 * Classe service des réservations qui implémente l'interface IServiceReservation.
 * Injection du Repository de Reservation qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author ClémentF
 *
 */

@Service
public class ServiceReservationImpl implements IServiceReservation {

	/*Injection du logger*/
	@Autowired
	Logger logger;

	/*Injection du Repository Reservation*/
	@Autowired
	RepositoryReservation repositoryReservation;

	/**
	 * Récupération de la liste des reservation
	 * @return un Iterable qui renvoi la liste des reservation
	 */
	@Override
	public Iterable<Reservation> getAllReservation() {
		logger.info("Dans ServiceReservationImpl.getAllReservation");

		repositoryReservation.findAll().forEach(reservation -> logger.info(reservation.getIdReservation()+""+reservation.getDescription()));
		return repositoryReservation.findAll();
	}

	/**
	 * Récupère une Reservation via son id
	 * @param id de la reservation à récupèrer
	 * @return un Reservation 
	 */
	@Override
	public Optional<Reservation> getReservationById(Integer id) {
		logger.info("Dans ServiceReservationImpl.getReservationById");

		logger.info(repositoryReservation.findById(id));
		return repositoryReservation.findById(id);
	}

	/**
	 * Ajout d'une reservation dans la bdd
	 * @param reservation à ajouter
	 * @return la reservation qui a été ajouté
	 */
	@Override
	public Reservation addReservation(Reservation reservation) {
		logger.info("dans ServiceReservationImpl.addReservation");

		repositoryReservation.save(reservation);
		return reservation;
	}

	/**
	 * update d'une reservation déjà existante
	 * @param reservation à modifier
	 * @return Reservation mis à jour
	 */
	@Override
	public Reservation updateReservation(Reservation reservation) {
		logger.info("dans ServiceReservationImpl.updateReservation");

		return repositoryReservation.save(reservation);	
	}

	/**
	 * Suppression d'une reservation via son id
	 * @param id de la reservation à supprimer
	 */
	@Override
	public void deleteReservationById(Integer id) {
		logger.info("dans ServiceReservationImpl.deleteReservationById");

		repositoryReservation.deleteById(id);
	}
}
