/**
 * 
 */
package afpa.projetGestionSalles.service.impl;



import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.User;
import afpa.projetGestionSalles.repository.RepositoryUser;
import afpa.projetGestionSalles.service.IServiceUser;

/**
 * Classe service des Utilisateurs qui implémente l'interface IServiceUser.
 * Injection du Repository de Users nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * 
 * @author Pierre
 */
@Service
public class ServiceUserImpl implements IServiceUser {
		@Autowired
		RepositoryUser daoUser;
		
		@Autowired
		Logger log;

		/**
		 * Sauvegarde d'un nouvel Utilisateur
		 * @param Utilisateur a creer
		 * @return Utilisateur  créé
		 */
		@Override
		public User add(User u) {
			log.info("ServiceUserImpl.add appelé");
			log.info("Avec pour parametre");
			log.info(u);
			log.info("valeur retournee :");
			User monUser=daoUser.save(u);
			log.info(monUser);
			return monUser;
		}

		/**
		 * Recherche d'un Utilisateur par son ID
		 * @param id de l'Utilisateur a récupérer
		 * @return l'Utilisateur
		 */
		@Override
		public Optional<User> findById(Integer i) {
			log.info("ServiceUserImpl.add appelé");
			log.info("Avec pour parametre");
			log.info(i);
			log.info("valeur retournee :");
			User monUser=daoUser.findById(i).get();
			log.info(monUser);
			return Optional.ofNullable(monUser);
		}

		/**
		 * Mise a jour d'un Eta_User
		 * @param l'Utilisateur a mettre a jour
		 * @return l'Utilisateur tel que persisté
		 */
		@Override
		public User update(User u) {
			log.info("ServiceUserImpl.update appelé");
			log.info("Avec pour parametre");
			log.info(u);
			log.info("valeur retournee :");
			User monUser=daoUser.save(u);
			log.info(monUser);
			return monUser;
		}

		/**
		 * Récupération d'une liste Iterable de tous les l'Utilisateurs
		 * @return Iterable de tous les l'Utilisateurs
		 */
		@Override
		public Iterable<User> findAll() {
			log.info("ServiceUserImpl.findAll appelé");
			log.info("valeurs retournées");
			daoUser.findAll().forEach(User -> log.info(User));
			return daoUser.findAll();
		}

		/**
		 * 	Suppresion d'un l'Utilisateur
		 * @param l'Utilisateurs a supprimer
		 */
		@Override
		public void delete(User u) {
			log.info("ServiceUserImpl.delete appelé");
			log.info("Avec pour parametre");
			log.info(u);
			daoUser.delete(u);			
		}

		@Override
		public Optional<User> findByNomuserAndPassword(String nom_user, String password) {
			log.info("ServiceUserImpl.findByNom_userAndPassword appelé");
			log.info("Avec pour parametres");
			log.info("nom_user :" + nom_user);
			log.info("password :" + password);
			log.info("valeur retournee :");
			User monUser=daoUser.findByNomuserAndPassword(nom_user, password).get();
			log.info(monUser);
			return  Optional.ofNullable(monUser);	
		}
			
}
