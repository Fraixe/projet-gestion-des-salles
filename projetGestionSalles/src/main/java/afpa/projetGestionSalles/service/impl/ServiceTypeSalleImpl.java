package afpa.projetGestionSalles.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import afpa.projetGestionSalles.beans.TypeSalle;
import afpa.projetGestionSalles.repository.RepositoryTypeSalle;
import afpa.projetGestionSalles.service.IServiceTypeSalle;
/**
 * Classe service des type de salles qui implémente l'interface IServiceTypeSalle.
 * Injection du Repository de TypeSalle qui nous permet d'accèder au CrudRepository 
 * et donc de requêter la BDD
 * @author ClémentF
 *
 */
@Service
public class ServiceTypeSalleImpl implements IServiceTypeSalle {

	/*Injection du logger*/
	@Autowired
	Logger logger;

	/*Injection du Repository Personne*/
	@Autowired
	RepositoryTypeSalle repositoryTypeSalle;

	/**
	 * Récupération de la liste des types de salles
	 * @return un Iterable qui renvoi la liste des types de salles
	 */
	@Override
	public Iterable<TypeSalle> getAllTypeSalle() {
		logger.info("dans ServiceTypeSalleImpl.getAllTypeSalle");

		repositoryTypeSalle.findAll().forEach(typeSalle -> logger.info(typeSalle.getNom_type_salle()));
		return repositoryTypeSalle.findAll();
	}

	/**
	 * Récupère un TypeSalle via son id
	 * @param id du typeSalle à récupèrer
	 * @return un TypeSalle 
	 */
	@Override
	public Optional<TypeSalle> getTypeSalleById(Integer id) {
		logger.info("dans ServiceTypeSalleImpl.getTypeSalleById");

		logger.info(repositoryTypeSalle.findById(id));
		return repositoryTypeSalle.findById(id);
	}

	/**
	 * Ajout d'un type de salle dans la bdd
	 * @param typeSalle à ajouter
	 * @return le TypeSalle qui a été ajouté
	 */
	@Override
	public TypeSalle addTypeSalle(TypeSalle typeSalle) {
		logger.info("dans ServiceTypeSalleImpl.addTypeSalle");

		repositoryTypeSalle.save(typeSalle);
		return typeSalle;
	}

	/**
	 * update d'un type de salle déjà existant
	 * @param typeSalle à modifier
	 * @return TypeSalle mis à jour
	 */
	@Override
	public TypeSalle updateTypeSalle(TypeSalle typeSalle) {
		logger.info("dans ServiceTypeSalleImpl.updateTypeSalle");
		
		return repositoryTypeSalle.save(typeSalle);
	}

	/**
	 * Suppression d'un type de salle via son id
	 * @param id du typeSalle à supprimer
	 */
	@Override
	public void deleteTypeSalleById(Integer id) {
		logger.info("dans ServiceTypeSalleImpl.deleteTypeSalleById");	
		
		repositoryTypeSalle.deleteById(id);
	}
}
