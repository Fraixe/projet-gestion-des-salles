/**
 * 
 */
package afpa.projetGestionSalles.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * Bean User linked with User table into database
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_user")
public class User implements Serializable{
	
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="iduser")
	private Integer idUser;
	@Column(name="prenomuser")
	private String prenomUser;
	@Column(name="nomuser")
	private String nomuser;
	@Column(name="matriculeuser")
	private Integer matriculeUser;
	@Column(name="emailuser")
	private String emailUser;
	@Column(name="password")
	private String password;
	@Column(name="telephoneuser")
	private Integer telephoneUser;
	@Column(name="mdpachanger")
	private Boolean mdpachanger;
	
	@OneToOne ( cascade= {CascadeType.MERGE})
	@JoinColumn( name="idrole" )
	private Role idRole;
	@OneToOne ( cascade= {CascadeType.MERGE})
	@JoinColumn( name="idlangue" )
	private Langue idLangue;
	@OneToOne ( cascade= {CascadeType.MERGE})
	@JoinColumn( name="idetatuser" )	
	private Etat_User idEtatUser;
	
	
	public User() {
		
	}
	
	
	/**
	 * Constructor using all parameters
	 * @param id_user
	 * @param prenom_user
	 * @param nom_user
	 * @param matricule_user
	 * @param email_user
	 * @param telephone_user
	 * @param password
	 * @param mdp_a_changer
	 * @param id_role
	 * @param id_langue
	 * @param id_etat_user
	 */
	public User(Integer id_user, String prenom_user, String nom_user, Integer matricule_user, String email_user,
			Integer telephone_user, String password, Boolean mdp_a_changer, Role id_role, Langue id_langue,
			Etat_User id_etat_user) {
		super();
		this.idUser = id_user;
		this.prenomUser = prenom_user;
		this.nomuser = nom_user;
		this.matriculeUser = matricule_user;
		this.emailUser = email_user;
		this.telephoneUser = telephone_user;
		this.password = password;
		this.mdpachanger = mdp_a_changer;
		this.idRole = id_role;
		this.idLangue = id_langue;
		this.idEtatUser = id_etat_user;
	}
	
	
	
	
	/**
	 * Constructor using parameters except ID
	 * @param prenom_user
	 * @param nom_user
	 * @param matricule_user
	 * @param email_user
	 * @param telephone_user
	 * @param password
	 * @param mdp_a_changer
	 * @param id_role
	 * @param id_langue
	 * @param id_etat_user
	 */
	public User(String prenom_user, String nom_user, Integer matricule_user, String email_user, Integer telephone_user,
			String password, Boolean mdp_a_changer, Role id_role, Langue id_langue, Etat_User id_etat_user) {
		super();
		this.prenomUser = prenom_user;
		this.nomuser = nom_user;
		this.matriculeUser = matricule_user;
		this.emailUser = email_user;
		this.telephoneUser = telephone_user;
		this.password = password;
		this.mdpachanger = mdp_a_changer;
		this.idRole = id_role;
		this.idLangue = id_langue;
		this.idEtatUser = id_etat_user;
	}




	/**
	 * @return the id_user
	 */
	public Integer getIdUser() {
		return idUser;
	}
	/**
	 * @param id_user the id_user to set
	 */
	public void setIdUser(Integer id_user) {
		this.idUser = id_user;
	}
	/**
	 * @return the prenom_user
	 */
	public String getPrenomUser() {
		return prenomUser;
	}
	/**
	 * @param prenom_user the prenom_user to set
	 */
	public void setPrenomUser(String prenom_user) {
		this.prenomUser = prenom_user;
	}
	/**
	 * @return the nom_user
	 */
	public String getNomUser() {
		return nomuser;
	}
	/**
	 * @param nom_user the nom_user to set
	 */
	public void setNomUser(String nom_user) {
		this.nomuser = nom_user;
	}
	/**
	 * @return the matricule_user
	 */
	public Integer getMatriculeUser() {
		return matriculeUser;
	}
	/**
	 * @param matricule_user the matricule_user to set
	 */
	public void setMatriculeUser(Integer matricule_user) {
		this.matriculeUser = matricule_user;
	}
	/**
	 * @return the email_user
	 */
	public String getEmailUser() {
		return emailUser;
	}
	/**
	 * @param email_user the email_user to set
	 */
	public void setEmailUser(String email_user) {
		this.emailUser = email_user;
	}
	/**
	 * @return the telephone_user
	 */
	public Integer getTelephoneUser() {
		return telephoneUser;
	}
	/**
	 * @param telephone_user the telephone_user to set
	 */
	public void setTelephoneUser(Integer telephone_user) {
		this.telephoneUser = telephone_user;
	}
	/**
	 * @return the id_role
	 */
	public Role getIdRole() {
		return idRole;
	}
	/**
	 * @param id_role the id_role to set
	 */
	public void setIdRole(Role id_role) {
		this.idRole = id_role;
	}
	/**
	 * @return the id_langue
	 */
	public Langue getIdLangue() {
		return idLangue;
	}
	/**
	 * @param id_langue the id_langue to set
	 */
	public void setIdLangue(Langue id_langue) {
		this.idLangue = id_langue;
	}
	/**
	 * @return the id_etat_user
	 */
	public Etat_User getIdEtatUser() {
		return idEtatUser;
	}
	/**
	 * @param id_etat_user the id_etat_user to set
	 */
	public void setIdEtatUser(Etat_User id_etat_user) {
		this.idEtatUser = id_etat_user;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the mdp_a_changer
	 */
	public Boolean getMdpAChanger() {
		return mdpachanger;
	}
	/**
	 * @param mdp_a_changer the mdp_a_changer to set
	 */
	public void setMdpAChanger(Boolean mdp_a_changer) {
		this.mdpachanger = mdp_a_changer;
	}


	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", prenomUser=" + prenomUser + ", nomuser=" + nomuser + ", matriculeUser="
				+ matriculeUser + ", emailUser=" + emailUser + ", password=" + password + ", telephoneUser="
				+ telephoneUser + ", mdpachanger=" + mdpachanger + ", idRole=" + idRole + ", idLangue=" + idLangue
				+ ", idEtatUser=" + idEtatUser + "]";
	}
	
	

}
