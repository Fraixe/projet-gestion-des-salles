/**
 * 
 */
package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Bean Langue linked with Langue table into database
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_langue")
public class Langue {
	
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="idlangue")
	private Integer idLangue;
	
	@Column(name="langue")
	private String langue;

	/**
	 * Default constructor
	 */
	public Langue() {
	}
	
	
	/**
	 * Constructor using all parameters
	 * @param id_langue
	 * @param langue
	 */
	public Langue(Integer id_langue, String langue) {
		super();
		this.idLangue = id_langue;
		this.langue = langue;
	}
	
	/**
	 * Constructor using Parameter except ID
	 * @param langue
	 */
	public Langue( String langue) {
		super();
		this.langue = langue;
	}
	
	/**
	 * @return the id_langue
	 */
	public Integer getIdLangue() {
		return idLangue;
	}
	/**
	 * @param id_langue the id_langue to set
	 */
	public void setIdLangue(Integer id_langue) {
		this.idLangue = id_langue;
	}
	/**
	 * @return the langue
	 */
	public String getLangue() {
		return langue;
	}
	/**
	 * @param langue the langue to set
	 */
	public void setLangue(String langue) {
		this.langue = langue;
	}

	
	
}
