package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Bean Batiment linked with t_batiment table into database
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_batiment")
public class Batiment {

	/**
	 * Default Contructor
	 */
	public Batiment() {
		
	}
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="idbatiment")
	private Integer idBatiment;
	@Column(name="nombatiment")
	private String  nomBatiment;
	@Column(name="adressebatiment")
	private String adresseBatiment;
	
	/**
	 * Constructor using parameters
	 * @param id_batiment Identifier of Batiment
	 * @param nom_batiment Name of the Batiment
	 * @param adresse_batiment : Adress of the batiment
	 */
	public Batiment(Integer id_batiment, String nom_batiment, String adresse_batiment) {
		super();
		this.idBatiment = id_batiment;
		this.nomBatiment = nom_batiment;
		this.adresseBatiment = adresse_batiment;
	}
	
	/**
	 * Constructor with parameters except ID
	 * @param nom_batiment Name of the Batiment
	 * @param adresse_batiment : Adress of the batiment
	 * @param nb_etage
	 */
	public Batiment(String nom_batiment, String adresse_batiment) {
		super();
		this.nomBatiment = nom_batiment;
		this.adresseBatiment = adresse_batiment;
	}

	/**
	 * Constructor with parameters except adresse and etage
	 * @param idBatiment Identifier of Batiment
	 * @param nomBatiment Name of the Batiment
	 */
	public Batiment(Integer idBatiment, String nomBatiment) {
		super();
		this.idBatiment = idBatiment;
		this.nomBatiment = nomBatiment;
	}
	/**
	 * @return the id_batiment
	 */
	public Integer getId_batiment() {
		return idBatiment;
	}
	
	@Override
	public String toString() {
		return "Batiment [idBatiment=" + idBatiment + ", nomBatiment=" + nomBatiment + ", adresseBatiment="
				+ adresseBatiment + "]";
	}

	/**
	 * @param id_batiment the id_batiment to set
	 */
	public void setId_batiment(Integer id_batiment) {
		this.idBatiment = id_batiment;
	}
		
	/**
	 * @return the nomBatiment
	 */
	public String getNomBatiment() {
		return nomBatiment;
	}

	/**
	 * @param nomBatiment the nomBatiment to set
	 */
	public void setNomBatiment(String nomBatiment) {
		this.nomBatiment = nomBatiment;
	}

	/**
	 * @return the adresse_batiment
	 */
	public String getAdresseBatiment() {
		return adresseBatiment;
	}
	
	/**
	 * @param adresse_batiment the adresse_batiment to set
	 */
	public void setAdresseBatiment(String adresseBatiment) {
		this.adresseBatiment = adresseBatiment;
	}
}
