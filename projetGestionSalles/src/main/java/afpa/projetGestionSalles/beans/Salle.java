package afpa.projetGestionSalles.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;


/**POJO Salle
 * @author jordy
 *
 */
@Table(name = "t_salle")
@Entity
public class Salle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idsalle", unique = true, nullable = false) 
	private Integer idsalle;

	@Column(name = "nomsalle")
	private String nomsalle;

	@Column(name = "capacitesalle")
	private Integer capacitesalle;

	@Column(name = "etagesalle")
	private Integer etagesalle;
	
	@Column(name = "descriptionsalle")
	private String descriptionsalle;
	
	@OneToOne @JoinColumn(name="idbatiment")
	private Batiment idbatiment; 
	
	@OneToOne @JoinColumn(name="idtypesalle")
	private TypeSalle idtypesalle;
	
	@ManyToMany
	@JoinTable(
			name = "t_posseder", 
			joinColumns = @JoinColumn(name = "idsalle"), 
			inverseJoinColumns = @JoinColumn(name = "idtypeequipement"))
	private List<Type_equipement> listetypeequipement = 
						new ArrayList<Type_equipement>();
	
	
	//Les constructeurs
	public Salle(Integer idsalle, String nomsalle, Integer capacitesalle, Integer etagesalle,
			String descriptionsalle) {
		this.idsalle = idsalle;
		this.nomsalle = nomsalle;
		this.capacitesalle = capacitesalle;
		this.etagesalle = etagesalle;
		this.descriptionsalle = descriptionsalle;
	}


	public Salle(String nomsalle, Integer capacitesalle, Integer etagesalle, String descriptionsalle) {
		this.nomsalle = nomsalle;
		this.capacitesalle = capacitesalle;
		this.etagesalle = etagesalle;
		this.descriptionsalle = descriptionsalle;
	}

	public Salle() {

	}



	//Les guetteurs et setteurs
	/**
	 * @return the id_salle
	 */
	public Integer getIdsalle() {
		return idsalle;
	}

	/**
	 * @param id_salle the id_salle to set
	 */
	public void setIdsalle(Integer id_salle) {
		this.idsalle = id_salle;
	}

	/**
	 * @return the nom_salle
	 */
	public String getNomsalle() {
		return nomsalle;
	}

	/**
	 * @param nom_salle the nom_salle to set
	 */
	public void setNomsalle(String nom_salle) {
		this.nomsalle = nom_salle;
	}

	/**
	 * @return the capacite_salle
	 */
	public Integer getCapacitesalle() {
		return capacitesalle;
	}

	/**
	 * @param capacite_salle the capacite_salle to set
	 */
	public void setCapacitesalle(Integer capacite_salle) {
		this.capacitesalle = capacite_salle;
	}

	/**
	 * @return the etage_salle
	 */
	public Integer getEtagesalle() {
		return etagesalle;
	}

	/**
	 * @param etage_salle the etage_salle to set
	 */
	public void setEtagesalle(Integer etage_salle) {
		this.etagesalle = etage_salle;
	}

	/**
	 * @return the description_salle
	 */
	public String getDescriptionsalle() {
		return descriptionsalle;
	}

	/**
	 * @param description_salle the description_salle to set
	 */
	public void setDescriptionsalle(String description_salle) {
		this.descriptionsalle = description_salle;
	}



	/**
	 * @return the listetypeequipement
	 */
	public List<Type_equipement> getListetypeequipement() {
		return listetypeequipement;
	}


	/**
	 * @param listetypeequipement the listetypeequipement to set
	 */
	public void setListetypeequipement(List<Type_equipement> listetypeequipement) {
		this.listetypeequipement = listetypeequipement;
	}


	/**
	 * @return the batiment
	 */
	public Batiment getBatiment() {
		return idbatiment;
	}


	/**
	 * @param batiment the batiment to set
	 */
	public void setBatiment(Batiment batiment) {
		this.idbatiment = batiment;
	}


	/**
	 * @return the typesalle
	 */
	public TypeSalle getTypesalle() {
		return idtypesalle;
	}


	/**
	 * @param typesalle the typesalle to set
	 */
	public void setTypesalle(TypeSalle typesalle) {
		this.idtypesalle = typesalle;
	}


	//Redéfinition de la méthode toString
	@Override
	public String toString() {
		return "Salle [idsalle=" + idsalle + ", nomsalle=" + nomsalle + ", capacitesalle=" + capacitesalle
				+ ", etagesalle=" + etagesalle + ", descriptionsalle=" + descriptionsalle + "]";
	}

}
