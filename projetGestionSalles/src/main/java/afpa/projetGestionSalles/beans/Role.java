package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * POJO Rôle qui correspond à la table t_role de la BDD
 * @author ClémentF
 *
 */
@Entity
@Table(name="t_role")
public class Role {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idrole")
	private Integer idRole;
	
	@Column(name="nomrole")
	private String nomRole;
	
	@Column(name="descriptionrole")
	private String descriptionRole;

	public Role() {
	}

	public Role(Integer id_role, String nom_role, String description_role) {
		this.idRole = id_role;
		this.nomRole = nom_role;
		this.descriptionRole = description_role;
	}
	
	public Role(String nom_role, String description_role) {
		this.nomRole = nom_role;
		this.descriptionRole = description_role;
	}

	public Integer getId_role() {
		return idRole;
	}

	public void setId_role(Integer id_role) {
		this.idRole = id_role;
	}

	public String getNom_role() {
		return nomRole;
	}

	public void setNom_role(String nom_role) {
		this.nomRole = nom_role;
	}

	public String getDescription_role() {
		return descriptionRole;
	}

	public void setDescription_role(String description_role) {
		this.descriptionRole = description_role;
	}

	@Override
	public String toString() {
		return "Rôle [id_role=" + idRole + ", nom_role=" + nomRole + ", description_role=" + descriptionRole + "]";
	}
}
