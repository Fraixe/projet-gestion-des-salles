/**
 * 
 */
package afpa.projetGestionSalles.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**POJO type_equipement
 * @author jordy
 *
 */
@Entity
@Table(name = "t_type_equipement")
public class Type_equipement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idtypeequipement", unique = true, nullable = false)
	private Integer idtypeequipement;
	
	@Column(name = "nomtypeequipement")
	private String nomtypeequipement;
	
	@ManyToMany(mappedBy = "listetypeequipement")
	private List<Salle> listesalle = new ArrayList<Salle>();
	
	
	//Les constructeurs
	public Type_equipement(Integer idtypeequipement, String nomtypeequipement) {
		this.idtypeequipement = idtypeequipement;
		this.nomtypeequipement = nomtypeequipement;
	}

	public Type_equipement(String nomtypeequipement) {
		this.nomtypeequipement = nomtypeequipement;
	}
	
	public Type_equipement() {
		
	}


	//Les guetteurs et setteurs
	/**
	 * @return the id_type_equipement
	 */
	public Integer getIdtypeequipement() {
		return idtypeequipement;
	}

	/**
	 * @param id_type_equipement the id_type_equipement to set
	 */
	public void setIdtypeequipement(Integer id_type_equipement) {
		this.idtypeequipement = id_type_equipement;
	}

	/**
	 * @return the nom_type_equipement
	 */
	public String getNomtypeequipement() {
		return nomtypeequipement;
	}

	/**
	 * @param nom_type_equipement the nom_type_equipement to set
	 */
	public void setNomtypeequipement(String nom_type_equipement) {
		this.nomtypeequipement = nom_type_equipement;
	}

	/**
	 * @return the listesalle
	 */
	public List<Salle> getListesalle() {
		return listesalle;
	}
	
	/**
	 * @param listesalle the listesalle to set
	 */
	public void setListesalle(List<Salle> listesalle) {
		this.listesalle = listesalle;
	}
	
	//Redéfinition de la méthode toString
	@Override
	public String toString() {
		return "type_equipement [id_type_equipement=" + idtypeequipement + ", nom_type_equipement="
				+ nomtypeequipement + "]";
	}
	

}
