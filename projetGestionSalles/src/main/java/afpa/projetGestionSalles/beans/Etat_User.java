package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_etat_user")
public class Etat_User {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="idetatuser")
	private Integer idEtatUser;
	@Column(name="etatuser")
	private String etatUser;

	/**
	 * Default constructor
	 */
	public Etat_User() {
	}

	/**
	 * Constructor using parameters
	 * @param id_etat_user
	 * @param etat_user
	 */
	public Etat_User(Integer id_etat_user, String etat_user) {
		super();
		this.idEtatUser = id_etat_user;
		this.etatUser = etat_user;
	}

	/**
	 * Constructor with parameter except ID
	 * @param etat_user
	 */
	public Etat_User(String etat_user) {
		super();
		this.etatUser = etat_user;
	}

	/**
	 * 
	 * @return the id_etat_user
	 */
	public Integer getId_etat_user() {
		return idEtatUser;
	}
	/**
	 * @param id_etat_user the id_etat_user to set
	 */
	public void setId_etat_user(Integer id_etat_user) {
		this.idEtatUser = id_etat_user;
	}
	/**
	 * @return the etat_user
	 */
	public String getEtat_user() {
		return etatUser;
	}
	/**
	 * @param etat_user the etat_user to set
	 */
	public void setEtat_user(String etat_user) {
		this.etatUser = etat_user;
	}


	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Etat_User [id_etat_user=" + idEtatUser + ", etat_user=" + etatUser + "]";
	}





}
