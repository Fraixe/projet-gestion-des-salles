/**
 * 
 */
package afpa.projetGestionSalles.beans;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * POJO Paramètre qui correspond à la table t_paramètre de la BDD
 * un paramètre sert à la configuration du site (ex. langue affichée)
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_parametre")
public class Parametre {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="idparametre")
	private Integer idParametre;
	@Column(name="logo")
	private String logo;
	@OneToOne @JoinColumn(name="idlangue")
	private Langue langue;
	@Column(name="debplagejournaliere")
	private Time debPlageJournaliere;
	@Column(name="finplagejournaliere")
	private Time finPlageJournaliere;
	@Column(name="granularite")
	private Time granularite;
	@Column(name="server")
	private String server;
	@Column(name="port")
	private Integer port;
	@Column(name="loginserver")
	private Integer loginServer;
	@Column(name="motdepasseserver")
	private Integer motDePasseServer; 
	
	
	/**
	 * Default Constructor
	 */
	public Parametre() {
	}

	/**
	 * 
	 * Constructor with all parameters
	 * @param id_parametre
	 * @param nom_parametre
	 * @param valeur_parametre
	 */
	public Parametre(Integer idParametre, Langue langue, Time debPlageJournaliere,
			Time finPlageJournaliere, Time granularite, String server, Integer port, Integer loginServer,
			Integer motDePasseServer) {
		this.idParametre = idParametre;
		this.langue = langue;
		this.debPlageJournaliere = debPlageJournaliere;
		this.finPlageJournaliere = finPlageJournaliere;
		this.granularite = granularite;
		this.server = server;
		this.port = port;
		this.loginServer = loginServer;
		this.motDePasseServer = motDePasseServer;
	}

	/**
	 * Constructor W/O ID
	 * @param nom_parametre
	 * @param valeur_parametre
	 */
	public Parametre(Langue langue, Time debPlageJournaliere,
			Time finPlageJournaliere, Time granularite, String server, Integer port, Integer loginServer,
			Integer motDePasseServer) {
		this.langue = langue;
		this.debPlageJournaliere = debPlageJournaliere;
		this.finPlageJournaliere = finPlageJournaliere;
		this.granularite = granularite;
		this.server = server;
		this.port = port;
		this.loginServer = loginServer;
		this.motDePasseServer = motDePasseServer;
	}
	
	/*Guetteur et setter*/
	public Integer getIdParametre() {
		return idParametre;
	}

	public void setIdParametre(Integer idParametre) {
		this.idParametre = idParametre;
	}

	public Langue getLangue() {
		return langue;
	}

	public void setLangue(Langue langue) {
		this.langue = langue;
	}

	public Date getDebPlageJournaliere() {
		return debPlageJournaliere;
	}

	public void setDebPlageJournaliere(Time debPlageJournaliere) {
		this.debPlageJournaliere = debPlageJournaliere;
	}

	public Date getFinPlageJournaliere() {
		return finPlageJournaliere;
	}

	public void setFinPlageJournaliere(Time finPlageJournaliere) {
		this.finPlageJournaliere = finPlageJournaliere;
	}

	public Date getGranularite() {
		return granularite;
	}

	public void setGranularite(Time granularite) {
		this.granularite = granularite;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getLoginServer() {
		return loginServer;
	}

	public void setLoginServer(Integer loginServer) {
		this.loginServer = loginServer;
	}

	public Integer getMotDePasseServer() {
		return motDePasseServer;
	}

	public void setMotDePasseServer(Integer motDePasseServer) {
		this.motDePasseServer = motDePasseServer;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
}
