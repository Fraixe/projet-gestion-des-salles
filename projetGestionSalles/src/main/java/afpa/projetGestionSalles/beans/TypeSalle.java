package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * POJO TypeSalle qui correspond à la table t_typeSalle de la BDD
 * @author ClémentF
 *
 */
@Entity
@Table(name="t_typeSalle")
public class TypeSalle {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idtypesalle")
	private Integer idTypeSalle;
	
	@Column(name="nomtypesalle")
	private String nomTypeSalle;
	
	public TypeSalle() {
	}

	public TypeSalle(Integer id_type_salle, String nom_type_salle) {
		this.idTypeSalle = id_type_salle;
		this.nomTypeSalle = nom_type_salle;
	}	
	
	public TypeSalle(String nom_type_salle) {
		this.nomTypeSalle = nom_type_salle;
	}

	public Integer getId_type_salle() {
		return idTypeSalle;
	}

	public void setId_type_salle(Integer id_type_salle) {
		this.idTypeSalle = id_type_salle;
	}

	public String getNom_type_salle() {
		return nomTypeSalle;
	}

	public void setNom_type_salle(String nom_type_salle) {
		this.nomTypeSalle = nom_type_salle;
	}

	@Override
	public String toString() {
		return "TypeSalle [id_type_salle=" + idTypeSalle + ", nom_type_salle=" + nomTypeSalle + "]";
	}	
}
