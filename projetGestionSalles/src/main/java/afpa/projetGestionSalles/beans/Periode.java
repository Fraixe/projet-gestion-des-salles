/**
 * 
 */
package afpa.projetGestionSalles.beans;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 * POJO Periode qui correspond à la table t_periode de la BDD
 * Une periode correspond à la durée d'une réservation ou d'un vérouillage d'une salle
 * @author Pierre
 *
 */
@Entity
@Table(name = "t_periode")
public class Periode {

	/**
	 * 
	 */
	public Periode() {

	}
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer idperiode;
	private Date debutdateperiode;
	private Date findateperiode;
	private Time debutheureperiode;
	private Time finheureperiode;
	
	
	/**
	 * @return the id_periode
	 */
	public Integer getId_periode() {
		return idperiode;
	}
	/**
	 * @param id_periode the id_periode to set
	 */
	public void setId_periode(Integer id_periode) {
		this.idperiode = id_periode;
	}
	/**
	 * @return the debut_date_periode
	 */
	public Date getDebut_date_periode() {
		return debutdateperiode;
	}
	/**
	 * @param debut_date_periode the debut_date_periode to set
	 */
	public void setDebut_date_periode(Date debut_date_periode) {
		this.debutdateperiode = debut_date_periode;
	}
	/**
	 * @return the fin_date_periode
	 */
	public Date getFin_date_periode() {
		return findateperiode;
	}
	/**
	 * @param fin_date_periode the fin_date_periode to set
	 */
	public void setFin_date_periode(Date fin_date_periode) {
		this.findateperiode = fin_date_periode;
	}
	/**
	 * @return the debut_heure_periode
	 */
	public Time getDebut_heure_periode() {
		return debutheureperiode;
	}
	/**
	 * @param debut_heure_periode the debut_heure_periode to set
	 */
	public void setDebut_heure_periode(Time debut_heure_periode) {
		this.debutheureperiode = debut_heure_periode;
	}
	/**
	 * @return the fin_heure_periode
	 */
	public Time getFin_heure_periode() {
		return finheureperiode;
	}
	/**
	 * @param fin_heure_periode the fin_heure_periode to set
	 */
	public void setFin_heure_periode(Time fin_heure_periode) {
		this.finheureperiode = fin_heure_periode;
	}
	
	
}
