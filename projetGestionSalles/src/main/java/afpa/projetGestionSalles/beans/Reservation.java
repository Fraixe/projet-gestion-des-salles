package afpa.projetGestionSalles.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * POJO Reservation qui correspond à la table t_reserver de la BDD
 * @author ClémentF
 *
 */
@Entity
@Table(name="t_reserver")
public class Reservation {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idreservation")
	private Integer idReservation;
	
	@OneToOne @JoinColumn(name="idsalle")
	private Salle salle;
	
	@OneToOne @JoinColumn(name="iduser")
	private User user;
	
	@OneToOne @JoinColumn(name="idperiode")
	private Periode periode;
	
	@Column(name="descriptionreservation")
	private String description;
	
	public Reservation() {

	}

	public Reservation(Integer idReservation, Salle salle, User user, Periode periode, String description) {
		this.idReservation = idReservation;
		this.salle = salle;
		this.user = user;
		this.periode = periode;
		this.description = description;
	}
	
	public Reservation(Salle idSalle, User user, Periode periode, String description) {
		this.salle = idSalle;
		this.user = user;
		this.periode = periode;
		this.description = description;
	}
	
	public Integer getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(Integer idReservation) {
		this.idReservation = idReservation;
	}

	public Salle getIdSalle() {
		return salle;
	}

	public void setIdSalle(Salle salle) {
		this.salle = salle;
	}

	public User getIdUser() {
		return user;
	}

	public void setIdUser(User user) {
		this.user = user;
	}

	public Periode getIdPeriode() {
		return periode;
	}

	public void setIdPeriode(Periode periode) {
		this.periode = periode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Reservation [idReservation=" + idReservation + ", idSalle=" + salle + ", idUser=" + user
				+ ", idPeriode=" + periode + ", description=" + description + "]";
	}
}
