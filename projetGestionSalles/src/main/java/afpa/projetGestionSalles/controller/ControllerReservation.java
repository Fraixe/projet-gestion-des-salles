package afpa.projetGestionSalles.controller;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Reservation;
import afpa.projetGestionSalles.service.IServiceReservation;

/**
 * Service Restful des reservations,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs reservation
 * 
 * @author ClémentF
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/reservation")
public class ControllerReservation {
	
	/*Injection du Service des rôles*/
	@Autowired
	IServiceReservation serviceReservation;

	/*Injection du Logger*/
	@Autowired
	Logger logger;
	
	/**
	 * Méthode GET pour récuperer l'ensemble des reservation
	 * de la table t_reserver
	 * @return une liste de reservation
	 * 
	 */
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Reservation> getAllReservationRest(){
		logger.info("Dans ControllerReservation.getAllReservationRest");
		return serviceReservation.getAllReservation();	
	}
	
	/**
	 * Méthode GET pour récupérer une reservation
	 * de la table t_reserver
	 * @param id du role à récupérer
	 * @return reservation 
	 */
	@RequestMapping(value={"getbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public Optional<Reservation> getReservationByIdRest(@PathVariable Integer id){
		logger.info("Dans ControllerReservation.getReservationByIdRest");
		return serviceReservation.getReservationById(id);
	}
	
	/**
	 * Méthode DELETE pour supprimer une reservation
	 * @param id de la reservation à supprimer
	 */
	@RequestMapping(value={"deletebyid/{id}"}, method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteReservationByIdRest(@PathVariable Integer id) {
		logger.info("Dans ControllerReservation.deleteReservationByIdRest");
		serviceReservation.deleteReservationById(id);
	}
	
	/**
	 * Méthode PUT pour ajouter une reservation dans la table
	 * t_reserver, on passe les éléments de la reservation à ajouter
	 * dans le corps de la requête
	 * 
	 * @param reservation à ajouter
	 * @return un Reservation
	 */
	@RequestMapping(value= {"add"}, method=RequestMethod.PUT)
	@ResponseBody
	public Reservation addReservationRest(@RequestBody Reservation reservation) {
		logger.info("Dans ControllerReservation.addReservationRest");
		return serviceReservation.addReservation(reservation);
	}
	
	/**
	 * Méthode POST pour modifier une reservation dans la table
	 * t_reserver, on passe les éléments de la reservation à modifier
	 * dans le corps de la requête
	 * 
	 * @param reservation à modifier
	 * @return Reservation
	 */
	@RequestMapping(value= {"update"}, method=RequestMethod.POST)
	@ResponseBody
	public Reservation updateReservationRest(@RequestBody Reservation reservation) {
		logger.info("Dans ControllerReservation.updateReservationRest");
		return serviceReservation.updateReservation(reservation);
	}
}
