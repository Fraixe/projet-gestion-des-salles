package afpa.projetGestionSalles.controller;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Parametre;
import afpa.projetGestionSalles.service.IServiceParametre;

/**
 * Service Restful des paramètres,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs paramètres
 * 
 * @author ClémentF
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/parametre")
public class ControllerParametre {
	
	/*Injection du Service des parametre*/
	@Autowired
	IServiceParametre serviceParametre;

	/*Injection du Logger*/
	@Autowired
	Logger logger;
	
	/**
	 * Méthode GET pour récuperer l'ensemble des paramètres
	 * de la table t_parametre
	 * @return une liste de parametres
	 * 
	 */
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Parametre> getAllParametreRest(){
		logger.info("ControllerParametre.getAllParametreRest");
		return serviceParametre.findAll();
	}

	/**
	 * Méthode GET pour récupérer un parametre
	 * de la table t_parametre
	 * @param id du parametre à récupérer
	 * @return parametre 
	 */
	@RequestMapping(value={"getbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public Optional<Parametre> getParametreByIdRest(@PathVariable Integer id){
		logger.info("ControllerParametre.getParametreByIdRest");
		return serviceParametre.findById(id);
	}

	/**
	 * Méthode DELETE pour supprimer un parametre
	 * @param param à supprimer
	 */
	@RequestMapping(value={"delete"}, method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteParametreByIdRest(@RequestBody Parametre param) {
		logger.info("ControllerParametre.deleteParametreByIdRest");
		serviceParametre.delete(param);
	}

	/**
	 * Méthode PUT pour ajouter un parametre dans la table
	 * t_parametre, on passe les éléments du parametre à ajouter
	 * dans le corps de la requête
	 * 
	 * @param param à ajouter
	 * @return un Parametre
	 */
	@RequestMapping(value= {"add"}, method=RequestMethod.PUT)
	@ResponseBody
	public Parametre addParametreRest(@RequestBody Parametre param) {
		logger.info("ControllerParametre.addParametreRest");
		serviceParametre.add(param);
		return param;
	}

	/**
	 * Méthode POST pour modifier un parametre dans la table
	 * t_parametre, on passe les éléments du parametre à modifier
	 * dans le corps de la requête
	 * 
	 * @param param à modifier
	 * @return Parametre
	 */
	@RequestMapping(value= {"update"}, method=RequestMethod.POST)
	@ResponseBody
	public Parametre updateRoleRest(@RequestBody Parametre param) {
		logger.info("ControllerParametre.updateRoleRest");
		return serviceParametre.update(param);
	}
}
