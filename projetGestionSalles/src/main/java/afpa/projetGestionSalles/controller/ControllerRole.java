package afpa.projetGestionSalles.controller;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Role;
import afpa.projetGestionSalles.service.IServiceRole;

/**
 * Service Restful des rôles,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs rôles
 * 
 * @author ClémentF
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/role")
public class ControllerRole {

	/*Injection du Service des rôles*/
	@Autowired
	IServiceRole serviceRole;

	/*Injection du Logger*/
	@Autowired
	Logger logger;

	/**
	 * Méthode GET pour récuperer l'ensemble des rôles
	 * de la table t_role
	 * @return une liste de role
	 * 
	 */
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Role> getAllRoleRest(){
		logger.info("Dans ControllerRole.getAllRoleRest");
		return serviceRole.getAllRole();	
	}

	/**
	 * Méthode GET pour récupérer un rôle
	 * de la table t_role
	 * @param id du role à récupérer
	 * @return role 
	 */
	@RequestMapping(value={"getbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public Optional<Role> getRoleByIdRest(@PathVariable Integer id){
		logger.info("Dans ControllerRole.getRoleByIdRest");
		return serviceRole.getRoleById(id);
	}

	/**
	 * Méthode DELETE pour supprimer un rôle
	 * @param id du role à supprimer
	 */
	@RequestMapping(value={"deletebyid/{id}"}, method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteRoleByIdRest(@PathVariable Integer id) {
		logger.info("Dans ControllerRole.deleteRoleByIdRest");
		serviceRole.deleteRoleById(id);
	}

	/**
	 * Méthode PUT pour ajouter un role dans la table
	 * t_role, on passe les éléments du role à ajouter
	 * dans le corps de la requête
	 * 
	 * @param role à ajouter
	 * @return un Role
	 */
	@RequestMapping(value= {"add"}, method=RequestMethod.PUT)
	@ResponseBody
	public Role addRoleRest(@RequestBody Role role) {
		logger.info("Dans ControllerRole.addRoleRest");
		return serviceRole.addRole(role);
	}

	/**
	 * Méthode POST pour modifier un role dans la table
	 * t_role, on passe les éléments du role à modifier
	 * dans le corps de la requête
	 * 
	 * @param role à modifier
	 * @return Role
	 */
	@RequestMapping(value= {"update"}, method=RequestMethod.POST)
	@ResponseBody
	public Role updateRoleRest(@RequestBody Role role) {
		logger.info("Dans ControllerRole.updateRoleRest");
		return serviceRole.updateRole(role);
	}
}

