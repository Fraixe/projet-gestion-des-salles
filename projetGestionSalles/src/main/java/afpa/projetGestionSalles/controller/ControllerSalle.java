package afpa.projetGestionSalles.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Salle;
import afpa.projetGestionSalles.service.IServiceSalle;

/**RestController des salles
 * @author jordy
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/restSalle/")
public class ControllerSalle {
	
	@Autowired
	IServiceSalle servicesalle;
	
	@Autowired
	Logger logger;
	
	/**
	 * GET pour récupérer une liste de salle
	 * @return la liste des salles
	 */
	@RequestMapping(value={"/listesalles"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Salle>> listeSalle(){
		logger.info("/restSalle/allusers appelé");
		List<Salle> salles = (List<Salle>) servicesalle.getAllSalles();
		return new ResponseEntity<>(salles, HttpStatus.OK);
	}
	
	/**
	 * GET pour récupérer une salle via son id
	 * @param id de la salle à récuperer
	 * @return la salle séléctionnée
	 */
	@RequestMapping(value={"/listesalles/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Salle> getSalle(@PathVariable("id") Integer id) {
		logger.info("/restSalle/listesalles/id appelé");
		Salle salle = servicesalle.getSalleById(id).get();
		return new ResponseEntity<>(salle, HttpStatus.OK);
	}
	
	
	/**
	 * Put pour ajouter une salle
	 * @param  un objet salle
	 */
	@RequestMapping(value= {"/salle"}, method=RequestMethod.PUT)
	public ResponseEntity<Salle> addSalle(@RequestBody Salle salle) {
		Salle salleToAdd = servicesalle.addOrMergeSalle(salle);
		return new ResponseEntity<>(salleToAdd, HttpStatus.CREATED);
	}
	
	/**
	 * Post pour mettre à jour une salle
	 * @param une objet salle
	 */
	@PostMapping("/salle")
	public ResponseEntity<Salle> updateSalle(@RequestBody Salle salle) {
		logger.info("/restSalle/listesalles/id appelé");
		Salle salleToUpdate = servicesalle.addOrMergeSalle(salle);
		return new ResponseEntity<>(salleToUpdate, HttpStatus.OK);
	}
	
	/**
	 * DELETE pour supprimer une salle
	 * @param id  l'id de la salle 
	 */
	@DeleteMapping("/deletesalle/{id}")
	public ResponseEntity<?> deleteSalle(@PathVariable("id") Integer id ) {
		logger.info("/restSalle/deletesalle appelé");
		servicesalle.deleteSalleById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * GET pour retrouver une salle par son nom
	 * @param une chaine de caractère correspondant au nom d'une salle
	 */
	@GetMapping("/sallenom/{nom}")
	public ResponseEntity<Salle> getSallebyName(@PathVariable("nom") String nom_salle) {
		logger.info("/restSalle/sallenom appelé");
		
		Salle salle = servicesalle.findByNomsalle(nom_salle);
		return new ResponseEntity<>(salle, HttpStatus.OK);
	}
	
	/**
	 * GET pour afficher la liste des salles par ordre croissant selon la capacité de personne que 
	 * celles ci peuvent acceuillir
	 */
	@GetMapping("/capasc")
	public ResponseEntity<List<Salle>> findAllCapAsc() {
		logger.info("/restSalle/capasc appelé");
		List<Salle> liste = servicesalle.findAllByOrderByCapacitesalleAsc();
		return new ResponseEntity<>(liste, HttpStatus.OK);
	}
	
	/**
	 * GET pour afficher la liste des salles par ordre descroissant selon la capacité de personne que 
	 * celles ci peuvent acceuillir
	 */
	@GetMapping("/capdesc")
	public ResponseEntity<List<Salle>> findAllCapDesc() {
		logger.info("/restSalle/capdesc appelé");
		List<Salle> liste = servicesalle.findAllByOrderByCapacitesalleDesc();
		return new ResponseEntity<>(liste, HttpStatus.OK);
	}

}
