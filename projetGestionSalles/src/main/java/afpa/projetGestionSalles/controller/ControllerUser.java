/**
 * 
 */
package afpa.projetGestionSalles.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.User;
import afpa.projetGestionSalles.service.IServiceUser;

/**
 * Rest Controller pour l'utilisateur
 * @author Lucas
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class ControllerUser {

	@Autowired
	public IServiceUser serviceUser;
	
	// Instanciation du logger
	@Autowired
	Logger log;
	
	
	@Autowired
	HttpSession session; 
	
	//idUser stocker dans la session une fois connecter
		Integer idUser;
		
		
		
	/**
	 * Get pour récupérer la liste de tous les users
	 * @return la liste de tous les utilisteurs
	 */
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<User>> getAllUsers(){
		log.info("/user/allusers appelé");
		List<User> users = (List<User>) serviceUser.findAll();
		
		
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Get pour récupérer un User grace à son id
	 * @param id identidfiant de l'User recherché
	 * @return /!\Optional User
	 */
	@RequestMapping(value={"/findbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<User> findById(@PathVariable("id") Integer id){
		log.info("/user/findbyid appele");
		User user = serviceUser.findById(id).get();
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Get pour récupérer un User grace à son id
	 * @param fakeUser de l'User recherché
	 * @return /!\Optional User
	 */
	@RequestMapping(value={"/login"}, method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> findByNameAndPassword(@RequestBody User fakeUser){
		log.info("/user/login appele");
		String login=fakeUser.getNomUser();
		String password=fakeUser.getPassword();
		User user = serviceUser.findByNomuserAndPassword(login, password).get();
		if (user!=null) {
			log.info("user if not NULL !");
			session.setAttribute("user", user);
			log.info("session : "+ session.getAttribute("user"));
			session.setAttribute("userId", user.getIdUser());
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	
	/**
	 * Delete pour effacer le user passé en paramètre
	 * @param monUser Utilisateur a effacer
	 * @return httpStatut OK
	 */
	@RequestMapping(value={"/delete"}, method=RequestMethod.DELETE)
	@ResponseBody
	public  ResponseEntity<?> delete(@RequestBody User monUser) {
		log.info("/user/delete appele");
		
		serviceUser.delete(monUser);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Add pour ajouter un user
	 * @param monUser User a ajouter (avec id null)
	 * @return user tel que persisté (avec ID)
	 */
	@RequestMapping(value= {"adduser"}, method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<User> addUser(@RequestBody User monUser) {
		log.info("/user/adduser appele");
		log.info("Utilisateur reçu du front");
		log.info(monUser);
		User userToAdd = serviceUser.add(monUser);
		return new ResponseEntity<>(userToAdd, HttpStatus.CREATED);
	}
	
	/**
	 * Méthode pour update un user
	 * @param monUser l'user en param à modifier
	 * @return l'user après modification dans la BDD
	 */
	@RequestMapping(value= {"updateuser"}, method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<User> updateUser(@RequestBody User monUser) {
		log.info("/user/updateuser appele");
		
		User userToUpdate = serviceUser.update(monUser);
		return new ResponseEntity<>(userToUpdate, HttpStatus.OK);
	}
	
	/**
	 * Méthode qui récupère l'id user de l'user connecté
	 * @return l'user connecté après la demande de connexion
	 * @author Valentine
	 */
	@RequestMapping(value={"/monprofil"}, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<User> findById(){
		log.info("Information sur l'utilisateur dans la page mon profil");
		idUser = (Integer) session.getAttribute("iduser");
		
		log.info("role user = " + serviceUser.findById(idUser).get().getIdRole());
		User userprofil = serviceUser.findById(idUser).get() ;
		return new ResponseEntity<>(userprofil, HttpStatus.OK);
	}
	
	/**
	 * méthode pour deconnecter l'user du site
	 * @return null
	 */
	
	@RequestMapping(value={"/disconnect"}, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<User> disconnect(){
		log.info("Deconnexion de l'utilisateur");

		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
