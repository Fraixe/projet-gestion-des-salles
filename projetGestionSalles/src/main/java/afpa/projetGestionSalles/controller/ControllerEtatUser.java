package afpa.projetGestionSalles.controller;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Etat_User;
import afpa.projetGestionSalles.service.IServiceEtat_User;

/**
 * Rest Controller pour l'etat utilisateur
 * @author Lucas
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/etatuser")
public class ControllerEtatUser {

	@Autowired
	IServiceEtat_User serviceEtatUser;
	
	// Instanciation du logger
		@Autowired
		Logger log;
		
		/**
		 * Get pour récupérer la liste de tous les users
		 * @return la liste de tous les utilisteurs
		 */
		@RequestMapping(value={"getall"}, method=RequestMethod.GET)
		@ResponseBody
		public ResponseEntity<List<Etat_User>> getAllUsers(){
			log.info("/user/allusers appelé");
			List<Etat_User> users = (List<Etat_User>) serviceEtatUser.findAll();
			
			
			return new ResponseEntity<>(users, HttpStatus.OK);
		}
}
