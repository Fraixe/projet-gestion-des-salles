package afpa.projetGestionSalles.controller;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.TypeSalle;
import afpa.projetGestionSalles.service.IServiceTypeSalle;

/**
 * Service Restful des types de salles,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs type de salle
 * 
 * @author ClémentF
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/typesalle")
public class ControllerTypeSalle {
	
	/*Injection du Service des types de salles*/
	@Autowired
	IServiceTypeSalle serviceTypeSalle;

	/*Injection du Logger*/
	@Autowired
	Logger logger;

	/**
	 * Méthode GET pour récuperer l'ensemble des types de sakkes
	 * de la table t_typeSalle
	 * @return une liste de typeSalle
	 * 
	 */
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public Iterable<TypeSalle> getAllTypeSalleRest(){
		logger.info("Dans ControllerRole.getAllTypeSalleRest");
		return serviceTypeSalle.getAllTypeSalle();	
	}

	/**
	 * Méthode GET pour récupérer un type de salle
	 * de la table t_typeSalle
	 * @param id du typeSalle à récupérer
	 * @return TypeSalle
	 */
	@RequestMapping(value={"getbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public Optional<TypeSalle> getTypeSalleByIdRest(@PathVariable Integer id){
		logger.info("Dans ControllerRole.getTypeSalleByIdRest");
		return serviceTypeSalle.getTypeSalleById(id);
	}

	/**
	 * Méthode DELETE pour supprimer un type de salle
	 * @param id du typeSalle à supprimer
	 */
	@RequestMapping(value={"deletebyid/{id}"}, method=RequestMethod.DELETE)
	@ResponseBody
	public void deleteTypeSalleByIdRest(@PathVariable Integer id) {
		logger.info("Dans ControllerRole.deleteTypeSalleByIdRest");
		serviceTypeSalle.deleteTypeSalleById(id);
	}

	/**
	 * Méthode PUT pour ajouter un type de salle dans la table
	 * t_typeSalle, on passe les éléments du type de salle à ajouter
	 * dans le corps de la requête
	 * 
	 * @param typeSalle à ajouter
	 * @return un TypeSalle
	 */
	@RequestMapping(value= {"add"}, method=RequestMethod.PUT)
	@ResponseBody
	public TypeSalle addTypeSalleRest(@RequestBody TypeSalle typeSalle) {
		logger.info("Dans ControllerRole.addTypeSalleRest");
		return serviceTypeSalle.addTypeSalle(typeSalle);
	}
	
	/**
	 * Méthode POST pour modifier un type de salle dans la table
	 * t_typeSalle, on passe les éléments du type de salle à modifier
	 * dans le corps de la requête
	 * 
	 * @param typeSalle à modifier
	 * @return TypeSalle
	 */
	@RequestMapping(value= {"update"}, method=RequestMethod.POST)
	@ResponseBody
	public TypeSalle updateTypeSalleRest(@RequestBody TypeSalle typeSalle) {
		logger.info("Dans ControllerRole.updateTypeSalleRest");
		return serviceTypeSalle.updateTypeSalle(typeSalle);
	}
}
