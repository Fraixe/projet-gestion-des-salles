package afpa.projetGestionSalles.controller;

import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Batiment;
import afpa.projetGestionSalles.service.IServiceBatiment;



/**
 * Service Restful des batiments,
 * fourni des API de services permettant de supprimer, ajouter ou obtenir un ou plusieurs batiments
 * @author Lucas
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/batiment")
public class ControllerBatiment {
	
	/*Injection du Logger*/
	@Autowired
	Logger logger;
	
	@Autowired
	IServiceBatiment serviceBatiment;
	
	/**
	 * Méthode GET pour récupérer tout les batiment
	 * de la table t_batiment
	 * @return une liste de batiments
	 */
	
	@RequestMapping(value={"getall"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Batiment>> getAllBatimentRest(){
		logger.info("Appel REST : getAllBatimentRest");
		List<Batiment> batiments = (List<Batiment>) serviceBatiment.findAll();
		return new ResponseEntity<>(batiments, HttpStatus.OK);
	}
	
	/**
	 * Méthode GET pour récupérer un batiment
	 * de la table t_batiment
	 * @param id du batiment à récuperer
	 * @return le batiment
	 */
	@RequestMapping(value={"getbyid/{id}"}, method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Batiment> getBatimentByIdRest(@PathVariable("id") Integer id){
		logger.info("Appel REST : getBatimentByIdRest");
		Batiment batiment =  serviceBatiment.findById(id).get();
		return new ResponseEntity<>(batiment, HttpStatus.OK);
	}

	/**
	 * Méthode DELETE pour supprimer un batiment
	 * @param id l'id du batiment à supprimer
	 */
	@RequestMapping(value={"deletebyid/{id}"}, method=RequestMethod.DELETE)
	@ResponseBody
	public  ResponseEntity<?> deleteBatimentByIdRest(@PathVariable("id") Integer id) {
		logger.info("Appel REST : deleteBatimentByIdRest");
		
		serviceBatiment.deleteById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	/**
	 * Méthode PUT pour ajouter un batiment dans la table
	 * t_batiment, on passe les éléments du batiment à ajouter
	 * dans le corps de la requête
	 * @param batiment le batiment à ajouter dans la BDD
	 * @return l'instance sauvegardé du Batiment dans la BDD
	 */
	@RequestMapping(value= {"add"}, method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Batiment>  addBatimentRest(@RequestBody Batiment batiment) {
		logger.info("Appel REST : addBatiment");
		Batiment batimentToAdd = serviceBatiment.add(batiment);
		return new ResponseEntity<>(batimentToAdd, HttpStatus.CREATED);
	}
	
	/**
	 * Méthode POST pour modifier un batiment dans la table
	 * t_batiment, on passe les éléments du batiment à modifier
	 * dans le corps de la requête
	 * @param batiment le batiment à modifier
	 * @return l'instance sauvegardé du Batiment MIS A JOUR dans la BDD
	 */
	@RequestMapping(value= {"update"}, method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Batiment> updateBatimentRest(@RequestBody Batiment batiment) {
		logger.info("Appel REST : updateBatimentRest");
		Batiment batimentToUpdate = serviceBatiment.update(batiment);
		return new ResponseEntity<>(batimentToUpdate, HttpStatus.OK);
	}
}