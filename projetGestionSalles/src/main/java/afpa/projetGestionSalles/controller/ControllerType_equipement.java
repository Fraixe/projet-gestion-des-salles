package afpa.projetGestionSalles.controller;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import afpa.projetGestionSalles.beans.Type_equipement;
import afpa.projetGestionSalles.service.IServiceType_equipement;


/**RestController des types d'équipements
 * @author jordy
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/restequip/")
public class ControllerType_equipement {
	
	@Autowired
	IServiceType_equipement service_equip;
	
	@Autowired
	Logger logger;
	
	
	/**
	 * GET pour récupérer une liste de type d'équipements
	 * @return une liste d'équipement
	 */
	@GetMapping("/listeequip")
	public List<Type_equipement> listeEquipement(){
		return (List<Type_equipement>) service_equip.getAllType_equipement();
	}
	
	/**
	 * GET pour récupérer un type d'équipement via son id
	 * @param id du type d'équipement récupéré
	 * @return le type d'équipement séléctionné
	 */
	@GetMapping("/listeequip/{id}")
	public Optional<Type_equipement> getEquipement(@PathVariable Integer id) {
		return service_equip.getEquipementById(id);
	}
	
	/**
	 * PUT pour ajouter une type d'équipement
	 * @param equip un objet de type d'équipement
	 */
	@PutMapping("/equipement")
	public void addEquipement(@RequestBody Type_equipement equip) {
		service_equip.addOrMergeType_equipement(equip);
	}
	
	/**
	 * POST pour mettre à jour type d'équipement
	 * @param equip une objetde type d'équipement
	 */
	@PostMapping("/equipement")
	public void updateEquipement(@RequestBody Type_equipement equip) {
		service_equip.addOrMergeType_equipement(equip);
	}
	
	/**
	 * DELETE pour supprimer un type d'équipement
	 * @param equip un objet de type d'équipement
	 */
	@DeleteMapping("/deleteequip")
	public void deleteEquipement(@RequestBody Type_equipement equip) {
		service_equip.deleteType_equipement(equip);
	}
	
	@GetMapping("/equipnom")
	public Type_equipement findbyName(@RequestBody String nomtypeequipement) {
		logger.info(service_equip.findByNomtypeequipement(nomtypeequipement));
		return service_equip.findByNomtypeequipement(nomtypeequipement);
	}
	

}
