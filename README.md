https://dhtmlx.com/x/download/scheduler/scheduler_trial.zip

Adresse pour lancer l'appli dans le navigateur : **localhost:8080**

Script pour créer la base de donnée : 

Créer le user :
```
CREATE ROLE admin WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'afpa123';
```

Créer la base : 
```
CREATE DATABASE "DB_Salles"
    WITH 
    OWNER = admin
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
```
	
	
psql -d DB_Salles -U admin;

Script de création de la base :
```
DROP TABLE if exists t_batiment cascade;

DROP TABLE if exists t_etat_user cascade;

DROP TABLE if exists t_langue cascade;

DROP TABLE if exists t_parametre cascade;

DROP TABLE if exists t_periode cascade;

DROP TABLE if exists t_reserver cascade;

DROP TABLE if exists t_role cascade;

DROP TABLE if exists t_salle cascade;

DROP TABLE if exists t_type_equipement cascade;

DROP TABLE if exists t_type_salle cascade;

DROP TABLE if exists t_user cascade;

DROP TABLE if exists t_posseder cascade;




CREATE TABLE t_batiment 
(
    idbatiment SERIAL NOT NULL PRIMARY KEY,
    nombatiment VARCHAR NOT NULL,
    adressebatiment VARCHAR NULL
);


CREATE TABLE t_etat_user 
(
    idetatuser SERIAL NOT NULL PRIMARY KEY,
    etatuser VARCHAR NOT NULL
);


CREATE TABLE t_langue 
(
    idlangue SERIAL NOT NULL PRIMARY KEY,
    langue VARCHAR NOT NULL
);


CREATE TABLE t_parametre 
(
    idparametre SERIAL NOT NULL  PRIMARY KEY,
    logo VARCHAR NOT NULL,
    idlangue INTEGER NOT NULL,
    debplagejournaliere TIME NOT NULL,
    finplagejournaliere TIME NOT NULL,
    granularite TIME NOT NULL,
    server VARCHAR NOT NULL,
    port INTEGER NOT NULL,
    loginserver INTEGER NULL,
    motdepasseserver INTEGER NULL
);


CREATE TABLE t_periode 
(
    idperiode SERIAL NOT NULL  PRIMARY KEY,
    debutdateperiode DATE NOT NULL,
    findateperiode DATE NOT NULL,
    debutheureperiode TIME NOT NULL,
    finheureperiode TIME NOT NULL
);


CREATE TABLE t_reserver 
(
	idreservation SERIAL  NOT NULL PRIMARY KEY,
    idsalle INTEGER NOT NULL UNIQUE,
    iduser INTEGER NOT NULL  UNIQUE,
    idperiode INTEGER NOT NULL UNIQUE,
    descriptionreservation VARCHAR NULL
);

CREATE TABLE t_posseder
(
    idsalle INTEGER NOT NULL  PRIMARY KEY,
    idtypeequipement INTEGER NOT NULL  UNIQUE
    
);


CREATE TABLE t_role 
(
    idrole SERIAL NOT NULL  PRIMARY KEY,
    nomrole VARCHAR NOT NULL,
    descriptionrole VARCHAR NULL
);


CREATE TABLE t_salle 
(
    idsalle SERIAL NOT NULL  PRIMARY KEY,
    nomsalle VARCHAR NOT NULL,
    capacitesalle NUMERIC NOT NULL,
    etagesalle NUMERIC NOT NULL,
    descriptionsalle VARCHAR NULL,
    idbatiment INTEGER NOT NULL,
    idtypesalle INTEGER NOT NULL
);


CREATE TABLE t_type_equipement
(
    idtypeequipement SERIAL NOT NULL  PRIMARY KEY,
    nomtypeequipement VARCHAR NOT NULL,
	  idsalle INTEGER  NULL
);


CREATE TABLE t_type_salle 
(
    idtypesalle SERIAL NOT NULL  PRIMARY KEY,
    nomtypesalle VARCHAR NOT NULL
);


CREATE TABLE t_user 
(
    iduser SERIAL NOT NULL  PRIMARY KEY,
    prenomuser VARCHAR NOT NULL,
    nomuser VARCHAR NOT NULL,
    matriculeuser NUMERIC NOT NULL,
    emailuser VARCHAR NOT NULL,
    telephoneuser NUMERIC NULL,
    idrole INTEGER NOT NULL,
    idlangue INTEGER NOT NULL,
    idetatuser INTEGER NOT NULL,
	password VARCHAR NOT NULL
);



ALTER TABLE t_salle
  ADD 
    FOREIGN KEY (idbatiment)
      REFERENCES t_batiment;

ALTER TABLE t_salle
  ADD 
    FOREIGN KEY (idtypesalle)
      REFERENCES t_type_Salle;

ALTER TABLE t_user
  ADD 
    FOREIGN KEY (idrole)
      REFERENCES t_role;

ALTER TABLE t_user
  ADD 
    FOREIGN KEY (idlangue)
      REFERENCES t_langue;
	  
ALTER TABLE t_posseder
  ADD 
    FOREIGN KEY (idtypeequipement)
      REFERENCES t_type_Equipement;
	  
ALTER TABLE t_posseder
  ADD 
    FOREIGN KEY (idsalle)
      REFERENCES t_salle;

ALTER TABLE t_user
  ADD 
    FOREIGN KEY (idetatuser)
      REFERENCES t_etat_user;


ALTER TABLE t_reserver
  ADD 
    FOREIGN KEY (idsalle)
      REFERENCES t_salle;

ALTER TABLE t_reserver
  ADD 
    FOREIGN KEY (iduser)
      REFERENCES t_user;

ALTER TABLE t_reserver
  ADD 
    FOREIGN KEY (idperiode)
      REFERENCES t_periode;

ALTER TABLE t_parametre
  ADD 
    FOREIGN KEY (idlangue)
      REFERENCES t_langue;	

ALTER TABLE t_user
  ADD 
    FOREIGN KEY (idlangue)
      REFERENCES t_langue;	 

 

    END


```
DATA 

```
INSERT INTO t_batiment ( nombatiment) VALUES ( 'batiment winner');
INSERT INTO t_batiment ( nombatiment, adressebatiment) VALUES ('batiment looz', 'avenue de la looz');

INSERT INTO t_etat_user (etatuser) VALUES ( 'actif');
INSERT INTO t_etat_user (etatuser) VALUES ( 'desactivé');

INSERT INTO t_langue ( langue) 
VALUES 
('Français'),
('Anglais');


INSERT INTO t_role ( nomrole) VALUES ( 'consultant');
INSERT INTO t_role ( nomrole) VALUES ( 'administrateur');
INSERT INTO t_role ( nomrole) VALUES ( 'utilisateur');

INSERT INTO t_user ( prenomuser, nomuser, matriculeuser, emailuser, password, telephoneuser, 
					idrole, 
					idlangue, 
					idetatuser) 
VALUES 
( 'prenom_admin', 'nom_admin', 007, 'admin@admin.com','admin', 0874052595, 
 (SELECT idrole from t_role where nomrole = 'consultant'), 
 (SELECT idlangue from t_langue where langue = 'Français'), 
 (SELECT idetatuser from t_etat_user where etatuser = 'actif')),
( 'prenom_utilisateur', 'nom_utilisateur', 008, 'utilisateur@gmail.com','utilisateur', 0987888888, 
 (SELECT idrole from t_role where nomrole = 'utilisateur'), 
 (SELECT idlangue from t_langue where langue = 'Français'), 
 (SELECT idetatuser from t_etat_user where etatuser = 'actif')),
( 'prenom_consultant', 'nom_consultant', 009, 'consultant@gmail.com','consultant', 0101010101, 
 (SELECT idrole from t_role where nomrole = 'consultant'), 
 (SELECT idlangue from t_langue where langue = 'Anglais'), 
 (SELECT idetatuser from t_etat_user where etatuser = 'actif'));

INSERT INTO t_type_salle ( nomtypesalle)
VALUES ( 'Réunion'), ( 'Bureau'), ( 'Conférence'), ( 'Co-working');

INSERT INTO t_salle ( nomsalle, capacitesalle, etagesalle, descriptionsalle, 
					 idbatiment, idtypesalle)
VALUES 
( 'A110', 30, 1, 'une description', 
 (SELECT idbatiment from t_batiment where nombatiment = 'batiment winner'), 
 (SELECT idtypesalle  from t_type_salle where nomtypesalle = 'Bureau')),
( 'A120', 100, 1, 'une autre description', 
 (SELECT idbatiment from t_batiment where nombatiment = 'batiment winner'), 
 (SELECT idtypesalle from t_type_salle where nomtypesalle = 'Conférence')),
( 'B240', 10, 2, 'encore une description', 
 (SELECT idbatiment from t_batiment where nombatiment = 'batiment looz'), 
 (SELECT idtypesalle from t_type_salle where nomtypesalle = 'Réunion'));

INSERT INTO t_type_equipement ( nomtypeequipement)
VALUES
( 'Ordinateur'),
( 'Video-projecteur'),
( 'Bureau'),
( 'Tableau electronique'),
( 'Tableau normal');

INSERT INTO t_periode ( debutdateperiode, findateperiode, debutheureperiode, finheureperiode) 
VALUES 
( '2021-05-12', '2021-05-12', '13:00', '17:30'),
( '2021-05-20', '2021-05-21', '08:00', '12:00');

INSERT INTO t_reserver ( idsalle, iduser, idperiode) 
VALUES (
	(SELECT idsalle from t_salle where nomsalle = 'A110'), 
	(SELECT iduser from t_user where nomuser = 'nom_admin'), 
	(SELECT idperiode from t_periode where debutdateperiode = '2021-05-12')),
	
	(
	 (SELECT idsalle from t_salle where nomsalle = 'B240'), 
	 (SELECT iduser from t_user where nomuser = 'nom_utilisateur'),
	 (SELECT idperiode from t_periode where debutdateperiode = '2021-05-20'));



INSERT INTO t_parametre ( logo, idlangue, debplagejournaliere, finplagejournaliere, granularite, server, port) 
VALUES ('https://www.tvt.fr/var/tvt/storage/images/notre-ecosysteme/nos-entreprises-et-nos-startups/afpa-beziers/160817-1-fre-FR/AFPA-BEZIERS_company_logo_full.jpg', 
		(SELECT idlangue  from t_langue where langue = 'Français'), 
		'7:00', 
		'19:00', 
		'00:30', 
		'123.44.55', 
		'4400');

```

